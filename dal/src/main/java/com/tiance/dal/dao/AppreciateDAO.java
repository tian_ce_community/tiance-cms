package com.tiance.dal.dao;

import com.tiance.dal.dataobject.AppreciateDO;
import com.tiance.dal.dataobject.SignInDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface AppreciateDAO {

    Long insert(AppreciateDO appreciateDO);

    AppreciateDO queryByAppreciateId(Long appreciateID);


    int queryAppreciateCount(Map<String, Object> paramMap);

    List<AppreciateDO> queryAppreciateList(Map<String, Object> paramMap);
}