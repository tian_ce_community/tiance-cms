package com.tiance.dal.dao;

import com.tiance.dal.dataobject.ActivityDO;
import com.tiance.dal.dataobject.PersonMessageDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface PersonMessageDAO {

    int queryPersonMessageCount(Map<String, Object> paramMap);

    List<PersonMessageDO> queryPersonMessageList(Map<String, Object> paramMap);

    int update(PersonMessageDO personMessageDO);

    PersonMessageDO queryPersonMessageDetail(Long id);
}