package com.tiance.dal.dao;

import com.tiance.dal.dataobject.MemberAuthenticationInfoDO;
import com.tiance.dal.dataobject.SignInDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface MemberAuthenticationInfoDAO {

    Long insert(MemberAuthenticationInfoDO memberAuthenticationInfoDO);

    int queryMemberAuthApplyCount(Map<String, Object> paramMap);

    List<MemberAuthenticationInfoDO> queryMemberAuthApplyList(Map<String, Object> paramMap);

    MemberAuthenticationInfoDO queryMemberAuthApplyDetail(Map<String, Object> map);

    Long update(MemberAuthenticationInfoDO memberAuthenticationInfoDO);
}