package com.tiance.dal.dao;

import com.tiance.dal.dataobject.MemberDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface MemberDAO {

    Long insert(MemberDO memberDO);

    MemberDO queryMember(Map<String, Object> map);

    Long updateMember(MemberDO memberDO);

    Long updateMemberIntegralAndCredit(MemberDO newMemberDO);

    Long queryRecommendMemberId(Long id);

    int queryMemberListCount(Map<String, Object> paramMap);

    List<MemberDO> queryMemberList(Map<String, Object> paramMap);

    Long registerMember(MemberDO memberDO);

    MemberDO queryMemberByMemberNo(String substituteNo);
}