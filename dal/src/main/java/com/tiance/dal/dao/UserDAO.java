package com.tiance.dal.dao;

import com.tiance.dal.dataobject.UserDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface UserDAO {

    /**
     * 根据账户查询用户信息
     * @param userName
     * @return
     */
    UserDO queryUserByUserName(@Param("userName")String userName);

    int queryUserListCount(Map<String, Object> paramMap);

    List<UserDO> queryUserList(Map<String, Object> paramMap);

    UserDO queryUserByUserId(Long id);

    int updatePassword(UserDO userDO);
}