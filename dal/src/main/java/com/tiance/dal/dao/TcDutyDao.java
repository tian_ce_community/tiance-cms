package com.tiance.dal.dao;

import com.tiance.dal.dataobject.TcDutyDO;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface TcDutyDao {
    int deleteByPrimaryKey(Long id);

    int insert(TcDutyDO record);

    int insertSelective(TcDutyDO record);

    TcDutyDO selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(TcDutyDO record);


    List<TcDutyDO> queryDutyList(Map<String, Object> paramMap);

    int queryDutyCount(Map<String, Object> paramMap);

    int querySubstituteDutyApplyListCount(Map<String, Object> paramMap);

    List<TcDutyDO> querySubstituteDutyApplyList(Map<String, Object> paramMap);

    int updateSubstituteDutyApply(TcDutyDO updateTcDutyDO);

    int cancel(TcDutyDO tcDutyDO);

    int finish(TcDutyDO tcDutyDO);

    long audit(TcDutyDO tcDutyDO);
}