package com.tiance.dal.dao;

import com.tiance.dal.dataobject.IntegrationChangeDictDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface IntegrationChangeDictDAO {

    List<IntegrationChangeDictDO> queryIntegrationChangeDictList(Map<String, Object> map);

    IntegrationChangeDictDO queryIntegrationChangeDictById(Long integrationChangeDictId);
}