package com.tiance.dal.dao;

import com.tiance.dal.dataobject.CommonDO;
import com.tiance.dal.dataobject.MeetingRoomDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface MeetingRoomDAO {

    /**
     * @param id
     * @return
     */
    MeetingRoomDO queryById(@Param("id") Long id);

    int queryMeetingRoomListCount(Map<String, Object> paramMap);

    List<MeetingRoomDO> queryMeetingRoomList(Map<String, Object> paramMap);

}