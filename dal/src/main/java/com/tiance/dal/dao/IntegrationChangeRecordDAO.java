package com.tiance.dal.dao;

import com.tiance.dal.dataobject.IntegrationChangeRecordDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface IntegrationChangeRecordDAO {


    Long insert(IntegrationChangeRecordDO integrationChangeRecordDO);

    int queryIntegrationChangeRecordListCount(Map<String, Object> paramMap);

    List<IntegrationChangeRecordDO> queryIntegrationChangeRecordList(Map<String, Object> paramMap);
}