package com.tiance.dal.dao;

import com.tiance.dal.dataobject.IntegrationHistoryDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface IntegrationHistoryDAO {

    List<IntegrationHistoryDO> queryIntegrationHistory(Map<String, Object> map);

    Long insert(IntegrationHistoryDO integrationHistoryDO);
}