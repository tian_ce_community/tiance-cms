package com.tiance.dal.dao;

import com.tiance.dal.dataobject.CommonDO;
import com.tiance.dal.dataobject.UserDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface CommonDAO {

    /**
     * @param id
     * @return
     */
    CommonDO queryById(@Param("id") Long id);

    void update(CommonDO commonDO1);
}