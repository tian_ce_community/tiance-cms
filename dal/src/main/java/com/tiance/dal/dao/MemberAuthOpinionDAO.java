package com.tiance.dal.dao;

import com.tiance.dal.dataobject.MemberAuthFileDO;
import com.tiance.dal.dataobject.MemberAuthOpinionDO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MemberAuthOpinionDAO {

    Long insert(MemberAuthOpinionDO memberAuthFileDO);
}