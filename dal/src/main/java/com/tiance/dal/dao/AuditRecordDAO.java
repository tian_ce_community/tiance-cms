package com.tiance.dal.dao;

import com.tiance.dal.dataobject.AuditRecordDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface AuditRecordDAO {

    Long insert(AuditRecordDO auditRecordDO);

    int queryAuditListCount(Map<String, Object> paramMap);

    List<AuditRecordDO> queryAuditList(Map<String, Object> paramMap);

    AuditRecordDO queryAuditById(Long id);

    long auditRecord(AuditRecordDO updateAuditRecordDO);

    AuditRecordDO queryAuditDetail(Map<String, Object> paramMap);
}