package com.tiance.dal.dao;

import com.tiance.dal.dataobject.SignInDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface SignInDAO {

    Long insert(SignInDO signInDO);

    SignInDO queryBySignID(Long signID);

    Integer querySignInCount(Map<String, Object> paramMap);

    List<SignInDO> querySignInList(Map<String, Object> paramMap);
}