package com.tiance.dal.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface RolePermissionDAO {

    List<String> queryPermissionCodeListByRoleId(@Param("roleId")Long roleId);
}