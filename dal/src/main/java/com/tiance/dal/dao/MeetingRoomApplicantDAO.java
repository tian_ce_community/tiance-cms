package com.tiance.dal.dao;

import com.tiance.dal.dataobject.CommonDO;
import com.tiance.dal.dataobject.MeetingRoomApplicantDO;
import com.tiance.dal.dataobject.MeetingRoomDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface MeetingRoomApplicantDAO {

    /**
     * @param id
     * @return
     */
    CommonDO queryById(@Param("id") Long id);

    void update(CommonDO commonDO1);

    Long insert(MeetingRoomApplicantDO meetingRoomApplicantDO);

    long audit(MeetingRoomApplicantDO meetingRoomDO);

    List<MeetingRoomApplicantDO> queryMeetingRoomApplyList(Map<String, Object> paramMap);

    int queryMeetingRoomApplyListCount(Map<String, Object> paramMap);

    MeetingRoomApplicantDO queryMeetingRoomApplyById(Long id);

    int cancelMeetingRoomApplicant(MeetingRoomApplicantDO updateMeetingRoomApplicantDO);
}