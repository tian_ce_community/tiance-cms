package com.tiance.dal.dao;

import com.tiance.dal.dataobject.ActivityDO;
import com.tiance.dal.dataobject.TaskDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface TaskDAO {

    int queryTaskListCount(Map<String, Object> paramMap);

    List<TaskDO> queryTaskList(Map<String, Object> paramMap);

    Long update(TaskDO newTaskDO);

    TaskDO queryByTaskId(Long id);

    int insert(TaskDO id);

    long audit(TaskDO taskDO);

    List<TaskDO> querySignUpTaskList(Map<String, Object> paramMap);

    int querySignUpTaskListCount(Map<String, Object> paramMap);
}