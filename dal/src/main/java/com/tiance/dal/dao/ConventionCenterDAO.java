package com.tiance.dal.dao;

import com.tiance.dal.dataobject.CommonDO;
import com.tiance.dal.dataobject.ConventionCenterDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ConventionCenterDAO {

    /**
     * @param id
     * @return
     */
    ConventionCenterDO queryById(@Param("id") Long id);

    void update(CommonDO commonDO1);

    int queryConventionCenterListCount(Map<String, Object> paramMap);

    List<ConventionCenterDO> queryConventionCenterList(Map<String, Object> paramMap);

    List<ConventionCenterDO> queryUnoccupiedConventionCenterList();

}