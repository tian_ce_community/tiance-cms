package com.tiance.dal.dao;

import com.tiance.dal.dataobject.IntegrationRuleDO;
import com.tiance.enums.IntegrationRuleCodeEnum;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface IntegrationRuleDAO {

    IntegrationRuleDO queryRuleByIntegrationRuleCode(Map<String, Object> integrationRuleCode);

    int queryIntegrationRuleListCount(Map<String, Object> paramMap);

    List<IntegrationRuleDO> queryIntegrationRuleList(Map<String, Object> paramMap);
}