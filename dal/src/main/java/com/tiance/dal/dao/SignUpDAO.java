package com.tiance.dal.dao;

import com.tiance.dal.dataobject.SignUpDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface SignUpDAO {

    Long insert(SignUpDO signUpDO);

    int querySignUpListCount(Map<String, Object> paramMap);

    List<SignUpDO> querySignUpList(Map<String, Object> paramMap);

    Long cancelSignUp(SignUpDO signUpDO);
}