package com.tiance.dal.dao;

import com.tiance.dal.dataobject.PublishInfoDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface PublishInfoDAO {

    Long insert(PublishInfoDO publishInfoDO);

    long audit(PublishInfoDO publishInfoDO);

    PublishInfoDO queryPublishInfoById(Long id);

    Long cancel(PublishInfoDO updatePublishInfoDO);

    List<PublishInfoDO> applyPublishInfoList(Map<String, Object> paramMap);

    int applyPublishInfoListCount(Map<String, Object> paramMap);

    int querySignUpPublishedInfoListCount(Map<String, Object> paramMap);

    List<PublishInfoDO> querySignUpPublishedInfoList(Map<String, Object> paramMap);
}