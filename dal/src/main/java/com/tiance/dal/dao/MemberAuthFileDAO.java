package com.tiance.dal.dao;

import com.tiance.dal.dataobject.MemberAuthFileDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface MemberAuthFileDAO {

    Long insert(MemberAuthFileDO memberAuthFileDO);

    List<MemberAuthFileDO> queryMemberAuthFileList(Map<String, Object> map);
}