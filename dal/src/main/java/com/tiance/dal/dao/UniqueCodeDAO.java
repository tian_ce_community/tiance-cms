package com.tiance.dal.dao;

import com.tiance.dal.dataobject.UniqueCodeDO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UniqueCodeDAO {

    Long insert(UniqueCodeDO uniqueCodeDO);

    UniqueCodeDO queryByUniqueCode(String uniqueCode);
}