package com.tiance.dal.dao;

import com.tiance.dal.dataobject.RoleDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface RoleDAO {

    int queryRoleListCount(Map<String, Object> paramMap);

    List<RoleDO> queryRoleList(Map<String, Object> paramMap);

    RoleDO queryRoleDetail(Map<String, Object> paramMap);

    int update(RoleDO roleDO);
}