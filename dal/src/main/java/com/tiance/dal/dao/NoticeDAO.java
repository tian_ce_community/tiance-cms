package com.tiance.dal.dao;

import com.tiance.dal.dataobject.ActivityDO;
import com.tiance.dal.dataobject.NoticeDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface NoticeDAO {

    int queryNoticeListCount(Map<String, Object> paramMap);

    List<NoticeDO> queryNoticeList(Map<String, Object> paramMap);

    int update(Map<String, Object> paramMap);

    String queryNoticeContent(Long id);
}