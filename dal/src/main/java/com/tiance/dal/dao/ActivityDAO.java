package com.tiance.dal.dao;

import com.tiance.dal.dataobject.ActivityDO;
import com.tiance.dal.dataobject.AppreciateDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface ActivityDAO {

    Long insert(ActivityDO activityDO);

    ActivityDO queryByActivityId(Long activityId);

    Long update(ActivityDO newActivityDO);

    int queryApplyActivityCount(Map<String, Object> paramMap);

    List<ActivityDO> queryApplyActivityList(Map<String, Object> paramMap);

    long audit(ActivityDO activityDO);

    int querySignUpActivityListCount(Map<String, Object> paramMap);

    List<ActivityDO> querySignUpActivityList(Map<String, Object> paramMap);
}