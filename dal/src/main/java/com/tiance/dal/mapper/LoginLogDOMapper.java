package com.tiance.dal.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tiance.dal.dataobject.LoginLogDO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public interface LoginLogDOMapper extends BaseMapper<LoginLogDO> {

    /**
     * 获取登录日志
     */
    List<Map<String, Object>> getLoginLogs(@Param("page") Page page,
                                           @Param("beginTime") String beginTime,
                                           @Param("endTime") String endTime,
                                           @Param("logName") String logName);
}
