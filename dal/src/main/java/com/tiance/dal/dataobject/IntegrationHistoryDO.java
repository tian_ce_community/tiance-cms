package com.tiance.dal.dataobject;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：角色表
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class IntegrationHistoryDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     * ID
     */
    private Long id;
    /**
     * 会员ID
     * MEMBER_ID
     */
    private Long memberId;
    /**
     * OPENID
     */
    private String openid;
    /**
     * 类型(INTEGRATION:积分，CREDIT:信用)
     * INTEGRATION_TYPE
     *
     */
    private String integrationType;
    /**
     * 操作类型（ADD:加;SUBTRACT:减
     * OPERATE_TYPE
     */
    private String operateType;
    /**
     * 分值或信用点
     * VALUE
     */
    private Integer integrationValue;
    /**
     * 创建时间
     * CREATE_TIME
     */
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getIntegrationType() {
        return integrationType;
    }

    public void setIntegrationType(String integrationType) {
        this.integrationType = integrationType;
    }

    public String getOperateType() {
        return operateType;
    }

    public void setOperateType(String operateType) {
        this.operateType = operateType;
    }

    public Integer getIntegrationValue() {
        return integrationValue;
    }

    public void setIntegrationValue(Integer integrationValue) {
        this.integrationValue = integrationValue;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
