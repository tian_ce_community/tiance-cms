package com.tiance.dal.dataobject;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：活动表
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class MemberAuthFileDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 会员ID
     * MEMBER_ID
     */
    private Long memberId;
    /**
     * 认证材料ID
     * AUTHENTICATION_INFO_ID
     */
    private Long authenticationInfoId;

    /**
     * 微信openID
     * OPENID
     */
    private String openid;
    /**
     * 认证文件类型 (ID_CARD_FRONT:身份证正面，ID_CARD_BACK：身份证背面; CHOOSE_JOB_CERTIFICATE:择业证;JOB_QUALIFICATION_CERTIFICATE：职业资格证;RETIREMENT_RESUME_CERTIFICATE:退役时履历表)
     * FILE_TYPE
     */
    private String fileType;
    /**
     * 文件名称
     * FILE_NAME
     */
    private String fileName;
    /**
     * FILE_FORMAT
     * 文件格式
     */
    private String fileFormat;
    /**
     * 文件标题（中文名）
     * FILE_TITLE
     */
    private String fileTitle;
    /**
     * 文件路径
     * FILE_PATH
     */
    private String filePath;

    /**
     * createTime
     */
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getAuthenticationInfoId() {
        return authenticationInfoId;
    }

    public void setAuthenticationInfoId(Long authenticationInfoId) {
        this.authenticationInfoId = authenticationInfoId;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileFormat() {
        return fileFormat;
    }

    public void setFileFormat(String fileFormat) {
        this.fileFormat = fileFormat;
    }

    public String getFileTitle() {
        return fileTitle;
    }

    public void setFileTitle(String fileTitle) {
        this.fileTitle = fileTitle;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
