package com.tiance.dal.dataobject;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：公共表
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class MeetingRoomDO implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * ID
     */
    private Long id;
    /**
     * 会议中心ID
     * CONVENTION_CENTER_ID
     */
    private Long conventionCenterId;
    /**
     * 会议中心名称
     * NAME
     */
    private String name;
    /**
     * 会议中心名称
     * CONVENTION_CENTER_NAME
     */
    private String conventionCenterName;
    /**
     * 会议中心位置
     * POSITION
     */
    private String position;
    /**
     * 会议中心坐标
     * COORDINATE
     */
    private String coordinate;
    /**
     * 会议室容量（人数
     * CAPACITY
     */
    private Integer capacity;
    /**
     * 会议室状态(NORMAL:正常，FORBIDDEN:禁用)
     * STATUS
     */
    private String status;
    /**
     * 会议室硬件配套
     * HARD_WARE_MEMO
     */
    private String hardWareMemo;
    /**
     * 会议室描述
     * MEETING_ROOM_MEMO
     */
    private String meetingRoomMemo;


    /**
     * CREATE_TIME
     */
    private Date createTime;
    /**
     * UPDATE_TIME
     */
    private Date updateTime;
    /**
     * REMARK
     */
    private String remark;

    public String getMeetingRoomMemo() {
        return meetingRoomMemo;
    }

    public void setMeetingRoomMemo(String meetingRoomMemo) {
        this.meetingRoomMemo = meetingRoomMemo;
    }

    public Long getConventionCenterId() {
        return conventionCenterId;
    }

    public void setConventionCenterId(Long conventionCenterId) {
        this.conventionCenterId = conventionCenterId;
    }

    public String getConventionCenterName() {
        return conventionCenterName;
    }

    public void setConventionCenterName(String conventionCenterName) {
        this.conventionCenterName = conventionCenterName;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public String getHardWareMemo() {
        return hardWareMemo;
    }

    public void setHardWareMemo(String hardWareMemo) {
        this.hardWareMemo = hardWareMemo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(String coordinate) {
        this.coordinate = coordinate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
