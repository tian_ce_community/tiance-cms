package com.tiance.dal.dataobject;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：会员表
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class IntegrationChangeRecordDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     * ID
     */
    private Long id;
    /**
     * MEMBER_ID
     */
    private Long memberId;
    /**
     * 积分信用变更字典主键
     * INTEGRATION_CHANGE_DICT_ID
     */
    private Long integrationChangeDictId;
    /**
     * 操作者
     * OPERATOR_ID
     */
    private Long operatorId;
    /**
     * OPENID
     */
    private String openid;
    /**
     * 会员编号
     */
    private String memberNo;


    /**
     * 调整名称
     * CHANGE_NAME
     */
    private String changeName;
    /**
     * 调整码
     * CHANGE_CODE
     */
    private String changeCode;
    /**
     * REAL_NAME
     */
    private String realName;
    /**
     * 变更积分值
     * CHANGE_INTEGRATION_VALUE
     */
    private Integer changeIntegrationValue;
    /**
     * 变更信用值
     * CHANGE_CREDIT_VALUE
     */
    private Integer changeCreditValue;
    /**
     * REMARK
     */
    private String remark;
    /**
     * 变更时间
     * CHANGE_TIME
     */
    private Date changeTime;
    /**
     * CREATE_TIME
     */
    private Date createTime;


    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getIntegrationChangeDictId() {
        return integrationChangeDictId;
    }

    public void setIntegrationChangeDictId(Long integrationChangeDictId) {
        this.integrationChangeDictId = integrationChangeDictId;
    }

    public String getChangeName() {
        return changeName;
    }

    public void setChangeName(String changeName) {
        this.changeName = changeName;
    }

    public String getChangeCode() {
        return changeCode;
    }

    public void setChangeCode(String changeCode) {
        this.changeCode = changeCode;
    }

    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public Integer getChangeIntegrationValue() {
        return changeIntegrationValue;
    }

    public void setChangeIntegrationValue(Integer changeIntegrationValue) {
        this.changeIntegrationValue = changeIntegrationValue;
    }

    public Integer getChangeCreditValue() {
        return changeCreditValue;
    }

    public void setChangeCreditValue(Integer changeCreditValue) {
        this.changeCreditValue = changeCreditValue;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getChangeTime() {
        return changeTime;
    }

    public void setChangeTime(Date changeTime) {
        this.changeTime = changeTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
