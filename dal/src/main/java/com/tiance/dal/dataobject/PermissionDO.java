package com.tiance.dal.dataobject;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：
 *权限表
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class PermissionDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id ID
     */
    private Long id;
    /**
     * 权限名称
     * PERMISSION_NAME
     */
    private String permissionName;
    /**
     * 权限名称英文
     * PERMISSION_NAME_EN
     */
    private String permissionNameEn;
    /**
     * 权限URL
     * PERMISSION_URL
     */
    private String permissionUrl;
    /**
     * 权限码
     * PERMISSION_CODE
     */
    private String permissionCode;
    /**
     * 类型
     * PERMISSION_TYPE
     */
    private String permissionType;
    /**
     * 排序
     * SORT
     */
    private String sort;

    /**
     * REMARK
     */
    private String remark;
    /**
     * CREATE_TIME
     */
    private String createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public String getPermissionNameEn() {
        return permissionNameEn;
    }

    public void setPermissionNameEn(String permissionNameEn) {
        this.permissionNameEn = permissionNameEn;
    }

    public String getPermissionUrl() {
        return permissionUrl;
    }

    public void setPermissionUrl(String permissionUrl) {
        this.permissionUrl = permissionUrl;
    }

    public String getPermissionCode() {
        return permissionCode;
    }

    public void setPermissionCode(String permissionCode) {
        this.permissionCode = permissionCode;
    }

    public String getPermissionType() {
        return permissionType;
    }

    public void setPermissionType(String permissionType) {
        this.permissionType = permissionType;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
