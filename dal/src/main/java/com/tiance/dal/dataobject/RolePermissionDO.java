package com.tiance.dal.dataobject;

import java.io.Serializable;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：
 *角色权限关系表
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class RolePermissionDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id ID
     */
    private Long id;
    /**
     * 角色ID
     * ROLE_ID
     */
    private Long roleId;
    /**
     * 权限ID
     * PERMISSION_ID
     */
    private Long permissionId;


    /**
     * 排序
     * SORT
     */
    private Long sort;

    /**
     * REMARK
     */
    private String remark;
    /**
     * CREATE_TIME
     */
    private String createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Long permissionId) {
        this.permissionId = permissionId;
    }

    public Long getSort() {
        return sort;
    }

    public void setSort(Long sort) {
        this.sort = sort;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
