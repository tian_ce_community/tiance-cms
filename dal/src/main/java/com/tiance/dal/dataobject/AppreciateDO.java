package com.tiance.dal.dataobject;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：赞赏表
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class AppreciateDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     * ID
     */
    private Long id;
    /**
     * 赞赏会员ID
     * FROM_MEMBER_ID
     */
    private Long fromMemberId;
    /**
     * 被赞赏会员ID
     * TO_MEMBER_ID
     */
    private Long toMemberId;

    /**
     * 实体ID
     * ENTITY_ID
     */
    private Long entityId;

    /**
     * 实体类型(ACTIVITY:活动)
     * ENTITY_TYPE
     */
    private String entityType;
    /**
     * 指定名称（活动就是活动名称，会议中心就是会议中心名称）
     * APPOINT_NAME
     */
    private String appointName;
    /**
     * 指定坐标点
     * APPOINT_COORDINATE
     */
    private String appointCoordinate;
    /**
     * 实际赞赏坐标点
     * APPRECIATE_COORDINATE
     */
    private String appreciateCoordinate;
    /**
     * 赞赏类型（SITE_APPRECIATE:现场签到赞赏;ACTIVITY_APPRECIATE:活动赞赏）
     * APPRECIATE_TYPE
     */
    private String appreciateType;
    /**
     * 备注
     * REMARK
     */
    private String remark;
    /**
     * 创建时间
     * CREATE_TIME
     */
    private Date createTime;
    /**
     * 赞赏时间
     * APPRECIATE_TIME
     */
    private Date appreciateTime;

    public Long getFromMemberId() {
        return fromMemberId;
    }

    public void setFromMemberId(Long fromMemberId) {
        this.fromMemberId = fromMemberId;
    }

    public Long getToMemberId() {
        return toMemberId;
    }

    public void setToMemberId(Long toMemberId) {
        this.toMemberId = toMemberId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getAppointName() {
        return appointName;
    }

    public void setAppointName(String appointName) {
        this.appointName = appointName;
    }

    public String getAppointCoordinate() {
        return appointCoordinate;
    }

    public void setAppointCoordinate(String appointCoordinate) {
        this.appointCoordinate = appointCoordinate;
    }

    public String getAppreciateCoordinate() {
        return appreciateCoordinate;
    }

    public void setAppreciateCoordinate(String appreciateCoordinate) {
        this.appreciateCoordinate = appreciateCoordinate;
    }

    public String getAppreciateType() {
        return appreciateType;
    }

    public void setAppreciateType(String appreciateType) {
        this.appreciateType = appreciateType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getAppreciateTime() {
        return appreciateTime;
    }

    public void setAppreciateTime(Date appreciateTime) {
        this.appreciateTime = appreciateTime;
    }
}
