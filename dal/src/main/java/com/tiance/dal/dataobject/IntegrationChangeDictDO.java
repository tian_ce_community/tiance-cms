package com.tiance.dal.dataobject;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：会员表
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class IntegrationChangeDictDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     * ID
     */
    private Long id;


    /**
     * 调整名称
     * CHANGE_NAME
     */
    private String changeName;

    /**
     * status
     * 状态（USE:使用;UN_USE:未使用;DELETE:删除）
     */
    private String status;
    /**
     * 调整码
     * CHANGE_CODE
     */
    private String changeCode;


    /**
     * REMARK
     */
    private String remark;

    /**
     * CREATE_TIME
     */
    private Date createTime;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChangeName() {
        return changeName;
    }

    public void setChangeName(String changeName) {
        this.changeName = changeName;
    }

    public String getChangeCode() {
        return changeCode;
    }

    public void setChangeCode(String changeCode) {
        this.changeCode = changeCode;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
