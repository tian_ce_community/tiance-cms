package com.tiance.dal.dataobject;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：报名表
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class CCOccupationDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 会议中心ID
     * CONVENTION_CENTER_ID
     */
    private Long conventionCenterId;


    /**
     * 实体
     * ENTITY_ID
     */
    private Long entityId;
    /**
     * 实体类型(ACTIVITY:活动;TASK:任务，PUBLISH_INFO:发布信息;)
     * ENTITY_TYPE
     */
    private String entityType;
    /**
     * 使用类型(OPEN:开放；CLOSE:不开放)
     * USE_TYPE
     */
    private String useType;


    /**
     * REMARK
     */
    private String remark;

    /**
     * 时间段类型(MORNING:上午;AFTERNOON:下午:ALL_DAY:全天;OTHER:其他)
     */
    private String timeType;


    /**
     * END_TIME
     */
    private Date endTime;
    /**
     * START_TIME
     */
    private Date startTime;
    private Date createTime;

    public String getTimeType() {
        return timeType;
    }

    public void setTimeType(String timeType) {
        this.timeType = timeType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getConventionCenterId() {
        return conventionCenterId;
    }

    public void setConventionCenterId(Long conventionCenterId) {
        this.conventionCenterId = conventionCenterId;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getUseType() {
        return useType;
    }

    public void setUseType(String useType) {
        this.useType = useType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }
}
