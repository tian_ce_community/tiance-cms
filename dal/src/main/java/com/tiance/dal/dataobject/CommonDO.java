package com.tiance.dal.dataobject;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：公共表
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class CommonDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     */
    private Long memberNo;
    /**
     */
    private Long memberCardNo;

    /**
     * CREATE_TIME
     */
    private Date createTime;
    /**
     * UPDATE_TIME
     */
    private Date updateTime;

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(Long memberNo) {
        this.memberNo = memberNo;
    }

    public Long getMemberCardNo() {
        return memberCardNo;
    }

    public void setMemberCardNo(Long memberCardNo) {
        this.memberCardNo = memberCardNo;
    }
}
