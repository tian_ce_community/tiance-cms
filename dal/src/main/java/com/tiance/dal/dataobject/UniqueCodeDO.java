package com.tiance.dal.dataobject;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：实体关联表
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class UniqueCodeDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     * ID
     */
    private Long id;

    /**
     * 实体ID
     * ENTITY_ID
     */
    private Long entityId;

    /**
     * 实体类型(ACTIVITY:活动)
     * ENTITY_TYPE
     */
    private String entityType;

    /**
     * 备注
     * REMARK
     */
    private String remark;
    /**
     * 唯一码
     * UNIQUE_CODE
     */
    private String uniqueCode;
    /**
     * 创建时间
     * CREATE_TIME
     */
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
