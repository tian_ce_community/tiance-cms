package com.tiance.dal.dataobject;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：报名表
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class SignUpDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 申请人
     * APPLICANT_ID
     */
    private Long applicantId;
    /**
     * 申请人
     *
     */
    private String applicantName;
    /**
     * 审批人
     * APPROVAL_ID
     */
    private Long approvalId;
    /**
     * 联系人
     * CONTACTS_ID
     */
    private Long contactsId;
    /**
     * 实体
     * ENTITY_ID
     */
    private Long entityId;
    /**
     * 实体类型(ACTIVITY:活动;TASK:任务，PUBLISH_INFO:发布信息;)
     * ENTITY_TYPE
     */
    private String entityType;
    /**
     * 联系人姓名
     * CONTACTS_NAME
     */
    private String contactsName;
    /**
     * 唯一标识码(报名的时候调后台接口)'
     * UNIQUE_CODE
     */
    private String uniqueCode;
    /**
     * 报名状态 SIGN_UP:已报名;CANCEL:取消
     * SIGN_UP_STATE
     */
    private String signUpState;
    /**
     * 报名人数
     * SIGN_UP_NUM
     */
    private Integer signUpNum;


    /**
     * 联系人电话
     * CONTACTS_PHONE
     */
    private String contactsPhone;

    /**
     * REMARK
     */
    private String remark;
    /**
     * UPDATE_USER
     */
    private Long updateUser;
    /**
     * CREATE_USER
     */
    private Long createUser;
    /**
     * CREATE_TIME
     */
    private Date createTime;
    /**
     * UPDATE_TIME
     */
    private Date updateTime;


    public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    public Long getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Long updateUser) {
        this.updateUser = updateUser;
    }

    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(Long applicantId) {
        this.applicantId = applicantId;
    }

    public Long getApprovalId() {
        return approvalId;
    }

    public void setApprovalId(Long approvalId) {
        this.approvalId = approvalId;
    }

    public Long getContactsId() {
        return contactsId;
    }

    public void setContactsId(Long contactsId) {
        this.contactsId = contactsId;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getContactsName() {
        return contactsName;
    }

    public void setContactsName(String contactsName) {
        this.contactsName = contactsName;
    }

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public String getSignUpState() {
        return signUpState;
    }

    public void setSignUpState(String signUpState) {
        this.signUpState = signUpState;
    }

    public Integer getSignUpNum() {
        return signUpNum;
    }

    public void setSignUpNum(Integer signUpNum) {
        this.signUpNum = signUpNum;
    }

    public String getContactsPhone() {
        return contactsPhone;
    }

    public void setContactsPhone(String contactsPhone) {
        this.contactsPhone = contactsPhone;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
