package com.tiance.dal.dataobject;

import java.io.Serializable;
import java.util.Date;

/**
 * tc_duty
 * @author 
 */
public class TcDutyDO implements Serializable {
    /**
     * 主键id
     */
    private Long id;

    /**
     * 值班ID
     */
    private Long memberId;

    /**
     * 微信openID
     */
    private String openid;

    /**
     * 会议中心ID
     */
    private Long conventionCenterId;

    /**
     * 值班类型（SELF:本人值班;SUBSTITUTE:替班）
     */
    private String dutyType;

    /**
     * 申请状态（UN_AUDIT:申请待审核;APPLY_CANCEL:申请取消;AUDIT_FAIL:审核不通过;AUDIT_PASS:审核通过;ON_DUTY:值班中;DUTY_FINISH:值班结束（值班经理身份变为认证会员））
     */
    private String applicantState;

    /**
     * 申请时间
     */
    private Date applicantTime;

    /**
     * 替班人姓名
     */
    private String substituteName;

    /**
     * 替班人人ID
     */
    private Long substituteId;
    /**
     * 替班人编号
     */
    private String substituteNo;

    /**
     * 审批人
     */
    private Long approvalId;


    /**
     * 申请人ID
     */
    private Long applicantId;
    /**
     * 申请人
     */
    private String applicantName;

    /**
     * 审核人
     */
    private String approvalName;
    /**
     * 会议中心名称
     */
    private String conventionCenterName;

    /**
     * 审批意见
     */
    private Long approvalOpinion;

    /**
     * 审批时间
     */
    private Date approvalTime;

    /**
     * 值班是否签到(ISSIGN:已经签到;NOSIGN:未签到)
     */
    private String dutySign;

    /**
     * 值班签到时间
     */
    private Date signTime;

    /**
     * 系统给积分
     */
    private Integer sysIntegral;

    /**
     * 系统给信用
     */
    private Integer sysCredit;

    /**
     * 系统评价
     */
    private Integer sysEvaluation;

    /**
     * 系统打赏积分
     */
    private Integer sysRewardIntegral;

    /**
     * 系统打赏信用
     */
    private Integer sysRewardCredit;

    /**
     * 值班开始时间
     */
    private Date startTime;

    /**
     * 值班结束时间
     */
    private Date endTime;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 备注
     */
    private String remark;

    public String getSubstituteName() {
        return substituteName;
    }

    public void setSubstituteName(String substituteName) {
        this.substituteName = substituteName;
    }

    public Long getSubstituteId() {
        return substituteId;
    }

    public void setSubstituteId(Long substituteId) {
        this.substituteId = substituteId;
    }

    public String getSubstituteNo() {
        return substituteNo;
    }

    public void setSubstituteNo(String substituteNo) {
        this.substituteNo = substituteNo;
    }

    public String getApprovalName() {
        return approvalName;
    }

    public void setApprovalName(String approvalName) {
        this.approvalName = approvalName;
    }

    public Long getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(Long applicantId) {
        this.applicantId = applicantId;
    }

    public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    public String getConventionCenterName() {
        return conventionCenterName;
    }

    public void setConventionCenterName(String conventionCenterName) {
        this.conventionCenterName = conventionCenterName;
    }

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public Long getConventionCenterId() {
        return conventionCenterId;
    }

    public void setConventionCenterId(Long conventionCenterId) {
        this.conventionCenterId = conventionCenterId;
    }

    public String getDutyType() {
        return dutyType;
    }

    public void setDutyType(String dutyType) {
        this.dutyType = dutyType;
    }

    public String getApplicantState() {
        return applicantState;
    }

    public void setApplicantState(String applicantState) {
        this.applicantState = applicantState;
    }

    public Date getApplicantTime() {
        return applicantTime;
    }

    public void setApplicantTime(Date applicantTime) {
        this.applicantTime = applicantTime;
    }

    public Long getApprovalId() {
        return approvalId;
    }

    public void setApprovalId(Long approvalId) {
        this.approvalId = approvalId;
    }

    public Long getApprovalOpinion() {
        return approvalOpinion;
    }

    public void setApprovalOpinion(Long approvalOpinion) {
        this.approvalOpinion = approvalOpinion;
    }

    public Date getApprovalTime() {
        return approvalTime;
    }

    public void setApprovalTime(Date approvalTime) {
        this.approvalTime = approvalTime;
    }

    public String getDutySign() {
        return dutySign;
    }

    public void setDutySign(String dutySign) {
        this.dutySign = dutySign;
    }

    public Date getSignTime() {
        return signTime;
    }

    public void setSignTime(Date signTime) {
        this.signTime = signTime;
    }

    public Integer getSysIntegral() {
        return sysIntegral;
    }

    public void setSysIntegral(Integer sysIntegral) {
        this.sysIntegral = sysIntegral;
    }

    public Integer getSysCredit() {
        return sysCredit;
    }

    public void setSysCredit(Integer sysCredit) {
        this.sysCredit = sysCredit;
    }

    public Integer getSysEvaluation() {
        return sysEvaluation;
    }

    public void setSysEvaluation(Integer sysEvaluation) {
        this.sysEvaluation = sysEvaluation;
    }

    public Integer getSysRewardIntegral() {
        return sysRewardIntegral;
    }

    public void setSysRewardIntegral(Integer sysRewardIntegral) {
        this.sysRewardIntegral = sysRewardIntegral;
    }

    public Integer getSysRewardCredit() {
        return sysRewardCredit;
    }

    public void setSysRewardCredit(Integer sysRewardCredit) {
        this.sysRewardCredit = sysRewardCredit;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", memberId=").append(memberId);
        sb.append(", openid=").append(openid);
        sb.append(", conventionCenterId=").append(conventionCenterId);
        sb.append(", dutyType=").append(dutyType);
        sb.append(", applicantState=").append(applicantState);
        sb.append(", applicantTime=").append(applicantTime);
        sb.append(", approvalId=").append(approvalId);
        sb.append(", approvalOpinion=").append(approvalOpinion);
        sb.append(", approvalTime=").append(approvalTime);
        sb.append(", dutySign=").append(dutySign);
        sb.append(", signTime=").append(signTime);
        sb.append(", sysIntegral=").append(sysIntegral);
        sb.append(", sysCredit=").append(sysCredit);
        sb.append(", sysEvaluation=").append(sysEvaluation);
        sb.append(", sysRewardIntegral=").append(sysRewardIntegral);
        sb.append(", sysRewardCredit=").append(sysRewardCredit);
        sb.append(", startTime=").append(startTime);
        sb.append(", endTime=").append(endTime);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", remark=").append(remark);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}