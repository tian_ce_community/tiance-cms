package com.tiance.dal.dataobject;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：个人消息表
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class PersonMessageDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     * ID
     */
    private Long id;
    /**
     * 会员ID
     * MEMBER_ID
     */
    private Long memberId;


    /**
     * OPENID
     */
    private String openid;
    /**
     * 标题
     */
    private String title;
    /**
     * 消息类型
     * MESSAGE_TYPE
     */
    private String messageType;
    /**
     * 消息URL
     * MESSAGE_URL
     */
    private String messageUrl;
    /**
     * 消息URL类型(IMAGE:图片;PAGE:网页)
     */
    private String messageUrlType;
    /**
     * 是否已读（READ:已读;UN_READ:未读
     * READ_STATUS
     */
    private String readStatus;
    /**
     * 内容
     * CONTENT
     */
    private String content;
    /**
     * 创建时间
     * CREATE_TIME
     */
    private Date createTime;
    private Date updateTime;

    public String getMessageUrlType() {
        return messageUrlType;
    }

    public void setMessageUrlType(String messageUrlType) {
        this.messageUrlType = messageUrlType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getMessageUrl() {
        return messageUrl;
    }

    public void setMessageUrl(String messageUrl) {
        this.messageUrl = messageUrl;
    }

    public String getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(String readStatus) {
        this.readStatus = readStatus;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
