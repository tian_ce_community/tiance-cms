package com.tiance.web.controller.front;

import com.alibaba.fastjson.JSON;
import com.tiance.constants.CommonConstants;
import com.tiance.dal.dataobject.MemberDO;
import com.tiance.domainservice.checker.MemberParamsChecker;
import com.tiance.domainservice.domain.MemberAuthenticationInfoDomain;
import com.tiance.domainservice.domain.MemberDomain;
import com.tiance.domainservice.service.MemberAuthService;
import com.tiance.domainservice.service.MemberService;
import com.tiance.domainservice.service.SignInService;
import com.tiance.enums.OperateTypeEnum;
import com.tiance.enums.SexEnum;
import com.tiance.exception.BusinessException;
import com.tiance.integration.cache.MapCacheUtil;
import com.tiance.integration.config.CommonProperties;
import com.tiance.integration.wechat.WxUserInfo;
import com.tiance.utils.BeanConverterUtil;
import com.tiance.vo.ImageVO;
import com.tiance.vo.MemberAuthenticationInfoVO;
import com.tiance.vo.MemberVO;
import com.tiance.web.aspect.OperatorControllerLog;
import com.tiance.web.controller.base.FrontController;
import com.tiance.web.controller.common.BaseResponse;
import com.tiance.web.convertor.ModelConvert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：会员管理
 *
 * Modification History:
 *
 */
@Controller
@RequestMapping(value = "/front/memberCenter")
public class MemberAuthController extends FrontController {
    private static String LOG_PREFIX ="MemberController| 会员审核控制器===>";



    @Autowired
    private CommonProperties commonProperties;





    @Autowired
    private MemberService     memberService;
    @Autowired
    private MemberAuthService memberAuthService;


    @RequestMapping(value = "/toApplyMemberAuth.action",method = RequestMethod.GET)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到会员认证页面")
    public String toApplyMemberAuth(HttpServletRequest request,Model model, MemberAuthenticationInfoVO memberAuthenticationInfoVO) {
        try {
            HttpSession session = request.getSession();
            Object memberId = session.getAttribute(CommonConstants.MEMBER_ID);
            if (null != memberId) {
                memberAuthenticationInfoVO.setId((Long) memberId);
            }
            MemberAuthenticationInfoDomain memberDomain = ModelConvert.memberAuthenticationInfoVO2MemberDomain(memberAuthenticationInfoVO);
            MemberAuthenticationInfoVO memberDO = memberAuthService.queryMemberAuthApplyDetail(memberDomain);

            MemberParamsChecker.checkApplyMemberAuthInfo(memberDO,memberAuthenticationInfoVO);

            model.addAttribute("memberAuthenticationInfoVO", memberAuthenticationInfoVO);

        }catch (Exception e){

        }

        return "/front/certification";
    }


    @RequestMapping(value = "/toMemberAuthBaseInfo.action",method = RequestMethod.GET)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到会员认证基本信息页面")
    public String toMemberAuthBaseInfo(HttpServletRequest request,Model model, MemberAuthenticationInfoVO memberAuthenticationInfoVO) {
        try {
            HttpSession session=request.getSession();
            Object memberId=session.getAttribute(CommonConstants.MEMBER_ID);
            if(null!=memberId){
                memberAuthenticationInfoVO.setId((Long)memberId);
            }
            MemberAuthenticationInfoDomain memberDomain = ModelConvert.memberAuthenticationInfoVO2MemberDomain(memberAuthenticationInfoVO);

            MemberAuthenticationInfoVO memberDO = memberAuthService.queryMemberAuthApplyDetail(memberDomain);
            if (null != memberDO) {
                BeanConverterUtil.copyProperties(memberAuthenticationInfoVO, memberDO);
            }
            model.addAttribute("memberAuthenticationInfoVO", memberAuthenticationInfoVO);
        }catch (Exception e){

        }
        return "/front/registered";
    }


    @RequestMapping(value = "/toMemberAuthOldWorkInfo.action",method = RequestMethod.GET)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到会员认证基本信息页面")
    public String toMemberAuthOldWorkInfo(HttpServletRequest request,Model model, MemberAuthenticationInfoVO memberAuthenticationInfoVO) {
        try {
            HttpSession session=request.getSession();
            Object memberId=session.getAttribute(CommonConstants.MEMBER_ID);
            if(null!=memberId){
                memberAuthenticationInfoVO.setId((Long)memberId);
            }
            MemberAuthenticationInfoDomain memberDomain = ModelConvert.memberAuthenticationInfoVO2MemberDomain(memberAuthenticationInfoVO);

            MemberAuthenticationInfoVO memberDO = memberAuthService.queryMemberAuthApplyDetail(memberDomain);
            if (null != memberDO) {
                BeanConverterUtil.copyProperties(memberAuthenticationInfoVO, memberDO);
            }
            model.addAttribute("memberAuthenticationInfoVO", memberAuthenticationInfoVO);
        }catch (Exception e){

        }
        return "/front/oldwork";
    }

    @RequestMapping(value = "/toMemberAuthNewWorkInfo.action",method = RequestMethod.GET)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到会员认证基本信息页面")
    public String toMemberAuthNewWorkInfo(HttpServletRequest request,Model model, MemberAuthenticationInfoVO memberAuthenticationInfoVO) {
        try {
            HttpSession session=request.getSession();
            Object memberId=session.getAttribute(CommonConstants.MEMBER_ID);
            if(null!=memberId){
                memberAuthenticationInfoVO.setId((Long)memberId);
            }
            MemberAuthenticationInfoDomain memberDomain = ModelConvert.memberAuthenticationInfoVO2MemberDomain(memberAuthenticationInfoVO);

            MemberAuthenticationInfoVO memberDO = memberAuthService.queryMemberAuthApplyDetail(memberDomain);
            if (null != memberDO) {
                BeanConverterUtil.copyProperties(memberAuthenticationInfoVO, memberDO);
            }
            model.addAttribute("memberAuthenticationInfoVO", memberAuthenticationInfoVO);
        }catch (Exception e){

        }
        return "/front/newwork";
    }


    @RequestMapping(value = "/applyMemberAuth.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="会员认证")
    public BaseResponse applyMemberAuth(HttpServletRequest request, Model model,MemberAuthenticationInfoVO memberAuthenticationInfoVO){
        logger.info("开始会员认证，请求参数:{}", JSON.toJSONString(memberAuthenticationInfoVO));

        String jsonData = request.getParameter("imageList");
        List<ImageVO> imageList =JSON.parseArray(jsonData, ImageVO.class);



        BaseResponse<MemberAuthenticationInfoVO> response=new BaseResponse<>();

        try {
            HttpSession session = request.getSession();
            Object memberId=session.getAttribute(CommonConstants.MEMBER_ID);
            if(null!=memberId){
                memberAuthenticationInfoVO.setMemberId((Long)memberId);
            }
            MemberAuthenticationInfoDomain memberAuthenticationInfoDomain= ModelConvert.memberVO2MemberAuthenticationInfoDomain(memberAuthenticationInfoVO);
            memberAuthenticationInfoDomain.setJobQualificationCertificateList(imageList);

            if(MapCacheUtil.lock(memberAuthenticationInfoVO.getMemberId()+"")){
               memberService.applyMemberAuth(memberAuthenticationInfoDomain);
            }else{
                throw new BusinessException("该请求正在处理中，请稍后再试!");

            }
            response.setSuccess(true);
            response.setMessage("提交认证成功!");
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"提交会员认证失败，失败原因：",e1);
            response.setSuccess(false);
            response.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"提交会员认证失败,失败原因",e2);
            response.setSuccess(false);
            response.setMessage("提交会员认证失败");
        }finally {
            MapCacheUtil.unlock(memberAuthenticationInfoVO.getMemberId() + "");
        }

        logger.info("结束会员认证，返回数据:{}", model);

        return  response;
    }



    @RequestMapping(value = "/queryMemberAuthDetail.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="会员认证查询")
    public BaseResponse queryMemberAuthDetail(HttpServletRequest request, Model model,MemberAuthenticationInfoVO memberAuthenticationInfoVO){
        logger.info("开始查询会员认证，请求参数:{}", JSON.toJSONString(memberAuthenticationInfoVO));

        BaseResponse<MemberAuthenticationInfoVO> response=new BaseResponse<>();

        try {
            HttpSession session = request.getSession();
            Object memberId=session.getAttribute(CommonConstants.MEMBER_ID);
            if(null!=memberId){
                memberAuthenticationInfoVO.setMemberId((Long)memberId);
            }
            MemberAuthenticationInfoDomain memberAuthenticationInfoDomain= ModelConvert.memberVO2MemberAuthenticationInfoDomain(memberAuthenticationInfoVO);
            if(MapCacheUtil.lock(memberAuthenticationInfoVO.getMemberId()+"")){
                MemberAuthenticationInfoVO memberAuthenticationInfoVO1= memberAuthService.queryMemberAuthApplyDetail(memberAuthenticationInfoDomain);
                response.setResult(memberAuthenticationInfoVO1);
                response.setSuccess(true);
                response.setMessage("查询成功!");
            }else{
                throw new BusinessException("该请求正在处理中，请稍后再试!");

            }

        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"查询失败，失败原因：",e1);
            response.setSuccess(false);
            response.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"查询会员认证失败,失败原因",e2);
            response.setSuccess(false);
            response.setMessage("查询会员认证失败");
        }finally {
            MapCacheUtil.unlock(memberAuthenticationInfoVO.getMemberId() + "");
        }

        logger.info("结束查询会员认证，返回数据:{}", model);

        return  response;
    }


}