package com.tiance.web.controller.back;

import com.alibaba.fastjson.JSON;
import com.tiance.constants.CommonConstants;
import com.tiance.dal.dataobject.UserDO;
import com.tiance.domainservice.checker.UserParamsChecker;
import com.tiance.domainservice.domain.UserDomain;
import com.tiance.domainservice.service.UserService;
import com.tiance.enums.ErrorCodeEnum;
import com.tiance.enums.OperateTypeEnum;
import com.tiance.exception.BusinessException;
import com.tiance.page.QueryPage;
import com.tiance.vo.UserVO;
import com.tiance.web.aspect.OperatorControllerLog;
import com.tiance.web.controller.base.BackController;
import com.tiance.web.controller.common.BaseResponse;
import com.tiance.web.convertor.ModelConvert;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/3/12
 *
 * Description：
 *
 * Modification History:
 *
 */
@Controller
@RequestMapping(value = "/back/user")
public class BackUserController extends BackController{
    private static String LOG_PREFIX ="BackUserController| 后台用户控制器===>";

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/toUserList.action",method = RequestMethod.GET)
    @RequiresPermissions("menu:system:user")
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到后台用户列表页面")
    public String toUserList() {
        return "/back/system/user/userList";
    }


    @RequestMapping(value = "/editMyPassword.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="修改密码")
    public BaseResponse editMyPassword(UserVO userVO){
        logger.info("开始修改密码，请求参数:{}", JSON.toJSONString(userVO));
        BaseResponse<String> backResponse=new BaseResponse<>();
        try {

            Session session = SecurityUtils.getSubject().getSession();
            UserDO userDO=(UserDO)session.getAttribute(CommonConstants.USER);
            userVO.setId(userDO.getId());
            UserParamsChecker.checkEditPassword(userVO);
            UserDomain userDomain= ModelConvert.userVO2UserDomain(userVO);
            userService.editPassword(userDomain);
            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"修改密码失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"修改密码失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("修改密码失败");
        }
        logger.info("结束修改密码，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }


    @RequestMapping(value = "/editPassword.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="修改密码")
    public BaseResponse editPassword(UserVO userVO){
        logger.info("开始修改密码，请求参数:{}", JSON.toJSONString(userVO));
        BaseResponse<String> backResponse=new BaseResponse<>();
        try {
            UserParamsChecker.checkEditPassword(userVO);
            UserDomain userDomain= ModelConvert.userVO2UserDomain(userVO);
            userService.editPassword(userDomain);
            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"修改密码失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"修改密码失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("修改密码失败");
        }
        logger.info("结束修改密码，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }


    @RequestMapping(value = "/queryUserList.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="查询后台用户列表")
    public BaseResponse queryUserList(UserVO userVO, QueryPage queryPage){
        logger.info("开始查询后台用户列表，请求参数:{}", JSON.toJSONString(userVO));
        BaseResponse<UserDO> backResponse=new BaseResponse<>();
        try {
            UserParamsChecker.checkPageQuery(queryPage);
            userVO.setQueryPage(queryPage);
            UserDomain userDomain= ModelConvert.userVO2UserDomain(userVO);

            List<UserDO> list= userService.queryUserList(userDomain);
            backResponse.setResultList(list);
            backResponse.setQueryPage(userDomain.getQueryPage());
            backResponse.setTotalRecord(list.size());
            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"查询后台用户列表失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"查询后台用户列表失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("查询后台用户列表失败");
        }
        logger.info("结束查询后台用户列表，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }
}
