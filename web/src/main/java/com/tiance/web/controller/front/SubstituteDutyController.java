package com.tiance.web.controller.front;

import com.alibaba.fastjson.JSON;
import com.tiance.constants.CommonConstants;
import com.tiance.dal.dataobject.TcDutyDO;
import com.tiance.domainservice.checker.DutyChecker;
import com.tiance.domainservice.domain.DutyDomain;
import com.tiance.domainservice.service.DutyService;
import com.tiance.enums.EntityTypeEnum;
import com.tiance.enums.ErrorCodeEnum;
import com.tiance.enums.OperateTypeEnum;
import com.tiance.exception.BusinessException;
import com.tiance.integration.cache.MapCacheUtil;
import com.tiance.page.QueryPage;
import com.tiance.vo.DutyVO;
import com.tiance.web.aspect.OperatorControllerLog;
import com.tiance.web.controller.base.BackController;
import com.tiance.web.controller.base.FrontController;
import com.tiance.web.controller.common.BaseResponse;
import com.tiance.web.convertor.ModelConvert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 *
 * @author hmz
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：替班
 *
 * Modification History:
 *
 */
@Controller
@RequestMapping(value = "/front/substitute")
public class SubstituteDutyController extends FrontController{

    private static String LOG_PREFIX ="BackSubstituteDutyController| 替班控制器===>";

    @Autowired
    private DutyService dutyService;




    @RequestMapping(value = "/substituteDutyApply.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="申请替班")
    public BaseResponse substituteDutyApply(HttpServletRequest request, DutyVO dutyVO){
        logger.info("开始申请替班，请求参数:{}", JSON.toJSONString(dutyVO));
        BaseResponse backResponse=new BaseResponse<>();

        try {
            HttpSession session=request.getSession();
            Object memberId=session.getAttribute(CommonConstants.MEMBER_ID);
            if(null!=memberId){
                dutyVO.setMemberId((Long)memberId);
                dutyVO.setApplicantId((Long)memberId);
            }

            DutyChecker.checkSubstituteDutyApply(dutyVO);
            DutyDomain dutyDomain= ModelConvert.dutyVO2DutyDomain(dutyVO);
            if(!MapCacheUtil.lock(dutyVO.getId() + EntityTypeEnum.SUBSTITUTE_DUTY_APPLICANT.getCode())) {
                throw new BusinessException("该值班记录正在被操作，请稍后重试!");
            }
            dutyService.substituteDutyApply(dutyDomain);
            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"申请替班失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"申请替班失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("申请替班失败");
        }finally {
            MapCacheUtil.unlock(dutyVO.getId() +EntityTypeEnum.SUBSTITUTE_DUTY_APPLICANT.getCode());
        }
        logger.info("结束会员申请替班，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }



}