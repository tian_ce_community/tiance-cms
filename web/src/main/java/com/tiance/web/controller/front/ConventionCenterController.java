package com.tiance.web.controller.front;

import com.alibaba.fastjson.JSON;
import com.tiance.dal.dataobject.ConventionCenterDO;
import com.tiance.domainservice.checker.MeetingRoomChecker;
import com.tiance.domainservice.domain.ConventionCenterDomain;
import com.tiance.domainservice.service.ConventionCenterService;
import com.tiance.enums.ErrorCodeEnum;
import com.tiance.enums.OperateTypeEnum;
import com.tiance.exception.BusinessException;
import com.tiance.page.QueryPage;
import com.tiance.vo.ConventionCenterVO;
import com.tiance.web.aspect.OperatorControllerLog;
import com.tiance.web.controller.base.BackController;
import com.tiance.web.controller.base.FrontController;
import com.tiance.web.controller.common.BaseResponse;
import com.tiance.web.convertor.ModelConvert;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：会议室管理
 *
 * Modification History:
 *
 */
@Controller
@RequestMapping(value = "/front/conventionCenter")
public class ConventionCenterController extends FrontController {



    private static String LOG_PREFIX ="ConventionCenterController| 会议中心控制器===>";

    @Autowired
    private ConventionCenterService conventionCenterService;


    @RequestMapping(value = "/toMeettype.action",method = RequestMethod.GET)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到会议中心列表页面")
    public String toMeettype() {
        return "/front/meettype";
    }


    @RequestMapping(value = "/queryMonthConventionCenterStatusList.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="查询空闲的会议中心列表")
    public BaseResponse queryMonthConventionCenterStatusList(ConventionCenterVO conventionCenterVO){
        logger.info("开始查询月会议中心使用状态列表，请求参数:{}", JSON.toJSONString(conventionCenterVO));

        BaseResponse<ConventionCenterDO> backResponse=new BaseResponse<>();
        try {

            MeetingRoomChecker.checkQueryConventionCenterList(conventionCenterVO);
            ConventionCenterDomain conventionCenterDomain= ModelConvert.conventionCenterVO2ConventionCenterDomain(conventionCenterVO);
            List<ConventionCenterDO> applyActivityList= conventionCenterService.queryMonthConventionCenterStatusList(conventionCenterDomain);
            backResponse.setResultList(applyActivityList);
            backResponse.setTotalRecord(applyActivityList.size());
            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"查询月会议中心使用状态列表失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"查询月会议中心使用状态列表失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("查询月会议中心使用状态列表失败");
        }
        logger.info("结束查询月会议中心使用状态列表，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }



    @RequestMapping(value = "/queryUnoccupiedConventionCenterList.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="查询空闲的会议中心列表")
    public BaseResponse queryUnoccupiedConventionCenterList(ConventionCenterVO conventionCenterVO){
        logger.info("开始查询空闲会议中心列表，请求参数:{}", JSON.toJSONString(conventionCenterVO));

        BaseResponse<ConventionCenterDO> backResponse=new BaseResponse<>();
        try {

            MeetingRoomChecker.checkQueryConventionCenterList(conventionCenterVO);
            ConventionCenterDomain conventionCenterDomain= ModelConvert.conventionCenterVO2ConventionCenterDomain(conventionCenterVO);
            List<ConventionCenterDO> applyActivityList= conventionCenterService.queryUnoccupiedConventionCenterList(conventionCenterDomain);
            backResponse.setResultList(applyActivityList);
            backResponse.setTotalRecord(applyActivityList.size());
            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"查询空闲会议中心列表失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"查询空闲会议中心列表列表失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("查询空闲会议中心列表失败");
        }
        logger.info("结束查询空闲会议中心列表，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }




    @RequestMapping(value = "/queryConventionCenterList.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="查询会议中心列表")
    public BaseResponse queryConventionCenterList(ConventionCenterVO conventionCenterVO, QueryPage queryPage){
        logger.info("开始查询会议中心列表，请求参数:{}", JSON.toJSONString(conventionCenterVO));

        BaseResponse<ConventionCenterDO> backResponse=new BaseResponse<>();
        try {
            MeetingRoomChecker.checkPageQuery(queryPage);
            conventionCenterVO.setQueryPage(queryPage);

            MeetingRoomChecker.checkQueryConventionCenterList(conventionCenterVO);
            ConventionCenterDomain conventionCenterDomain= ModelConvert.conventionCenterVO2ConventionCenterDomain(conventionCenterVO);

            List<ConventionCenterDO> applyActivityList= conventionCenterService.queryConventionCenterList(conventionCenterDomain);
            backResponse.setResultList(applyActivityList);
            backResponse.setQueryPage(conventionCenterDomain.getQueryPage());
            backResponse.setTotalRecord(applyActivityList.size());
            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"查询会议中心列表失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"查询会议中心列表列表失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("查询会议中心列表失败");
        }
        logger.info("结束查询会议中心列表，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }



}