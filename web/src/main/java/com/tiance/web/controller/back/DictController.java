package com.tiance.web.controller.back;

import com.alibaba.fastjson.JSON;
import com.tiance.dal.dataobject.IntegrationChangeDictDO;
import com.tiance.domainservice.domain.IntegrationChangeDictDomain;
import com.tiance.domainservice.service.IntegrationChangeDictService;
import com.tiance.enums.OperateTypeEnum;
import com.tiance.exception.BusinessException;
import com.tiance.web.aspect.OperatorControllerLog;
import com.tiance.domainservice.checker.DictParamsChecker;
import com.tiance.web.controller.base.BackController;
import com.tiance.web.convertor.ModelConvert;
import com.tiance.vo.IntegrationChangeDictVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：字典控制器
 *
 * Modification History:
 *
 */
@Controller
@RequestMapping(value = "/back/dict")
public class DictController extends BackController {
    private static String LOG_PREFIX ="DictController| 字典控制器===>";

    @Autowired
    private IntegrationChangeDictService integrationChangeDictService;

    @RequestMapping(value = "/toDict.action",method = RequestMethod.GET)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到字段页面")
    public String toDict() {
        return "/back/test/dict.html";

    }

    @RequestMapping(value = "/queryIntegrationChangeDictList.action",method = RequestMethod.POST)
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="查询积分/信用变更字典")
    public String queryIntegrationChangeDictList(HttpServletRequest request, Model model,IntegrationChangeDictVO integrationChangeDictVO){
        logger.info("开始查询积分/信用变更字典，请求参数:{}", JSON.toJSONString(integrationChangeDictVO));

        try {
            DictParamsChecker.checkQueryIntegrationChangeDictList(integrationChangeDictVO);
            IntegrationChangeDictDomain integrationChangeDictDomain= ModelConvert.integrationVO2IntegrationChangeDictDomain(integrationChangeDictVO);
            List<IntegrationChangeDictDO> integrationChangeDictList= integrationChangeDictService.queryIntegrationChangeDictList(integrationChangeDictDomain);
            model.addAttribute("integrationChangeDictList",integrationChangeDictList);

        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"积分/信用变更字典查询失败，失败原因：",e1);
            model.addAttribute("errorInfo","积分/信用变更字典查询失败，请稍后重试!");
            return  "/views/initFail";
        }catch (Exception e2){
            model.addAttribute("errorInfo","积分/信用变更字典查询失败,请稍后重试!");
            logger.error(LOG_PREFIX+"积分/信用变更字典查询失败,失败原因",e2);
            return "/views/initFail";
        }
        logger.info("结束查询会员，返回数据:{}", model);
        return  "/views/initFail";

    }

}