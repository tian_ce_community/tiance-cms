package com.tiance.web.controller.back;

import com.alibaba.fastjson.JSON;
import com.tiance.dal.dataobject.IntegrationHistoryDO;
import com.tiance.dal.dataobject.IntegrationRuleDO;
import com.tiance.dal.dataobject.RoleDO;
import com.tiance.domainservice.checker.IntegrationParamsChecker;
import com.tiance.domainservice.domain.IntegrationDomain;
import com.tiance.domainservice.domain.IntegrationRuleDomain;
import com.tiance.domainservice.domain.RoleDomain;
import com.tiance.domainservice.service.IntegrationRuleService;
import com.tiance.domainservice.service.IntegrationService;
import com.tiance.enums.ErrorCodeEnum;
import com.tiance.enums.OperateTypeEnum;
import com.tiance.exception.BusinessException;
import com.tiance.page.QueryPage;
import com.tiance.vo.IntegrationHistoryVO;
import com.tiance.vo.IntegrationRuleVO;
import com.tiance.web.aspect.OperatorControllerLog;
import com.tiance.web.controller.base.BackController;
import com.tiance.web.controller.base.FrontController;
import com.tiance.web.controller.common.BaseResponse;
import com.tiance.web.convertor.ModelConvert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：积分/信用管理
 *
 * Modification History:
 *
 */
@Controller
@RequestMapping(value = "/back/integrationRule")
public class BackIntegrationRuleController extends BackController {

    private static String LOG_PREFIX ="BackIntegrationController| 积分/信用控制器===>";

    @Autowired
    private IntegrationRuleService integrationRuleService;


    @RequestMapping(value = "/toIntegralRuleList.action",method = RequestMethod.GET)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到积分/信用页面")
    public String toIntegralRuleList() {
        return "/back/system/integration/integrationRuleList";

    }



    @RequestMapping(value = "/queryIntegrationRuleList.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="查询积分/信用规则列表")
    public BaseResponse queryIntegrationRuleList(IntegrationRuleVO integrationRuleVO, QueryPage queryPage){
        logger.info("开始查询积分/信用规则列表，请求参数:{}", JSON.toJSONString(integrationRuleVO));
        BaseResponse<IntegrationRuleDO> backResponse=new BaseResponse();
        try {
            IntegrationParamsChecker.checkPageQuery(queryPage);
            integrationRuleVO.setQueryPage(queryPage);

            IntegrationRuleDomain integrationRuleDomain= ModelConvert.integrationRuleVO2IntegrationRuleDomain(integrationRuleVO);
            List<IntegrationRuleDO> list= integrationRuleService.queryIntegrationRuleList(integrationRuleDomain);
            backResponse.setResultList(list);
            backResponse.setQueryPage(integrationRuleDomain.getQueryPage());
            backResponse.setTotalRecord(list.size());
            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);

        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"查询积分/信用规则列表失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"查询积分/信用规则列表失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("查询积分/信用规则列表失败");
        }
        logger.info("结束查询积分/信用规则列表，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }


}