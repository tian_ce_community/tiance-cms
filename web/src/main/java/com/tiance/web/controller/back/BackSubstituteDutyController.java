package com.tiance.web.controller.back;

import com.alibaba.fastjson.JSON;
import com.tiance.dal.dataobject.TcDutyDO;
import com.tiance.domainservice.checker.DutyChecker;
import com.tiance.domainservice.domain.DutyDomain;
import com.tiance.domainservice.service.DutyService;
import com.tiance.enums.DutyTypeEnum;
import com.tiance.enums.ErrorCodeEnum;
import com.tiance.enums.OperateTypeEnum;
import com.tiance.exception.BusinessException;
import com.tiance.page.QueryPage;
import com.tiance.vo.DutyVO;
import com.tiance.web.aspect.OperatorControllerLog;
import com.tiance.web.controller.base.BackController;
import com.tiance.web.controller.base.FrontController;
import com.tiance.web.controller.common.BaseResponse;
import com.tiance.web.convertor.ModelConvert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 *
 * @author hmz
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：替班
 *
 * Modification History:
 *
 */
@Controller
@RequestMapping(value = "/back/substitute")
public class BackSubstituteDutyController extends BackController{

    private static String LOG_PREFIX ="BackSubstituteDutyController| 替班控制器===>";

    @Autowired
    private DutyService dutyService;


    @RequestMapping(value = "/touSubstituteDutyApplyList.action",method = RequestMethod.GET)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到替班页面")
    public String toDutyApplyList() {
        return "/back/content/duty/substituteApplyList";

    }



    @RequestMapping(value = "/querySubstituteDutyApplyList.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="查询会员申请的替班列表")
    public BaseResponse querySubstituteDutyApplyList(DutyVO dutyVO, QueryPage queryPage){
        logger.info("开始查询会员申请替班，请求参数:{}", JSON.toJSONString(dutyVO));
        BaseResponse<TcDutyDO> backResponse=new BaseResponse<>();

        try {
            DutyChecker.checkPageQuery(queryPage);
            dutyVO.setDutyType(DutyTypeEnum.SUBSTITUTE.getCode());
            DutyDomain dutyDomain= ModelConvert.dutyVO2DutyDomain(dutyVO);
            dutyDomain.setQueryPage(queryPage);
            List<TcDutyDO> dutyDOList= dutyService.querySubstituteDutyApplyList(dutyDomain);
            backResponse.setResultList(dutyDOList);
            backResponse.setQueryPage(dutyDomain.getQueryPage());
            backResponse.setTotalRecord(dutyDOList.size());
            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"查询会员申请替班失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"查询会员申请替班失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("查询会员申请替班失败");
        }
        logger.info("结束查询会员申请替班，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }



}