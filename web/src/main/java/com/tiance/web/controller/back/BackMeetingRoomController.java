package com.tiance.web.controller.back;

import com.alibaba.fastjson.JSON;
import com.tiance.constants.CommonConstants;
import com.tiance.dal.dataobject.ConventionCenterDO;
import com.tiance.dal.dataobject.MeetingRoomApplicantDO;
import com.tiance.dal.dataobject.MeetingRoomDO;
import com.tiance.dal.dataobject.UserDO;
import com.tiance.domainservice.checker.AuditChecker;
import com.tiance.domainservice.checker.MeetingRoomChecker;
import com.tiance.domainservice.domain.ConventionCenterDomain;
import com.tiance.domainservice.domain.MeetingRoomApplicantDomain;
import com.tiance.domainservice.domain.MeetingRoomDomain;
import com.tiance.domainservice.service.ConventionCenterService;
import com.tiance.domainservice.service.MeetingRoomInfoService;
import com.tiance.enums.ApplicantTypeEnum;
import com.tiance.enums.ErrorCodeEnum;
import com.tiance.enums.OperateTypeEnum;
import com.tiance.exception.BusinessException;
import com.tiance.integration.cache.MapCacheUtil;
import com.tiance.page.QueryPage;
import com.tiance.vo.ConventionCenterVO;
import com.tiance.vo.MeetingRoomApplicantVO;
import com.tiance.vo.MeetingRoomVO;
import com.tiance.web.aspect.OperatorControllerLog;
import com.tiance.web.controller.base.BackController;
import com.tiance.web.controller.common.BaseResponse;
import com.tiance.web.convertor.ModelConvert;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：会议室管理
 *
 * Modification History:
 *
 */
@Controller
@RequestMapping(value = "/back/meetingRoom")
public class BackMeetingRoomController extends BackController {



    private static String LOG_PREFIX ="BackMeetingRoomController| 会议室控制器===>";


    @Autowired
    private MeetingRoomInfoService meetingRoomInfoService;

    @RequestMapping(value = "/toMeetingRoomList.action",method = RequestMethod.GET)
    @RequiresPermissions("menu:meetingRoom")
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到会议室列表页面")
    public String toMeetingRoomList() {
        return "/back/content/meetingRoom/meetingRoomList";
    }

    @RequestMapping(value = "/toMeetingRoomApplyList.action",method = RequestMethod.GET)
    @RequiresPermissions("menu:meetingRoom")
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到会议室申请列表页面")
    public String toMeetingRoomApplyList() {
        return "/back/content/meetingRoom/meetingRoomApplyList";
    }


    @RequestMapping(value = "/queryMeetingRoomList.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="查询会议室列表")
    public BaseResponse queryMeetingRoomList(MeetingRoomVO meetingRoomVO, QueryPage queryPage){
        logger.info("开始查询会议室列表，请求参数:{}", JSON.toJSONString(meetingRoomVO));

        BaseResponse<MeetingRoomDO> backResponse=new BaseResponse<>();
        try {
            MeetingRoomChecker.checkPageQuery(queryPage);
            meetingRoomVO.setQueryPage(queryPage);

            MeetingRoomChecker.checkQueryMeetingRoomVOList(meetingRoomVO);
            MeetingRoomDomain meetingRoomDomain= ModelConvert.meetingRoomVOO2MeetingRoomDomain(meetingRoomVO);

            List<MeetingRoomDO> meetingRoomList= meetingRoomInfoService.queryMeetingRoomList(meetingRoomDomain);
            backResponse.setResultList(meetingRoomList);
            backResponse.setQueryPage(meetingRoomDomain.getQueryPage());
            backResponse.setTotalRecord(meetingRoomList.size());
            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"查询会议室列表失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"查询会议室列表列表失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("查询会议室列表失败");
        }
        logger.info("结束查询会议室列表，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }


    @RequestMapping(value = "/queryMeetingRoomApplyList.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="查询会议室申请列表")
    public BaseResponse queryMeetingRoomApplyList(MeetingRoomApplicantVO meetingRoomApplicantVO, QueryPage queryPage){
        logger.info("开始查询会议室申请列表，请求参数:{}", JSON.toJSONString(meetingRoomApplicantVO));

        BaseResponse<MeetingRoomApplicantDO> backResponse=new BaseResponse<>();
        try {
            MeetingRoomChecker.checkPageQuery(queryPage);
            meetingRoomApplicantVO.setQueryPage(queryPage);

            MeetingRoomApplicantDomain meetingRoomApplicantDomain= ModelConvert.meetingRoomApplicantVO2MeetingRoomApplicantDomain(meetingRoomApplicantVO);

            List<MeetingRoomApplicantDO> meetingRoomApplyList= meetingRoomInfoService.queryMeetingRoomApplyList(meetingRoomApplicantDomain);
            backResponse.setResultList(meetingRoomApplyList);
            backResponse.setQueryPage(meetingRoomApplicantDomain.getQueryPage());
            backResponse.setTotalRecord(meetingRoomApplyList.size());
            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"查询会议室申请列表失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"查询会议室申请列表列表失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("查询会议室申请列表失败");
        }
        logger.info("结束查询会议室申请列表，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }

    @RequestMapping(value = "/meetingRoomApplicant.action",method = RequestMethod.POST)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="插入会议室申请信息")
    public BaseResponse meetingRoomApplicant(MeetingRoomApplicantVO meetingRoomApplicantVO){
        logger.info("开始插入会议室申请信息，请求参数:{}", JSON.toJSONString(meetingRoomApplicantVO));
        BaseResponse<MeetingRoomApplicantVO> response=new BaseResponse();

        try {
            Session session = SecurityUtils.getSubject().getSession();
            UserDO userDO=(UserDO)session.getAttribute(CommonConstants.USER);
            meetingRoomApplicantVO.setApplicantId(userDO.getId());
            meetingRoomApplicantVO.setApplicantType(ApplicantTypeEnum.SYSTEM.getCode());

            MeetingRoomChecker.meetingRoomApplicant(meetingRoomApplicantVO);
            MeetingRoomApplicantDomain meetingRoomApplicantDomain= ModelConvert.meetingRoomApplicantVO2MeetingRoomApplicantDomain(meetingRoomApplicantVO);
            meetingRoomInfoService.meetingRoomApplicant(meetingRoomApplicantDomain);
            response.setSuccess(true);
            response.setMessage("会议室申请成功!");
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"插入会议室申请信息失败，失败原因：",e1);
            response.setSuccess(false);
            response.setMessage(e1.getMessage());
        }catch (Exception e2){
            response.setSuccess(false);
            response.setMessage("会议室申请失败，请稍后再试");
            logger.error(LOG_PREFIX+"插入会议室申请信息失败,失败原因",e2);
        }
        logger.info("结束插入会议室申请信息，返回数据:{}", JSON.toJSONString(meetingRoomApplicantVO));
        return  response;

    }

}