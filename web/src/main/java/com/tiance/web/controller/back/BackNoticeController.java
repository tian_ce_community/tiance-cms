package com.tiance.web.controller.back;

import com.alibaba.fastjson.JSON;
import com.tiance.dal.dataobject.ConventionCenterDO;
import com.tiance.dal.dataobject.NoticeDO;
import com.tiance.domainservice.checker.MeetingRoomChecker;
import com.tiance.domainservice.checker.NoticeChecker;
import com.tiance.domainservice.domain.ConventionCenterDomain;
import com.tiance.domainservice.domain.NoticeDomain;
import com.tiance.domainservice.service.ConventionCenterService;
import com.tiance.domainservice.service.NoticeService;
import com.tiance.enums.ErrorCodeEnum;
import com.tiance.enums.OperateTypeEnum;
import com.tiance.exception.BusinessException;
import com.tiance.page.QueryPage;
import com.tiance.vo.ConventionCenterVO;
import com.tiance.vo.NoticeVO;
import com.tiance.web.aspect.OperatorControllerLog;
import com.tiance.web.controller.base.BackController;
import com.tiance.web.controller.common.BaseResponse;
import com.tiance.web.convertor.ModelConvert;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.aspectj.weaver.ast.Not;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：会议室管理
 *
 * Modification History:
 *
 */
@Controller
@RequestMapping(value = "/back/notice")
public class BackNoticeController extends BackController {



    private static String LOG_PREFIX ="BackNoticeController| 会员须知控制器===>";

    @Autowired
    private NoticeService noticeService;


    @RequestMapping(value = "/toNoticeList.action",method = RequestMethod.GET)
    @RequiresPermissions("menu:notice")
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到会员须知列表页面")
    public String toNoticeList() {
        return "/back/memberCenter/notice/memberNoticeList";
    }


    @RequestMapping(value = "/queryNoticeList.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="查询会员须知列表")
    public BaseResponse queryNoticeList(NoticeVO noticeVO, QueryPage queryPage){
        logger.info("开始查询会员须知列表，请求参数:{}", JSON.toJSONString(noticeVO));

        BaseResponse<NoticeDO> backResponse=new BaseResponse<>();
        try {
            NoticeChecker.checkPageQuery(queryPage);
            noticeVO.setQueryPage(queryPage);

            NoticeDomain noticeDomain= ModelConvert.noticeVO2NoticeDomain(noticeVO);

            List<NoticeDO> noticeList= noticeService.queryNoticeList(noticeDomain);
            backResponse.setResultList(noticeList);
            backResponse.setQueryPage(noticeDomain.getQueryPage());
            backResponse.setTotalRecord(noticeList.size());
            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"查询会员须知列表失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"查询会员须知列表列表失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("查询会员须知列表失败");
        }
        logger.info("结束查询会员须知列表，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }


    @RequestMapping(value = "/edit.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="修改会员须知")
    public BaseResponse edit(NoticeVO noticeVO){
        logger.info("开始修改会员须知，请求参数:{}", JSON.toJSONString(noticeVO));

        BaseResponse<NoticeDO> backResponse=new BaseResponse<>();
        try {
            NoticeDomain noticeDomain= ModelConvert.noticeVO2NoticeDomain(noticeVO);

             noticeService.edit(noticeDomain);
            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"修改会员须知失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"修改会员须知失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("修改会员须知失败");
        }
        logger.info("结束修改会员须知，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }


    @RequestMapping(value = "/queryNoticeContent.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="查询会员须知内容")
    public BaseResponse queryNoticeContent(NoticeVO noticeVO){
        logger.info("开始查询会员须知内容，请求参数:{}", JSON.toJSONString(noticeVO));

        BaseResponse<String> backResponse=new BaseResponse<>();
        try {
            NoticeDomain noticeDomain= ModelConvert.noticeVO2NoticeDomain(noticeVO);

            String noticeContent=noticeService.queryNoticeContent(noticeDomain);
            backResponse.setResult(noticeContent);
            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"查询会员须知内容失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"查询会员须知内容失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("查询会员须知内容失败");
        }
        logger.info("结束查询会员须知内容，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }



}