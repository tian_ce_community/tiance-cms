package com.tiance.web.controller.back;

import com.alibaba.fastjson.JSON;
import com.tiance.constants.CommonConstants;
import com.tiance.dal.dataobject.SignInDO;
import com.tiance.dal.dataobject.SignUpDO;
import com.tiance.domainservice.checker.SignInChecker;
import com.tiance.domainservice.checker.SignUpChecker;
import com.tiance.domainservice.domain.SignInDomain;
import com.tiance.domainservice.domain.SignUpDomain;
import com.tiance.domainservice.service.SignInService;
import com.tiance.domainservice.service.SignUpService;
import com.tiance.enums.OperateTypeEnum;
import com.tiance.enums.SignInStatusEnum;
import com.tiance.exception.BusinessException;
import com.tiance.integration.cache.MapCacheUtil;
import com.tiance.page.QueryPage;
import com.tiance.vo.SignInVO;
import com.tiance.vo.SignUpVO;
import com.tiance.web.aspect.OperatorControllerLog;
import com.tiance.web.controller.base.FrontController;
import com.tiance.web.controller.common.BaseResponse;
import com.tiance.web.convertor.ModelConvert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：报名管理
 *
 * Modification History:
 *
 */
@Controller
@RequestMapping(value = "/back/signUp")
public class BackSignUpController extends FrontController {

    private static String LOG_PREFIX ="BackSignUpController| 报名控制器===>";

    @Autowired
    private SignUpService signUpService;


    @RequestMapping(value = "/toSignUp.action",method = RequestMethod.GET)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到报名页面")
    public String toSignIn() {
        return "/back/test/signUp";

    }




    @RequestMapping(value = "/addSignUp.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="插入报名")
    public BaseResponse addSignUp(HttpServletRequest request, SignUpVO signUpVO){
        logger.info("开始插入报名，请求参数:{}", JSON.toJSONString(signUpVO));
        BaseResponse<SignInVO> response=new BaseResponse();
        try {
            HttpSession session=request.getSession();
            Object memberId=session.getAttribute(CommonConstants.MEMBER_ID);
            if(null!=memberId){
                signUpVO.setMemberId((Long)memberId);
            }
            SignUpChecker.checkAddSignUp(signUpVO);
            SignUpDomain signUpDomain= ModelConvert.signUpVO2SignUpDomain(signUpVO);

            if(!MapCacheUtil.lock(signUpVO.getMemberId() + CommonConstants.SIGN_OUT)) {
                throw new BusinessException("正在操作中，请稍后再试!");
            }

            signUpService.addSignUp(signUpDomain);
            response.setSuccess(true);
            response.setMessage("报名成功!");
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"插入报名失败，失败原因：",e1);
            response.setSuccess(false);
            response.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"插入报名失败,失败原因",e2);
            response.setSuccess(false);
            response.setMessage("插入报名失败,请稍后重试!");
        }finally {
            MapCacheUtil.unlock(signUpVO.getMemberId() + CommonConstants.SIGN_OUT);
        }
        logger.info("结束插入报名，返回数据:{}", JSON.toJSONString(response));
        return  response;

    }

    @RequestMapping(value = "/querySignUpList.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="查询报名记录")
    public BaseResponse querySignUpList( Model model, SignUpVO signUpVO,QueryPage queryPage){
        logger.info("开始查询报名记录，请求参数:{}", JSON.toJSONString(signUpVO));
        BaseResponse<SignUpDO> response=new BaseResponse();

        try {
            SignUpChecker.checkPageQuery(queryPage);
            SignUpChecker.checkEntityId(signUpVO.getEntityId());
            signUpVO.setQueryPage(queryPage);
            SignUpDomain signUpDomain= ModelConvert.signUpVO2SignUpDomain(signUpVO);
            List<SignUpDO> signUpDOList = signUpService.querySignUpList(signUpDomain);
            response.setSuccess(true);
            response.setMessage("查询报名成功!");
            response.setResultList(signUpDOList);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"查询报名记录失败，失败原因：",e1);
            response.setSuccess(false);
            response.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"查询报名记录失败,失败原因",e2);
            response.setSuccess(false);
            response.setMessage("查询报名失败,请稍后重试!");
        }
        logger.info("结束查询报名记录，返回数据:{}", model);
        return  response;

    }

}