package com.tiance.web.controller.front;

import com.alibaba.fastjson.JSON;
import com.tiance.dal.dataobject.IntegrationHistoryDO;
import com.tiance.dal.dataobject.SignUpDO;
import com.tiance.domainservice.domain.IntegrationDomain;
import com.tiance.domainservice.service.IntegrationService;
import com.tiance.enums.OperateTypeEnum;
import com.tiance.exception.BusinessException;
import com.tiance.web.aspect.OperatorControllerLog;
import com.tiance.domainservice.checker.IntegrationParamsChecker;
import com.tiance.web.controller.base.FrontController;
import com.tiance.web.controller.common.BaseResponse;
import com.tiance.web.convertor.ModelConvert;
import com.tiance.vo.IntegrationHistoryVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：积分/信用管理
 *
 * Modification History:
 *
 */
@Controller
@RequestMapping(value = "/front/integration")
public class IntegrationController extends FrontController {

    private static String LOG_PREFIX ="IntegrationController| 积分/信用控制器===>";

    @Autowired
    private IntegrationService integrationService;


    @RequestMapping(value = "/toIntegral.action",method = RequestMethod.GET)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到积分/信用页面")
    public String toIntegration() {
        return "/front/integral";

    }



    @RequestMapping(value = "/queryIntegrationHistory.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="查询积分/信用")
    public BaseResponse queryIntegrationHistory(HttpServletRequest request, Model model,IntegrationHistoryVO integrationHistoryVO){
        logger.info("开始查询积分/信用，请求参数:{}", JSON.toJSONString(integrationHistoryVO));
        BaseResponse<IntegrationHistoryDO> response=new BaseResponse();
        try {
            IntegrationParamsChecker.checkQueryIntegrationHistory(integrationHistoryVO);
            IntegrationDomain integrationDomain= ModelConvert.integrationVO2IntegrationDomain(integrationHistoryVO);
            List<IntegrationHistoryDO> integrationHistoryDOList= integrationService.queryIntegrationHistory(integrationDomain);
            response.setSuccess(true);
            response.setMessage("查询成功!");
            response.setResultList(integrationHistoryDOList);

        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"积分/信用查询失败，失败原因：",e1);
            response.setSuccess(false);
            response.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"积分/信用查询失败,失败原因",e2);
            response.setSuccess(false);
            response.setMessage("积分/信用查询失败");
        }
        logger.info("结束查询积分/信用，返回数据:{}", model);
        return  response;

    }


}