package com.tiance.web.controller.front;

import com.alibaba.fastjson.JSON;
import com.tiance.constants.CommonConstants;
import com.tiance.dal.dataobject.CCOccupationDO;
import com.tiance.dal.dataobject.MeetingRoomApplicantDO;
import com.tiance.dal.dataobject.MeetingRoomDO;
import com.tiance.dal.dataobject.PublishInfoDO;
import com.tiance.domainservice.checker.MeetingRoomChecker;
import com.tiance.domainservice.checker.PublishInfoChecker;
import com.tiance.domainservice.domain.MeetingRoomApplicantDomain;
import com.tiance.domainservice.domain.MeetingRoomDomain;
import com.tiance.domainservice.domain.PublishInfoDomain;
import com.tiance.domainservice.service.MeetingRoomInfoService;
import com.tiance.domainservice.service.PublishInfoService;
import com.tiance.enums.EntityTypeEnum;
import com.tiance.enums.ErrorCodeEnum;
import com.tiance.enums.OperateTypeEnum;
import com.tiance.exception.BusinessException;
import com.tiance.integration.cache.MapCacheUtil;
import com.tiance.page.QueryPage;
import com.tiance.utils.BeanConverterUtil;
import com.tiance.utils.DateUtils;
import com.tiance.vo.CCOccupationVO;
import com.tiance.vo.MeetingRoomApplicantVO;
import com.tiance.vo.MeetingRoomVO;
import com.tiance.vo.PublishInfoVO;
import com.tiance.web.aspect.OperatorControllerLog;
import com.tiance.web.controller.base.FrontController;
import com.tiance.web.controller.common.BaseResponse;
import com.tiance.web.convertor.ModelConvert;
import org.apache.commons.collections.CollectionUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：会议室管理
 *
 * Modification History:
 *
 */
@Controller
@RequestMapping(value = "/front/meetingRoom")
public class MeetingRoomController extends FrontController {



    private static String LOG_PREFIX ="ConferenceController| 会议室控制器===>";

    @Autowired
    private MeetingRoomInfoService meetingRoomInfoService;



    @RequestMapping(value = "/toSelectmeetroom.action",method = RequestMethod.GET)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到选择会议室申请页面")
    public String toSelectmeetroom() {
        return "/front/selectmeetroom";

    }
    @RequestMapping(value = "/toSelectmeet.action",method = RequestMethod.GET)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到选择会议室申请页面")
    public String toSelectmeet() {
        return "/front/selectmeet";

    }
    @RequestMapping(value = "/toSelectroom.action",method = RequestMethod.GET)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到选择会议室申请页面")
    public String toSelectroom() {
        return "/front/selectroom";

    }
    @RequestMapping(value = "/toAppinstructions.action",method = RequestMethod.GET)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到选择会议室申请页面")
    public String toAppinstructions() {
        return "/front/appinstructions";

    }

    @RequestMapping(value = "/toApplyMeetingRoom.action",method = RequestMethod.GET)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到会议室申请页面")
    public String toApplyMeetingRoom() {
        return "/front/appmeetroom";

    }
    @RequestMapping(value = "/toMeetingRoomList.action",method = RequestMethod.GET)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到会议室列表页面")
    public String toMeetingRoomList() {
        return "/back/content/meetingRoom/meetingRoomList";
    }



    @RequestMapping(value = "/toSelectroomtime.action",method = RequestMethod.GET)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到选择会议室时间页面")
    public String toSelectroomtime() {
        return "/front/selectroomtime";
    }


    @RequestMapping(value = "/toMeetingRoomApplyList.action",method = RequestMethod.GET)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到会议室申请列表页面")
    public String toMeetingRoomApplyList() {
        return "/back/content/meetingRoom/meetingRoomApplyList";
    }


    @RequestMapping(value = "/queryMeetingRoomList.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="查询会议室列表")
    public BaseResponse queryMeetingRoomList(MeetingRoomVO meetingRoomVO, QueryPage queryPage){
        logger.info("开始查询会议室列表，请求参数:{}", JSON.toJSONString(meetingRoomVO));

        BaseResponse<MeetingRoomVO> backResponse=new BaseResponse<>();
        try {
            MeetingRoomChecker.checkPageQuery(queryPage);
            meetingRoomVO.setQueryPage(queryPage);

            MeetingRoomChecker.checkQueryMeetingRoomVOList(meetingRoomVO);
            MeetingRoomDomain meetingRoomDomain= ModelConvert.meetingRoomVOO2MeetingRoomDomain(meetingRoomVO);

            List<MeetingRoomDO> meetingRoomList= meetingRoomInfoService.queryMeetingRoomList(meetingRoomDomain);
            List<MeetingRoomVO> meetingRoomVOList=new ArrayList<>(meetingRoomList.size());
            if(!CollectionUtils.isEmpty(meetingRoomList)){
                for(MeetingRoomDO meetingRoomDO:meetingRoomList){
                    MeetingRoomVO meetingRoomVO1=new MeetingRoomVO();
                    BeanConverterUtil.copyProperties(meetingRoomVO1, meetingRoomDO);
                    if(null!=meetingRoomDO.getCreateTime()){
                        meetingRoomVO1.setCreateTimeStr(DateUtils.getYYYY_MM_DD_HH_MM_SS(meetingRoomDO.getCreateTime()));
                    }
                    if(null!=meetingRoomDO.getUpdateTime()){
                        meetingRoomVO1.setUpdateTimeStr(DateUtils.getYYYY_MM_DD_HH_MM_SS(meetingRoomDO.getUpdateTime()));
                    }

                    meetingRoomVOList.add(meetingRoomVO1);
                }
            }
            backResponse.setResultList(meetingRoomVOList);
            backResponse.setQueryPage(meetingRoomDomain.getQueryPage());
            backResponse.setTotalRecord(meetingRoomList.size());
            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"查询会议室列表失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"查询会议室列表列表失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("查询会议室列表失败");
        }
        logger.info("结束查询会议室列表，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }



    @RequestMapping(value = "/queryOccupationMeetingRoomByTime.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="根据时间查询会议室占用情况")
    public BaseResponse queryOccupationMeetingRoomByTime(MeetingRoomVO meetingRoomVO){
        logger.info("开始根据时间查询会议室占用情况，请求参数:{}", JSON.toJSONString(meetingRoomVO));

        BaseResponse<MeetingRoomVO> backResponse=new BaseResponse<>();
        try {
            MeetingRoomChecker.checkQueryMeetingRoomVOList(meetingRoomVO);
            MeetingRoomDomain meetingRoomDomain= ModelConvert.meetingRoomVOO2MeetingRoomDomain(meetingRoomVO);

            List<MeetingRoomDO> meetingRoomList= meetingRoomInfoService.queryMeetingRoomList(meetingRoomDomain);
            List<MeetingRoomVO> meetingRoomVOList=new ArrayList<>(meetingRoomList.size());

            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"根据时间查询会议室占用情况失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"根据时间查询会议室占用情况失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("根据时间查询会议室占用情况失败");
        }
        logger.info("结束根据时间查询会议室占用情况，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }

    @RequestMapping(value = "/queryOccupationMeetingRoomList.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="根据时间查询会议室占用情况")
    public BaseResponse queryOccupationMeetingRoomList(MeetingRoomVO meetingRoomVO){
        logger.info("开始根据时间查询会议室占用情况，请求参数:{}", JSON.toJSONString(meetingRoomVO));

        BaseResponse<CCOccupationDO> backResponse=new BaseResponse<>();
        try {
            MeetingRoomChecker.checkQueryMeetingRoomVOList(meetingRoomVO);
            MeetingRoomDomain meetingRoomDomain= ModelConvert.meetingRoomVOO2MeetingRoomDomain(meetingRoomVO);

            List<CCOccupationDO> meetingRoomList= meetingRoomInfoService.queryOccupationMeetingRoomList(meetingRoomDomain);

            backResponse.setResultList(meetingRoomList);
            backResponse.setTotalRecord(meetingRoomList.size());
            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"根据时间查询会议室占用情况失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"根据时间查询会议室占用情况失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("根据时间查询会议室占用情况失败");
        }
        logger.info("结束根据时间查询会议室占用情况，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }


    @RequestMapping(value = "/queryMeetingRoomApplyList.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="查询会议室申请列表")
    public BaseResponse queryMeetingRoomApplyList(MeetingRoomApplicantVO meetingRoomApplicantVO, QueryPage queryPage){
        logger.info("开始查询会议室申请列表，请求参数:{}", JSON.toJSONString(meetingRoomApplicantVO));

        BaseResponse<MeetingRoomApplicantVO> backResponse=new BaseResponse<>();
        try {
            MeetingRoomChecker.checkPageQuery(queryPage);
            meetingRoomApplicantVO.setQueryPage(queryPage);

            MeetingRoomApplicantDomain meetingRoomApplicantDomain= ModelConvert.meetingRoomApplicantVO2MeetingRoomApplicantDomain(meetingRoomApplicantVO);

            List<MeetingRoomApplicantDO> meetingRoomApplyList= meetingRoomInfoService.queryMeetingRoomApplyList(meetingRoomApplicantDomain);

            List<MeetingRoomApplicantVO> meetingRoomApplicantVOList=new ArrayList<>(meetingRoomApplyList.size());
            if(!CollectionUtils.isEmpty(meetingRoomApplyList)){
                for(MeetingRoomApplicantDO meetingRoomApplicantDO:meetingRoomApplyList){
                    MeetingRoomApplicantVO meetingRoomApplicantVO1=new MeetingRoomApplicantVO();
                    BeanConverterUtil.copyProperties(meetingRoomApplicantVO1, meetingRoomApplicantDO);
                    if(null!=meetingRoomApplicantDO.getCreateTime()){
                        meetingRoomApplicantVO1.setCreateTimeStr(DateUtils.getYYYY_MM_DD_HH_MM_SS(meetingRoomApplicantDO.getCreateTime()));
                    }
                    if(null!=meetingRoomApplicantDO.getUpdateTime()){
                        meetingRoomApplicantVO1.setUpdateTimeStr(DateUtils.getYYYY_MM_DD_HH_MM_SS(meetingRoomApplicantDO.getUpdateTime()));
                    }

                    meetingRoomApplicantVOList.add(meetingRoomApplicantVO1);
                }
            }

            backResponse.setResultList(meetingRoomApplicantVOList);
            backResponse.setQueryPage(meetingRoomApplicantDomain.getQueryPage());
            backResponse.setTotalRecord(meetingRoomApplyList.size());
            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"查询会议室申请列表失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"查询会议室申请列表列表失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("查询会议室申请列表失败");
        }
        logger.info("结束查询会议室申请列表，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }

    @RequestMapping(value = "/meetingRoomApplicant.action",method = RequestMethod.POST)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="插入会议室申请信息")
    public BaseResponse meetingRoomApplicant(HttpServletRequest request, MeetingRoomApplicantVO meetingRoomApplicantVO){
        logger.info("开始插入会议室申请信息，请求参数:{}", JSON.toJSONString(meetingRoomApplicantVO));
        BaseResponse<MeetingRoomApplicantVO> response=new BaseResponse();
        try {
            HttpSession session=request.getSession();
            Object memberId=session.getAttribute(CommonConstants.MEMBER_ID);
            if(null!=memberId){
                meetingRoomApplicantVO.setApplicantId((Long)memberId);
            }
            MeetingRoomChecker.meetingRoomApplicant(meetingRoomApplicantVO);
            MeetingRoomApplicantDomain meetingRoomApplicantDomain= ModelConvert.meetingRoomApplicantVO2MeetingRoomApplicantDomain(meetingRoomApplicantVO);
            meetingRoomInfoService.meetingRoomApplicant(meetingRoomApplicantDomain);
            response.setSuccess(true);
            response.setMessage("会议室申请成功!");
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"插入会议室申请信息失败，失败原因：",e1);
            response.setSuccess(false);
            response.setMessage(e1.getMessage());
        }catch (Exception e2){
            response.setSuccess(false);
            response.setMessage("会议室申请失败，请稍后再试");
            logger.error(LOG_PREFIX+"插入会议室申请信息失败,失败原因",e2);
        }
        logger.info("结束插入会议室申请信息，返回数据:{}", JSON.toJSONString(meetingRoomApplicantVO));
        return  response;

    }

    @RequestMapping(value = "/cancelMeetingRoomApplicant.action",method = RequestMethod.POST)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="取消会议室申请信息")
    public BaseResponse cancelMeetingRoomApplicant(MeetingRoomApplicantVO meetingRoomApplicantVO){
        logger.info("开始取消会议室申请信息，请求参数:{}", JSON.toJSONString(meetingRoomApplicantVO));
        BaseResponse<MeetingRoomApplicantVO> response=new BaseResponse();
        try {
            MeetingRoomChecker.cancelMeetingRoomApplicant(meetingRoomApplicantVO);
            MeetingRoomApplicantDomain meetingRoomApplicantDomain= ModelConvert.meetingRoomApplicantVO2MeetingRoomApplicantDomain(meetingRoomApplicantVO);

            if(!MapCacheUtil.lock(meetingRoomApplicantVO.getId() + EntityTypeEnum.MEETING_ROOM_APPLICANT.getCode())) {
                throw new BusinessException("该会议室申请正在操作中，请稍后再试!");
            }
            meetingRoomInfoService.cancelMeetingRoomApplicant(meetingRoomApplicantDomain);
            response.setSuccess(true);
            response.setMessage("取消会议室申请成功!");
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"取消会议室申请信息失败，失败原因：",e1);
            response.setSuccess(false);
            response.setMessage(e1.getMessage());
        }catch (Exception e2){
            response.setSuccess(false);
            response.setMessage("取消会议室申请失败，请稍后再试");
            logger.error(LOG_PREFIX+"取消会议室申请信息失败,失败原因",e2);
        }finally {
            MapCacheUtil.unlock(meetingRoomApplicantVO.getId() + EntityTypeEnum.MEETING_ROOM_APPLICANT.getCode());
        }
        logger.info("结束取消会议室申请信息，返回数据:{}", JSON.toJSONString(meetingRoomApplicantVO));
        return  response;

    }
}