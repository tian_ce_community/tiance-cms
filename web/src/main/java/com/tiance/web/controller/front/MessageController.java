package com.tiance.web.controller.front;

import com.alibaba.fastjson.JSON;
import com.tiance.constants.CommonConstants;
import com.tiance.dal.dao.PersonMessageDAO;
import com.tiance.dal.dataobject.IntegrationHistoryDO;
import com.tiance.dal.dataobject.PersonMessageDO;
import com.tiance.dal.dataobject.SignInDO;
import com.tiance.domainservice.checker.PersonMessageChecker;
import com.tiance.domainservice.checker.SignInChecker;
import com.tiance.domainservice.domain.PersonMessageDomain;
import com.tiance.domainservice.domain.SignInDomain;
import com.tiance.domainservice.service.PersonMessageService;
import com.tiance.domainservice.service.SignInService;
import com.tiance.enums.OperateTypeEnum;
import com.tiance.exception.BusinessException;
import com.tiance.integration.cache.MapCacheUtil;
import com.tiance.page.QueryPage;
import com.tiance.utils.BeanConverterUtil;
import com.tiance.utils.DateUtils;
import com.tiance.vo.PersonMessageVO;
import com.tiance.vo.SignInVO;
import com.tiance.web.aspect.OperatorControllerLog;
import com.tiance.web.controller.base.FrontController;
import com.tiance.web.controller.common.BaseResponse;
import com.tiance.web.convertor.ModelConvert;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：消息管理
 *
 * Modification History:
 *
 */
@Controller
@RequestMapping(value = "/front/message")
public class MessageController extends FrontController {

    private static String LOG_PREFIX ="SignInController| 消息控制器===>";

    @Autowired
    private PersonMessageService personMessageService;


    @RequestMapping(value = "/toMessage.action",method = RequestMethod.GET)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到消息页面")
    public String toMessage() {
        return "/front/message";

    }

    @RequestMapping(value = "/toMessagedetail.action",method = RequestMethod.GET)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到消息页面")
    public String toMessagedetail() {
        return "/front/messagedetail";

    }

    @RequestMapping(value = "/readPersonMessage.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="阅读个人消息记录")
    public BaseResponse readPersonMessage(HttpServletRequest request, Model model, PersonMessageVO personMessageVO){
        logger.info("开始阅读个人消息记录，请求参数:{}", JSON.toJSONString(personMessageVO));
        BaseResponse response=new BaseResponse();

        try {
            PersonMessageChecker.checkOpenId(personMessageVO.getOpenid());
            PersonMessageDomain personMessageDomain= ModelConvert.personMessageVO2PersonMessageDomain(personMessageVO);
            personMessageService.readPersonMessage(personMessageDomain);
            response.setSuccess(true);
            response.setMessage("阅读成功!");
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"阅读个人消息记录失败，失败原因：",e1);
            response.setSuccess(false);
            response.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"阅读个人消息记录失败,失败原因",e2);
            response.setSuccess(false);
            response.setMessage("阅读个人消息记录失败!");
        }
        logger.info("结束阅读个人消息记录，返回数据:{}", model);
        return  response;

    }

    @RequestMapping(value = "/queryPersonMessageDetail.action",method = RequestMethod.GET)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="查询个人消息记录详情")
    public String  queryPersonMessageDetail( Model model, PersonMessageVO personMessageVO){
        logger.info("开始查询个人消息记录详情，请求参数:{}", JSON.toJSONString(personMessageVO));

        try {

            PersonMessageChecker.checkId(personMessageVO.getId() );
            PersonMessageDomain personMessageDomain= ModelConvert.personMessageVO2PersonMessageDomain(personMessageVO);
            PersonMessageDO personMessageDO = personMessageService.queryPersonMessageDetail(personMessageDomain);
            BeanConverterUtil.copyProperties(personMessageVO, personMessageDO);
            if(null!=personMessageDO.getCreateTime()){
                personMessageVO.setCreateTimeStr(DateUtils.getYYYY_MM_DD_HH_MM_SS(personMessageDO.getCreateTime()));
            }
            if(null!=personMessageDO.getUpdateTime()){
                personMessageVO.setUpdateTimeStr(DateUtils.getYYYY_MM_DD_HH_MM_SS(personMessageDO.getUpdateTime()));
            }

            personMessageService.readPersonMessage(personMessageDomain);

            model.addAttribute("personMessageVO",personMessageVO);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"查询个人消息记录详情失败，失败原因：",e1);
            personMessageVO.setErrorInfo(e1.getMessage());
            model.addAttribute("personMessageVO",personMessageVO);

        }catch (Exception e2){
            logger.error(LOG_PREFIX+"查询个人消息记录详情失败,失败原因",e2);
            personMessageVO.setErrorInfo("查询个人消息记录详情失败");
            model.addAttribute("personMessageVO",personMessageVO);
        }
        logger.info("结束查询个人消息记录详情");
        return  "/front/messagedetail";

    }
    @RequestMapping(value = "/queryPersonMessageList.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="查询个人消息记录")
    public BaseResponse queryPersonMessageList(HttpServletRequest request, Model model, PersonMessageVO personMessageVO, QueryPage queryPage){
        logger.info("开始查询个人消息记录，请求参数:{}", JSON.toJSONString(personMessageVO));
        BaseResponse<PersonMessageVO> response=new BaseResponse();

        try {

            HttpSession session=request.getSession();
            Object memberId=session.getAttribute(CommonConstants.MEMBER_ID);
            if(null!=memberId){
                personMessageVO.setMemberId((Long)memberId);
            }
            PersonMessageChecker.checkPageQuery(queryPage);
            personMessageVO.setQueryPage(queryPage);
            PersonMessageDomain personMessageDomain= ModelConvert.personMessageVO2PersonMessageDomain(personMessageVO);
            List<PersonMessageDO> personMessageDOList = personMessageService.queryPersonMessageList(personMessageDomain);

            List<PersonMessageVO> personMessageVOList=new ArrayList<>(personMessageDOList.size());
            if(!CollectionUtils.isEmpty(personMessageDOList)){
                for(PersonMessageDO personMessageDO:personMessageDOList){
                    PersonMessageVO personMessageVO1=new PersonMessageVO();
                    BeanConverterUtil.copyProperties(personMessageVO1, personMessageDO);
                    if(null!=personMessageDO.getCreateTime()){
                        personMessageVO1.setCreateTimeStr(DateUtils.getYYYY_MM_DD_HH_MM_SS(personMessageDO.getCreateTime()));
                    }
                    if(null!=personMessageDO.getUpdateTime()){
                        personMessageVO1.setUpdateTimeStr(DateUtils.getYYYY_MM_DD_HH_MM_SS(personMessageDO.getUpdateTime()));
                    }

                    personMessageVOList.add(personMessageVO1);
                }
            }
            response.setSuccess(true);
            response.setMessage("查询成功!");
            response.setResultList(personMessageVOList);

        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"查询个人消息记录失败，失败原因：",e1);
            response.setSuccess(false);
            response.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"查询个人消息记录失败,失败原因",e2);
            response.setSuccess(false);
            response.setMessage("查询个人消息记录失败");
        }
        logger.info("结束查询个人消息记录，返回数据:{}", model);
        return  response;

    }

}