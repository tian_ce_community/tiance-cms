package com.tiance.web.controller.back;

import com.alibaba.fastjson.JSON;
import com.tiance.dal.dataobject.IntegrationChangeRecordDO;
import com.tiance.domainservice.checker.IntegrationParamsChecker;
import com.tiance.domainservice.domain.IntegrationChangeDomain;
import com.tiance.domainservice.service.IntegrationChangeService;
import com.tiance.domainservice.service.IntegrationRuleService;
import com.tiance.enums.ErrorCodeEnum;
import com.tiance.enums.OperateTypeEnum;
import com.tiance.exception.BusinessException;
import com.tiance.page.QueryPage;
import com.tiance.vo.IntegrationChangeVO;
import com.tiance.web.aspect.OperatorControllerLog;
import com.tiance.web.controller.base.BackController;
import com.tiance.web.controller.common.BaseResponse;
import com.tiance.web.convertor.ModelConvert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：积分/信用管理
 *
 * Modification History:
 *
 */
@Controller
@RequestMapping(value = "/back/integrationChangeRecord")
public class BackIntegrationChangeRecordController extends BackController {

    private static String LOG_PREFIX ="BackIntegrationController| 积分/信用控制器===>";

    @Autowired
    private IntegrationChangeService integrationChangeService;


    @RequestMapping(value = "/toRecordList.action",method = RequestMethod.GET)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到积分/信用页面")
    public String toIntegralRuleList() {
        return "/back/system/integration/integrationChangeRecordList";

    }



    @RequestMapping(value = "/queryIntegrationChangeRecordList.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="查询积分/信用变更记录列表")
    public BaseResponse queryIntegrationChangeRecordList(IntegrationChangeVO integrationChangeVO, QueryPage queryPage){
        logger.info("开始查询积分/信用变更记录列表，请求参数:{}", JSON.toJSONString(integrationChangeVO));
        BaseResponse<IntegrationChangeRecordDO> backResponse=new BaseResponse();
        try {
            IntegrationParamsChecker.checkPageQuery(queryPage);
            integrationChangeVO.setQueryPage(queryPage);

            IntegrationChangeDomain integrationChangeDomain= ModelConvert.integrationChangeVO2IntegrationChangeDomain(integrationChangeVO);
            List<IntegrationChangeRecordDO> list= integrationChangeService.queryIntegrationChangeRecordList(integrationChangeDomain);
            backResponse.setResultList(list);
            backResponse.setQueryPage(integrationChangeDomain.getQueryPage());
            backResponse.setTotalRecord(list.size());
            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);

        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"查询积分/信用变更记录列表失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"查询积分/信用变更记录列表失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("查询积分/信用变更记录列表失败");
        }
        logger.info("结束查询积分/信用变更记录列表，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }


}