package com.tiance.web.controller.back;

import com.alibaba.fastjson.JSON;
import com.tiance.dal.dataobject.RoleDO;
import com.tiance.dal.dataobject.UserDO;
import com.tiance.domainservice.checker.RoleParamsChecker;
import com.tiance.domainservice.checker.UserParamsChecker;
import com.tiance.domainservice.domain.RoleDomain;
import com.tiance.domainservice.domain.UserDomain;
import com.tiance.domainservice.service.RoleService;
import com.tiance.domainservice.service.UserService;
import com.tiance.enums.ErrorCodeEnum;
import com.tiance.enums.OperateTypeEnum;
import com.tiance.exception.BusinessException;
import com.tiance.page.QueryPage;
import com.tiance.vo.RoleVO;
import com.tiance.vo.UserVO;
import com.tiance.web.aspect.OperatorControllerLog;
import com.tiance.web.controller.base.BackController;
import com.tiance.web.controller.common.BaseResponse;
import com.tiance.web.convertor.ModelConvert;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/3/12
 *
 * Description：
 *
 * Modification History:
 *
 */
@Controller
@RequestMapping(value = "/back/role")
public class BackRoleController extends BackController{
    private static String LOG_PREFIX ="BackRoleController| 后台用户角色控制器===>";

    @Autowired
    private RoleService roleService;

    @RequestMapping(value = "/toRoleList.action",method = RequestMethod.GET)
    @RequiresPermissions("menu:system:role")
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到后台用户角色列表页面")
    public String toRoleList() {
        return "/back/system/user/roleList";
    }



    @RequestMapping(value = "/queryRoleList.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="查询后台用户角色列表")
    public BaseResponse queryRoleList(RoleVO roleVO, QueryPage queryPage){
        logger.info("开始查询后台用户角色列表，请求参数:{}", JSON.toJSONString(roleVO));
        BaseResponse<RoleDO> backResponse=new BaseResponse<>();
        try {
            RoleParamsChecker.checkPageQuery(queryPage);
            roleVO.setQueryPage(queryPage);
            RoleDomain roleDomain= ModelConvert.roleVO2RoleDomain(roleVO);

            List<RoleDO> list= roleService.queryRoleList(roleDomain);
            backResponse.setResultList(list);
            backResponse.setQueryPage(roleDomain.getQueryPage());
            backResponse.setTotalRecord(list.size());
            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"查询后台用户角色列表失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"查询后台用户角色列表失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("查询后台用户角色列表失败");
        }
        logger.info("结束查询后台用户角色列表，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }

    @RequestMapping(value = "/edit.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="修改后台用户角色详情")
    public BaseResponse edit(RoleVO roleVO){
        logger.info("开始修改后台用户角色详情，请求参数:{}", JSON.toJSONString(roleVO));
        BaseResponse<RoleDO> backResponse=new BaseResponse<>();
        try {
            RoleParamsChecker.checkQueryDetail(roleVO);
            RoleDomain roleDomain= ModelConvert.roleVO2RoleDomain(roleVO);
            RoleDO roleDO= roleService.edit(roleDomain);
            backResponse.setResult(roleDO);
            backResponse.setQueryPage(roleDomain.getQueryPage());
            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"修改后台用户角色详情失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"修改后台用户角色详情失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("修改后台用户角色详情失败");
        }
        logger.info("结束修改后台用户角色详情，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }
    @RequestMapping(value = "/queryRoleDetail.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="查询后台用户角色详情")
    public BaseResponse queryRoleDetail(RoleVO roleVO){
        logger.info("开始查询后台用户角色详情，请求参数:{}", JSON.toJSONString(roleVO));
        BaseResponse<RoleDO> backResponse=new BaseResponse<>();
        try {
            RoleParamsChecker.checkQueryDetail(roleVO);
            RoleDomain roleDomain= ModelConvert.roleVO2RoleDomain(roleVO);
            RoleDO roleDO= roleService.queryRoleDetail(roleDomain);
            backResponse.setResult(roleDO);
            backResponse.setQueryPage(roleDomain.getQueryPage());
            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"查询后台用户角色详情失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"查询后台用户角色详情失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("查询后台用户角色详情失败");
        }
        logger.info("结束查询后台用户角色详情，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }
}
