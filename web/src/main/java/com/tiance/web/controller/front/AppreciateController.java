package com.tiance.web.controller.front;

import com.alibaba.fastjson.JSON;
import com.tiance.constants.CommonConstants;
import com.tiance.dal.dataobject.AppreciateDO;
import com.tiance.dal.dataobject.SignInDO;
import com.tiance.domainservice.domain.AppreciateDomain;
import com.tiance.domainservice.domain.SignInDomain;
import com.tiance.domainservice.service.AppreciateService;
import com.tiance.enums.OperateTypeEnum;
import com.tiance.exception.BusinessException;
import com.tiance.integration.cache.MapCacheUtil;
import com.tiance.page.QueryPage;
import com.tiance.utils.BeanConverterUtil;
import com.tiance.vo.SignInVO;
import com.tiance.web.aspect.OperatorControllerLog;
import com.tiance.domainservice.checker.AppreciateChecker;
import com.tiance.web.controller.base.FrontController;
import com.tiance.web.controller.common.BaseResponse;
import com.tiance.web.convertor.ModelConvert;
import com.tiance.vo.AppreciateVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：赞赏管理
 *
 * Modification History:
 *
 */
@Controller
@RequestMapping(value = "/front/appreciate")
public class AppreciateController extends FrontController {
    private static String LOG_PREFIX ="AppreciateController| 赞赏控制器===>";

    @Autowired
    private AppreciateService appreciateService;


    @RequestMapping(value = "/toAppreciate.action",method = RequestMethod.GET)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到赞赏页面")
    public String toAppreciate() {
        return "/front/test/appreciate.html";

    }


    @RequestMapping(value = "/queryAppreciateList.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="查询赞赏记录")
    public BaseResponse queryAppreciateList(HttpServletRequest request, Model model, AppreciateVO appreciateVO, QueryPage queryPage){
        logger.info("开始查询赞赏记录，请求参数:{}", JSON.toJSONString(appreciateVO));
        BaseResponse<AppreciateDO> response=new BaseResponse();

        try {
            AppreciateChecker.checkPageQuery(queryPage);
            AppreciateChecker.checkOpenId(appreciateVO.getOpenid());
            appreciateVO.setQueryPage(queryPage);
            AppreciateDomain appreciateDomain= ModelConvert.AppreciateVO2AppreciateDomain(appreciateVO);
            List<AppreciateDO> appreciateDOList = appreciateService.queryAppreciateList(appreciateDomain);
            response.setSuccess(true);
            response.setMessage("查询赞赏成功!");
            response.setResultList(appreciateDOList);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"查询赞赏记录失败，失败原因：",e1);
            response.setSuccess(false);
            response.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"查询赞赏记录失败,失败原因",e2);
            response.setSuccess(false);
            response.setMessage("查询赞赏记录失败");
        }
        logger.info("结束查询赞赏记录，返回数据:{}", model);
        return  response;

    }



    @RequestMapping(value = "/addAppreciate.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="插入赞赏")
    public BaseResponse addAppreciate( AppreciateVO appreciateVO){
        logger.info("开始插入赞赏，请求参数:{}", JSON.toJSONString(appreciateVO));
        BaseResponse<AppreciateVO> response=new BaseResponse();

        try {
            AppreciateChecker.checkAddAppreciate(appreciateVO);
            AppreciateDomain appreciateDomain= ModelConvert.AppreciateVO2AppreciateDomain(appreciateVO);
            if(MapCacheUtil.lock(appreciateVO.getMemberId() + CommonConstants.APPRECIATE)) {
                appreciateService.addAppreciate(appreciateDomain);
                response.setSuccess(true);
                response.setMessage("赞赏成功!");
                response.setResult(appreciateVO);
            }else {
                throw new BusinessException("正在操作中，请稍后再试!");
            }
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"插入赞赏失败，失败原因：",e1);
            response.setSuccess(false);
            response.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"插入赞赏失败,失败原因",e2);
            response.setSuccess(false);
            response.setMessage("插入赞赏失败，请稍后重试！");
        }finally {
            MapCacheUtil.unlock(appreciateVO.getMemberId() + CommonConstants.APPRECIATE);
        }
        logger.info("结束插入赞赏，返回数据:{}", JSON.toJSONString(response));
        return  response;

    }


}