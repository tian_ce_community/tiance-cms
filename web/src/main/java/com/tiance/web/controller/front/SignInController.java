package com.tiance.web.controller.front;

import com.alibaba.fastjson.JSON;
import com.tiance.constants.CommonConstants;
import com.tiance.dal.dataobject.SignInDO;
import com.tiance.domainservice.domain.SignInDomain;
import com.tiance.domainservice.service.SignInService;
import com.tiance.enums.OperateTypeEnum;
import com.tiance.enums.SignInStatusEnum;
import com.tiance.exception.BusinessException;
import com.tiance.integration.cache.MapCacheUtil;
import com.tiance.page.QueryPage;
import com.tiance.utils.BeanConverterUtil;
import com.tiance.web.aspect.OperatorControllerLog;
import com.tiance.domainservice.checker.SignInChecker;
import com.tiance.web.controller.base.FrontController;
import com.tiance.web.controller.common.BaseResponse;
import com.tiance.web.convertor.ModelConvert;
import com.tiance.vo.SignInVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：签到管理
 *
 * Modification History:
 *
 */
@Controller
@RequestMapping(value = "/front/signIn")
public class SignInController extends FrontController {

    private static String LOG_PREFIX ="SignInController| 签到控制器===>";

    @Autowired
    private SignInService signInService;


    @RequestMapping(value = "/toSignreciate.action",method = RequestMethod.GET)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到签到页面")
    public String toSignIn() {
        return "/front/signreciate";
    }

    @RequestMapping(value = "/toReciatedetail.action",method = RequestMethod.GET)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到签到页面")
    public String toReciatedetail() {
        return "/front/reciatedetail";
    }


    @RequestMapping(value = "/toSignindetail.action",method = RequestMethod.GET)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到签到页面")
    public String toSignindetail() {
        return "/front/signindetail";
    }

    @RequestMapping(value = "/addSignIn.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="插入签到")
    public BaseResponse addSignIn( HttpServletRequest request,SignInVO signInVO){
        logger.info("开始插入签到，请求参数:{}", JSON.toJSONString(signInVO));
        BaseResponse<SignInVO> response=new BaseResponse();
        try {

            HttpSession session=request.getSession();
            Object memberId=session.getAttribute(CommonConstants.MEMBER_ID);
            if(null!=memberId){
                signInVO.setMemberId((Long)memberId);
            }

            SignInChecker.checkAddSignIn(signInVO);
            SignInDomain signInDomain= ModelConvert.signInVO2SignInDomain(signInVO);
            if(!MapCacheUtil.lock(signInVO.getMemberId() + CommonConstants.SIGN_IN)) {
                throw new BusinessException("正在操作中，请稍后再试!");
            }

            signInService.addSignIn(signInDomain);
            signInVO.setSignInStatus(SignInStatusEnum.SIGN_IN.getCode());
            response.setSuccess(true);
            response.setMessage("签到成功!");
            response.setResult(signInVO);

        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"插入签到失败，失败原因：",e1);
            response.setSuccess(false);
            response.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"插入签到失败,失败原因",e2);
            response.setSuccess(false);
            response.setMessage("插入签到失败,请稍后重试!");
        }finally {
            MapCacheUtil.unlock(signInVO.getMemberId() + CommonConstants.SIGN_IN);
        }
        logger.info("结束插入签到，返回数据:{}", JSON.toJSONString(response));
        return  response;

    }

    @RequestMapping(value = "/querySignInList.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="查询签到记录")
    public BaseResponse querySignInList( Model model, SignInVO signInVO,QueryPage queryPage){
        logger.info("开始查询签到记录，请求参数:{}", JSON.toJSONString(signInVO));
        BaseResponse<SignInDO> response=new BaseResponse();

        try {
            SignInChecker.checkPageQuery(queryPage);
            SignInChecker.checkMemberId(signInVO.getMemberId());
            signInVO.setQueryPage(queryPage);
            SignInDomain signInDomain= ModelConvert.signInVO2SignInDomain(signInVO);
            List<SignInDO> signInDOList = signInService.querySignInList(signInDomain);
            response.setSuccess(true);
            response.setMessage("查询签到成功!");
            response.setResultList(signInDOList);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"查询签到记录失败，失败原因：",e1);
            response.setSuccess(false);
            response.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"查询签到记录失败,失败原因",e2);
            response.setSuccess(false);
            response.setMessage("查询签到失败,请稍后重试!");
        }
        logger.info("结束查询签到记录，返回数据:{}", model);
        return  response;

    }

}