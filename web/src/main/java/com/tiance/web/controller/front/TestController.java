package com.tiance.web.controller.front;

import com.tiance.enums.OperateTypeEnum;
import com.tiance.web.aspect.OperatorControllerLog;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/21
 *
 * Description：
 *
 * Modification History:
 *
 */
@Controller
@RequestMapping(value = "/front/test")
public class TestController {
    @RequestMapping(value = "/toRegistered.action",method = RequestMethod.GET)
    public String toRegistered(HttpServletRequest request, Model model)  {
        return "/front/registered";
    }
    @RequestMapping(value = "/toIndex.action",method = RequestMethod.GET)
    public String toIndex(HttpServletRequest request, Model model)  {
        return "/front/index";
    }
}
