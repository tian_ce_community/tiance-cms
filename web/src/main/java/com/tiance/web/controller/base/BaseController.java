package com.tiance.web.controller.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：
 *
 * Modification History:
 *
 */
@Controller
public class BaseController {
    
    /** logger */
    protected static final Logger logger = LoggerFactory.getLogger(BaseController.class);


}