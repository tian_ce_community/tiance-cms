package com.tiance.web.controller.back;

import com.alibaba.fastjson.JSON;
import com.tiance.constants.CommonConstants;
import com.tiance.dal.dataobject.AuditRecordDO;
import com.tiance.dal.dataobject.ConventionCenterDO;
import com.tiance.domainservice.checker.AuditChecker;
import com.tiance.domainservice.checker.MeetingRoomChecker;
import com.tiance.domainservice.domain.AuditRecordDomain;
import com.tiance.domainservice.domain.CCOccupationDomain;
import com.tiance.domainservice.domain.ConventionCenterDomain;
import com.tiance.domainservice.domain.MeetingRoomApplicantDomain;
import com.tiance.domainservice.service.ConventionCenterService;
import com.tiance.domainservice.service.MeetingRoomInfoService;
import com.tiance.enums.ErrorCodeEnum;
import com.tiance.enums.OperateTypeEnum;
import com.tiance.exception.BusinessException;
import com.tiance.integration.cache.MapCacheUtil;
import com.tiance.page.QueryPage;
import com.tiance.vo.AuditRecordVO;
import com.tiance.vo.CCOccupationVO;
import com.tiance.vo.ConventionCenterVO;
import com.tiance.vo.MeetingRoomApplicantVO;
import com.tiance.web.aspect.OperatorControllerLog;
import com.tiance.web.controller.base.BackController;
import com.tiance.web.controller.base.FrontController;
import com.tiance.web.controller.common.BaseResponse;
import com.tiance.web.convertor.ModelConvert;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：会议室管理
 *
 * Modification History:
 *
 */
@Controller
@RequestMapping(value = "/back/conventionCenter")
public class BackConventionCenterController extends BackController {



    private static String LOG_PREFIX ="BackConventionCenterController| 会议中心控制器===>";

    @Autowired
    private ConventionCenterService conventionCenterService;


    @RequestMapping(value = "/toConventionCenterList.action",method = RequestMethod.GET)
    @RequiresPermissions("menu:conventionCenter")
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到会议中心列表页面")
    public String toConventionCenterList() {
        return "/back/content/conventionCenter/conventionCenterList";
    }


    @RequestMapping(value = "/queryConventionCenterList.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="查询会议中心列表")
    public BaseResponse queryConventionCenterList(ConventionCenterVO conventionCenterVO, QueryPage queryPage){
        logger.info("开始查询会议中心列表，请求参数:{}", JSON.toJSONString(conventionCenterVO));

        BaseResponse<ConventionCenterDO> backResponse=new BaseResponse<>();
        try {
            MeetingRoomChecker.checkPageQuery(queryPage);
            conventionCenterVO.setQueryPage(queryPage);

            MeetingRoomChecker.checkQueryConventionCenterList(conventionCenterVO);
            ConventionCenterDomain conventionCenterDomain= ModelConvert.conventionCenterVO2ConventionCenterDomain(conventionCenterVO);

            List<ConventionCenterDO> applyActivityList= conventionCenterService.queryConventionCenterList(conventionCenterDomain);
            backResponse.setResultList(applyActivityList);
            backResponse.setQueryPage(conventionCenterDomain.getQueryPage());
            backResponse.setTotalRecord(applyActivityList.size());
            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"查询会议中心列表失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"查询会议中心列表列表失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("查询会议中心列表失败");
        }
        logger.info("结束查询会议中心列表，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }


    @RequestMapping(value = "/occupation.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="占用会议中心列表")
    public BaseResponse occupation(CCOccupationVO conventionCenterVO){
        logger.info("开始查询会议中心列表，请求参数:{}", JSON.toJSONString(conventionCenterVO));

        BaseResponse<ConventionCenterDO> backResponse=new BaseResponse<>();
        try {
            MeetingRoomChecker.checkOccupation(conventionCenterVO);
            CCOccupationDomain ccOccupationDomain= ModelConvert.cCOccupationVO2ConventionCenterDomain(conventionCenterVO);

            conventionCenterService.occupation(ccOccupationDomain);
            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"查询会议中心列表失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"查询会议中心列表列表失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("查询会议中心列表失败");
        }
        logger.info("结束查询会议中心列表，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }


}