package com.tiance.web.controller.back;

import com.alibaba.fastjson.JSON;
import com.tiance.dal.dataobject.MemberAuthenticationInfoDO;
import com.tiance.domainservice.checker.MemberParamsChecker;
import com.tiance.domainservice.domain.MemberAuthOpinionDomain;
import com.tiance.domainservice.domain.MemberAuthenticationInfoDomain;
import com.tiance.domainservice.service.MemberAuthService;
import com.tiance.enums.ErrorCodeEnum;
import com.tiance.enums.MemberStatusEnum;
import com.tiance.enums.OperateTypeEnum;
import com.tiance.exception.BusinessException;
import com.tiance.integration.cache.MapCacheUtil;
import com.tiance.page.QueryPage;
import com.tiance.vo.MemberAuthOpinionVO;
import com.tiance.vo.MemberAuthenticationInfoVO;
import com.tiance.web.aspect.OperatorControllerLog;
import com.tiance.web.controller.base.BackController;
import com.tiance.web.controller.base.FrontController;
import com.tiance.web.controller.common.BaseResponse;
import com.tiance.web.convertor.ModelConvert;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：会员管理
 *
 * Modification History:
 *
 */
@Controller
@RequestMapping(value = "/back/memberAuth")
public class BackMemberAuthController extends BackController {
    private static String LOG_PREFIX ="MemberController| 会员控制器===>";

    @Autowired
    private MemberAuthService memberAuthService;

    @RequestMapping(value = "/toMemberAuth.action",method = RequestMethod.GET)
    @RequiresPermissions("menu:memberCenter:memberAuth")
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到会员审核页面")
    public String toMemberAuth() {
        return "/back/memberCenter/memberAuth/memberAuthList";
    }



    @RequestMapping(value = "/queryMemberAuthApplyList.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="查询会员认证申请列表")
    public BaseResponse queryMemberAuthApplyList(MemberAuthenticationInfoVO memberAuthenticationInfoVO, QueryPage queryPage){
        logger.info("开始查询会员认证申请列表，请求参数:{}", JSON.toJSONString(memberAuthenticationInfoVO));
        BaseResponse<MemberAuthenticationInfoDO> backResponse=new BaseResponse<>();
        try {
            MemberParamsChecker.checkPageQuery(queryPage);
            memberAuthenticationInfoVO.setQueryPage(queryPage);
            MemberAuthenticationInfoDomain memberDomain= ModelConvert.memberAuthenticationInfoVO2MemberDomain(memberAuthenticationInfoVO);

            List<String> status = Arrays.asList(MemberStatusEnum.VERIFY_APPLIED.getCode(),
                    MemberStatusEnum.BACKGROUND_VERIFY_FAIL.getCode());
            memberDomain.setStatusScope(status);
           List<MemberAuthenticationInfoDO> list= memberAuthService.queryMemberAuthApplyList(memberDomain);
            backResponse.setResultList(list);
            backResponse.setQueryPage(memberDomain.getQueryPage());
            backResponse.setTotalRecord(list.size());
            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"查询会员认证申请列表失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"查询会员认证申请列表失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("查询会员认证申请列表失败");
        }
        logger.info("结束查询会员认证申请列表，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }
    @RequestMapping(value = "/queryMemberAuthApplyDetail.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="查询会员认证申请详情")
    public String queryMemberAuthApplyDetail(HttpServletRequest request, Model model, MemberAuthenticationInfoVO memberAuthenticationInfoVO){
        logger.info("开始查询会员认证申请详情，请求参数:{}", JSON.toJSONString(memberAuthenticationInfoVO));

        try {
            MemberParamsChecker.checkId(memberAuthenticationInfoVO.getId());
            MemberAuthenticationInfoDomain memberDomain= ModelConvert.memberAuthenticationInfoVO2MemberDomain(memberAuthenticationInfoVO);

            MemberAuthenticationInfoVO memberAuthInfoDetail= memberAuthService.queryMemberAuthApplyDetail(memberDomain);
            model.addAttribute("memberAuthInfoDetail",memberAuthInfoDetail);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"查询会员认证申请详情失败，失败原因：",e1);
            model.addAttribute("errorInfo","查询会员认证申请详情失败，请稍后重试!");
            return  "/views/initFail";
        }catch (Exception e2){
            model.addAttribute("errorInfo","查询会员认证申请详情失败,请稍后重试!");
            logger.error(LOG_PREFIX+"查询会员认证申请详情失败,失败原因",e2);
            return "/views/initFail";
        }
        logger.info("结束查询会员认证申请详情，返回数据:{}", model);
        return  "/views/initFail";

    }


    @RequestMapping(value = "/updateAuthInfo.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="更新会员认证信息")
    public String updateAuthInfo( Model model, MemberAuthenticationInfoVO memberAuthenticationInfoVO){
        logger.info("开始更新会员认证信息，请求参数:{}", JSON.toJSONString(memberAuthenticationInfoVO));

        try {
            MemberParamsChecker.checkMemberId(memberAuthenticationInfoVO.getMemberId());
            MemberAuthenticationInfoDomain memberDomain= ModelConvert.memberAuthenticationInfoVO2MemberDomain(memberAuthenticationInfoVO);
            memberAuthService.updateAuthInfo(memberDomain);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"更新会员认证信息失败，失败原因：",e1);
            model.addAttribute("errorInfo","更新会员认证信息失败，请稍后重试!");
            return  "/views/initFail";
        }catch (Exception e2){
            model.addAttribute("errorInfo","更新会员认证信息失败,请稍后重试!");
            logger.error(LOG_PREFIX+"更新会员认证信息失败,失败原因",e2);
            return "/views/initFail";
        }
        logger.info("更新会员认证信息审核，返回数据:{}", model);
        return  "/views/initFail";

    }


    @RequestMapping(value = "/auditMemberAuth.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="会员认证")
    public BaseResponse auditMemberAuth( Model model, MemberAuthOpinionVO memberAuthOpinionVO){
        logger.info("开始会员认证申请审核，请求参数:{}", JSON.toJSONString(memberAuthOpinionVO));
        BaseResponse response=new BaseResponse();
        try {
            MemberParamsChecker.auditMemberAuth(memberAuthOpinionVO);
            MemberAuthOpinionDomain memberDomain= ModelConvert.memberAuthOpinionVO2MemberAuthOpinionDomain(memberAuthOpinionVO);
            if(!MapCacheUtil.lock(memberAuthOpinionVO.getMemberId()+"")){
                throw new BusinessException("该会员正在操作中，请稍后再试!");
            }
            memberAuthService.auditMemberAuth(memberDomain);
            response.setSuccess(true);
            response.setMessage("审核成功!");
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"会员认证申请审核失败，失败原因：",e1);
            response.setSuccess(false);
            response.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"会员认证申请审核失败,失败原因",e2);
            response.setSuccess(false);
            response.setMessage("审核失败，请联系后台人员!");
        }finally {
            MapCacheUtil.unlock(memberAuthOpinionVO.getMemberId()+"");
        }
        logger.info("结束会员认证申请审核，返回数据:{}", model);
        return  response;

    }

}