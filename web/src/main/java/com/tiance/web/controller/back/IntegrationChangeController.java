package com.tiance.web.controller.back;

import com.alibaba.fastjson.JSON;
import com.tiance.domainservice.domain.IntegrationChangeDomain;
import com.tiance.domainservice.service.IntegrationChangeService;
import com.tiance.enums.OperateTypeEnum;
import com.tiance.exception.BusinessException;
import com.tiance.integration.cache.MapCacheUtil;
import com.tiance.web.aspect.OperatorControllerLog;
import com.tiance.domainservice.checker.IntegrationChangeParamsChecker;
import com.tiance.web.controller.base.BackController;
import com.tiance.web.convertor.ModelConvert;
import com.tiance.vo.IntegrationChangeVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：会员积分/信用变更控制器
 *
 * Modification History:
 *
 */
@Controller
@RequestMapping(value = "/back/integrationChange")
public class IntegrationChangeController extends BackController {
    private static String LOG_PREFIX ="IntegrationChangeController| 会员积分/信用变更控制器===>";

    @Autowired
    private IntegrationChangeService integrationChangeService;

    @RequestMapping(value = "/toIntegrationChange.action",method = RequestMethod.GET)
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到积分/信用变更页面")
    public String toDict() {
        return "/back/test/integrationChange.html";

    }

    @RequestMapping(value = "/memberIntegrationChange.action",method = RequestMethod.POST)
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="会员积分/信用变更")
    public String memberIntegrationChange(HttpServletRequest request, Model model,IntegrationChangeVO integrationChangeVO){
        logger.info("开始会员积分/信用变更，请求参数:{}", JSON.toJSONString(integrationChangeVO));

        try {
            IntegrationChangeParamsChecker.checkMemberIntegrationChange(integrationChangeVO);
            IntegrationChangeDomain integrationChangeDomain= ModelConvert.integrationChangeVO2IntegrationChangeVO(integrationChangeVO);

            if (!MapCacheUtil.lock(integrationChangeDomain.getMemberId() + "")) {
                throw new BusinessException("该会员正在被操作，请稍后再试!");
            }

            integrationChangeService.memberIntegrationChange(integrationChangeDomain);
            model.addAttribute("integrationChangeVO",integrationChangeVO);

        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"会员积分/信用变更失败，失败原因：",e1);
            model.addAttribute("errorInfo","会员积分/信用变更失败，请稍后重试!");
            return  "/views/initFail";
        }catch (Exception e2){
            model.addAttribute("errorInfo","会员积分/信用变更失败,请稍后重试!");
            logger.error(LOG_PREFIX+"会员积分/信用变更失败,失败原因",e2);
            return "/views/initFail";
        }finally {
            MapCacheUtil.unlock(integrationChangeVO.getMemberId()+"");
        }


        logger.info("结束会员积分/信用变更，返回数据:{}", model);

        return  "/views/initFail";

    }

}