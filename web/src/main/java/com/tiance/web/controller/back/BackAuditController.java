package com.tiance.web.controller.back;

import com.alibaba.fastjson.JSON;
import com.tiance.dal.dataobject.ActivityDO;
import com.tiance.dal.dataobject.AuditRecordDO;
import com.tiance.domainservice.checker.ActivityChecker;
import com.tiance.domainservice.checker.AuditChecker;
import com.tiance.domainservice.checker.MemberParamsChecker;
import com.tiance.domainservice.domain.ActivityDomain;
import com.tiance.domainservice.domain.AuditRecordDomain;
import com.tiance.domainservice.service.ActivityService;
import com.tiance.domainservice.service.AuditService;
import com.tiance.enums.AuditStatusEnum;
import com.tiance.enums.ErrorCodeEnum;
import com.tiance.enums.MemberStatusEnum;
import com.tiance.enums.OperateTypeEnum;
import com.tiance.exception.BusinessException;
import com.tiance.integration.cache.MapCacheUtil;
import com.tiance.page.QueryPage;
import com.tiance.utils.Validator;
import com.tiance.vo.ActivityVO;
import com.tiance.vo.AuditRecordVO;
import com.tiance.web.aspect.OperatorControllerLog;
import com.tiance.web.controller.base.BackController;
import com.tiance.web.controller.base.FrontController;
import com.tiance.web.controller.common.BaseResponse;
import com.tiance.web.convertor.ModelConvert;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：审核管理
 *
 * Modification History:
 *
 */
@Controller
@RequestMapping(value = "/back/audit")
public class BackAuditController extends BackController {

    private static String LOG_PREFIX ="BackAuditController| 审核控制器===>";

    @Autowired
    private AuditService auditService;


    @RequestMapping(value = "/toAuditList.action",method = RequestMethod.GET)
    @RequiresPermissions("menu:audit")
    @OperatorControllerLog(operationLogType= OperateTypeEnum.FRONT_INTERFACE,operationName="跳转到审核列表页面")
    public String toMemberAuth() {
        return "/back/audit/auditList";
    }



    @RequestMapping(value = "/queryAuditDetail.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="查询审核详情")
    public BaseResponse queryAuditDetail(AuditRecordVO auditRecordVO){
        logger.info("开始查审核详情，请求参数:{}", JSON.toJSONString(auditRecordVO));

        BaseResponse<AuditRecordDO> backResponse=new BaseResponse<>();
        try {
            AuditRecordDomain auditRecordDomain= ModelConvert.auditRecordVO2AuditRecordDomain(auditRecordVO);
            AuditRecordDO auditRecord= auditService.queryAuditDetail(auditRecordDomain);
            backResponse.setResult(auditRecord);
            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"查询审核详情失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"查询审核详情失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("查询审核详情失败");
        }
        logger.info("结束查询审详情，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }


    @RequestMapping(value = "/queryAuditList.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="查询审核列表")
    public BaseResponse queryAuditList(AuditRecordVO auditRecordVO, QueryPage queryPage){
        logger.info("开始查审核列表，请求参数:{}", JSON.toJSONString(auditRecordVO));

        BaseResponse<AuditRecordDO> backResponse=new BaseResponse<>();
        try {
            AuditChecker.checkPageQuery(queryPage);
            auditRecordVO.setQueryPage(queryPage);

            AuditRecordDomain auditRecordDomain= ModelConvert.auditRecordVO2AuditRecordDomain(auditRecordVO);

            List<AuditRecordDO> applyActivityList= auditService.queryAuditList(auditRecordDomain);
            backResponse.setResultList(applyActivityList);
            backResponse.setQueryPage(auditRecordDomain.getQueryPage());
            backResponse.setTotalRecord(applyActivityList.size());
            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"查询审核列表失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"查询审核列表列表失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("查询审核列表失败");
        }
        logger.info("结束查询审核列表，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }


    @RequestMapping(value = "/audit.action",method = RequestMethod.POST)
    @ResponseBody
    @OperatorControllerLog(operationLogType=OperateTypeEnum.FRONT_INTERFACE,operationName="审核")
    public BaseResponse audit(AuditRecordVO auditRecordVO){
        logger.info("开始审核，请求参数:{}", JSON.toJSONString(auditRecordVO));

        BaseResponse backResponse=new BaseResponse<>();
        try {
            AuditChecker.checkAudit(auditRecordVO);
            AuditRecordDomain auditRecordDomain= ModelConvert.auditRecordVO2AuditRecordDomain(auditRecordVO);
            if(!MapCacheUtil.lock(auditRecordVO.getId()+"")){
                throw new BusinessException("该审核记录正在被操作，请稍后重试!");
            }
             auditService.audit(auditRecordDomain);
            backResponse.setResponseCode(ErrorCodeEnum.SUCCESS.getCode());
            backResponse.setSuccess(true);
        }catch(BusinessException e1){
            logger.info(LOG_PREFIX+"审核失败，失败原因：",e1);
            backResponse.setSuccess(false);
            backResponse.setMessage(e1.getMessage());
        }catch (Exception e2){
            logger.error(LOG_PREFIX+"审核失败,失败原因",e2);
            backResponse.setSuccess(false);
            backResponse.setMessage("审核失败");
        }finally {
            MapCacheUtil.unlock(auditRecordVO.getId()+"");
        }
        logger.info("结束审核，返回数据:{}", JSON.toJSONString(backResponse));
        return  backResponse;

    }

}