package com.tiance.web.aspect;

import com.tiance.enums.OperateTypeEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2018/12/21
 *
 * Description：
 *
 * Modification History:
 *
 */
@Target({ ElementType.PARAMETER, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface OperatorControllerLog {
    /** 要执行的操作类型比如：LOGIN-登录；INTERFACE-请求接口；LOGOUT-登出 **/
    OperateTypeEnum operationLogType() default OperateTypeEnum.DEFAULT;

    /** 要执行的具体操作比如：添加用户 **/
     String operationName() default "";
    /**
     * 描述业务操作 例:Xxx管理-执行Xxx操作
     * @return
     */
    String description() default "";
}
