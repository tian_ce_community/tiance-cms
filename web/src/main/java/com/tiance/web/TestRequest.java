package com.tiance.web;

import java.io.Serializable;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2018/9/5
 *
 * Description：
 *
 * Modification History:
 *
 */
public class TestRequest implements Serializable {
    private String productCode;
    private String dataGram;
    private String data;
    private String dataType;
    private String subjectType;
    private String caseType;
    private String bizNo;
    private String accountNo;
    private String instOrderNo;

    public String getDataGram() {
        return dataGram;
    }

    public void setDataGram(String dataGram) {
        this.dataGram = dataGram;
    }

    public String getCaseType() {
        return caseType;
    }

    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }

    public String getBizNo() {
        return bizNo;
    }

    public void setBizNo(String bizNo) {
        this.bizNo = bizNo;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getInstOrderNo() {
        return instOrderNo;
    }

    public void setInstOrderNo(String instOrderNo) {
        this.instOrderNo = instOrderNo;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(String subjectType) {
        this.subjectType = subjectType;
    }
}
