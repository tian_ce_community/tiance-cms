package com.tiance.web.utils;

import java.util.UUID;


/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2018/12/21
 *
 * Description：
 *
 * Modification History:
 *
 */
public class UUIDUtil {

	public static String getRandomUUID() {
		String s = UUID.randomUUID().toString();
		// remove "-"
		return s.substring(0, 8) + s.substring(9, 13) + s.substring(14, 18) + s.substring(19, 23) + s.substring(24);
	}
}
