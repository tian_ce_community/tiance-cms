package com.tiance.web;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jms.JmsAutoConfiguration;
import org.springframework.boot.autoconfigure.jms.activemq.ActiveMQAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2018/12/20
 */

@SpringBootApplication(scanBasePackages = "com.tiance",
                       exclude = { ActiveMQAutoConfiguration.class,
                       JmsAutoConfiguration.class,DataSourceAutoConfiguration.class})
@ComponentScan(basePackages={"com.tiance"})
@MapperScan(basePackages = {"com.tiance.dal.dao"})
@ServletComponentScan
public class Main extends SpringBootServletInitializer {

        @Override
        protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
            return builder.sources(Main.class);
        }

        public static void main(String[] args) {
            SpringApplication.run(Main.class, args);
        }

}



