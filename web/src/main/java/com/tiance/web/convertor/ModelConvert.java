package com.tiance.web.convertor;

import com.tiance.domainservice.domain.*;
import com.tiance.enums.SexEnum;
import com.tiance.enums.SubscribleStatuslEnum;
import com.tiance.integration.wechat.WxUserInfo;
import com.tiance.utils.BeanConverterUtil;
import com.tiance.utils.StringUtil;
import com.tiance.vo.*;

import java.util.Date;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/4
 *
 * Description：
 *
 * Modification History:
 *
 */
public class ModelConvert {
    public static MemberAuthenticationInfoDomain memberAuthenticationInfoVO2MemberDomain(MemberAuthenticationInfoVO memberAuthenticationInfoVO)
            throws Exception {
        MemberAuthenticationInfoDomain memberAuthenticationInfoDomain=new MemberAuthenticationInfoDomain();
        BeanConverterUtil.copyProperties(memberAuthenticationInfoDomain,memberAuthenticationInfoVO);
        return memberAuthenticationInfoDomain;
    }

    public static UserDomain userVO2UserDomain(UserVO userVO)
            throws Exception {
        UserDomain userDomain=new UserDomain();
        BeanConverterUtil.copyProperties(userDomain,userVO);
        return userDomain;
    }
    public static MemberAuthOpinionDomain memberAuthOpinionVO2MemberAuthOpinionDomain(MemberAuthOpinionVO memberAuthOpinionVO)
            throws Exception {
        MemberAuthOpinionDomain memberAuthOpinionDomain=new MemberAuthOpinionDomain();
        BeanConverterUtil.copyProperties(memberAuthOpinionDomain,memberAuthOpinionVO);
        return memberAuthOpinionDomain;
    }

    public static MemberDomain wxUserInfo2MemberDomain(MemberDomain memberDomain,WxUserInfo wxUserInfo)
            throws Exception {
        memberDomain.setWxNickName(wxUserInfo.getNickname());
        memberDomain.setMemberSex(SexEnum.getCodeByWxSex(wxUserInfo.getSex()));
        memberDomain.setCountry(wxUserInfo.getCountry());
        memberDomain.setProvince(wxUserInfo.getProvince());
        memberDomain.setCity(wxUserInfo.getCity());
        memberDomain.setWxHeadPhoto(wxUserInfo.getHeadimgurl());
        memberDomain.setOpenid(wxUserInfo.getOpenid());
        memberDomain.setMemberLanguage(wxUserInfo.getLanguage());
        memberDomain.setSubscribeTime(new Date(wxUserInfo.getSubscribe_time()));
        memberDomain.setSubscribeStatus(SubscribleStatuslEnum.getCodeByWx(wxUserInfo.getSubscribe()));
        return memberDomain;
    }
    public static MemberVO wxUserInfo2MemberVo(MemberVO memberVO,WxUserInfo wxUserInfo)
            throws Exception {
        memberVO.setWxNickName(wxUserInfo.getNickname());
        memberVO.setMemberSex(SexEnum.getCodeByWxSex(wxUserInfo.getSex()));
        memberVO.setCountry(wxUserInfo.getCountry());
        memberVO.setProvince(wxUserInfo.getProvince());
        memberVO.setCity(wxUserInfo.getCity());
        memberVO.setWxHeadPhoto(wxUserInfo.getHeadimgurl());
        memberVO.setOpenid(wxUserInfo.getOpenid());
        memberVO.setMemberLanguage(wxUserInfo.getLanguage());
        memberVO.setSubscribeTime(new Date(wxUserInfo.getSubscribe_time()));
        memberVO.setSubscribeStatus(SubscribleStatuslEnum.getCodeByWx(wxUserInfo.getSubscribe()));
        return memberVO;
    }


    public static MemberDomain memberVO2MemberDomain(MemberVO memberVO)
            throws Exception {
        MemberDomain memberDomain=new MemberDomain();
        BeanConverterUtil.copyProperties(memberDomain,memberVO);

        return memberDomain;
    }


    public static MemberAuthenticationInfoDomain memberVO2MemberAuthenticationInfoDomain(MemberAuthenticationInfoVO memberAuthenticationInfoVO)
            throws Exception {
        MemberAuthenticationInfoDomain memberDomain=new MemberAuthenticationInfoDomain();
        BeanConverterUtil.copyProperties(memberDomain,memberAuthenticationInfoVO);
        return memberDomain;
    }


    public static IntegrationChangeDomain integrationChangeVO2IntegrationChangeVO(IntegrationChangeVO integrationChangeVO)
            throws Exception {
        IntegrationChangeDomain integrationChangeDomain=new IntegrationChangeDomain();
        BeanConverterUtil.copyProperties(integrationChangeDomain,integrationChangeVO);
        return integrationChangeDomain;
    }
    public static AppreciateDomain AppreciateVO2AppreciateDomain(AppreciateVO appreciateVO)
          throws Exception {
        AppreciateDomain appreciateDomain=new AppreciateDomain();
            BeanConverterUtil.copyProperties(appreciateDomain,appreciateVO);
            return appreciateDomain;
    }

    public static SignUpDomain signUpVO2SignUpDomain(SignUpVO signUpVO)
            throws Exception {
        SignUpDomain signUpDomain=new SignUpDomain();
        BeanConverterUtil.copyProperties(signUpDomain,signUpVO);
        return signUpDomain;
    }
    public static SignInDomain signInVO2SignInDomain(SignInVO signInVO)
            throws Exception {
        SignInDomain signInDomain=new SignInDomain();
        BeanConverterUtil.copyProperties(signInDomain,signInVO);
        return signInDomain;
    }

    public static PersonMessageDomain personMessageVO2PersonMessageDomain(PersonMessageVO personMessageVO)
            throws Exception {
        PersonMessageDomain personMessageDomain=new PersonMessageDomain();
        BeanConverterUtil.copyProperties(personMessageDomain,personMessageVO);
        return personMessageDomain;
    }

    public static ActivityDomain activityVO2ActivityDomain(ActivityVO activityVO)
            throws Exception {
        ActivityDomain activityDomain=new ActivityDomain();
        BeanConverterUtil.copyProperties(activityDomain,activityVO);
        return activityDomain;
    }


    public static DutyDomain dutyVO2DutyDomain(DutyVO dutyVO)
            throws Exception {
        DutyDomain dutyDomain=new DutyDomain();
        BeanConverterUtil.copyProperties(dutyDomain,dutyVO);
        return dutyDomain;
    }

    public static TaskDomain taskVO2TaskDomain(TaskVO taskVO)
            throws Exception {
        TaskDomain taskDomain=new TaskDomain();
        BeanConverterUtil.copyProperties(taskDomain,taskVO);
        return taskDomain;
    }

    public static NoticeDomain noticeVO2NoticeDomain(NoticeVO noticeVO)
            throws Exception {
        NoticeDomain noticeDomain=new NoticeDomain();
        BeanConverterUtil.copyProperties(noticeDomain,noticeVO);
        return noticeDomain;
    }


    public static ConventionCenterDomain conventionCenterVO2ConventionCenterDomain(ConventionCenterVO conventionCenterVO)
            throws Exception {
        ConventionCenterDomain conventionCenterDomain=new ConventionCenterDomain();
        BeanConverterUtil.copyProperties(conventionCenterDomain,conventionCenterVO);
        return conventionCenterDomain;
    }


    public static CCOccupationDomain cCOccupationVO2ConventionCenterDomain(CCOccupationVO ccOccupationVO)
            throws Exception {
        CCOccupationDomain ccOccupationDomain=new CCOccupationDomain();
        BeanConverterUtil.copyProperties(ccOccupationDomain,ccOccupationVO);
        return ccOccupationDomain;
    }

    public static AuditRecordDomain auditRecordVO2AuditRecordDomain(AuditRecordVO auditRecordVO)
            throws Exception {
        AuditRecordDomain auditRecordDomain=new AuditRecordDomain();
        BeanConverterUtil.copyProperties(auditRecordDomain,auditRecordVO);
        return auditRecordDomain;
    }




    public static MeetingRoomApplicantDomain meetingRoomApplicantVO2MeetingRoomApplicantDomain(MeetingRoomApplicantVO meetingRoomApplicantVO)
            throws Exception {
        MeetingRoomApplicantDomain meetingRoomApplicantDomain=new MeetingRoomApplicantDomain();
        BeanConverterUtil.copyProperties(meetingRoomApplicantDomain,meetingRoomApplicantVO);
        return meetingRoomApplicantDomain;
    }

    public static PublishInfoDomain publishInfoVO2PublishInfoDomain(PublishInfoVO publishInfoVO)
            throws Exception {
        PublishInfoDomain publishInfoDomain=new PublishInfoDomain();
        BeanConverterUtil.copyProperties(publishInfoDomain,publishInfoVO);
        return publishInfoDomain;
    }


    public static IntegrationDomain integrationVO2IntegrationDomain(IntegrationHistoryVO integrationHistoryVO)
            throws Exception {
        IntegrationDomain integrationDomain=new IntegrationDomain();
        BeanConverterUtil.copyProperties(integrationDomain, integrationHistoryVO);
        return integrationDomain;
    }

    public static IntegrationRuleDomain integrationRuleVO2IntegrationRuleDomain(IntegrationRuleVO integrationRuleVO)
            throws Exception {
        IntegrationRuleDomain integrationRuleDomain=new IntegrationRuleDomain();
        BeanConverterUtil.copyProperties(integrationRuleDomain, integrationRuleVO);
        return integrationRuleDomain;
    }
    public static IntegrationChangeDomain integrationChangeVO2IntegrationChangeDomain(IntegrationChangeVO integrationChangeVO)
            throws Exception {
        IntegrationChangeDomain integrationChangeDomain=new IntegrationChangeDomain();
        BeanConverterUtil.copyProperties(integrationChangeDomain, integrationChangeVO);
        return integrationChangeDomain;
    }

    public static IntegrationChangeDictDomain integrationVO2IntegrationChangeDictDomain(IntegrationChangeDictVO integrationChangeDictVO)
            throws Exception {
        IntegrationChangeDictDomain integrationChangeDictDomain=new IntegrationChangeDictDomain();
        BeanConverterUtil.copyProperties(integrationChangeDictDomain, integrationChangeDictVO);
        return integrationChangeDictDomain;
    }

    public static MeetingRoomDomain meetingRoomVOO2MeetingRoomDomain(MeetingRoomVO meetingRoomVO) throws Exception {
        MeetingRoomDomain meetingRoomDomain=new MeetingRoomDomain();
        BeanConverterUtil.copyProperties(meetingRoomDomain, meetingRoomVO);
        return meetingRoomDomain;
    }

    public static RoleDomain roleVO2RoleDomain(RoleVO roleVO)  throws Exception {
        RoleDomain roleDomain = new RoleDomain();
        BeanConverterUtil.copyProperties(roleDomain, roleVO);
        return roleDomain;
    }
}
