package com.tiance.web.filter;

import com.tiance.common.ServiceContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(urlPatterns = "/front/*", filterName = "frontSessionFilter")
public class FrontSessionFilter extends OncePerRequestFilter {

    private static Logger logger = LoggerFactory.getLogger(FrontSessionFilter.class);

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {

        logger.info("我来啦.................");
        try {
            filterChain.doFilter(request, response);
        } finally {
        }
    }



}
