package com.tiance.web.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 权限
 */
@WebFilter(urlPatterns = "/back/*", filterName = "backAuthFilter")
public class BackAuthFilter extends OncePerRequestFilter {

    private static Logger logger = LoggerFactory.getLogger(BackAuthFilter.class);

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {

        try {
            filterChain.doFilter(request, response);
        }catch (Exception e) {
            Throwable throwable=e.getCause().getCause();
            logger.info("Exception异常.................",throwable.getMessage());

        }
    }



}
