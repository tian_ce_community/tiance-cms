/**
 * Created by ly on 2019/2/17.
 */
$(document).on("click",".selectgender",function(){
    $("#mask").removeClass("hide");
    $(".gender").removeClass("hide");
    $(".gender li").each(function () {
        if($(".selectgender span").text()==$(this).text()){
            $(this).addClass("selectthis");
        }
    })
});
$(document).on("click",".genderno,.genderyes",function () {
    if(!$(this).hasClass("genderyes")){
        $(".gender li").removeClass("selectthis");
    }else{
        $("input[name='memberSex']").val($(".gender li.selectthis").attr("dataval"))
        $(".selectgender span").text($(".gender li.selectthis").text()).siblings("div").removeClass("color9");
    }
    $("#mask").addClass("hide");
    $(".gender").addClass("hide");
});
$(document).on("click",".selectlist li",function () {
    $(this).siblings("li").removeClass("selectthis");
    $(this).addClass("selectthis");
});
$(document).on("click",".professionalselect",function(){
    $("#mask").removeClass("hide");
    $(".professional").removeClass("hide");
    $(".professional li").each(function () {
        if($(".professionalselect").val()==$(this).text()){
            $(this).addClass("selectthis");
        }
    })
});
$(document).on("click",".professionalno,.professionalyes",function () {
    if(!$(this).hasClass("professionalyes")){
        $(".professional li").removeClass("selectthis");
    }else{
        $("input[name='arrangeForType']").val($(".professional li.selectthis").attr("dataval"))
        $(".professionalselect").text($(".professional li.selectthis").text());
        if($(".professional li.selectthis").text()=="自主"){
           $("#independent").removeClass("hide");
        }else{
            $("#independent").addClass("hide");
        }
    }
    $("#mask").addClass("hide");
    $(".professional").addClass("hide");
});
$(document).on("click",".selectnewwork",function(){
    $("#mask").removeClass("hide");
    $(".newworkselect").removeClass("hide");
    $(".newworkselect li").each(function () {
        if($(".selectnewwork").val()==$(this).text()){
            $(this).addClass("selectthis");
        }
    })
});
$(document).on("click",".newworkno,.newworkyes",function () {
    if(!$(this).hasClass("newworkyes")){
        $(".newworkselect li").removeClass("selectthis");
    }else{

        $("input[name='currentJobState']").val($(".newworkselect li.selectthis").attr("dataval"))
        $(".selectnewwork").text($(".newworkselect li.selectthis").text());
        if($(".newworkselect li.selectthis").text()=="待业"){
            $(".company").addClass("hide");
        }else {
            $(".company").removeClass("hide");
        }
    }
    $("#mask").addClass("hide");
    $(".newworkselect").addClass("hide");
});
$(document).on("click",".multi-select li",function(){
    $(this).toggleClass("multithis");
})
$(document).on("click",".showhide",function(){
    var show=$(this).attr("boxshow");
    var hide=$(this).attr("boxhide");
    $(hide).addClass("hide");
    $(show).removeClass("hide");
})
$(document).on("click",".multi",function(){
    var show=$(this).attr("multishow");
    $(show).removeClass("hide");
    $("#mask").removeClass("hide");
})
$(document).on("click",".multino",function(){
    $("#mask").addClass("hide");
    $(this).parent().parent().addClass("hide");
    var name=$(this).attr("multival");
    var selectlist=$("input[name='"+name+"']").val().split(",");
    $(this).parent().siblings(".multilist").find('li').removeClass("multithis");
    $(this).parent().siblings(".multilist").find('li').each(function(){
        for(var i=0;i<selectlist.length;i++){
            if($(this).text()==selectlist[i]){
                $(this).addClass("multithis");
            }
        }
    });
});
$(document).on("click",".multiyes",function(){
    $("#mask").addClass("hide");
    $(this).parent().parent().addClass("hide");
    var name=$(this).attr("multival");
    var strarr=[];
    $(this).parent().siblings(".multilist").find('li.multithis').each(function(){
        strarr.push($(this).text())
    })
    $("input[name='"+name+"']").val(strarr.join(","));
})
$(document).on("click",".radio",function(){
    var show=$(this).attr("radioshow");
    $(show).removeClass("hide");
    $("#mask").removeClass("hide");
})
$(document).on("click",".radiono",function(){
    $("#mask").addClass("hide");
    $(this).parent().parent().addClass("hide");
    var name=$(this).attr("radioval");
    var selectval=$("input[name='"+name+"']").val();
    $(this).parent().siblings(".radiolist").find('li').removeClass("selectthis");
    $(this).parent().siblings(".radiolist").find('li').each(function(){
        if($(this).text()==selectval){
            $(this).addClass("selectthis");
        }
    });
});
$(document).on("click",".radioyes",function(){
    $("#mask").addClass("hide");
    $(this).parent().parent().addClass("hide");
    var name=$(this).attr("radioval");
    $("input[name='"+name+"']").val($(this).parent().siblings(".radiolist").find('li.selectthis').text());
})
$(':radio').on("click",function(){
    $("#administration").addClass("hide");
    $("#technology").addClass("hide");
    if($(this).val()=="JOBTWOWAY"){
        $("#administration").removeClass("hide")
        $("#technology").removeClass("hide")
    }else if($(this).val()=="ADMINISTRATION"){
        $("#administration").removeClass("hide")
    }else if($(this).val()=="TECHNIQUE"){
        $("#technology").removeClass("hide")
    }
})