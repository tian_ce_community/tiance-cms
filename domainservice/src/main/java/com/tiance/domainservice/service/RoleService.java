package com.tiance.domainservice.service;

import com.tiance.dal.dataobject.RoleDO;
import com.tiance.domainservice.domain.RoleDomain;

import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/31
 *
 * Description：
 *
 * Modification History:
 *
 */
public interface RoleService {
    List<RoleDO> queryRoleList(RoleDomain roleDomain);

    RoleDO queryRoleDetail(RoleDomain roleDomain);

    RoleDO edit(RoleDomain roleDomain) throws Exception;
}
