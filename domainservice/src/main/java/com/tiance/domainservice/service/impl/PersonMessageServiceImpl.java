package com.tiance.domainservice.service.impl;

import com.alibaba.fastjson.JSON;
import com.tiance.dal.dao.PersonMessageDAO;
import com.tiance.dal.dataobject.PersonMessageDO;
import com.tiance.dal.dataobject.SignInDO;
import com.tiance.domainservice.domain.PersonMessageDomain;
import com.tiance.domainservice.service.PersonMessageService;
import com.tiance.enums.IsReadEnum;
import com.tiance.exception.BusinessException;
import com.tiance.page.QueryPage;
import com.tiance.utils.MapUtil;
import com.tiance.utils.StringUtil;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/16
 *
 * Description：
 *
 * Modification History:
 *
 */
@Service
public class PersonMessageServiceImpl implements PersonMessageService {

    protected static final Logger logger = LoggerFactory.getLogger(PersonMessageServiceImpl.class);

    @Resource
    private PersonMessageDAO personMessageDAO;

    @Override
    public List<PersonMessageDO> queryPersonMessageList(PersonMessageDomain domain) {
        Map<String,Object> paramMap= MapUtil.params2Map(new Object[]{ "memberId",StringUtil.trimNull(domain.getMemberId())},
                new Object[]{ "openid",StringUtil.trimNull(domain.getOpenid())});
        logger.info("开始查询消息总数，参数:{}", JSON.toJSONString(paramMap));
        int count  =personMessageDAO.queryPersonMessageCount(paramMap);
        logger.info("开始查询消息总数，返回总数:{}", count);


        if(0==count){
            return new ArrayList();
        }

        QueryPage queryPage = domain.getQueryPage();
        queryPage.setTotalItem(count);
        paramMap.put("queryPage", queryPage);
        if (queryPage.getEndRow() == 0) {
            queryPage.setStartRow(queryPage.getPageFristItem());
            queryPage.setEndRow(queryPage.getPageLastItem());
        }


        logger.info("开始查询消息数据，参数:{}", JSON.toJSONString(paramMap));
        List<PersonMessageDO> personMessageDOList =personMessageDAO.queryPersonMessageList(paramMap);
        logger.info("结束始查询消息，查询结果:{}", JSON.toJSONString(personMessageDOList));
        return personMessageDOList;
    }

    @Override
    public PersonMessageDO queryPersonMessageDetail(PersonMessageDomain personMessageDomain) {
        logger.info("开始查询个人消息数据，参数:ID={}", personMessageDomain.getId());
        PersonMessageDO personMessageDO=personMessageDAO.queryPersonMessageDetail(personMessageDomain.getId());
        logger.info("结束查询个人消息数据，查询结果{}", JSON.toJSONString(personMessageDO));
        return  personMessageDO;
    }

    @Override
    public void readPersonMessage(PersonMessageDomain personMessageDomain) {
        PersonMessageDO personMessageDO =new PersonMessageDO();
        personMessageDO.setId(personMessageDomain.getId());
        personMessageDO.setReadStatus(IsReadEnum.READ.getCode());
        logger.info("开始更新个人消息数据，参数:{}", JSON.toJSONString(personMessageDO));
         int count=personMessageDAO.update(personMessageDO);
        logger.info("结束更新个人消息，是否更新成功:{}", count>0?true:false);
        if(count<1){
            throw new BusinessException("更新失败");
        }
    }
}
