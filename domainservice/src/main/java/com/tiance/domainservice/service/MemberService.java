package com.tiance.domainservice.service;

import com.tiance.dal.dataobject.MemberDO;
import com.tiance.domainservice.domain.MemberAuthenticationInfoDomain;
import com.tiance.domainservice.domain.MemberDomain;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/31
 *
 * Description：
 *
 * Modification History:
 *
 */
public interface MemberService {
    /**
     * 注册
     * @param memberDomain
     * @return
     * @throws Exception
     */
    Long register(MemberDomain memberDomain) throws Exception;

    /**
     * 查询会员
     * @param memberDomain
     * @return
     */
    MemberDO queryMember(MemberDomain memberDomain);

    /**
     * 更新会员
     * @param memberDomain
     * @return
     */
    MemberDO updateMember(MemberDomain memberDomain) throws Exception;

    /**
     * 申请会员认证
     * @param memberAuthenticationInfoDomain
     * @throws Exception
     */
    void applyMemberAuth(MemberAuthenticationInfoDomain memberAuthenticationInfoDomain) throws Exception;

    List<MemberDO> queryMemberList(MemberDomain memberDomain);

    void addGuest(MemberDomain memberDomain);

}
