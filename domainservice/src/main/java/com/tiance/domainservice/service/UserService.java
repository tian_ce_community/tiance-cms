package com.tiance.domainservice.service;

import com.tiance.dal.dataobject.UserDO;
import com.tiance.domainservice.domain.UserDomain;

import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/31
 *
 * Description：
 *
 * Modification History:
 *
 */
public interface UserService {
    UserDO queryUserByUserName(String username);

    List<UserDO> queryUserList(UserDomain userDomain);

    void editPassword(UserDomain userDomain);
}
