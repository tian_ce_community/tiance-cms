package com.tiance.domainservice.service.impl;

import com.alibaba.fastjson.JSON;
import com.tiance.dal.dao.UserDAO;
import com.tiance.dal.dataobject.ActivityDO;
import com.tiance.dal.dataobject.UserDO;
import com.tiance.domainservice.domain.UserDomain;
import com.tiance.domainservice.service.UserService;
import com.tiance.domainservice.utils.PasswordUtil;
import com.tiance.page.QueryPage;
import com.tiance.utils.MapUtil;
import com.tiance.utils.StringUtil;
import com.tiance.utils.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/31
 *
 * Description：
 *
 * Modification History:
 *
 */
@Service
public class UserServiceImpl   implements UserService {

    private static Logger logger = LoggerFactory.getLogger(UserService.class);

    @Resource
    private UserDAO userDAO;



    @Override
    public UserDO queryUserByUserName(String userName) {
        return userDAO.queryUserByUserName(userName);
    }

    @Override
    public List<UserDO> queryUserList(UserDomain domain) {
        Map<String,Object> paramMap= MapUtil.params2Map(new Object[]{ "status", StringUtil.trimNull(domain.getStatus())},
                new Object[]{ "userName", StringUtil.trimNull(domain.getUserName())});
        logger.info("开始查询后台用户数据总数，参数:{}", JSON.toJSONString(paramMap));
        int count  =userDAO.queryUserListCount(paramMap);
        logger.info("结束查询后台用户数据总数，返回总数:{}", count);


        if(0==count){
            return new ArrayList();
        }

        QueryPage queryPage = domain.getQueryPage();
        queryPage.setTotalItem(count);
        paramMap.put("queryPage", queryPage);
        if (queryPage.getEndRow() == 0) {
            queryPage.setStartRow(queryPage.getPageFristItem());
            queryPage.setEndRow(queryPage.getPageLastItem());
        }

        domain.setQueryPage(queryPage);
        logger.info("开始查询后台用户数据列表数据，参数:{}", JSON.toJSONString(paramMap));
        List<UserDO> userDOList =userDAO.queryUserList(paramMap);
        logger.info("结束查询后台用户数据数据，查询结果:{}", JSON.toJSONString(userDOList));
        return userDOList;
    }

    @Override
    public void editPassword(UserDomain userDomain) {
        logger.info("开始查询后台用户数据，参数:{}", JSON.toJSONString(userDomain.getId()));
        UserDO  oldUser  =userDAO.queryUserByUserId(userDomain.getId());
        logger.info("结束查询后台用户数据，返回总数:{}", oldUser);

        Validator.assertNull(oldUser,"未查找到用户");
        Validator.assertNull(oldUser.getId(),"未查找到用户");
        String oldPassword=PasswordUtil.encryptPassword(oldUser.getUserName(),userDomain.getOldPassword());
        Validator.assertTrue(!(oldUser.getPassWord().equals(oldPassword)),"旧密码不正确!");

        UserDO userDO=new UserDO();
        userDO.setId(userDomain.getId());
        String newPassword=PasswordUtil.encryptPassword(oldUser.getUserName(),userDomain.getNewPassword());

        userDO.setPassWord(newPassword);
        logger.info("开始更新后台用户数据，参数:{}", JSON.toJSONString(userDO));
        int count=userDAO.updatePassword(userDO);
        logger.info("结束更新后台用户数据，返回总数:{}", count);

    }
}
