package com.tiance.domainservice.service;

import com.tiance.dal.dataobject.SignUpDO;
import com.tiance.domainservice.domain.SignUpDomain;

import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/31
 *
 * Description：
 *
 * Modification History:
 *
 */
public interface SignUpService {

    void addSignUp(SignUpDomain signUpDomain) throws Exception;

    List<SignUpDO> querySignUpList(SignUpDomain signUpDomain);

    void cancelSignUp(SignUpDomain signUpDomain);
}
