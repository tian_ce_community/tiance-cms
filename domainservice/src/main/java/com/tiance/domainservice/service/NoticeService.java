package com.tiance.domainservice.service;

import com.tiance.dal.dataobject.ConventionCenterDO;
import com.tiance.dal.dataobject.NoticeDO;
import com.tiance.domainservice.domain.ConventionCenterDomain;
import com.tiance.domainservice.domain.NoticeDomain;

import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/31
 *
 * Description：
 *
 * Modification History:
 *
 */
public interface NoticeService {

    List<NoticeDO> queryNoticeList(NoticeDomain noticeDomain);

    void edit(NoticeDomain noticeDomain);

    String queryNoticeContent(NoticeDomain noticeDomain);
}
