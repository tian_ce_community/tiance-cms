package com.tiance.domainservice.service;

import com.tiance.dal.dataobject.AppreciateDO;
import com.tiance.domainservice.domain.AppreciateDomain;

import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/31
 *
 * Description：
 *
 * Modification History:
 *
 */
public interface AppreciateService {
    AppreciateDO addAppreciate(AppreciateDomain appreciateDomain) throws Exception;

    List<AppreciateDO> queryAppreciateList(AppreciateDomain appreciateDomain);
}
