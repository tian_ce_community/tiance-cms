package com.tiance.domainservice.service;

import com.tiance.dal.dataobject.SignInDO;
import com.tiance.domainservice.domain.SignInDomain;

import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/31
 *
 * Description：
 *
 * Modification History:
 *
 */
public interface SignInService {

    SignInDO addSignIn(SignInDomain signInDomain) throws Exception;

    List<SignInDO> querySignInList(SignInDomain signInDomain);

    String queryMemberSignStatus(Long memberId);
}
