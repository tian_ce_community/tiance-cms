package com.tiance.domainservice.service.impl;

import com.alibaba.fastjson.JSON;
import com.tiance.dal.dao.AppreciateDAO;
import com.tiance.dal.dataobject.AppreciateDO;
import com.tiance.dal.dataobject.SignInDO;
import com.tiance.domainservice.assemble.DOAssemble;
import com.tiance.domainservice.domain.AppreciateDomain;
import com.tiance.domainservice.service.AppreciateService;
import com.tiance.exception.BusinessException;
import com.tiance.page.QueryPage;
import com.tiance.utils.MapUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/13
 *
 * Description：
 *
 * Modification History:
 *
 */
@Service
public class AppreciateServiceImpl implements AppreciateService {

    protected static final Logger logger = LoggerFactory.getLogger(AppreciateServiceImpl.class);

    @Resource
    private AppreciateDAO appreciateDAO;
    @Override
    public AppreciateDO addAppreciate(AppreciateDomain appreciateDomain) throws Exception {
        AppreciateDO appreciateDO= DOAssemble.assembleAppreciateDO(appreciateDomain);
        logger.info("开始插入赞赏数据，参数:{}", JSON.toJSONString(appreciateDO));
        Long count=appreciateDAO.insert(appreciateDO);
        logger.info("结束插入赞赏，是否插入成功:{}", count>0?true:false);
        if(count<1){
            throw new BusinessException("插入失败");
        }
        return appreciateDO;
    }

    @Override
    public List<AppreciateDO> queryAppreciateList(AppreciateDomain domain) {

        Map<String,Object> paramMap= MapUtil.params2Map(new Object[]{ "fromMemberId", domain.getFromMemberId()},
                new Object[]{ "toMemberId", domain.getToMemberId()});
        logger.info("开始查询赞赏总数，参数:{}", JSON.toJSONString(paramMap));
        int count  =appreciateDAO.queryAppreciateCount(paramMap);
        logger.info("开始查询赞赏总数，返回总数:{}", count);


        if(0==count){
            return new ArrayList();
        }

        QueryPage queryPage = domain.getQueryPage();
        queryPage.setTotalItem(count);
        paramMap.put("queryPage", queryPage);
        if (queryPage.getEndRow() == 0) {
            queryPage.setStartRow(queryPage.getPageFristItem());
            queryPage.setEndRow(queryPage.getPageLastItem());
        }


        logger.info("开始查询赞赏记录列表，参数:{}", JSON.toJSONString(paramMap));
        List<AppreciateDO> appreciateDOList =appreciateDAO.queryAppreciateList(paramMap);
        logger.info("结束查询赞赏记录列表，查询结果:{}", JSON.toJSONString(appreciateDOList));
        return appreciateDOList;
    }
}
