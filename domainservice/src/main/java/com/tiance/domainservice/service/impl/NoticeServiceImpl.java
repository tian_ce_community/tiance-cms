package com.tiance.domainservice.service.impl;

import com.alibaba.fastjson.JSON;
import com.tiance.dal.dao.ActivityDAO;
import com.tiance.dal.dao.NoticeDAO;
import com.tiance.dal.dataobject.AuditRecordDO;
import com.tiance.dal.dataobject.NoticeDO;
import com.tiance.domainservice.domain.NoticeDomain;
import com.tiance.domainservice.service.NoticeService;
import com.tiance.page.QueryPage;
import com.tiance.utils.MapUtil;
import com.tiance.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/14
 *
 * Description：
 *
 * Modification History:
 *
 */
@Service
public class NoticeServiceImpl implements NoticeService {
    protected static final Logger logger = LoggerFactory.getLogger(NoticeServiceImpl.class);

    @Resource
    private NoticeDAO noticeDAO;
    @Override
    public List<NoticeDO> queryNoticeList(NoticeDomain domain) {
        Map<String,Object> paramMap= MapUtil.params2Map(new Object[]{ "noticeType", StringUtil.trimNull(domain.getNoticeType())});
        logger.info("开始查询会员须知数据总数，参数:{}", JSON.toJSONString(paramMap));
        int count  =noticeDAO.queryNoticeListCount(paramMap);
        logger.info("开始查询会员须知数据总数，返回总数:{}", count);


        if(0==count){
            return new ArrayList();
        }

        QueryPage queryPage = domain.getQueryPage();
        queryPage.setTotalItem(count);
        paramMap.put("queryPage", queryPage);
        if (queryPage.getEndRow() == 0) {
            queryPage.setStartRow(queryPage.getPageFristItem());
            queryPage.setEndRow(queryPage.getPageLastItem());
        }

        domain.setQueryPage(queryPage);
        logger.info("开始查询会员须知数据列表数据，参数:{}", JSON.toJSONString(paramMap));
        List<NoticeDO> noticeDOList =noticeDAO.queryNoticeList(paramMap);
        logger.info("结束查询会员须知列表数据，查询结果:{}", JSON.toJSONString(noticeDOList));
        return noticeDOList;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void edit(NoticeDomain noticeDomain) {
        Map<String,Object> paramMap= MapUtil.params2Map(new Object[]{ "noticeContent", StringUtil.trimNull(noticeDomain.getNoticeContent())},
                new Object[]{ "id", StringUtil.trimNull(noticeDomain.getId())});

        logger.info("开始修改会员须知，参数:{}", JSON.toJSONString(paramMap));
        int count  =noticeDAO.update(paramMap);
        logger.info("开始修改会员须知，更新条数:{}", count);

    }

    @Override
    public String queryNoticeContent(NoticeDomain noticeDomain) {
        logger.info("开始查询会员须知内容，参数:{}", noticeDomain.getId());
        String  noticeContent  =noticeDAO.queryNoticeContent(noticeDomain.getId());
        logger.info("开始查询会员须知内容，查询结果:{}", noticeContent);

        return noticeContent;
    }
}
