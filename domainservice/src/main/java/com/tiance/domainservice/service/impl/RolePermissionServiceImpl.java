package com.tiance.domainservice.service.impl;

import com.tiance.dal.dao.RolePermissionDAO;
import com.tiance.dal.dataobject.RolePermissionDO;
import com.tiance.domainservice.service.RolePermissionService;
import com.tiance.domainservice.service.UserService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/3
 *
 * Description：
 *
 * Modification History:
 *
 */
@Service
public class RolePermissionServiceImpl implements RolePermissionService {


    private static Logger logger = LoggerFactory.getLogger(RolePermissionService.class);

    @Resource
    private RolePermissionDAO rolePermissionDAO;

    @Override
    public Set<String> queryRolePermissionSetByRoleId(Long roleId) {

        Set<String> set=new HashSet();
        List<String>  permissionCodeList= rolePermissionDAO.queryPermissionCodeListByRoleId(roleId);

        if(CollectionUtils.isNotEmpty(permissionCodeList)){
            for(String permissionCode :permissionCodeList){
                set.add(permissionCode);
            }
        }
        return set;
    }
}
