package com.tiance.domainservice.service.impl;

import com.tiance.dal.dao.IntegrationChangeDictDAO;
import com.tiance.dal.dataobject.IntegrationChangeDictDO;
import com.tiance.domainservice.domain.IntegrationChangeDictDomain;
import com.tiance.domainservice.service.IntegrationChangeDictService;
import com.tiance.utils.MapUtil;
import com.tiance.utils.StringUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/15
 *
 * Description：
 *
 * Modification History:
 *
 */
@Service
public class IntegrationChangeDictServiceImpl implements IntegrationChangeDictService {

    @Resource
    private IntegrationChangeDictDAO integrationChangeDictDAO;



    @Override
    public List<IntegrationChangeDictDO> queryIntegrationChangeDictList(
            IntegrationChangeDictDomain integrationChangeDictDomain) {

        Map<String,Object> map= MapUtil.params2Map(new Object[]{ "status", StringUtil.trimNull(integrationChangeDictDomain.getStatus())});
        return integrationChangeDictDAO.queryIntegrationChangeDictList(map);
    }
}
