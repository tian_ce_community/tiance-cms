package com.tiance.domainservice.service;

import com.tiance.dal.dataobject.IntegrationRuleDO;
import com.tiance.domainservice.domain.IntegrationRuleDomain;

import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/31
 *
 * Description：
 *
 * Modification History:
 *
 */
public interface IntegrationRuleService {

    List<IntegrationRuleDO> queryIntegrationRuleList(IntegrationRuleDomain integrationRuleDomain);
}
