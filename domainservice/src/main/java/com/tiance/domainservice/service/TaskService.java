package com.tiance.domainservice.service;

import com.tiance.dal.dataobject.TaskDO;
import com.tiance.domainservice.domain.TaskDomain;

import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/31
 *
 * Description：
 *
 * Modification History:
 *
 */
public interface TaskService {

    List<TaskDO> queryTaskList(TaskDomain taskDomain);

    void cancel(TaskDomain taskDomain);

    void finish(TaskDomain taskDomain);

    void addTask(TaskDomain taskDomain) throws Exception;

    List<TaskDO> querySignUpTaskList(TaskDomain taskDomain);
}
