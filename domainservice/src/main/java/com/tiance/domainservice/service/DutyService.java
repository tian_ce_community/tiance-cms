package com.tiance.domainservice.service;

import com.tiance.dal.dataobject.TcDutyDO;
import com.tiance.domainservice.domain.DutyDomain;
import com.tiance.vo.DutyVO;

import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/31
 *
 * Description：
 *
 * Modification History:
 *
 */
public interface DutyService {

    TcDutyDO addDuty(DutyDomain activityDomain) throws Exception;

    List<TcDutyDO> queryDuty(DutyDomain activityDomain);

    void cancel(DutyDomain activityDomain);

    List<TcDutyDO> querySubstituteDutyApplyList(DutyDomain dutyDomain);

    void substituteDutyApply(DutyDomain dutyDomain) throws Exception;

    void finish(DutyDomain dutyDomain);
}
