package com.tiance.domainservice.service.impl;

import com.alibaba.fastjson.JSON;
import com.tiance.dal.dao.*;
import com.tiance.dal.dataobject.*;
import com.tiance.domainservice.assemble.DOAssemble;
import com.tiance.domainservice.domain.MeetingRoomApplicantDomain;
import com.tiance.domainservice.domain.MeetingRoomDomain;
import com.tiance.domainservice.domain.PublishInfoDomain;
import com.tiance.domainservice.service.MeetingRoomInfoService;
import com.tiance.enums.MeetingRoomApplicantStatusEnum;
import com.tiance.enums.UseStatusEnum;
import com.tiance.exception.BusinessException;
import com.tiance.page.QueryPage;
import com.tiance.utils.MapUtil;
import com.tiance.utils.StringUtil;
import com.tiance.utils.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/3/1
 *
 * Description：
 *
 * Modification History:
 *
 */
@Service
public class MeetingRoomInfoServiceImpl implements MeetingRoomInfoService {
    protected static final Logger logger = LoggerFactory.getLogger(PersonMessageServiceImpl.class);


    @Resource
    private MeetingRoomApplicantDAO meetingRoomApplicantDAO;

    @Autowired
    private MeetingRoomDAO meetingRoomDAO;

    @Autowired
    private ConventionCenterDAO conventionCenterDAO;

    @Resource
    private AuditRecordDAO auditRecordDAO;
    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void meetingRoomApplicant(MeetingRoomApplicantDomain meetingRoomApplicantDomain)
            throws Exception {

        MeetingRoomApplicantDO meetingRoomApplicantDO= DOAssemble.assembleMeetingRoomApplicantDO(meetingRoomApplicantDomain);

        logger.info("开始查询会议中心信息，参数:{}", JSON.toJSONString(meetingRoomApplicantDomain));
        ConventionCenterDO conventionCenterDO=conventionCenterDAO.queryById(meetingRoomApplicantDomain.getConventionCenterId());
        logger.info("结束查询会议中心信息，返回数据:{}", JSON.toJSONString(conventionCenterDO));
        Validator.assertNull(conventionCenterDO,"会议中心不存在!");
        Validator.assertNull(conventionCenterDO.getId(),"会议中心不存在!");
        Validator.assertTrue(!(UseStatusEnum.NORMAL.getCode().equals(conventionCenterDO.getStatus())),"会议中心不可用!");
        meetingRoomApplicantDO.setConventionCenterName(conventionCenterDO.getName());
        meetingRoomApplicantDO.setCoordinate(conventionCenterDO.getCoordinate());
        meetingRoomApplicantDO.setPosition(conventionCenterDO.getPosition());

        logger.info("开始查询会议室信息，参数:{}", JSON.toJSONString(meetingRoomApplicantDomain));
        MeetingRoomDO meetingRoomDO=meetingRoomDAO.queryById(meetingRoomApplicantDomain.getMeetingRoomId());
        logger.info("结束查询会议室信息，返回数据:{}", JSON.toJSONString(meetingRoomDO));
        Validator.assertNull(meetingRoomDO,"会议室不存在!");
        Validator.assertNull(meetingRoomDO.getId(),"会议室不存在!");
        Validator.assertTrue(!(UseStatusEnum.NORMAL.getCode().equals(meetingRoomDO.getStatus())),"会议室不可用!");
        meetingRoomApplicantDO.setCapacity(meetingRoomDO.getCapacity());
        meetingRoomApplicantDO.setMeetingRoomName(meetingRoomDO.getName());
        meetingRoomApplicantDO.setMeetingRoomStatus(meetingRoomDO.getStatus());

        logger.info("开始插入会议室申请信息数据，参数:{}", JSON.toJSONString(meetingRoomApplicantDO));
        Long count=meetingRoomApplicantDAO.insert(meetingRoomApplicantDO);
        logger.info("结束插入会议室申请信息数据，是否插入成功:{}", count>0?true:false);
        if(count<1){
            throw new BusinessException("发布信息插入失败");
        }

        AuditRecordDO auditRecordDO= DOAssemble.assembleAuditRecordDO(meetingRoomApplicantDomain);
        auditRecordDO.setEntityId(meetingRoomApplicantDO.getId());
        logger.info("开始插入会议室申请审核信息数据，参数:{}", JSON.toJSONString(auditRecordDO));
        Long auditRecordount=auditRecordDAO.insert(auditRecordDO);
        logger.info("结束插入会议室申请审核信息数据，是否插入成功:{}", auditRecordount>0?true:false);
        if(auditRecordount<1){
            throw new BusinessException("会议室申请审核信息插入失败");
        }
    }

    @Override
    public List<MeetingRoomDO> queryMeetingRoomList(MeetingRoomDomain domain) {
        Map<String,Object> paramMap= MapUtil.params2Map(
                new Object[]{ "status", domain.getStatus()},
                new Object[]{ "name", StringUtil.trimNull(domain.getName())});
        logger.info("开始查询会议室数据总数，参数:{}", JSON.toJSONString(paramMap));
        int count  =meetingRoomDAO.queryMeetingRoomListCount(paramMap);
        logger.info("开始查询会议室数据总数，返回总数:{}", count);


        if(0==count){
            return new ArrayList();
        }

        QueryPage queryPage = domain.getQueryPage();
        queryPage.setTotalItem(count);
        paramMap.put("queryPage", queryPage);
        if (queryPage.getEndRow() == 0) {
            queryPage.setStartRow(queryPage.getPageFristItem());
            queryPage.setEndRow(queryPage.getPageLastItem());
        }

        domain.setQueryPage(queryPage);
        logger.info("开始查询会议室列表数据，参数:{}", JSON.toJSONString(paramMap));
        List<MeetingRoomDO>  meetingRoomList =meetingRoomDAO.queryMeetingRoomList(paramMap);
        logger.info("结束查询会议室列表数据，查询结果:{}", JSON.toJSONString(meetingRoomList));
        return meetingRoomList;
    }

    @Override
    public List<MeetingRoomApplicantDO> queryMeetingRoomApplyList(
            MeetingRoomApplicantDomain domain) {
        Map<String,Object> paramMap= MapUtil.params2Map(new Object[]{ "applicantId", StringUtil.trimNull(domain.getApplicantId())},
                new Object[]{ "applicantState", domain.getApplicantState()});
        logger.info("开始查询会议室申请数据总数，参数:{}", JSON.toJSONString(paramMap));
        int count  =meetingRoomApplicantDAO.queryMeetingRoomApplyListCount(paramMap);
        logger.info("结束查询会议室申请数据总数，返回总数:{}", count);


        if(0==count){
            return new ArrayList();
        }

        QueryPage queryPage = domain.getQueryPage();
        queryPage.setTotalItem(count);
        paramMap.put("queryPage", queryPage);
        if (queryPage.getEndRow() == 0) {
            queryPage.setStartRow(queryPage.getPageFristItem());
            queryPage.setEndRow(queryPage.getPageLastItem());
        }

        domain.setQueryPage(queryPage);
        logger.info("开始查询会议室申请列表数据，参数:{}", JSON.toJSONString(paramMap));
        List<MeetingRoomApplicantDO>  meetingRoomDOList =meetingRoomApplicantDAO.queryMeetingRoomApplyList(paramMap);
        logger.info("结束查询会议室申请列表数据，查询结果:{}", JSON.toJSONString(meetingRoomDOList));
        return meetingRoomDOList;
    }

    @Override
    public void cancelMeetingRoomApplicant(MeetingRoomApplicantDomain meetingRoomApplicantDomain) {
        logger.info("开始查询会议室申请数据，参数:{}", JSON.toJSONString(meetingRoomApplicantDomain.getId()));
        MeetingRoomApplicantDO oldMeetingRoomApplicantDO  =meetingRoomApplicantDAO.queryMeetingRoomApplyById(meetingRoomApplicantDomain.getId());
        logger.info("结束查询会议室申请数据，返回总数:{}", oldMeetingRoomApplicantDO);
        Validator.assertNull(oldMeetingRoomApplicantDO,"申请不存在!");
        Validator.assertTrue(MeetingRoomApplicantStatusEnum.APPLY_CANCEL.getCode().equals(oldMeetingRoomApplicantDO.getApplicantState()),"该申请已经是取消状态，请勿重复操作!");
        Validator.assertTrue(MeetingRoomApplicantStatusEnum.USE_FINISHED.getCode().equals(oldMeetingRoomApplicantDO.getApplicantState()),"该会议室申请已经使用结束!");


        MeetingRoomApplicantDO updateMeetingRoomApplicantDO =new MeetingRoomApplicantDO();
        updateMeetingRoomApplicantDO.setId(meetingRoomApplicantDomain.getId());
        updateMeetingRoomApplicantDO.setApplicantState(MeetingRoomApplicantStatusEnum.APPLY_CANCEL.getCode());
        updateMeetingRoomApplicantDO.setRemark(meetingRoomApplicantDomain.getRemark());
        logger.info("开始更新会议室申请数据，参数:{}", JSON.toJSONString(updateMeetingRoomApplicantDO));
        int updateCount=meetingRoomApplicantDAO.cancelMeetingRoomApplicant(updateMeetingRoomApplicantDO);
        logger.info("结束更新会议室申请数据，更新个数:{}", updateCount);

    }

    @Override
    public List<CCOccupationDO> queryOccupationMeetingRoomList(
            MeetingRoomDomain meetingRoomDomain) {
        return null;
    }
}
