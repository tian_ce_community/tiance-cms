package com.tiance.domainservice.service;

import com.tiance.dal.dataobject.AuditRecordDO;
import com.tiance.domainservice.domain.AuditRecordDomain;

import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/31
 *
 * Description：
 *
 * Modification History:
 *
 */
public interface AuditService {

    List<AuditRecordDO> queryAuditList(AuditRecordDomain auditRecordDomain);

    void audit(AuditRecordDomain auditRecordDomain);

    AuditRecordDO queryAuditDetail(AuditRecordDomain auditRecordDomain);
}
