package com.tiance.domainservice.service;

import com.tiance.dal.dataobject.IntegrationHistoryDO;
import com.tiance.dal.dataobject.SignInDO;
import com.tiance.domainservice.domain.IntegrationDomain;
import com.tiance.domainservice.domain.SignInDomain;

import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/31
 *
 * Description：
 *
 * Modification History:
 *
 */
public interface IntegrationService {

    List<IntegrationHistoryDO> queryIntegrationHistory(IntegrationDomain integrationDomain);

    void addIntegration(boolean getLock,IntegrationDomain integrationDomain);
}
