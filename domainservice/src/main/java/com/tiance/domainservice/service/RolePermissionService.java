package com.tiance.domainservice.service;

import java.util.Set;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/31
 *
 * Description：
 *
 * Modification History:
 *
 */
public interface RolePermissionService {
    Set<String> queryRolePermissionSetByRoleId(Long roleId);
}
