package com.tiance.domainservice.service.impl;

import com.alibaba.fastjson.JSON;
import com.tiance.dal.dao.ActivityDAO;
import com.tiance.dal.dao.IntegrationRuleDAO;
import com.tiance.dal.dataobject.IntegrationRuleDO;
import com.tiance.dal.dataobject.RoleDO;
import com.tiance.domainservice.domain.IntegrationRuleDomain;
import com.tiance.domainservice.service.IntegrationRuleService;
import com.tiance.page.QueryPage;
import com.tiance.utils.MapUtil;
import com.tiance.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/14
 *
 * Description：
 *
 * Modification History:
 *
 */
@Service
public class IntegrationRuleServiceImpl implements IntegrationRuleService {
    protected static final Logger logger = LoggerFactory.getLogger(IntegrationRuleServiceImpl.class);

    @Resource
    private IntegrationRuleDAO integrationRuleDAO;

    @Override
    public List<IntegrationRuleDO> queryIntegrationRuleList(IntegrationRuleDomain domain) {

        Map<String,Object> paramMap= MapUtil.params2Map(
                new Object[]{ "operatorType", StringUtil.trimNull(domain.getOperatorType())},
                new Object[]{ "operateType", StringUtil.trimNull(domain.getOperateType())},
                new Object[]{ "belongType", StringUtil.trimNull(domain.getBelongType())},
                new Object[]{ "ruleType", StringUtil.trimNull(domain.getRuleType())});
        logger.info("开始查询规则总数，参数:{}", JSON.toJSONString(paramMap));
        int count  =integrationRuleDAO.queryIntegrationRuleListCount(paramMap);
        logger.info("开始查询规则总数，返回总数:{}", count);


        if(0==count){
            return new ArrayList();
        }

        QueryPage queryPage = domain.getQueryPage();
        queryPage.setTotalItem(count);
        paramMap.put("queryPage", queryPage);
        if (queryPage.getEndRow() == 0) {
            queryPage.setStartRow(queryPage.getPageFristItem());
            queryPage.setEndRow(queryPage.getPageLastItem());
        }

        domain.setQueryPage(queryPage);
        logger.info("开始查询规则列表数据，参数:{}", JSON.toJSONString(paramMap));
        List<IntegrationRuleDO> roleDOList =integrationRuleDAO.queryIntegrationRuleList(paramMap);
        logger.info("结束查询规则数据，查询结果:{}", JSON.toJSONString(roleDOList));
        return roleDOList;
    }
}
