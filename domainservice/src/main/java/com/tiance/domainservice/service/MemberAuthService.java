package com.tiance.domainservice.service;

import com.tiance.dal.dataobject.MemberAuthenticationInfoDO;
import com.tiance.dal.dataobject.MemberDO;
import com.tiance.domainservice.domain.MemberAuthOpinionDomain;
import com.tiance.domainservice.domain.MemberAuthenticationInfoDomain;
import com.tiance.domainservice.domain.MemberDomain;
import com.tiance.vo.MemberAuthenticationInfoVO;

import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/31
 *
 * Description：
 *
 * Modification History:
 *
 */
public interface MemberAuthService {
    List<MemberAuthenticationInfoDO> queryMemberAuthApplyList(MemberAuthenticationInfoDomain memberDomain);

    MemberAuthenticationInfoVO queryMemberAuthApplyDetail(MemberAuthenticationInfoDomain memberDomain) throws Exception;

    void auditMemberAuth(MemberAuthOpinionDomain memberDomain);

    void updateAuthInfo(MemberAuthenticationInfoDomain memberDomain);
}
