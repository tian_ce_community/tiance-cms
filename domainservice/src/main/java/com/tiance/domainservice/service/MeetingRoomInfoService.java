package com.tiance.domainservice.service;

import com.tiance.dal.dataobject.CCOccupationDO;
import com.tiance.dal.dataobject.MeetingRoomApplicantDO;
import com.tiance.dal.dataobject.MeetingRoomDO;
import com.tiance.domainservice.domain.MeetingRoomApplicantDomain;
import com.tiance.domainservice.domain.MeetingRoomDomain;
import com.tiance.domainservice.domain.PublishInfoDomain;

import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/31
 *
 * Description：
 *
 * Modification History:
 *
 */
public interface MeetingRoomInfoService {

    void meetingRoomApplicant(MeetingRoomApplicantDomain meetingRoomApplicantDomain)
            throws Exception;

    List<MeetingRoomDO> queryMeetingRoomList(MeetingRoomDomain meetingRoomDomain);

    List<MeetingRoomApplicantDO> queryMeetingRoomApplyList(MeetingRoomApplicantDomain meetingRoomApplicantDomain);

    void cancelMeetingRoomApplicant(MeetingRoomApplicantDomain meetingRoomApplicantDomain);

    List<CCOccupationDO> queryOccupationMeetingRoomList(MeetingRoomDomain meetingRoomDomain);
}
