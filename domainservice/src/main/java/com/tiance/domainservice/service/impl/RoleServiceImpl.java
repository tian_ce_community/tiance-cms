package com.tiance.domainservice.service.impl;

import com.alibaba.fastjson.JSON;
import com.tiance.dal.dao.ActivityDAO;
import com.tiance.dal.dao.AuditRecordDAO;
import com.tiance.dal.dao.RoleDAO;
import com.tiance.dal.dataobject.ActivityDO;
import com.tiance.dal.dataobject.AuditRecordDO;
import com.tiance.dal.dataobject.RoleDO;
import com.tiance.domainservice.assemble.DOAssemble;
import com.tiance.domainservice.checker.ActivityChecker;
import com.tiance.domainservice.domain.ActivityDomain;
import com.tiance.domainservice.domain.RoleDomain;
import com.tiance.domainservice.service.ActivityService;
import com.tiance.domainservice.service.RoleService;
import com.tiance.enums.ActivityStatusEnum;
import com.tiance.exception.BusinessException;
import com.tiance.page.QueryPage;
import com.tiance.utils.BeanConverterUtil;
import com.tiance.utils.MapUtil;
import com.tiance.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/14
 *
 * Description：
 *
 * Modification History:
 *
 */
@Service
public class RoleServiceImpl implements RoleService {
    protected static final Logger logger = LoggerFactory.getLogger(RoleServiceImpl.class);

    @Resource
    private RoleDAO roleDAO;




    @Override
    public List<RoleDO> queryRoleList(RoleDomain domain) {

        Map<String,Object> paramMap= MapUtil.params2Map(
                new Object[]{ "roleName", StringUtil.trimNull(domain.getRoleName())},
                new Object[]{ "roleCode", StringUtil.trimNull(domain.getRoleCode())});
        logger.info("开始查询用户角色总数，参数:{}", JSON.toJSONString(paramMap));
        int count  =roleDAO.queryRoleListCount(paramMap);
        logger.info("开始查询用户角色总数，返回总数:{}", count);


        if(0==count){
            return new ArrayList();
        }

        QueryPage queryPage = domain.getQueryPage();
        queryPage.setTotalItem(count);
        paramMap.put("queryPage", queryPage);
        if (queryPage.getEndRow() == 0) {
            queryPage.setStartRow(queryPage.getPageFristItem());
            queryPage.setEndRow(queryPage.getPageLastItem());
        }

        domain.setQueryPage(queryPage);
        logger.info("开始查询用户角色列表数据，参数:{}", JSON.toJSONString(paramMap));
        List<RoleDO> roleDOList =roleDAO.queryRoleList(paramMap);
        logger.info("结束查询用户角色数据，查询结果:{}", JSON.toJSONString(roleDOList));
        return roleDOList;
    }

    @Override
    public RoleDO queryRoleDetail(RoleDomain domain) {
        Map<String,Object> paramMap= MapUtil.params2Map(
                new Object[]{ "id", StringUtil.trimNull(domain.getId())},
                new Object[]{ "roleName", StringUtil.trimNull(domain.getRoleName())},
                new Object[]{ "roleCode", StringUtil.trimNull(domain.getRoleCode())});
        logger.info("开始查询用户角色详情，参数:{}", JSON.toJSONString(paramMap));
        RoleDO roleDO  =roleDAO.queryRoleDetail(paramMap);
        logger.info("开始查询用户角色详情，返回总数:{}", JSON.toJSONString(roleDO));
        return roleDO;

    }

    @Override
    public RoleDO edit (RoleDomain roleDomain) throws Exception{
        RoleDO roleDO=new RoleDO();
        BeanConverterUtil.copyProperties(roleDO,roleDomain);
        logger.info("开始更新用户角色详情，参数:{}", JSON.toJSONString(roleDO));
         int count=roleDAO.update(roleDO);
        logger.info("开始更新用户角色详情，更新格式:{}",count);
        return roleDO;
    }
}
