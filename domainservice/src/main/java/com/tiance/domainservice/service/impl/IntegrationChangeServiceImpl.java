package com.tiance.domainservice.service.impl;

import com.alibaba.fastjson.JSON;
import com.tiance.dal.dao.IntegrationChangeDictDAO;
import com.tiance.dal.dao.IntegrationChangeRecordDAO;
import com.tiance.dal.dao.MemberDAO;
import com.tiance.dal.dataobject.IntegrationChangeDictDO;
import com.tiance.dal.dataobject.IntegrationChangeRecordDO;
import com.tiance.dal.dataobject.MemberDO;
import com.tiance.dal.dataobject.RoleDO;
import com.tiance.domainservice.assemble.DOAssemble;
import com.tiance.domainservice.domain.IntegrationChangeDomain;
import com.tiance.domainservice.service.IntegrationChangeService;
import com.tiance.domainservice.utils.MemberUtil;
import com.tiance.enums.IntegrationTypeEnum;
import com.tiance.exception.BusinessException;
import com.tiance.integration.cache.MapCacheUtil;
import com.tiance.page.QueryPage;
import com.tiance.utils.DataUtil;
import com.tiance.utils.MapUtil;
import com.tiance.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/15
 *
 * Description：
 *
 * Modification History:
 *
 */
@Service
public class IntegrationChangeServiceImpl implements IntegrationChangeService {

    /** logger */
    protected static final Logger logger = LoggerFactory.getLogger(IntegrationChangeServiceImpl.class);

    @Resource
    private IntegrationChangeRecordDAO integrationChangeRecordDAO;
    @Resource
    private IntegrationChangeDictDAO   integrationChangeDictDAO;

    @Resource
    private MemberDAO memberDAO;

    @Override
    public void memberIntegrationChange(IntegrationChangeDomain integrationChangeDomain)
            throws Exception {

        Map<String,Object> map= MapUtil.params2Map(new Object[]{ "id", integrationChangeDomain.getMemberId()});
        logger.info("开始查询会员信息，参数:{}", JSON.toJSONString(map));
        MemberDO  oldMemberDO=memberDAO.queryMember(map);
        logger.info("结束查询会员信息，返回数据:{}", JSON.toJSONString(oldMemberDO));
        if(null==oldMemberDO){
            throw new BusinessException("会员["+integrationChangeDomain.getMemberId()+"]不存在!");
        }
        //2.调整会员积分/信用
        //todo 积分信用操作类型还未增加
        MemberDO newMemberDO= MemberUtil.changeIntegration2(oldMemberDO,integrationChangeDomain.getChangeIntegrationValue(),integrationChangeDomain.getChangeCreditValue());
        logger.info("开始更新会员信息，参数:{}", JSON.toJSONString(newMemberDO));
        Long updateCount=memberDAO.updateMember(newMemberDO);
        logger.info("结束更新会员信息，更新条数:{}", updateCount);

        //插入变更历史记录
        IntegrationChangeRecordDO integrationChangeRecordDO= DOAssemble.assembleIntegrationChangeRecordDO(integrationChangeDomain);
        IntegrationChangeDictDO integrationChangeDictDO=integrationChangeDictDAO.queryIntegrationChangeDictById(integrationChangeDomain.getIntegrationChangeDictId());
        integrationChangeRecordDO.setChangeCode(integrationChangeDictDO.getChangeCode());
        integrationChangeRecordDO.setChangeName(integrationChangeDictDO.getChangeName());
        integrationChangeRecordDO.setRealName(oldMemberDO.getRealName());
        integrationChangeRecordDO.setOpenid(oldMemberDO.getOpenid());
        logger.info("开始插入会员积分变更数据，参数:{}", JSON.toJSONString(integrationChangeRecordDO));
        Long count=integrationChangeRecordDAO.insert(integrationChangeRecordDO);
        logger.info("结束插入会员积分变更数据，是否插入成功:{}", count>0?true:false);
        if(count<1){
            throw new BusinessException("插入失败");
        }
    }

    @Override
    public List<IntegrationChangeRecordDO> queryIntegrationChangeRecordList(
            IntegrationChangeDomain domain) {

        Map<String,Object> paramMap= MapUtil.params2Map(
                new Object[]{ "memberId", StringUtil.trimNull(domain.getMemberId())},
                new Object[]{ "integrationChangeDictId", StringUtil.trimNull(domain.getIntegrationChangeDictId())});
        logger.info("开始查询积分信用变更总数，参数:{}", JSON.toJSONString(paramMap));
        int count  =integrationChangeRecordDAO.queryIntegrationChangeRecordListCount(paramMap);
        logger.info("开始查询积分信用总数，返回总数:{}", count);


        if(0==count){
            return new ArrayList();
        }

        QueryPage queryPage = domain.getQueryPage();
        queryPage.setTotalItem(count);
        paramMap.put("queryPage", queryPage);
        if (queryPage.getEndRow() == 0) {
            queryPage.setStartRow(queryPage.getPageFristItem());
            queryPage.setEndRow(queryPage.getPageLastItem());
        }

        domain.setQueryPage(queryPage);
        logger.info("开始查询积分信用列表数据，参数:{}", JSON.toJSONString(paramMap));
        List<IntegrationChangeRecordDO> integrationChangeRecordDOList =integrationChangeRecordDAO.queryIntegrationChangeRecordList(paramMap);
        logger.info("结束查询积分信用数据，查询结果:{}", JSON.toJSONString(integrationChangeRecordDOList));
        return integrationChangeRecordDOList;
    }
}
