package com.tiance.domainservice.service.impl;

import com.alibaba.fastjson.JSON;
import com.tiance.dal.dao.ActivityDAO;
import com.tiance.dal.dao.AuditRecordDAO;
import com.tiance.dal.dao.UniqueCodeDAO;
import com.tiance.dal.dataobject.ActivityDO;
import com.tiance.dal.dataobject.AuditRecordDO;
import com.tiance.dal.dataobject.MemberAuthenticationInfoDO;
import com.tiance.dal.dataobject.UniqueCodeDO;
import com.tiance.domainservice.assemble.DOAssemble;
import com.tiance.domainservice.checker.ActivityChecker;
import com.tiance.domainservice.domain.ActivityDomain;
import com.tiance.domainservice.service.ActivityService;
import com.tiance.enums.ActivityStatusEnum;
import com.tiance.enums.EntityTypeEnum;
import com.tiance.exception.BusinessException;
import com.tiance.page.QueryPage;
import com.tiance.utils.MapUtil;
import com.tiance.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/14
 *
 * Description：
 *
 * Modification History:
 *
 */
@Service
public class ActivityServiceImpl implements ActivityService {
    protected static final Logger logger = LoggerFactory.getLogger(ActivityServiceImpl.class);

    @Resource
    private ActivityDAO activityDAO;


    @Resource
    private AuditRecordDAO auditRecordDAO;



    @Resource
    private UniqueCodeDAO uniqueCodeDAO;

    @Override
    public ActivityDO addActivity(ActivityDomain activityDomain) throws Exception {




        ActivityDO activityDO= DOAssemble.assembleActivityDO(activityDomain);
        logger.info("开始插入活动数据，参数:{}", JSON.toJSONString(activityDO));
        Long count=activityDAO.insert(activityDO);
        logger.info("结束插入活动数据，是否插入成功:{}", count>0?true:false);
        if(count<1){
            throw new BusinessException("插入失败");
        }

        UniqueCodeDO uniqueCodeDO=new UniqueCodeDO();
        uniqueCodeDO.setEntityId(activityDO.getId());
        uniqueCodeDO.setEntityType(EntityTypeEnum.ACTIVITY.getCode());
        logger.info("开始插入活动实体数据，参数:{}", JSON.toJSONString(uniqueCodeDO));
        Long uniqueCount=uniqueCodeDAO.insert(uniqueCodeDO);
        logger.info("结束插入活动实体数据，是否插入成功:{}", uniqueCount>0?true:false);
        if(uniqueCount<1){
            throw new BusinessException("插入失败");
        }

        AuditRecordDO auditRecordDO= DOAssemble.assembleAuditRecordDO(activityDomain);
        auditRecordDO.setEntityId(activityDO.getId());
        auditRecordDO.setEntityType(EntityTypeEnum.ACTIVITY.getCode());
        logger.info("开始插入活动审核信息数据，参数:{}", JSON.toJSONString(auditRecordDO));
        Long auditRecordount=auditRecordDAO.insert(auditRecordDO);
        logger.info("结束插入活动审核信息数据，是否插入成功:{}", auditRecordount>0?true:false);
        if(auditRecordount<1){
            throw new BusinessException("活动审核信息插入失败");
        }
        return activityDO;
    }

    @Override
    public List<ActivityDO> queryApplyActivity(ActivityDomain domain) {

        Map<String,Object> paramMap= MapUtil.params2Map(new Object[]{ "applicantId", StringUtil.trimNull(domain.getApplicantId())},
                new Object[]{ "statusScope", domain.getStatusScope()},
                new Object[]{ "openid", StringUtil.trimNull(domain.getOpenid())});
        logger.info("开始查询活动数据总数，参数:{}", JSON.toJSONString(paramMap));
        int count  =activityDAO.queryApplyActivityCount(paramMap);
        logger.info("结束查询活动数据总数，返回总数:{}", count);


        if(0==count){
            return new ArrayList();
        }

        QueryPage queryPage = domain.getQueryPage();
        queryPage.setTotalItem(count);
        paramMap.put("queryPage", queryPage);
        if (queryPage.getEndRow() == 0) {
            queryPage.setStartRow(queryPage.getPageFristItem());
            queryPage.setEndRow(queryPage.getPageLastItem());
        }

        domain.setQueryPage(queryPage);
        logger.info("开始查询活动数据列表数据，参数:{}", JSON.toJSONString(paramMap));
        List<ActivityDO> activityDOList =activityDAO.queryApplyActivityList(paramMap);
        logger.info("结束查询活动数据数据，查询结果:{}", JSON.toJSONString(activityDOList));
        return activityDOList;
    }

    @Override
    public void cancel(ActivityDomain activityDomain) {
        logger.info("开始查询活动数据，参数:{}", JSON.toJSONString(activityDomain.getId()));
        ActivityDO oldActivityDO=activityDAO.queryByActivityId(activityDomain.getId());
        logger.info("结束查询活动数据，查询结果:{}", JSON.toJSONString(oldActivityDO));
        ActivityChecker.checkCancelActivity(oldActivityDO);


        ActivityDO newActivityDO= DOAssemble.assembleUpdateActivityDO(activityDomain);
        logger.info("开始更新活动数据，参数:{}", JSON.toJSONString(newActivityDO));
        Long count=activityDAO.update(newActivityDO);
        logger.info("结束更新活动数据，是否更新成功:{}", count>0?true:false);
        if(count<1){
            throw new BusinessException("更新失败");
        }
    }

    @Override
    public void finish(ActivityDomain activityDomain) {
        logger.info("开始查询活动数据，参数:{}", JSON.toJSONString(activityDomain.getId()));
        ActivityDO oldActivityDO=activityDAO.queryByActivityId(activityDomain.getId());
        logger.info("结束查询活动数据，查询结果:{}", JSON.toJSONString(oldActivityDO));
        ActivityChecker.checkFinishActivity(oldActivityDO);


        ActivityDO newActivityDO= DOAssemble.assembleUpdateActivityDO(activityDomain);
        newActivityDO.setActivityStatus(ActivityStatusEnum.ACTIVITY_FINISHED.getCode());
        logger.info("开始更新活动数据，参数:{}", JSON.toJSONString(newActivityDO));
        Long count=activityDAO.update(newActivityDO);
        logger.info("结束更新活动数据，是否更新成功:{}", count>0?true:false);
        if(count<1){
            throw new BusinessException("更新失败");
        }
    }

    @Override
    public List<ActivityDO> querySignUpActivityList(ActivityDomain domain) {

        Map<String,Object> paramMap= MapUtil.params2Map(new Object[]{ "applicantId", StringUtil.trimNull(domain.getApplicantId())});
        logger.info("开始查询报名活动数据总数，参数:{}", JSON.toJSONString(paramMap));
        int count  =activityDAO.querySignUpActivityListCount(paramMap);
        logger.info("结束查询报名活动数据总数，返回总数:{}", count);


        if(0==count){
            return new ArrayList();
        }

        QueryPage queryPage = domain.getQueryPage();
        queryPage.setTotalItem(count);
        paramMap.put("queryPage", queryPage);
        if (queryPage.getEndRow() == 0) {
            queryPage.setStartRow(queryPage.getPageFristItem());
            queryPage.setEndRow(queryPage.getPageLastItem());
        }

        domain.setQueryPage(queryPage);
        logger.info("开始查询报名活动数据列表数据，参数:{}", JSON.toJSONString(paramMap));
        List<ActivityDO> activityDOList =activityDAO.querySignUpActivityList(paramMap);
        logger.info("结束查询报名活动数据数据，查询结果:{}", JSON.toJSONString(activityDOList));
        return activityDOList;
    }

    @Override
    public ActivityDO queryActivityDetailById(ActivityDomain activityDomain) {
        logger.info("开始查询活动详情，参数:{}", JSON.toJSONString(activityDomain.getId()));
        ActivityDO activityDO  =activityDAO.queryByActivityId(activityDomain.getId());
        logger.info("结束查询活动详情，返回总数:{}", JSON.toJSONString(activityDO));
        return activityDO;

    }
}
