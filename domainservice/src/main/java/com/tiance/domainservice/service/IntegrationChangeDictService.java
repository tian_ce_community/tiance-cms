package com.tiance.domainservice.service;

import com.tiance.dal.dataobject.IntegrationChangeDictDO;
import com.tiance.domainservice.domain.IntegrationChangeDictDomain;

import java.util.List;
import java.util.Set;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/31
 *
 * Description：
 *
 * Modification History:
 *
 */
public interface IntegrationChangeDictService {
    List<IntegrationChangeDictDO> queryIntegrationChangeDictList(IntegrationChangeDictDomain integrationChangeDictDomain);
}
