package com.tiance.domainservice.service.impl;

import com.alibaba.fastjson.JSON;
import com.tiance.dal.dao.AuditRecordDAO;
import com.tiance.dal.dao.ConventionCenterDAO;
import com.tiance.dal.dao.MeetingRoomApplicantDAO;
import com.tiance.dal.dao.MeetingRoomDAO;
import com.tiance.dal.dataobject.AuditRecordDO;
import com.tiance.dal.dataobject.ConventionCenterDO;
import com.tiance.dal.dataobject.MeetingRoomApplicantDO;
import com.tiance.dal.dataobject.MeetingRoomDO;
import com.tiance.domainservice.assemble.DOAssemble;
import com.tiance.domainservice.domain.CCOccupationDomain;
import com.tiance.domainservice.domain.ConventionCenterDomain;
import com.tiance.domainservice.domain.MeetingRoomApplicantDomain;
import com.tiance.domainservice.service.ConventionCenterService;
import com.tiance.domainservice.service.MeetingRoomInfoService;
import com.tiance.enums.UseStatusEnum;
import com.tiance.exception.BusinessException;
import com.tiance.page.QueryPage;
import com.tiance.utils.MapUtil;
import com.tiance.utils.StringUtil;
import com.tiance.utils.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/3/1
 *
 * Description：
 *
 * Modification History:
 *
 */
@Service
public class ConventionCenterServiceImpl implements ConventionCenterService {
    protected static final Logger logger = LoggerFactory.getLogger(ConventionCenterServiceImpl.class);


    @Autowired
    private ConventionCenterDAO conventionCenterDAO;

    @Override
    public List<ConventionCenterDO> queryMonthConventionCenterStatusList(
            ConventionCenterDomain conventionCenterDomain) {
        return null;
    }

    @Override
    public List<ConventionCenterDO> queryConventionCenterList(ConventionCenterDomain domain) {
        Map<String,Object> paramMap= MapUtil.params2Map(
                new Object[]{ "status", domain.getStatus()},
                new Object[]{ "name", StringUtil.trimNull(domain.getName())});
        logger.info("开始查询会议中心数据总数，参数:{}", JSON.toJSONString(paramMap));
        int count  =conventionCenterDAO.queryConventionCenterListCount(paramMap);
        logger.info("开始查询会议中心数据总数，返回总数:{}", count);


        if(0==count){
            return new ArrayList();
        }

        QueryPage queryPage = domain.getQueryPage();
        queryPage.setTotalItem(count);
        paramMap.put("queryPage", queryPage);
        if (queryPage.getEndRow() == 0) {
            queryPage.setStartRow(queryPage.getPageFristItem());
            queryPage.setEndRow(queryPage.getPageLastItem());
        }

        domain.setQueryPage(queryPage);
        logger.info("开始查询会议中心列表数据，参数:{}", JSON.toJSONString(paramMap));
        List<ConventionCenterDO>  conventionCenterDOList =conventionCenterDAO.queryConventionCenterList(paramMap);
        logger.info("结束查询会议中心列表数据，查询结果:{}", JSON.toJSONString(conventionCenterDOList));
        return conventionCenterDOList;
    }

    @Override
    public List<ConventionCenterDO> queryUnoccupiedConventionCenterList(ConventionCenterDomain conventionCenterDomain) {
        logger.info("开始查询会议中心列表数据");
        List<ConventionCenterDO>  conventionCenterDOList=new ArrayList<>();
        conventionCenterDOList =conventionCenterDAO.queryUnoccupiedConventionCenterList();
        logger.info("结束查询会议中心列表数据，查询结果:{}", JSON.toJSONString(conventionCenterDOList));
        return conventionCenterDOList;
    }

    @Override
    public void occupation(CCOccupationDomain ccOccupationDomain) {


    }
}
