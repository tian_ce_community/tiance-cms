package com.tiance.domainservice.service.impl;

import com.alibaba.fastjson.JSON;
import com.tiance.constants.CommonConstants;
import com.tiance.dal.dao.*;
import com.tiance.dal.dataobject.*;
import com.tiance.domainservice.domain.AuditRecordDomain;
import com.tiance.domainservice.service.AuditService;
import com.tiance.enums.ActivityStatusEnum;
import com.tiance.enums.AuditStatusEnum;
import com.tiance.enums.DutyTypeEnum;
import com.tiance.enums.EntityTypeEnum;
import com.tiance.page.QueryPage;
import com.tiance.utils.MapUtil;
import com.tiance.utils.StringUtil;
import com.tiance.utils.Validator;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/3/3
 *
 * Description：
 *
 * Modification History:
 *
 */
@Service
public class AuditServiceImpl implements AuditService {

    protected static final Logger logger = LoggerFactory.getLogger(AuditServiceImpl.class);

    @Resource
    private AuditRecordDAO auditRecordDAO;

    @Resource
    private ActivityDAO  activityDAO;


    @Resource
    private PublishInfoDAO publishInfoDAO;

    @Resource
    private MeetingRoomApplicantDAO meetingRoomApplicantDAO;



    @Resource
    private TaskDAO taskDAO;

    @Resource
    private TcDutyDao tcDutyDao;
    @Override
    public List<AuditRecordDO> queryAuditList(AuditRecordDomain domain) {

        Map<String,Object> paramMap= MapUtil.params2Map(new Object[]{ "memberId", StringUtil.trimNull(domain.getMemberId())},
                new Object[]{ "entityType", domain.getEntityType()},
                new Object[]{ "auditState", domain.getAuditState()},
                new Object[]{ "statusScope", domain.getStatusScope()},
                new Object[]{ "openid", StringUtil.trimNull(domain.getOpenid())});
        logger.info("开始查询待审核数据总数，参数:{}", JSON.toJSONString(paramMap));
        int count  =auditRecordDAO.queryAuditListCount(paramMap);
        logger.info("开始查询待审核数据总数，返回总数:{}", count);


        if(0==count){
            return new ArrayList();
        }

        QueryPage queryPage = domain.getQueryPage();
        queryPage.setTotalItem(count);
        paramMap.put("queryPage", queryPage);
        if (queryPage.getEndRow() == 0) {
            queryPage.setStartRow(queryPage.getPageFristItem());
            queryPage.setEndRow(queryPage.getPageLastItem());
        }

        domain.setQueryPage(queryPage);
        logger.info("开始查询待审核数据列表数据，参数:{}", JSON.toJSONString(paramMap));
        List<AuditRecordDO> auditRecordDOList =auditRecordDAO.queryAuditList(paramMap);
        logger.info("结束查询待审核列表数据，查询结果:{}", JSON.toJSONString(auditRecordDOList));
        return auditRecordDOList;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void audit(AuditRecordDomain auditRecordDomain) {
        Map<String,Object> paramMap= MapUtil.params2Map(
                new Object[]{ "entityType", auditRecordDomain.getEntityType()},
                new Object[]{ "entityId", auditRecordDomain.getEntityId()});
        logger.info("开始查询待审核详情，参数 :{}", auditRecordDomain.getId());
        AuditRecordDO auditRecordDO=auditRecordDAO.queryAuditDetail(paramMap);
        logger.info("结束查询待审详情，参数 :{}", JSON.toJSONString(auditRecordDO));
        Validator.assertNull(auditRecordDO,"未查询到审核记录 !");
        Validator.assertNull(auditRecordDO.getId(),"未查询到审核记录 !");
        Validator.assertTrue(AuditStatusEnum.AUDIT_PASS.getCode().equals(auditRecordDO.getAuditState()),"该申请已经是审核成功状态，请勿重复操作!");


        AuditRecordDO updateAuditRecordDO=new AuditRecordDO();
        updateAuditRecordDO.setId(auditRecordDO.getId());
        updateAuditRecordDO.setAuditState(auditRecordDomain.getAuditState());
        updateAuditRecordDO.setApprovalOpinion(auditRecordDomain.getApprovalOpinion());

        Session session = SecurityUtils.getSubject().getSession();
        UserDO userDO=(UserDO)session.getAttribute(CommonConstants.USER);
        updateAuditRecordDO.setApprovalId(userDO.getId());

        logger.info("开始更新待审核数据，参数:{}", JSON.toJSONString(updateAuditRecordDO));
        long auditCount=auditRecordDAO.auditRecord(updateAuditRecordDO);
        logger.info("结束更新待审核数据，auditCount:{}", auditCount);

        //开始更新审核实体
        if(EntityTypeEnum.ACTIVITY.getCode().equals(auditRecordDO.getEntityType())){
            ActivityDO activityDO=new ActivityDO();
            activityDO.setId(auditRecordDO.getEntityId());
            activityDO.setActivityStatus(auditRecordDomain.getAuditState());
            activityDO.setApprovalId(userDO.getId());
            activityDO.setRemark(auditRecordDomain.getApprovalOpinion());
            logger.info("开始更新活动数据，参数:{}", JSON.toJSONString(activityDO));
            long activityCount= activityDAO.audit(activityDO);
            logger.info("结束更新活动数据，activityCount:{}", activityCount);
        }else if(EntityTypeEnum.PUBLISH_INFO.getCode().equals(auditRecordDO.getEntityType())){
            PublishInfoDO publishInfoDO=new PublishInfoDO();
            publishInfoDO.setId(auditRecordDO.getEntityId());
            publishInfoDO.setInfoStatus(auditRecordDomain.getAuditState());
            publishInfoDO.setApprovalId(userDO.getId());
            publishInfoDO.setRemark(auditRecordDomain.getApprovalOpinion());
            logger.info("开始更新发布信息数据，参数:{}", JSON.toJSONString(publishInfoDO));
            long publishCount= publishInfoDAO.audit(publishInfoDO);
            logger.info("结束更新发布信息数据，publishCount:{}", publishCount);
        }else if(EntityTypeEnum.MEETING_ROOM_APPLICANT.getCode().equals(auditRecordDO.getEntityType())){
            MeetingRoomApplicantDO meetingRoomDO=new MeetingRoomApplicantDO();
            meetingRoomDO.setId(auditRecordDO.getEntityId());
            meetingRoomDO.setApplicantState(auditRecordDomain.getAuditState());
            meetingRoomDO.setApprovalId(userDO.getId());
            meetingRoomDO.setRemark(auditRecordDomain.getApprovalOpinion());
            logger.info("开始更新会议室申请数据，参数:{}", JSON.toJSONString(meetingRoomDO));
            long meetingCount= meetingRoomApplicantDAO.audit(meetingRoomDO);
            logger.info("结束更新会议室申请数据，publishCount:{}", meetingCount);
        }else if(EntityTypeEnum.DUTY_APPLICANT.getCode().equals(auditRecordDO.getEntityType())){
            TcDutyDO tcDutyDO=new TcDutyDO();
            tcDutyDO.setId(auditRecordDO.getEntityId());
            tcDutyDO.setApplicantState(auditRecordDomain.getAuditState());
            tcDutyDO.setApprovalId(userDO.getId());
            tcDutyDO.setRemark(auditRecordDomain.getApprovalOpinion());
            logger.info("开始更新值班申请数据，参数:{}", JSON.toJSONString(tcDutyDO));
            long meetingCount= tcDutyDao.audit(tcDutyDO);
            logger.info("结束更新值班申请数据，publishCount:{}", meetingCount);
        }else if(EntityTypeEnum.TASK.getCode().equals(auditRecordDO.getEntityType())){
            TaskDO taskDO=new TaskDO();
            taskDO.setId(auditRecordDO.getEntityId());
            taskDO.setTaskStatus(auditRecordDomain.getAuditState());
            taskDO.setApprovalId(userDO.getId());
            taskDO.setRemark(auditRecordDomain.getApprovalOpinion());
            logger.info("开始更新任务申请数据，参数:{}", JSON.toJSONString(taskDO));
            long meetingCount= taskDAO.audit(taskDO);
            logger.info("结束更新任务申请数据，publishCount:{}", meetingCount);
        }


    }

    @Override
    public AuditRecordDO queryAuditDetail(AuditRecordDomain domain) {
        Map<String,Object> paramMap= MapUtil.params2Map(
                new Object[]{ "entityType", domain.getEntityType()},
                new Object[]{ "entityId", domain.getEntityId()});
        logger.info("开始查询待审核详情，参数:{}", JSON.toJSONString(paramMap));
        AuditRecordDO auditRecordDO  =auditRecordDAO.queryAuditDetail(paramMap);
        logger.info("开始查询待审核详情，返回数据:{}", auditRecordDO);

        return auditRecordDO;
    }

}
