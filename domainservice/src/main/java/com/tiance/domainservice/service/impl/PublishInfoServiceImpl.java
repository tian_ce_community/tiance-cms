package com.tiance.domainservice.service.impl;

import com.alibaba.fastjson.JSON;
import com.tiance.dal.dao.ActivityDAO;
import com.tiance.dal.dao.AuditRecordDAO;
import com.tiance.dal.dao.PublishInfoDAO;
import com.tiance.dal.dao.UniqueCodeDAO;
import com.tiance.dal.dataobject.ActivityDO;
import com.tiance.dal.dataobject.AuditRecordDO;
import com.tiance.dal.dataobject.PublishInfoDO;
import com.tiance.dal.dataobject.UniqueCodeDO;
import com.tiance.domainservice.assemble.DOAssemble;
import com.tiance.domainservice.domain.PublishInfoDomain;
import com.tiance.domainservice.service.PublishInfoService;
import com.tiance.enums.EntityTypeEnum;
import com.tiance.enums.PublishInfoStatusEnum;
import com.tiance.exception.BusinessException;
import com.tiance.page.QueryPage;
import com.tiance.utils.MapUtil;
import com.tiance.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/16
 *
 * Description：
 *
 * Modification History:
 *
 */
@Service
public class PublishInfoServiceImpl implements PublishInfoService {
    protected static final Logger logger = LoggerFactory.getLogger(ActivityServiceImpl.class);

    @Resource
    private PublishInfoDAO publishInfoDAO;

    @Resource
    private AuditRecordDAO auditRecordDAO;

    @Resource
    private UniqueCodeDAO uniqueCodeDAO;
    @Override
    public void addPublishInfo(PublishInfoDomain publishInfoDomain) throws Exception {
        PublishInfoDO publishInfoDO= DOAssemble.assemblePublishInfoDO(publishInfoDomain);
        logger.info("开始插入发布信息数据，参数:{}", JSON.toJSONString(publishInfoDO));
        Long count=publishInfoDAO.insert(publishInfoDO);
        logger.info("结束插入发布信息数据，是否插入成功:{}", count>0?true:false);
        if(count<1){
            throw new BusinessException("发布信息插入失败");
        }


        UniqueCodeDO uniqueCodeDO=new UniqueCodeDO();
        uniqueCodeDO.setEntityId(publishInfoDO.getId());
        uniqueCodeDO.setEntityType(EntityTypeEnum.PUBLISH_INFO.getCode());
        logger.info("开始插入活动实体数据，参数:{}", JSON.toJSONString(uniqueCodeDO));
        Long uniqueCount=uniqueCodeDAO.insert(uniqueCodeDO);
        logger.info("结束插入活动实体数据，是否插入成功:{}", uniqueCount>0?true:false);
        if(uniqueCount<1){
            throw new BusinessException("插入失败");
        }


        AuditRecordDO auditRecordDO= DOAssemble.assembleAuditRecordDO(publishInfoDomain);
        auditRecordDO.setEntityId(publishInfoDO.getId());
        auditRecordDO.setEntityType(EntityTypeEnum.PUBLISH_INFO.getCode());
        logger.info("开始插入发布信息审核数据，参数:{}", JSON.toJSONString(auditRecordDO));
        Long auditRecordount=auditRecordDAO.insert(auditRecordDO);
        logger.info("结束插入发布信息审核数据，是否插入成功:{}", auditRecordount>0?true:false);
        if(auditRecordount<1){
            throw new BusinessException("发布信息审核插入失败");
        }
    }

    @Override
    public void cancel(PublishInfoDomain publishInfoDomain) throws Exception {
        logger.info("开始查询发布信息数据，参数:{}", publishInfoDomain.getId());
        PublishInfoDO oldPublishInfoDO=publishInfoDAO.queryPublishInfoById(publishInfoDomain.getId());
        logger.info("结束查询发布信息数据:{}", JSON.toJSONString(oldPublishInfoDO));


        PublishInfoDO updatePublishInfoDO=new PublishInfoDO();
        updatePublishInfoDO.setId(publishInfoDomain.getId());
        updatePublishInfoDO.setInfoStatus(publishInfoDomain.getInfoStatus());
        updatePublishInfoDO.setRemark(publishInfoDomain.getRemark());
        logger.info("开始取消发布信息数据，参数:{}", JSON.toJSONString(updatePublishInfoDO));
        Long count=publishInfoDAO.cancel(updatePublishInfoDO);
        logger.info("结束取消发布信息数据，是否取消成功:{}", count>0?true:false);
        if(count<1){
            throw new BusinessException("取消失败");
        }
    }

    @Override
    public List<PublishInfoDO> applyPublishInfoList(PublishInfoDomain domain) {

        Map<String,Object> paramMap= MapUtil.params2Map(new Object[]{ "applicantId", StringUtil.trimNull(domain.getApplicantId())},
                new Object[]{ "statusScope", domain.getStatusScope()},
                new Object[]{ "openid", StringUtil.trimNull(domain.getOpenid())});
        logger.info("开始查询发布信息数据总数，参数:{}", JSON.toJSONString(paramMap));
        int count  =publishInfoDAO.applyPublishInfoListCount(paramMap);
        logger.info("开始查询发布信息数据总数，返回总数:{}", count);


        if(0==count){
            return new ArrayList();
        }

        QueryPage queryPage = domain.getQueryPage();
        queryPage.setTotalItem(count);
        paramMap.put("queryPage", queryPage);
        if (queryPage.getEndRow() == 0) {
            queryPage.setStartRow(queryPage.getPageFristItem());
            queryPage.setEndRow(queryPage.getPageLastItem());
        }

        domain.setQueryPage(queryPage);
        logger.info("开始查询发布信息数据列表数据，参数:{}", JSON.toJSONString(paramMap));
        List<PublishInfoDO> publishInfoDOList =publishInfoDAO.applyPublishInfoList(paramMap);
        logger.info("结束查询发布信息列表数据，查询结果:{}", JSON.toJSONString(publishInfoDOList));
        return publishInfoDOList;
    }

    @Override
    public PublishInfoDO queryPublishInfoDetailById(PublishInfoDomain publishInfoDomain) {
        logger.info("开始查询发布信息详情，参数:{}", publishInfoDomain.getId());
        PublishInfoDO publishInfoDO =publishInfoDAO.queryPublishInfoById(publishInfoDomain.getId());
        logger.info("结束查询发布信息详情，查询结果:{}", JSON.toJSONString(publishInfoDO));
        return publishInfoDO;
    }

    @Override
    public List<PublishInfoDO> querySignUpPublishedInfoList(PublishInfoDomain domain) {

        Map<String,Object> paramMap= MapUtil.params2Map(new Object[]{ "applicantId", StringUtil.trimNull(domain.getApplicantId())});
        logger.info("开始查询发布信息数据总数，参数:{}", JSON.toJSONString(paramMap));
        int count  =publishInfoDAO.querySignUpPublishedInfoListCount(paramMap);
        logger.info("开始查询发布信息数据总数，返回总数:{}", count);


        if(0==count){
            return new ArrayList();
        }

        QueryPage queryPage = domain.getQueryPage();
        queryPage.setTotalItem(count);
        paramMap.put("queryPage", queryPage);
        if (queryPage.getEndRow() == 0) {
            queryPage.setStartRow(queryPage.getPageFristItem());
            queryPage.setEndRow(queryPage.getPageLastItem());
        }

        domain.setQueryPage(queryPage);
        logger.info("开始查询发布信息数据列表数据，参数:{}", JSON.toJSONString(paramMap));
        List<PublishInfoDO> publishInfoDOList =publishInfoDAO.querySignUpPublishedInfoList(paramMap);
        logger.info("结束查询发布信息列表数据，查询结果:{}", JSON.toJSONString(publishInfoDOList));
        return publishInfoDOList;
    }
}
