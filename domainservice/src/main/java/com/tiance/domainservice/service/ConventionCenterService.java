package com.tiance.domainservice.service;

import com.tiance.dal.dataobject.ConventionCenterDO;
import com.tiance.domainservice.domain.CCOccupationDomain;
import com.tiance.domainservice.domain.ConventionCenterDomain;

import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/31
 *
 * Description：
 *
 * Modification History:
 *
 */
public interface ConventionCenterService {

    List<ConventionCenterDO> queryConventionCenterList(ConventionCenterDomain conventionCenterDomain);

    List<ConventionCenterDO> queryUnoccupiedConventionCenterList(ConventionCenterDomain conventionCenterDomain);

    void occupation(CCOccupationDomain ccOccupationDomain);

    List<ConventionCenterDO> queryMonthConventionCenterStatusList(ConventionCenterDomain conventionCenterDomain);
}
