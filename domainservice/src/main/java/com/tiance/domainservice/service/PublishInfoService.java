package com.tiance.domainservice.service;

import com.tiance.dal.dataobject.PublishInfoDO;
import com.tiance.domainservice.domain.PublishInfoDomain;

import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/31
 *
 * Description：
 *
 * Modification History:
 *
 */
public interface PublishInfoService {

    void addPublishInfo(PublishInfoDomain publishInfoDomain) throws Exception;

    void cancel(PublishInfoDomain publishInfoDomain) throws Exception;

    List<PublishInfoDO> applyPublishInfoList(PublishInfoDomain publishInfoDomain);

    PublishInfoDO queryPublishInfoDetailById(PublishInfoDomain publishInfoDomain);

    List<PublishInfoDO> querySignUpPublishedInfoList(PublishInfoDomain publishInfoDomain);
}
