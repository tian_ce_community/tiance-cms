package com.tiance.domainservice.service;

import com.tiance.dal.dataobject.ActivityDO;
import com.tiance.dal.dataobject.AppreciateDO;
import com.tiance.domainservice.domain.ActivityDomain;
import com.tiance.domainservice.domain.AppreciateDomain;

import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/31
 *
 * Description：
 *
 * Modification History:
 *
 */
public interface ActivityService {

    ActivityDO addActivity(ActivityDomain activityDomain) throws Exception;

    List<ActivityDO> queryApplyActivity(ActivityDomain activityDomain);

    void cancel(ActivityDomain activityDomain);

    void finish(ActivityDomain activityDomain);

    List<ActivityDO> querySignUpActivityList(ActivityDomain activityDomain);

    ActivityDO queryActivityDetailById(ActivityDomain activityDomain);
}
