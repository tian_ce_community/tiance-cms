package com.tiance.domainservice.service.impl;

import com.alibaba.fastjson.JSON;
import com.tiance.dal.dao.ActivityDAO;
import com.tiance.dal.dao.AuditRecordDAO;
import com.tiance.dal.dao.SignUpDAO;
import com.tiance.dal.dao.UniqueCodeDAO;
import com.tiance.dal.dataobject.*;
import com.tiance.domainservice.assemble.DOAssemble;
import com.tiance.domainservice.checker.ActivityChecker;
import com.tiance.domainservice.domain.ActivityDomain;
import com.tiance.domainservice.domain.IntegrationDomain;
import com.tiance.domainservice.domain.SignUpDomain;
import com.tiance.domainservice.service.ActivityService;
import com.tiance.domainservice.service.SignUpService;
import com.tiance.enums.*;
import com.tiance.exception.BusinessException;
import com.tiance.page.QueryPage;
import com.tiance.utils.MapUtil;
import com.tiance.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/14
 *
 * Description：
 *
 * Modification History:
 *
 */
@Service
public class SignUpServiceImpl implements SignUpService {
    protected static final Logger logger = LoggerFactory.getLogger(SignUpServiceImpl.class);

    @Resource
    private SignUpDAO signUpDAO;


    @Resource
    private UniqueCodeDAO uniqueCodeDAO;

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void addSignUp(SignUpDomain signUpDomain) throws Exception {
        try {
            UniqueCodeDO uniqueCodeDO = uniqueCodeDAO.queryByUniqueCode(signUpDomain.getUniqueCode());
            SignUpDO signUpDO = DOAssemble.assembleSignUpDO(signUpDomain);
            signUpDO.setEntityId(uniqueCodeDO.getEntityId());
            signUpDO.setEntityType(uniqueCodeDO.getEntityType());
            signUpDO.setApplicantId(signUpDomain.getApplicantId());

            Map<String, Object> paramMap = MapUtil
                    .params2Map(new Object[] { "applicantId", signUpDomain.getApplicantId() },
                            new Object[] { "signUpState", SignUpStatusEnum.SIGN_UP.getCode() },
                            new Object[] { "entityId", signUpDomain.getEntityId() },
                            new Object[] { "entityType", signUpDomain.getEntityType() });
            logger.info("开始查询该会员是否已存在报名数据，参数:{}", JSON.toJSONString(signUpDO));
            int querySignUpListCount = signUpDAO.querySignUpListCount(paramMap);
            logger.info("结束查询该会员是否已存在报名，是否存在:{}", querySignUpListCount > 0 ? true : false);
            if (querySignUpListCount > 0) {
                throw new BusinessException("您已经成功报名，请勿重复报名！");
            }

            logger.info("开始插入报名数据，参数:{}", JSON.toJSONString(signUpDO));
            Long count = signUpDAO.insert(signUpDO);
            logger.info("结束插入报名，是否插入成功:{}", count > 0 ? true : false);
            if (count < 1) {
                throw new BusinessException("插入失败");
            }

        }catch (DuplicateKeyException e){
            throw new BusinessException("您已经成功报名，请勿重复报名！");

        }catch (Exception e){

        }
    }

    @Override
    public List<SignUpDO> querySignUpList(SignUpDomain domain) {
        Map<String,Object> paramMap= MapUtil.params2Map(new Object[]{ "memberId", domain.getMemberId()},
                new Object[]{ "entityId", domain.getEntityId()});
        logger.info("开始查询报名总数，参数:{}", JSON.toJSONString(paramMap));
        int count  =signUpDAO.querySignUpListCount(paramMap);
        logger.info("结束查询报名总数，返回总数:{}", count);

        if(0==count){
            return new ArrayList();
        }

        QueryPage queryPage = domain.getQueryPage();
        queryPage.setTotalItem(count);
        paramMap.put("queryPage", queryPage);
        if (queryPage.getEndRow() == 0) {
            queryPage.setStartRow(queryPage.getPageFristItem());
            queryPage.setEndRow(queryPage.getPageLastItem());
        }


        logger.info("开始查询报名数据，参数:{}", JSON.toJSONString(paramMap));
        List<SignUpDO> signUpList =signUpDAO.querySignUpList(paramMap);
        logger.info("结束查询报名，查询结果:{}", JSON.toJSONString(signUpList));
        return signUpList;
    }

    @Override
    public void cancelSignUp(SignUpDomain signUpDomain) {
        SignUpDO signUpDO=new  SignUpDO();
        signUpDO.setApplicantId(signUpDomain.getApplicantId());
        signUpDO.setEntityId(signUpDomain.getEntityId());
        signUpDO.setEntityType(signUpDomain.getEntityType());
        signUpDO.setSignUpState(SignUpStatusEnum.CANCEL.getCode());
        logger.info("开始取消报名，参数:{}", JSON.toJSONString(signUpDO));
        Long count=signUpDAO.cancelSignUp(signUpDO);
        logger.info("结束取消报名，是否取消成功:{}", count>0?true:false);
        if(count<1){
            throw new BusinessException("取消失败");
        }
    }
}
