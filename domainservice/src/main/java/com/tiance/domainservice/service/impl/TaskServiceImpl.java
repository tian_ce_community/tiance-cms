package com.tiance.domainservice.service.impl;

import com.alibaba.fastjson.JSON;
import com.tiance.dal.dao.ActivityDAO;
import com.tiance.dal.dao.AuditRecordDAO;
import com.tiance.dal.dao.TaskDAO;
import com.tiance.dal.dao.UniqueCodeDAO;
import com.tiance.dal.dataobject.ActivityDO;
import com.tiance.dal.dataobject.AuditRecordDO;
import com.tiance.dal.dataobject.TaskDO;
import com.tiance.dal.dataobject.UniqueCodeDO;
import com.tiance.domainservice.assemble.DOAssemble;
import com.tiance.domainservice.checker.ActivityChecker;
import com.tiance.domainservice.checker.TaskChecker;
import com.tiance.domainservice.domain.ActivityDomain;
import com.tiance.domainservice.domain.TaskDomain;
import com.tiance.domainservice.service.ActivityService;
import com.tiance.domainservice.service.TaskService;
import com.tiance.enums.ActivityStatusEnum;
import com.tiance.enums.EntityTypeEnum;
import com.tiance.enums.TaskStatusEnum;
import com.tiance.exception.BusinessException;
import com.tiance.page.QueryPage;
import com.tiance.utils.MapUtil;
import com.tiance.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/14
 *
 * Description：
 *
 * Modification History:
 *
 */
@Service
public class TaskServiceImpl implements TaskService {
    protected static final Logger logger = LoggerFactory.getLogger(TaskServiceImpl.class);

    @Resource
    private TaskDAO taskDAO;


    @Resource
    private AuditRecordDAO auditRecordDAO;

    @Resource
    private UniqueCodeDAO uniqueCodeDAO;

    @Override
    public List<TaskDO> queryTaskList(TaskDomain domain) {
        Map<String,Object> paramMap= MapUtil.params2Map(
                new Object[]{ "statusScope", domain.getStatusScope()});
        logger.info("开始查询任务数据总数，参数:{}", JSON.toJSONString(paramMap));
        int count  =taskDAO.queryTaskListCount(paramMap);
        logger.info("开始查询任务数据总数，返回总数:{}", count);


        if(0==count){
            return new ArrayList();
        }

        QueryPage queryPage = domain.getQueryPage();
        queryPage.setTotalItem(count);
        paramMap.put("queryPage", queryPage);
        if (queryPage.getEndRow() == 0) {
            queryPage.setStartRow(queryPage.getPageFristItem());
            queryPage.setEndRow(queryPage.getPageLastItem());
        }

        domain.setQueryPage(queryPage);
        logger.info("开始查询任务数据列表数据，参数:{}", JSON.toJSONString(paramMap));
        List<TaskDO> taskDOList =taskDAO.queryTaskList(paramMap);
        logger.info("结束查询任务数据数据，查询结果:{}", JSON.toJSONString(taskDOList));
        return taskDOList;
    }

    @Override
    public void cancel(TaskDomain taskDomain) {
        logger.info("开始查询任务数据，参数:{}", JSON.toJSONString(taskDomain.getId()));
        TaskDO oldTaskDO=taskDAO.queryByTaskId(taskDomain.getId());
        logger.info("结束查询任务数据，查询结果:{}", JSON.toJSONString(oldTaskDO));
        TaskChecker.checkCancelTask(oldTaskDO);


        TaskDO newTaskDO= DOAssemble.assembleUpdateTaskDO(taskDomain);
        newTaskDO.setTaskStatus(TaskStatusEnum.APPLY_CANCEL.getCode());
        logger.info("开始更新任务数据，参数:{}", JSON.toJSONString(newTaskDO));
        Long count=taskDAO.update(newTaskDO);
        logger.info("结束更新任务数据，是否更新成功:{}", count>0?true:false);
        if(count<1){
            throw new BusinessException("更新失败");
        }
    }

    @Override
    public void finish(TaskDomain taskDomain) {
        logger.info("开始查询任务数据，参数:{}", JSON.toJSONString(taskDomain.getId()));
        TaskDO oldTaskDO=taskDAO.queryByTaskId(taskDomain.getId());
        logger.info("结束查询任务数据，查询结果:{}", JSON.toJSONString(oldTaskDO));
        TaskChecker.checkFinishTask(oldTaskDO);
        TaskDO newTaskDO= DOAssemble.assembleUpdateTaskDO(taskDomain);
        newTaskDO.setTaskStatus(TaskStatusEnum.TASK_FINISHED.getCode());
        logger.info("开始更新任务数据，参数:{}", JSON.toJSONString(newTaskDO));
        Long count=taskDAO.update(newTaskDO);
        logger.info("结束更新任务数据，是否更新成功:{}", count>0?true:false);
        if(count<1){
            throw new BusinessException("更新失败");
        }
    }

    @Override
    public void addTask(TaskDomain taskDomain) throws Exception {
        TaskDO newTaskDO= DOAssemble.assembleTaskDO(taskDomain);
        logger.info("开始插入任务数据，参数:{}", JSON.toJSONString(newTaskDO));
        int count=taskDAO.insert(newTaskDO);
        logger.info("结束插入任务数据，count:{}", count);


        UniqueCodeDO uniqueCodeDO=new UniqueCodeDO();
        uniqueCodeDO.setEntityId(newTaskDO.getId());
        uniqueCodeDO.setEntityType(EntityTypeEnum.TASK.getCode());
        logger.info("开始插入活动实体数据，参数:{}", JSON.toJSONString(uniqueCodeDO));
        Long uniqueCount=uniqueCodeDAO.insert(uniqueCodeDO);
        logger.info("结束插入活动实体数据，是否插入成功:{}", uniqueCount>0?true:false);
        if(uniqueCount<1){
            throw new BusinessException("插入失败");
        }

        AuditRecordDO auditRecordDO= DOAssemble.assembleAuditRecordDO(taskDomain);
        auditRecordDO.setEntityId(newTaskDO.getId());
        auditRecordDO.setEntityType(EntityTypeEnum.TASK.getCode());
        logger.info("开始插入发布信息审核数据，参数:{}", JSON.toJSONString(auditRecordDO));
        Long auditRecordount=auditRecordDAO.insert(auditRecordDO);
        logger.info("结束插入发布信息审核数据，是否插入成功:{}", auditRecordount>0?true:false);
        if(auditRecordount<1){
            throw new BusinessException("发布信息审核插入失败");
        }

    }

    @Override
    public List<TaskDO> querySignUpTaskList(TaskDomain domain) {
        Map<String,Object> paramMap= MapUtil.params2Map(
                new Object[]{ "applicantId", domain.getApplicantId()});
        logger.info("开始查询任务数据总数，参数:{}", JSON.toJSONString(paramMap));
        int count  =taskDAO.querySignUpTaskListCount(paramMap);
        logger.info("开始查询任务数据总数，返回总数:{}", count);


        if(0==count){
            return new ArrayList();
        }

        QueryPage queryPage = domain.getQueryPage();
        queryPage.setTotalItem(count);
        paramMap.put("queryPage", queryPage);
        if (queryPage.getEndRow() == 0) {
            queryPage.setStartRow(queryPage.getPageFristItem());
            queryPage.setEndRow(queryPage.getPageLastItem());
        }

        domain.setQueryPage(queryPage);
        logger.info("开始查询任务数据列表数据，参数:{}", JSON.toJSONString(paramMap));
        List<TaskDO> taskDOList =taskDAO.querySignUpTaskList(paramMap);
        logger.info("结束查询任务数据数据，查询结果:{}", JSON.toJSONString(taskDOList));
        return taskDOList;
    }
}
