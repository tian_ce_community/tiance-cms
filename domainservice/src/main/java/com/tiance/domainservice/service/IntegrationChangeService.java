package com.tiance.domainservice.service;

import com.tiance.dal.dataobject.IntegrationChangeDictDO;
import com.tiance.dal.dataobject.IntegrationChangeRecordDO;
import com.tiance.domainservice.domain.IntegrationChangeDictDomain;
import com.tiance.domainservice.domain.IntegrationChangeDomain;

import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/31
 *
 * Description：
 *
 * Modification History:
 *
 */
public interface IntegrationChangeService {

    void memberIntegrationChange(IntegrationChangeDomain integrationChangeDomain) throws Exception;

    List<IntegrationChangeRecordDO> queryIntegrationChangeRecordList(IntegrationChangeDomain integrationChangeDomain);

}
