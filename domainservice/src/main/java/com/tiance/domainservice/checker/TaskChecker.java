package com.tiance.domainservice.checker;

import com.tiance.dal.dataobject.TaskDO;
import com.tiance.enums.ActivityStatusEnum;
import com.tiance.enums.TaskStatusEnum;
import com.tiance.exception.BusinessException;
import com.tiance.utils.Validator;
import com.tiance.vo.TaskVO;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/4
 *
 * Description：
 *
 * Modification History:
 *
 */
public class TaskChecker extends BaseChecker {



    public static void checkAddTask(TaskVO taskVO) {
        Validator.assertNull(taskVO.getTaskName(),"任务名称不能为空!");
        Validator.assertNull(taskVO.getTaskIntegral(),"任务积分不能为空");
        Validator.assertNull(taskVO.getTaskMaxNum(),"任务最大人数不能为空");
        Validator.assertNull(taskVO.getTaskMemo(),"任务描述不能为空");

    }

    public static void checkCancelTask(TaskDO oldTaskDO) {
        if(null==oldTaskDO){
            throw new BusinessException("未查询到任务数据");
        }

        if(TaskStatusEnum.TASK_CANCEL.getCode().equals( oldTaskDO.getTaskStatus())){
            throw new BusinessException("该任务已经是取消状态，请勿重复操作");
        }
        if(TaskStatusEnum.TASK_FINISHED.getCode().equals( oldTaskDO.getTaskStatus())){
            throw new BusinessException("该任务已结束，无法取消");
        }
        if(TaskStatusEnum.TASK_APPRECIATED.getCode().equals( oldTaskDO.getTaskStatus())){
            throw new BusinessException("该任务已打赏，无法取消");
        }
    }

    public static void checkFinishTask(TaskDO oldTaskDO) {
        if(null==oldTaskDO){
            throw new BusinessException("未查询到任务数据");
        }

        if(TaskStatusEnum.TASK_FINISHED.getCode().equals( oldTaskDO.getTaskStatus())){
            throw new BusinessException("该任务已是结束状态，请勿重复操作");
        }
        if(TaskStatusEnum.TASK_APPRECIATED.getCode().equals( oldTaskDO.getTaskStatus())){
            throw new BusinessException("该任务已打赏，无法结束");
        }
    }
}
