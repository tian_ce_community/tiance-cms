package com.tiance.domainservice.checker;

import com.tiance.dal.dataobject.ActivityDO;
import com.tiance.enums.ActivityStatusEnum;
import com.tiance.exception.BusinessException;
import com.tiance.utils.Validator;
import com.tiance.vo.ActivityVO;
import com.tiance.vo.AuditRecordVO;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/4
 *
 * Description：
 *
 * Modification History:
 *
 */
public class AuditChecker  extends BaseChecker{

    public static void checkQueryAuditList(AuditRecordVO auditRecordVO) {



    }
    public static void checkAudit(AuditRecordVO auditRecordVO) {
        Validator.assertNull(auditRecordVO.getEntityId(),"审核实体ID不能为空!");
        Validator.assertNull(auditRecordVO.getEntityType(),"审核实体类型不能为空!");
        Validator.assertNull(auditRecordVO.getAuditState(),"审核状态不能为空!");
        Validator.assertNull(auditRecordVO.getApprovalOpinion(),"审核意见不能为空!");

    }


}
