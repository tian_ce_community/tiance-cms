package com.tiance.domainservice.checker;

import com.tiance.utils.Validator;
import com.tiance.vo.PublishInfoVO;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/4
 *
 * Description：
 *
 * Modification History:
 *
 */
public class PublishInfoChecker extends BaseChecker {

    public static void addPublishInfo(PublishInfoVO publishInfoVO) {
            Validator.assertNull(publishInfoVO.getApplicantId(),"申请人不能为空!");
            Validator.assertNull(publishInfoVO.getInfoName(),"发布信息名称不能为空!");
            Validator.assertNull(publishInfoVO.getContactsName(),"发布信息联系人不能为空!");
            Validator.assertNull(publishInfoVO.getContactsPhone(),"发布信息联系人电话不能为空!");
            Validator.assertNull(publishInfoVO.getInfoType(),"发布信息类别不能为空!");

    }
}
