package com.tiance.domainservice.checker;

import com.tiance.utils.Validator;
import com.tiance.vo.ConventionCenterVO;
import com.tiance.vo.DutyVO;
import com.tiance.vo.MeetingRoomApplicantVO;
import com.tiance.vo.MeetingRoomVO;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/4
 *
 * Description：
 *
 * Modification History:
 *
 */
public class DutyChecker extends BaseChecker {

    public static void checkAddDuty(DutyVO dutyVO) {
        Validator.assertNull(dutyVO.getApplicantId(),"申请人不能为空!");
        Validator.assertNull(dutyVO.getConventionCenterId(),"请选择会议中心!");
        Validator.assertNull(dutyVO.getStartTime(),"请选择值班开始时间!");
        Validator.assertNull(dutyVO.getEndTime(),"请选择值班结束时间!");

    }

    public static void checkCancelDuty(DutyVO dutyVO) {
        Validator.assertNull(dutyVO.getId(),"值班ID不能为空!");

    }

    public static void checkSubstituteDutyApply(DutyVO dutyVO) {
        Validator.assertNull(dutyVO.getId(),"当前值班ID不能为空!");
        Validator.assertNull(dutyVO.getApplicantId(),"申请人不能为空!");
        Validator.assertNull(dutyVO.getSubstituteNo(),"替班人编号不能为空!");
        Validator.assertNull(dutyVO.getStartTime(),"请选择替班开始时间!");
        Validator.assertNull(dutyVO.getEndTime(),"请选择替班结束时间!");
    }
}
