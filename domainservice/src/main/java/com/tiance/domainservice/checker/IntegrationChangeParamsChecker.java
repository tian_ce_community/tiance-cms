package com.tiance.domainservice.checker;

import com.tiance.exception.BusinessException;
import com.tiance.vo.IntegrationChangeVO;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/4
 *
 * Description：
 *
 * Modification History:
 *
 */
public class IntegrationChangeParamsChecker {

    public static void checkMemberIntegrationChange(IntegrationChangeVO integrationChangeVO) {
        if(null==integrationChangeVO.getMemberId()){
            throw new BusinessException("会员ID不能为空");
        }
    }
}
