package com.tiance.domainservice.checker;

import com.tiance.dal.dataobject.MemberAuthFileDO;
import com.tiance.dal.dataobject.MemberAuthenticationInfoDO;
import com.tiance.enums.ArrangeForTypeEnum;
import com.tiance.enums.AuthFileTypeEnum;
import com.tiance.enums.FinishStatusEnum;
import com.tiance.enums.InfoTypeEnum;
import com.tiance.exception.BusinessException;
import com.tiance.utils.IdCardVerifyUtil;
import com.tiance.utils.Validator;
import com.tiance.vo.MemberAuthOpinionVO;
import com.tiance.vo.MemberAuthenticationInfoVO;
import com.tiance.vo.MemberVO;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.validation.ValidationUtils;

import java.text.ParseException;
import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/4
 *
 * Description：
 *
 * Modification History:
 *
 */
public class MemberParamsChecker extends BaseChecker {
    public static void checkRegisterMember(MemberVO memberVO) {

        Validator.assertNull(memberVO.getId(),"游客Id不能为空!");
/*
        Validator.assertNull(memberVO.getPartnerProperty(),"合伙人属性不能为空!");
*/
    }

    public static void checkQueryMember(MemberVO memberVO) {
        if(null==memberVO.getId()&&null==memberVO.getMemberNo()&&null==memberVO.getMemberCardNo()&&null==memberVO.getPhone()){
            throw new BusinessException("会员ID,会员编号,会员卡,会员手机号不能同时为空");
        }
    }

    public static void checkUpdateMember(MemberVO memberVO) {

    }
    public static void auditMemberAuth(MemberAuthOpinionVO memberVO) {
        Validator.assertNull(memberVO.getId(),"会员审核ID不能为空!");
        Validator.assertNull(memberVO.getAuthOpinion1(),"会员认证基本信息意见不能为空!");
        Validator.assertNull(memberVO.getAuthOpinion2(),"会员认证原工作信息意见不能为空!");
        Validator.assertNull(memberVO.getAuthOpinion3(),"会员认证现工作信息意见不能为空!");
        Validator.assertNull(memberVO.getAuthState1(),"会员认证基本信息状态不能为空!");
        Validator.assertNull(memberVO.getAuthState2(),"会员认证原工作信息状态不能为空!");
        Validator.assertNull(memberVO.getAuthState3(),"会员认证现信息状态不能为空!");

    }

    public static void checkMemberAuth(MemberAuthenticationInfoDO memberAuthenticationInfoVO)
            throws ParseException {



        Validator.assertNull(memberAuthenticationInfoVO.getRealName(),"会员真实姓名不能为空!");
        Validator.assertNull(memberAuthenticationInfoVO.getMemberSex(),"会员性别不能为空!");
        Validator.assertNull(memberAuthenticationInfoVO.getPhone(),"会员手机号不能为空!");
        Validator.assertNull(memberAuthenticationInfoVO.getMemberEmail(),"会员邮箱不能为空!");
        Validator.assertNull(memberAuthenticationInfoVO.getIdNumber(),"身份证号不能为空!");
        IdCardVerifyUtil.IDCardValidate(memberAuthenticationInfoVO.getIdNumber());
        Validator.assertNull(memberAuthenticationInfoVO.getArrangeForType(),"安置后状态不能为空!");
        if(ArrangeForTypeEnum.SELF_EMPLOYMENT.getCode().equals(memberAuthenticationInfoVO.getArrangeForType())){
            Validator.assertNull(memberAuthenticationInfoVO.getSelfEmploymentCode(),"自主择业证号不能为空!");
        }
        Validator.assertNull(memberAuthenticationInfoVO.getReside(),"常住地不能为空!");
        Validator.assertNull(memberAuthenticationInfoVO.getFormerJobNature(),"原工作属性不能为空!");
        Validator.assertNull(memberAuthenticationInfoVO.getFormerUnitType(),"原单位类别不能为空!");
        Validator.assertNull(memberAuthenticationInfoVO.getJobTwoWay(),"双轨制不能为空!");
        Validator.assertNull(memberAuthenticationInfoVO.getAdministrationPost(),"行政职务不能为空!");
        Validator.assertNull(memberAuthenticationInfoVO.getTechnologyPost(),"技术职称等级不能为空!");
        Validator.assertNull(memberAuthenticationInfoVO.getTechnologySeries(),"技术职称系列不能为空!");
        Validator.assertNull(memberAuthenticationInfoVO.getBachelorMajorYxOne(),"学士1院校不能为空!");
        Validator.assertNull(memberAuthenticationInfoVO.getBachelorMajorYxTwo(),"学士2院校不能为空!");
        Validator.assertNull(memberAuthenticationInfoVO.getBachelorMajorZyOne(),"学士1专业不能为空!");
        Validator.assertNull(memberAuthenticationInfoVO.getBachelorMajorZyTwo(),"学士2专业不能为空!");
        Validator.assertNull(memberAuthenticationInfoVO.getCurrentJobState(),"当前就业状态不能为空!");


    }

    public static void checkApplyMemberAuthInfo(MemberAuthenticationInfoVO memberDO,
                                                MemberAuthenticationInfoVO memberAuthenticationInfoVO) {



        try {
            Validator.assertNull(memberDO.getRealName(), "");
            Validator.assertNull(memberDO.getMemberSex(), "");
            Validator.assertNull(memberDO.getPhone(), "");
            Validator.assertNull(memberDO.getMemberEmail(), "");
            Validator.assertNull(memberDO.getIdNumber(), "");
            Validator.assertNull(memberDO.getIdCardBackUrl(), "");
            Validator.assertNull(memberDO.getIdCardFrontUrl(), "");
            Validator.assertNull(memberDO.getArrangeForType(), "");
            if (ArrangeForTypeEnum.SELF_EMPLOYMENT.getCode()
                    .equals(memberDO.getArrangeForType())) {
                Validator.assertNull(memberDO.getSelfEmploymentCode(),
                        "自主择业证号不能为空!");
                Validator.assertNull(memberDO.getChooseJobCertificateUrl(),
                        "择业证照片不能为空!");
            }
            memberAuthenticationInfoVO.setBaseInfo(FinishStatusEnum.FINISH.getCode());

        }catch (Exception e){
            memberAuthenticationInfoVO.setBaseInfo(FinishStatusEnum.UNFINISHED.getCode());
        }



        try {
            Validator.assertNull(memberDO.getFormerUnitType(), "");
            Validator.assertNull(memberDO.getFormerJobNature(), "");
            Validator.assertNull(memberDO.getJobTwoWay(), "");
            Validator.assertNull(memberDO.getAdministrationPost(), "");
            Validator.assertNull(memberDO.getTechnologySeries(), "");
            Validator.assertNull(memberDO.getTechnologyPost(), "");
            Validator.assertNull(memberDO.getBachelorMajorYxOne(), "");
            Validator.assertNull(memberDO.getBachelorMajorZyOne(), "");
            memberAuthenticationInfoVO.setPreWorkInfo(FinishStatusEnum.FINISH.getCode());

        }catch (Exception e){
            memberAuthenticationInfoVO.setPreWorkInfo(FinishStatusEnum.UNFINISHED.getCode());
        }

        try {
            Validator.assertNull(memberDO.getCurrentJobState(), "");
            Validator.assertNull(memberDO.getCompany(), "");
            memberAuthenticationInfoVO.setNowWorkInfo(FinishStatusEnum.FINISH.getCode());
        }catch (Exception e){
            memberAuthenticationInfoVO.setNowWorkInfo(FinishStatusEnum.UNFINISHED.getCode());
        }

    }

    public static void checkFiles(MemberAuthenticationInfoDO memberAuthenticationInfoDO, List<MemberAuthFileDO> memberAuthFileList) {
        boolean CHOOSE_JOB_CERTIFICATE=false;
        boolean ID_CARD_BACK=false;
        boolean ID_CARD_FRONT=false;
        boolean JOB_QUALIFICATION_CERTIFICATE=false;
        boolean RETIREMENT_RESUME_CERTIFICATE=false;
        if(CollectionUtils.isNotEmpty(memberAuthFileList)){
            for(MemberAuthFileDO memberAuthFileDO:memberAuthFileList){
                if(AuthFileTypeEnum.CHOOSE_JOB_CERTIFICATE.getCode().equals(memberAuthFileDO.getFileType())){
                    CHOOSE_JOB_CERTIFICATE=true;
                }
                if(AuthFileTypeEnum.ID_CARD_BACK.getCode().equals(memberAuthFileDO.getFileType())){
                    ID_CARD_BACK=true;
                }
                if(AuthFileTypeEnum.ID_CARD_FRONT.getCode().equals(memberAuthFileDO.getFileType())){
                    ID_CARD_FRONT=true;
                }
                if(AuthFileTypeEnum.JOB_QUALIFICATION_CERTIFICATE.getCode().equals(memberAuthFileDO.getFileType())){
                    JOB_QUALIFICATION_CERTIFICATE=true;
                }
                if(AuthFileTypeEnum.RETIREMENT_RESUME_CERTIFICATE.getCode().equals(memberAuthFileDO.getFileType())){
                    RETIREMENT_RESUME_CERTIFICATE=true;
                }
            }
        }

        Validator.assertTrue(!ID_CARD_BACK, "身份证背面照片不能为空!");
        Validator.assertTrue(!ID_CARD_FRONT, "身份证正面照片不能为空!");
        Validator.assertTrue(!RETIREMENT_RESUME_CERTIFICATE, "退役时履历表不能为空!");
        if(ArrangeForTypeEnum.SELF_EMPLOYMENT.getCode().equals(memberAuthenticationInfoDO.getArrangeForType())){
            Validator.assertTrue(!CHOOSE_JOB_CERTIFICATE, "身份证正面照片不能为空!");
        }

    }
}
