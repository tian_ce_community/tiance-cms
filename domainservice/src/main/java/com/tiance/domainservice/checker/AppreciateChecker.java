package com.tiance.domainservice.checker;

import com.tiance.utils.Validator;
import com.tiance.vo.AppreciateVO;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/4
 *
 * Description：
 *
 * Modification History:
 *
 */
public class AppreciateChecker extends BaseChecker {

    public static void checkAddAppreciate(AppreciateVO appreciateVO) {
        Validator.assertNull(appreciateVO.getFromMemberId(),"赞赏人不能为空!");
        Validator.assertNull(appreciateVO.getToMemberId(),"被赞赏人不能为空!");
        Validator.assertNull(appreciateVO.getEntityId(),"实体ID不能为空!");
        Validator.assertNull(appreciateVO.getEntityType(),"实体类型不能为空!");
    }
}
