package com.tiance.domainservice.checker;

import com.tiance.dal.dataobject.ActivityDO;
import com.tiance.enums.ActivityStatusEnum;
import com.tiance.exception.BusinessException;
import com.tiance.utils.Validator;
import com.tiance.vo.ActivityVO;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/4
 *
 * Description：
 *
 * Modification History:
 *
 */
public class ActivityChecker extends BaseChecker {

    public static void checkAddActivity(ActivityVO activityVO) {
        Validator.assertNull(activityVO.getApplicantType(),"申请人类型不能为空!");
        Validator.assertNull(activityVO.getApplicantId(),"申请人不能为空!");
        Validator.assertNull(activityVO.getActivityName(),"活动名称不能为空!");
        Validator.assertNull(activityVO.getActivityMaxNum(),"活动最大人数不能为空!");
        Validator.assertNull(activityVO.getActivityIntegral(),"活动收取积分不能为空!");
        Validator.assertNull(activityVO.getRemark(),"活动说明不能为空!");
    }

    public static void checkCancelActivity(ActivityDO activityDO) {
        if(null==activityDO){
            throw new BusinessException("未查询到活动数据");
        }

        if(ActivityStatusEnum.APPLY_CANCEL.getCode().equals( activityDO.getActivityStatus())){
            throw new BusinessException("该活动已经是取消状态，请勿重复操作");
        }
        if(ActivityStatusEnum.ACTIVITY_CANCEL.getCode().equals( activityDO.getActivityStatus())){
            throw new BusinessException("该活动已经是取消状态，请勿重复操作");
        }
        if(ActivityStatusEnum.ACTIVITY_FINISHED.getCode().equals( activityDO.getActivityStatus())){
            throw new BusinessException("该活动已结束，无法取消");
        }
        if(ActivityStatusEnum.ACTIVITY_APPRECIATED.getCode().equals( activityDO.getActivityStatus())){
            throw new BusinessException("该活动已打赏，无法取消");
        }
    }
    public static void checkFinishActivity(ActivityDO activityDO) {
        if(null==activityDO){
            throw new BusinessException("未查询到活动数据");
        }

        if(ActivityStatusEnum.ACTIVITY_FINISHED.getCode().equals( activityDO.getActivityStatus())){
            throw new BusinessException("该活动已是结束状态，请勿重复操作！");
        }
    }
}
