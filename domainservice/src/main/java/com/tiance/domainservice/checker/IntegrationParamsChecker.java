package com.tiance.domainservice.checker;

import com.tiance.exception.BusinessException;
import com.tiance.vo.IntegrationHistoryVO;
import com.tiance.vo.MemberVO;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/4
 *
 * Description：
 *
 * Modification History:
 *
 */
public class IntegrationParamsChecker extends BaseChecker{
    public static void checkRegisterMember(MemberVO memberVO) {

    }

    public static void checkQueryIntegrationHistory(IntegrationHistoryVO integrationHistoryVO) {

        if(null==integrationHistoryVO.getIntegrationType()){
            throw new BusinessException("类型不能为空");
        }

        if(null==integrationHistoryVO.getMemberId()&&null==integrationHistoryVO.getOpenid()){
            throw new BusinessException("会员ID,OPENID,不能同时为空");
        }
    }
}
