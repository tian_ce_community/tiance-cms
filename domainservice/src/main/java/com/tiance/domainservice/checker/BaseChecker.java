package com.tiance.domainservice.checker;

import com.tiance.exception.BusinessException;
import com.tiance.page.QueryPage;
import com.tiance.vo.SignInVO;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/16
 *
 * Description：
 *
 * Modification History:
 *
 */
public class BaseChecker {

    public static void checkPageQuery(QueryPage queryPage) {
        if(null==queryPage){
            throw new BusinessException("分页信息不能为空!");
        }
        if(null==queryPage.getPageSize()){
            throw new BusinessException("分页大小不能为空!");
        }
        if(null==queryPage.getCurrentPage()){
            throw new BusinessException("当前页不能为空!");
        }

    }

    public static void checkOpenId(String openId) {
        if(StringUtils.isBlank(openId)){
            throw new BusinessException("会员OPENID不能为空!");
        }
    }

    public static void checkId(Long id) {
        if(null==id){
            throw new BusinessException("ID不能为空!");
        }
    }

    public static void checkMemberId(Long memberId) {
        if(null==memberId){
            throw new BusinessException("会员ID不能为空!");
        }
    }
}
