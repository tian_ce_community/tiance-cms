package com.tiance.domainservice.checker;

import com.tiance.utils.Validator;
import com.tiance.vo.RoleVO;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/4
 *
 * Description：
 *
 * Modification History:
 *
 */
public class RoleParamsChecker extends BaseChecker {

    public static void checkQueryDetail(RoleVO roleVO) {
        Validator.assertNull(roleVO.getId(),"角色ID不能为空!");
    }
}
