package com.tiance.domainservice.checker;

import com.tiance.utils.Validator;
import com.tiance.vo.SignUpVO;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/4
 *
 * Description：
 *
 * Modification History:
 *
 */
public class SignUpChecker extends BaseChecker {

    public static void checkAddSignUp(SignUpVO signUpVO) {
        Validator.assertNull(signUpVO.getUniqueCode(),"报名标识不能为空!");

    }
    public static void checkEntityId(Long entityId) {
        Validator.assertNull(entityId,"报名实体ID不能为空!");

    }

    public static void checkCancelSignUp(SignUpVO signUpVO) {
        Validator.assertNull(signUpVO.getApplicantId(),"申请人ID不能为空!");
        Validator.assertNull(signUpVO.getEntityId(),"报名实体ID不能为空!");
        Validator.assertNull(signUpVO.getEntityType(),"报名实体类型不能为空!");
    }
}
