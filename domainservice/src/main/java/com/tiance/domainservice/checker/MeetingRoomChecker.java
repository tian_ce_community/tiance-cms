package com.tiance.domainservice.checker;

import com.tiance.utils.Validator;
import com.tiance.vo.CCOccupationVO;
import com.tiance.vo.ConventionCenterVO;
import com.tiance.vo.MeetingRoomApplicantVO;
import com.tiance.vo.MeetingRoomVO;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/4
 *
 * Description：
 *
 * Modification History:
 *
 */
public class MeetingRoomChecker extends BaseChecker {



    public static void meetingRoomApplicant(MeetingRoomApplicantVO meetingRoomApplicantVO) {
        Validator.assertNull(meetingRoomApplicantVO.getApplicantId(),"请输入申请人ID!");
        Validator.assertNull(meetingRoomApplicantVO.getConventionCenterId(),"请输入会议中心ID!");
        Validator.assertNull(meetingRoomApplicantVO.getMeetingRoomId(),"请输入会议室ID!");
        Validator.assertNull(meetingRoomApplicantVO.getParticipatorCount(),"请输入会议参加人数!");
       /* Validator.assertNull(meetingRoomApplicantVO.getUseStartTime(),"请输入会议开始时间!");
        Validator.assertNull(meetingRoomApplicantVO.getUseEndTime(),"请输入会议结束时间!");
*/
    }

    public static void checkQueryConventionCenterList(ConventionCenterVO conventionCenterVO) {

    }

    public static void checkQueryMeetingRoomVOList(MeetingRoomVO meetingRoomVO) {


    }

    public static void cancelMeetingRoomApplicant(MeetingRoomApplicantVO meetingRoomApplicantVO) {
        Validator.assertNull(meetingRoomApplicantVO.getId(),"请输入申请ID!");
    }

    public static void checkOccupation(CCOccupationVO conventionCenterVO) {
        Validator.assertNull(conventionCenterVO.getConventionCenterId(),"请输入会议中心ID!");

    }
}
