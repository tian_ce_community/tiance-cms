package com.tiance.domainservice.checker;

import com.tiance.dal.dataobject.MemberAuthFileDO;
import com.tiance.dal.dataobject.MemberAuthenticationInfoDO;
import com.tiance.enums.ArrangeForTypeEnum;
import com.tiance.enums.AuthFileTypeEnum;
import com.tiance.enums.FinishStatusEnum;
import com.tiance.exception.BusinessException;
import com.tiance.utils.IdCardVerifyUtil;
import com.tiance.utils.Validator;
import com.tiance.vo.MemberAuthOpinionVO;
import com.tiance.vo.MemberAuthenticationInfoVO;
import com.tiance.vo.MemberVO;
import com.tiance.vo.UserVO;
import org.apache.commons.collections.CollectionUtils;

import java.text.ParseException;
import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/4
 *
 * Description：
 *
 * Modification History:
 *
 */
public class UserParamsChecker extends BaseChecker {

    public static void checkEditPassword(UserVO userVO) {
        Validator.assertNull(userVO.getId(),"用户ID不能为空!");
        Validator.assertNull(userVO.getOldPassword(),"旧密码不能为空!");
        Validator.assertNull(userVO.getNewPassword(),"新密码不能为空!");
        Validator.assertNull(userVO.getConfirmNewPassword(),"请确认新密码!");
        Validator.assertTrue(!userVO.getConfirmNewPassword().equals(userVO.getNewPassword()),"两次密码不一致!");
    }
}
