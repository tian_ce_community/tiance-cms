package com.tiance.domainservice.checker;

import com.tiance.enums.SignInTypeEnum;
import com.tiance.exception.BusinessException;
import com.tiance.utils.Validator;
import com.tiance.vo.SignInVO;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/4
 *
 * Description：
 *
 * Modification History:
 *
 */
public class SignInChecker extends BaseChecker {

    public static void checkAddSignIn(SignInVO signInVO) {
        Validator.assertNull(signInVO.getMemberId(),"会员ID不能为空");
        if(!SignInTypeEnum.DAILY_SIGN_IN.getCode().equals(signInVO.getSignInType())){
            Validator.assertNull(signInVO.getEntityType(),"实体类型不能为空!");
            Validator.assertNull(signInVO.getEntityId(),"实体ID不能为空!");
        }
    }

    public static void checkQuery(SignInVO signInVO) {
        if(null==signInVO.getMemberId()){
            throw new BusinessException("会员ID不能为空!");
        }
    }
}
