package com.tiance.domainservice.assemble;

import com.tiance.dal.dataobject.*;
import com.tiance.domainservice.domain.*;
import com.tiance.enums.*;
import com.tiance.utils.BeanConverterUtil;
import com.tiance.utils.StringUtil;

import java.util.Date;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/4
 *
 * Description：
 *
 * Modification History:
 *
 */
public class DOAssemble {
    public static IntegrationChangeRecordDO assembleIntegrationChangeRecordDO(IntegrationChangeDomain integrationChangeDomain)
            throws Exception {
        IntegrationChangeRecordDO integrationChangeRecordDO=new IntegrationChangeRecordDO();
        BeanConverterUtil.copyProperties(integrationChangeRecordDO,integrationChangeDomain);
        integrationChangeRecordDO.setChangeTime(new Date());
        return integrationChangeRecordDO;
    }

    public static MemberDO assembleMemberDO(MemberDomain memberDomain)
            throws Exception {
        MemberDO memberDO=new MemberDO();
        BeanConverterUtil.copyProperties(memberDO,memberDomain);
        memberDO.setUseStatus(UseStatusEnum.NORMAL.getCode());
        memberDO.setMemberLevel(MemberLevelEnum.LEVEL_1.getCode());
        memberDO.setMemberStatus(MemberStatusEnum.REGISTER_SUCCESS.getCode());
        memberDO.setMemberType(MemberTypeEnum.REGULAR_MEMBER.getCode());
        memberDO.setTotalCredit(0);
        memberDO.setTotalIntegral(0);
        memberDO.setPartnerProperty(PartnerPropertyEnum.NOT_PARTNER.getCode());
        return memberDO;
    }

    public static MemberAuthenticationInfoDO assembleMemberAuthenticationInfoDO(MemberAuthenticationInfoDomain memberAuthenticationInfoDomain)
            throws Exception {
        MemberAuthenticationInfoDO memberAuthenticationInfoDO=new MemberAuthenticationInfoDO();
        BeanConverterUtil.copyProperties(memberAuthenticationInfoDO,memberAuthenticationInfoDomain);
        return memberAuthenticationInfoDO;
    }

    public static MemberAuthFileDO assembleMemberAuthFileDO(String fileTitle, String fileFormat, String fileName, String filePath, String fileType )
            throws Exception {
        MemberAuthFileDO memberAuthFileDO =new MemberAuthFileDO();
        memberAuthFileDO.setFileFormat(fileFormat);
        memberAuthFileDO.setFileName(fileName);
        memberAuthFileDO.setFilePath(filePath);
        memberAuthFileDO.setFileTitle(fileTitle);
        memberAuthFileDO.setFileType(fileType);
        return memberAuthFileDO;
    }

    public static AppreciateDO assembleAppreciateDO(AppreciateDomain appreciateDomain) throws Exception {
        AppreciateDO appreciateDO=new AppreciateDO();
        BeanConverterUtil.copyProperties(appreciateDO,appreciateDomain);
        appreciateDO.setAppreciateTime(new Date());
        return appreciateDO;
    }


    public static SignUpDO assembleSignUpDO(SignUpDomain signUpDomain) throws Exception {
        SignUpDO signUpDO=new SignUpDO();
        BeanConverterUtil.copyProperties(signUpDO,signUpDomain);
        signUpDO.setSignUpState(SignUpStatusEnum.SIGN_UP.getCode());
        return signUpDO;
    }
    public static SignInDO assembleSignInDO(SignInDomain signInDomain) throws Exception {
        SignInDO signInDO=new SignInDO();
        BeanConverterUtil.copyProperties(signInDO,signInDomain);
        signInDO.setSignInTime(new Date());
        return signInDO;
    }

    public static MeetingRoomApplicantDO assembleMeetingRoomApplicantDO(MeetingRoomApplicantDomain meetingRoomApplicantDomain) throws Exception{
        MeetingRoomApplicantDO meetingRoomApplicantDO=new MeetingRoomApplicantDO();
        BeanConverterUtil.copyProperties(meetingRoomApplicantDO,meetingRoomApplicantDomain);
        meetingRoomApplicantDO.setApplicantState(MeetingRoomApplicantStatusEnum.UN_AUDIT.getCode());
        return meetingRoomApplicantDO;

    }

    public static PublishInfoDO assemblePublishInfoDO(PublishInfoDomain publishInfoDomain) throws Exception{
        PublishInfoDO publishInfoDO=new PublishInfoDO();
        BeanConverterUtil.copyProperties(publishInfoDO,publishInfoDomain);
        publishInfoDO.setInfoStatus(PublishInfoStatusEnum.UN_AUDIT.getCode());
        return publishInfoDO;

    }
    public static AuditRecordDO assembleAuditRecordDO(DutyDomain dutyDomain) throws Exception{
        AuditRecordDO auditRecordDO=new AuditRecordDO();
        auditRecordDO.setApplicantTime(new Date());
        auditRecordDO.setEntityType(EntityTypeEnum.DUTY_APPLICANT.getCode());
        auditRecordDO.setAuditState(AuditStatusEnum.UN_AUDIT.getCode());
        return auditRecordDO;

    }
    public static AuditRecordDO assembleAuditRecordDO(TaskDomain taskDomain) throws Exception{
        AuditRecordDO auditRecordDO=new AuditRecordDO();
        auditRecordDO.setApplicantTime(new Date());
        auditRecordDO.setEntityType(EntityTypeEnum.TASK.getCode());
        auditRecordDO.setAuditState(AuditStatusEnum.UN_AUDIT.getCode());
        return auditRecordDO;

    }
    public static AuditRecordDO assembleAuditRecordDO(MeetingRoomApplicantDomain meetingRoomApplicantDomain) throws Exception{
        AuditRecordDO auditRecordDO=new AuditRecordDO();
        auditRecordDO.setApplicantTime(new Date());
        auditRecordDO.setEntityType(EntityTypeEnum.MEETING_ROOM_APPLICANT.getCode());
        auditRecordDO.setAuditState(AuditStatusEnum.UN_AUDIT.getCode());
        return auditRecordDO;

    }

    public static AuditRecordDO assembleAuditRecordDO(ActivityDomain activityDomain) throws Exception{
        AuditRecordDO auditRecordDO=new AuditRecordDO();
        auditRecordDO.setApplicantTime(new Date());
        auditRecordDO.setEntityType(EntityTypeEnum.ACTIVITY.getCode());
        auditRecordDO.setAuditState(AuditStatusEnum.UN_AUDIT.getCode());
        return auditRecordDO;

    }

    public static AuditRecordDO assembleAuditRecordDO(PublishInfoDomain publishInfoDomain) throws Exception{
        AuditRecordDO auditRecordDO=new AuditRecordDO();
        auditRecordDO.setApplicantTime(new Date());
        auditRecordDO.setEntityType(EntityTypeEnum.PUBLISH_INFO.getCode());
        auditRecordDO.setAuditState(AuditStatusEnum.UN_AUDIT.getCode());
        return auditRecordDO;

    }

    public static ActivityDO assembleActivityDO(ActivityDomain activityDomain) throws Exception{
        ActivityDO activityDO=new ActivityDO();
        BeanConverterUtil.copyProperties(activityDO,activityDomain);
        activityDO.setActivityStatus(ActivityStatusEnum.UN_AUDIT.getCode());
        return activityDO;

    }

    public static TcDutyDO assembleTcDutyDO(DutyDomain dutyDomain) throws Exception{
        TcDutyDO dutyDO=new TcDutyDO();
        BeanConverterUtil.copyProperties(dutyDO,dutyDomain);
        dutyDO.setApplicantState(DutyStatusEnum.UN_AUDIT.getCode());
        return dutyDO;

    }

    public static ActivityDO assembleUpdateActivityDO(ActivityDomain activityDomain) {
        ActivityDO activityDO=new ActivityDO();
        activityDO.setId(activityDomain.getId());
        activityDO.setRemark(activityDomain.getRemark());
        activityDO.setActivityStatus(activityDomain.getActivityStatus());
        return activityDO;
    }

    public static TcDutyDO assembleUpdateTcDutyDO(DutyDomain dutyDomain) {
        TcDutyDO tcDutyDO=new TcDutyDO();
        tcDutyDO.setId(dutyDomain.getId());
        tcDutyDO.setRemark(dutyDomain.getRemark());
        return tcDutyDO;
    }


    public static TaskDO assembleUpdateTaskDO(TaskDomain taskDomain) {
        TaskDO taskDO=new TaskDO();
        taskDO.setId(taskDomain.getId());
        taskDO.setRemark(taskDomain.getRemark());
        return taskDO;
    }

    public static TcDutyDO assembleSubstituteTcDutyDO(DutyDomain dutyDomain) throws Exception {
        TcDutyDO tcDutyDO=new TcDutyDO();
        BeanConverterUtil.copyProperties(tcDutyDO,dutyDomain);
        return tcDutyDO;
    }

    public static TaskDO assembleTaskDO(TaskDomain taskDomain) throws Exception{
        TaskDO taskDO=new TaskDO();
        BeanConverterUtil.copyProperties(taskDO,taskDomain);
        taskDO.setTaskStatus(TaskStatusEnum.AUDIT_PASS.getCode());
        taskDO.setApplicantType(ApplicantTypeEnum.SYSTEM.getCode());
        return taskDO;
    }


}
