package com.tiance.domainservice.domain;

import java.util.Date;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/4
 *
 * Description：
 *
 * Modification History:
 *
 */
public class MemberDomain extends BaseDomain{

    /**
     * 主键id
     * ID
     */
    private Long    id;
    /**
     * 微信昵称
     * WX_NICK_NAME
     */
    private String  wxNickName;
    /**
     * 微信头像
     * WX_HEAD_PHOTO
     */
    private String  wxHeadPhoto;

    /**
     * 会员总积分值
     * TOTAL_INTEGRAL
     */
    private Integer totalIntegral;
    /**
     * 会员总府宝值
     * TOTAL_CREDIT
     */
    private Integer totalCredit;

    /**
     * 电话
     * PHONE
     */
    private String  phone;
    /**
     * 用户真实姓名
     * REAL_NAME
     */
    private String realName;
    /**
     * 会员性别 MALE:男,FEMALE:女
     * MEMBER_SEX
     */
    private String memberSex;
    /**
     * 会员EMAIL
     * MEMBER_EMAIL
     */
    private String memberEmail;
    /**
     * 会员类型 GUEST:游客（允许授权）;REGULAR_MEMBER:普通会员;VERIFIED_MEMBER:认证会员;DUTY_MANAGER:值班经理;
     * MEMBER_TYPE
     */
    private String memberType;
    /**
     * 会员星级 1,2,3,4,5
     * MEMBER_LEVEL
     */
    private String memberLevel;
    /**
     * 会员卡号
     * MEMBER_CARD_NO
     */
    private String memberCardNo;
    /**
     * 会员编号
     * MEMBER_NO
     */
    private String memberNo;
    /**
     * 推荐会员编号
     * RECOMMEND_MEMBER_NO
     */
    private String recommendMemberNo;
    /**
     * 用户语言（需授权获得
     * MEMBER_LANGUAGE
     */
    private String memberLanguage;
    /**
     * 使用状态： NORMAL:正常;FORBIDDEN:禁用
     * USE_STATUS
     */
    private String useStatus;
    /**
     * 合伙人（PARTNER:是合伙人;NOT_PARTNER:不是合伙人）
     * PARTNER_PROPERTY
     */
    private String partnerProperty;
    /**
     * 二维码文件路径 (基于会员编号生成)
     * QR_CODE_FILE_PATH
     */
    private String qrCodeFilePath;
    /**
     * 会员状态 REGISTER_SUCCESS:会员注册成功;VERIFY_APPLIED:会员认证申请已提交;BACKGROUND_VERIFY_FAIL:后台认证失败;VERIFY_SUCCESS:会员认证成功;
     * MEMBER_STATUS
     */
    private String memberStatus;
    /**
     * 订阅状态 授权获得,NOT_SUBSCRIBE:表示没有关注，拉不到其他信息；SUBSCRIBED:表示关注，可以拉到其他授权信息
     * SUBSCRIBE_STATUS
     */
    private String  subscribeStatus;
    /**
     * COUNTRY
     */
    private String  country;
    /**
     * PROVINCE
     */
    private String  province;
    /**
     * CITY
     */
    private String  city;
    /**
     * 工作单位
     * COMPANY
     */
    private String  company;
    /**
     * 乐观锁
     * VERSION
     */
    private Integer version;
    /**
     * REMARK
     */
    private String  remark;
    /**
     * 关注时间
     * SUBSCRIBE_TIME
     */
    private Date    subscribeTime;
    /**
     * 取消关注时间
     * CANCEL_SUBSCRIBE_TIME
     */
    private Date    cancelSubscribeTime;
    /**
     * 创建时间
     * CREATE_TIME
     */
    private Date    createTime;
    /**
     * 更新时间
     * UPDATE_TIME
     */
    private Date    updateTime;
    /**
     * 注册时间
     * REGISTER_TIME
     */
    private Date    registerTime;

    public String getPartnerProperty() {
        return partnerProperty;
    }

    public void setPartnerProperty(String partnerProperty) {
        this.partnerProperty = partnerProperty;
    }

    public String getQrCodeFilePath() {
        return qrCodeFilePath;
    }

    public void setQrCodeFilePath(String qrCodeFilePath) {
        this.qrCodeFilePath = qrCodeFilePath;
    }

    public Integer getTotalCredit() {
        return totalCredit;
    }

    public void setTotalCredit(Integer totalCredit) {
        this.totalCredit = totalCredit;
    }

    public Integer getTotalIntegral() {
        return totalIntegral;
    }

    public void setTotalIntegral(Integer totalIntegral) {
        this.totalIntegral = totalIntegral;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWxNickName() {
        return wxNickName;
    }

    public void setWxNickName(String wxNickName) {
        this.wxNickName = wxNickName;
    }

    public String getWxHeadPhoto() {
        return wxHeadPhoto;
    }

    public void setWxHeadPhoto(String wxHeadPhoto) {
        this.wxHeadPhoto = wxHeadPhoto;
    }


    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getMemberSex() {
        return memberSex;
    }

    public void setMemberSex(String memberSex) {
        this.memberSex = memberSex;
    }

    public String getMemberEmail() {
        return memberEmail;
    }

    public void setMemberEmail(String memberEmail) {
        this.memberEmail = memberEmail;
    }

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    public String getMemberLevel() {
        return memberLevel;
    }

    public void setMemberLevel(String memberLevel) {
        this.memberLevel = memberLevel;
    }

    public String getMemberCardNo() {
        return memberCardNo;
    }

    public void setMemberCardNo(String memberCardNo) {
        this.memberCardNo = memberCardNo;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }

    public String getRecommendMemberNo() {
        return recommendMemberNo;
    }

    public void setRecommendMemberNo(String recommendMemberNo) {
        this.recommendMemberNo = recommendMemberNo;
    }

    public String getMemberLanguage() {
        return memberLanguage;
    }

    public void setMemberLanguage(String memberLanguage) {
        this.memberLanguage = memberLanguage;
    }

    public String getUseStatus() {
        return useStatus;
    }

    public void setUseStatus(String useStatus) {
        this.useStatus = useStatus;
    }

    public String getMemberStatus() {
        return memberStatus;
    }

    public void setMemberStatus(String memberStatus) {
        this.memberStatus = memberStatus;
    }

    public String getSubscribeStatus() {
        return subscribeStatus;
    }

    public void setSubscribeStatus(String subscribeStatus) {
        this.subscribeStatus = subscribeStatus;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getSubscribeTime() {
        return subscribeTime;
    }

    public void setSubscribeTime(Date subscribeTime) {
        this.subscribeTime = subscribeTime;
    }

    public Date getCancelSubscribeTime() {
        return cancelSubscribeTime;
    }

    public void setCancelSubscribeTime(Date cancelSubscribeTime) {
        this.cancelSubscribeTime = cancelSubscribeTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }
}
