package com.tiance.domainservice.domain;

import java.util.Date;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：公共表
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class MeetingRoomApplicantDomain extends BaseDomain {

    private static final long serialVersionUID = 1L;


    /**
     * ID
     */
    private Long id;
    /**
     * 会议中心ID
     * CONVENTION_CENTER_ID
     */
    private Long conventionCenterId;

    /**
     * 会议室ID
     * MEETING_ROOM_ID
     */
    private Long meetingRoomId;

    /**
     * 会员申请者id
     * APPLICANT_ID
     */
    private Long applicantId;

    /**
     * 申请人类型（SYSTEM:系统;MEMBER:会员）
     * APPLICANT_TYPE
     */
    private String applicantType;


    /**
     * 会员申请者
     */
    private String applicantName;
    /**
     * 审批人id
     * APPROVAL_ID
     */
    private Long approvalId;
    /**
     * 会议室名称
     * MEETING_ROOM_NAME
     */
    private String meetingRoomName;

    /**
     * 申请说明
     * APPLICANT_MEMO
     */
    private String applicantMemo;
    /**
     * 会议中心名称
     * CONVENTION_CENTER_NAME
     */
    private String conventionCenterName;
    /**
     * 会议中心位置
     * POSITION
     */
    private String position;
    /**
     * 会议中心坐标
     * COORDINATE
     */
    private String coordinate;
    /**
     * 会议室容量（人数
     * CAPACITY
     */
    private Integer capacity;
    /**
     * 会员参加者（人数）
     * PARTICIPATOR_COUNT
     */
    private Integer participatorCount;

    /**
     * 会议室硬件配套
     * HARD_WARE_MEMO
     */
    private String hardWareMemo;
    /**
     * 会议室状态(NORMAL:正常，FORBIDDEN:禁用)
     * MEETING_ROOM_STATUS
     */
    private String meetingRoomStatus;
    /**
     * APPLICANT_STATE
     * 申请状态 (UN_AUDIT:申请待审核;APPLY_CANCEL:申请取消;AUDIT_PASS:申请审核通过;AUDIT_FAIL:申请审核不通过;USING:会议室使用中;USE_FINISHED:会议室使用结束)

     */
    private String applicantState;

    /**
     * 会议室配套
     */
    private String meetingRoomMemo;
    /**
     * 审批意见
     * APPROVAL_OPINION
     */
    private String approvalOpinion;


    /**
     * CREATE_TIME
     */
    private Date createTime;
    /**
     * 会议室使用开始时间
     * USE_START_TIME
     */
    private Date useStartTime;
    /**
     * 使用结束时间
     * USE_END_TIME
     */
    private Date useEndTime;
    /**
     * UPDATE_TIME
     */
    private Date updateTime;
    /**
     * 申请时间
     * APPLICANT_TIME
     */
    private Date applicantTime;
    /**
     * 审批时间
     * APPROVAL_TIME
     */
    private Date approvalTime;
    /**
     * REMARK
     */
    private String remark;

    public String getApplicantType() {
        return applicantType;
    }

    public void setApplicantType(String applicantType) {
        this.applicantType = applicantType;
    }

    public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    public String getMeetingRoomMemo() {
        return meetingRoomMemo;
    }

    public void setMeetingRoomMemo(String meetingRoomMemo) {
        this.meetingRoomMemo = meetingRoomMemo;
    }

    public Long getMeetingRoomId() {
        return meetingRoomId;
    }

    public void setMeetingRoomId(Long meetingRoomId) {
        this.meetingRoomId = meetingRoomId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getConventionCenterId() {
        return conventionCenterId;
    }

    public void setConventionCenterId(Long conventionCenterId) {
        this.conventionCenterId = conventionCenterId;
    }

    public Long getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(Long applicantId) {
        this.applicantId = applicantId;
    }

    public Long getApprovalId() {
        return approvalId;
    }

    public void setApprovalId(Long approvalId) {
        this.approvalId = approvalId;
    }

    public String getMeetingRoomName() {
        return meetingRoomName;
    }

    public void setMeetingRoomName(String meetingRoomName) {
        this.meetingRoomName = meetingRoomName;
    }


    public String getApplicantMemo() {
        return applicantMemo;
    }

    public void setApplicantMemo(String applicantMemo) {
        this.applicantMemo = applicantMemo;
    }

    public String getConventionCenterName() {
        return conventionCenterName;
    }

    public void setConventionCenterName(String conventionCenterName) {
        this.conventionCenterName = conventionCenterName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(String coordinate) {
        this.coordinate = coordinate;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Integer getParticipatorCount() {
        return participatorCount;
    }

    public void setParticipatorCount(Integer participatorCount) {
        this.participatorCount = participatorCount;
    }


    public String getHardWareMemo() {
        return hardWareMemo;
    }

    public void setHardWareMemo(String hardWareMemo) {
        this.hardWareMemo = hardWareMemo;
    }

    public String getMeetingRoomStatus() {
        return meetingRoomStatus;
    }

    public void setMeetingRoomStatus(String meetingRoomStatus) {
        this.meetingRoomStatus = meetingRoomStatus;
    }

    public String getApplicantState() {
        return applicantState;
    }

    public void setApplicantState(String applicantState) {
        this.applicantState = applicantState;
    }

    public String getApprovalOpinion() {
        return approvalOpinion;
    }

    public void setApprovalOpinion(String approvalOpinion) {
        this.approvalOpinion = approvalOpinion;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUseStartTime() {
        return useStartTime;
    }

    public void setUseStartTime(Date useStartTime) {
        this.useStartTime = useStartTime;
    }

    public Date getUseEndTime() {
        return useEndTime;
    }

    public void setUseEndTime(Date useEndTime) {
        this.useEndTime = useEndTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getApplicantTime() {
        return applicantTime;
    }

    public void setApplicantTime(Date applicantTime) {
        this.applicantTime = applicantTime;
    }

    public Date getApprovalTime() {
        return approvalTime;
    }

    public void setApprovalTime(Date approvalTime) {
        this.approvalTime = approvalTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
