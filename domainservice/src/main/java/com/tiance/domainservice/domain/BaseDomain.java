package com.tiance.domainservice.domain;

import com.tiance.page.QueryPage;

import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/16
 *
 * Description：
 *
 * Modification History:
 *
 */
public class BaseDomain {


    /**
     * 状态范围
     */
    private List<String> statusScope;
    /**
     * OPENID
     */
    private String       openid;

    /**
     * OPENID
     */
    private Long  memberId;

    public List<String> getStatusScope() {
        return statusScope;
    }

    public void setStatusScope(List<String> statusScope) {
        this.statusScope = statusScope;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    /**
     * 分页信息
     */
    private QueryPage queryPage;

    public QueryPage getQueryPage() {
        return queryPage;
    }

    public void setQueryPage(QueryPage queryPage) {
        this.queryPage = queryPage;
    }
}
