package com.tiance.domainservice.domain;

import com.tiance.vo.BaseVO;
import com.tiance.vo.ImageVO;

import java.util.Date;
import java.util.List;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：会员认证信息表
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class MemberAuthenticationInfoDomain extends BaseDomain{


    /**
     * 信息类型
     *BASE_INFO("BASE_INFO", "基本信息"),
     PRE_WORK_INFO("PRE_WORK_INFO", "原工作信息"),
     NOW_WORK_INFO("NOW_WORK_INFO", "现工作信息"),
     ALL_INFO("ALL_INFO", "所有信息"),
     */
    private String infoType;

    /**
     * 电话
     * PHONE
     */
    private String phone;

    /**
     * 会员性别 MALE:男,FEMALE:女
     * MEMBER_SEX
     */
    private String memberSex;
    /**
     * 会员EMAIL
     * MEMBER_EMAIL
     */
    private String memberEmail;

    /**
     * 主键id
     * ID
     */
    private Long id;

    /**
     * 原单位(多选：军委、战区、陆军、海军、空军、战支、火箭、武警、联勤、院校、院所)
     * FORMER_UNIT_TYPE
     */
    private String formerUnitType;
    /**
     * 原工作属性(选项：政工、行管、技术、管理)
     * FORMER_JOB_NATURE
     */
    private String formerJobNature;

    /**
     * 双轨制(双轨制-行政、技术都可选。)
     * JOB_TWO_WAY
     */
    private String jobTwoWay;
    /**
     * 行政职务(多选：参谋、干事、助理、科级、处级、局级（缺省为无）。)
     * ADMINISTRATION_POST
     */
    private String administrationPost;
    /**
     * 技术职称等级：单选：初级职称、中级职称、高级职称（缺省为无）
     * TECHNOLOGY_POST
     */
    private String technologyPost;
    /**
     * 用户真实姓名
     * REAL_NAME
     */
    private String realName;
    /**
     * 技术职称系列：多选：工程系列、研究系列、教育系列、医疗系列（缺省为无）
     * TECHNOLOGY_SERIES
     */
    private String technologySeries;
    /**
     * 学士1院校(-必填：***院校**专业)
     * BACHELOR_MAJOR_YX_ONE
     */
    private String bachelorMajorYxOne;
    /**
     * 学士2院校(-必填：***院校**专业)
     * BACHELOR_MAJOR_YX_TWO
     */
    private String bachelorMajorYxTwo;
    /**
     * 学士1专业
     * BACHELOR_MAJOR_ZY_ONE
     */
    private String bachelorMajorZyOne;
    /**
     *学士2专业
     * BACHELOR_MAJOR_ZY_TWO
     */
    private String bachelorMajorZyTwo;
    /**
     *硕士1院校(选填)
     * MASTER_MAJOR_YX_ONE
     */
    private String masterMajorYxOne;
    /**
     * 硕士2院校(选填)
     * MASTER_MAJOR_YX_TWO
     */
    private String masterMajorYxTwo;
    /**
     * 硕士1专业(选填)
     * MASTER_MAJOR_ZY_ONE
     */
    private String masterMajorZyOne;
    /**
     * 硕士2专业(选填)
     * MASTER_MAJOR_ZY_TWO
     */
    private String masterMajorZyTwo;

    /**
     *博士1院校(选填：***院校**专业)
     * DOCTOR_MAJOR_YX_ONE
     */
    private String doctorMajorYxOne;
    /**
     * 博士1院校(选填：***院校**专业)
     * DOCTOR_MAJOR_YX_TWO
     */
    private String doctorMajorYxTwo;
    /**
     * 会员状态 REGISTER_SUCCESS:会员注册成功;VERIFY_APPLIED:会员认证申请已提交;BACKGROUND_VERIFY_FAIL:后台认证失败;VERIFY_SUCCESS:会员认证成功;
     * DOCTOR_MAJOR_ZY_ONE
     */
    private String doctorMajorZyOne;
    /**
     * 博士2专业(选填)
     * DOCTOR_MAJOR_ZY_TWO
     */
    private String doctorMajorZyTwo;
    /**
     * 博士后1院校(选填：***院校**专业)
     * POST_DOCTOR_MAJOR_YX_ONE
     */
    private String postDoctorMajorYxOne;
    /**
     *  博士后2院校(选填：***院校**专业)
     * POST_DOCTOR_MAJOR_YX_TWO
     */
    private String postDoctorMajorYxTwo;
    /**
     * 博士后1专业(选填)
     * POST_DOCTOR_MAJOR_ZY_ONE
     */
    private String postDoctorMajorZyOne;
    /**
     * 博士后2专业(选填)
     * POST_DOCTOR_MAJOR_ZY_TWO
     */
    private String postDoctorMajorZyTwo;
    /**
     * 访问学者1院校(选填：***国家**院校**专业)
     * VISITING_SCHOLAR_MAJOR_YX_ONE
     */
    private Integer visitingScholarMajorYxOne;


    /**
     * 访问学者2院校(选填：***国家**院校**专业)
     * VISITING_SCHOLAR_MAJOR_YX_TWO
     */
    private Integer visitingScholarMajorYxTwo;
    /**
     * 访问学者1专业(选填)
     * VISITING_SCHOLAR_MAJOR_ZY_ONE
     */
    private Integer visitingScholarMajorZyOne;
    /**
     * 访问学者2专业(选填)
     * VISITING_SCHOLAR_MAJOR_ZY_TWO
     */
    private Integer visitingScholarMajorZyTwo;
    /**
     * 当前工作状态: EMPLOYMENT：就业；STARTUP：创业；JOB_WAITING：待业(单选。选项：就业-填公司名-就业企业  选填，缺省为“无”创业-填公司名-创业企业 选填，缺省为“无” 待业-暂不填，后续通过会员状态更新。)
     * CURRENT_JOB_STATE
     */
    private String currentJobState;
    /**
     * 安置后（DIMISSION:安置后离职; 安置后:SELF_EMPLOYMENT：自主-单选）
     * ARRANGE_FOR_TYPE
     */
    private String arrangeForType;

    /**
     * 自主择业证号（选填）
     * SELF_EMPLOYMENT_CODE
     */
    private String selfEmploymentCode;
    /**
     * 身份证号
     * ID_NUMBER
     */
    private String idNumber;

    /**
     * 常住地（**省**市**区
     * RESIDE
     */
    private String reside;

    /**
     * 常住地（以英文逗号分隔
     * RESIDE_EN
     */
    private String resideEn;



    /**
     * 国家认证的职业资格证书名称
     * QUALIFICATIONS_NAME
     */
    private String qualificationName;



    /**
     * REMARK
     */
    private String remark;

    /**
     * 创建时间
     * CREATE_TIME
     */
    private Date createTime;
    /**
     * 更新时间
     * UPDATE_TIME
     */
    private Date updateTime;
    /**
     * 身份证正面
     */
    private String idCardBackUrl;

    /**
     * 身份证背面
     */
    private String idCardFrontUrl;
    /**
     * 择业证
     */
    private String chooseJobCertificateUrl;
    /**
     * 职业资格证
     */
    private String jobQualificationCertificateUrl;
    /**
     * 退役时履历表
     */
    private String retirementResumeCertificateUrl;


    /**
     * 职业资格证
     */
    private List<ImageVO> jobQualificationCertificateList;

    public List<ImageVO> getJobQualificationCertificateList() {
        return jobQualificationCertificateList;
    }

    public void setJobQualificationCertificateList(List<ImageVO> jobQualificationCertificateList) {
        this.jobQualificationCertificateList = jobQualificationCertificateList;
    }

    public String getInfoType() {
        return infoType;
    }

    public void setInfoType(String infoType) {
        this.infoType = infoType;
    }

    public MemberAuthenticationInfoDomain() {
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMemberSex() {
        return memberSex;
    }

    public void setMemberSex(String memberSex) {
        this.memberSex = memberSex;
    }

    public String getMemberEmail() {
        return memberEmail;
    }

    public void setMemberEmail(String memberEmail) {
        this.memberEmail = memberEmail;
    }

    public String getIdCardBackUrl() {
        return idCardBackUrl;
    }

    public void setIdCardBackUrl(String idCardBackUrl) {
        this.idCardBackUrl = idCardBackUrl;
    }

    public String getIdCardFrontUrl() {
        return idCardFrontUrl;
    }

    public void setIdCardFrontUrl(String idCardFrontUrl) {
        this.idCardFrontUrl = idCardFrontUrl;
    }

    public String getChooseJobCertificateUrl() {
        return chooseJobCertificateUrl;
    }

    public void setChooseJobCertificateUrl(String chooseJobCertificateUrl) {
        this.chooseJobCertificateUrl = chooseJobCertificateUrl;
    }

    public String getJobQualificationCertificateUrl() {
        return jobQualificationCertificateUrl;
    }

    public void setJobQualificationCertificateUrl(String jobQualificationCertificateUrl) {
        this.jobQualificationCertificateUrl = jobQualificationCertificateUrl;
    }

    public String getRetirementResumeCertificateUrl() {
        return retirementResumeCertificateUrl;
    }

    public void setRetirementResumeCertificateUrl(String retirementResumeCertificateUrl) {
        this.retirementResumeCertificateUrl = retirementResumeCertificateUrl;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getFormerUnitType() {
        return formerUnitType;
    }

    public void setFormerUnitType(String formerUnitType) {
        this.formerUnitType = formerUnitType;
    }

    public String getFormerJobNature() {
        return formerJobNature;
    }

    public void setFormerJobNature(String formerJobNature) {
        this.formerJobNature = formerJobNature;
    }


    public String getJobTwoWay() {
        return jobTwoWay;
    }

    public void setJobTwoWay(String jobTwoWay) {
        this.jobTwoWay = jobTwoWay;
    }

    public String getAdministrationPost() {
        return administrationPost;
    }

    public void setAdministrationPost(String administrationPost) {
        this.administrationPost = administrationPost;
    }

    public String getTechnologyPost() {
        return technologyPost;
    }

    public void setTechnologyPost(String technologyPost) {
        this.technologyPost = technologyPost;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getTechnologySeries() {
        return technologySeries;
    }

    public void setTechnologySeries(String technologySeries) {
        this.technologySeries = technologySeries;
    }

    public String getBachelorMajorYxOne() {
        return bachelorMajorYxOne;
    }

    public void setBachelorMajorYxOne(String bachelorMajorYxOne) {
        this.bachelorMajorYxOne = bachelorMajorYxOne;
    }

    public String getBachelorMajorYxTwo() {
        return bachelorMajorYxTwo;
    }

    public void setBachelorMajorYxTwo(String bachelorMajorYxTwo) {
        this.bachelorMajorYxTwo = bachelorMajorYxTwo;
    }

    public String getBachelorMajorZyOne() {
        return bachelorMajorZyOne;
    }

    public void setBachelorMajorZyOne(String bachelorMajorZyOne) {
        this.bachelorMajorZyOne = bachelorMajorZyOne;
    }

    public String getBachelorMajorZyTwo() {
        return bachelorMajorZyTwo;
    }

    public void setBachelorMajorZyTwo(String bachelorMajorZyTwo) {
        this.bachelorMajorZyTwo = bachelorMajorZyTwo;
    }

    public String getMasterMajorYxOne() {
        return masterMajorYxOne;
    }

    public void setMasterMajorYxOne(String masterMajorYxOne) {
        this.masterMajorYxOne = masterMajorYxOne;
    }

    public String getMasterMajorYxTwo() {
        return masterMajorYxTwo;
    }

    public void setMasterMajorYxTwo(String masterMajorYxTwo) {
        this.masterMajorYxTwo = masterMajorYxTwo;
    }

    public String getMasterMajorZyOne() {
        return masterMajorZyOne;
    }

    public void setMasterMajorZyOne(String masterMajorZyOne) {
        this.masterMajorZyOne = masterMajorZyOne;
    }

    public String getMasterMajorZyTwo() {
        return masterMajorZyTwo;
    }

    public void setMasterMajorZyTwo(String masterMajorZyTwo) {
        this.masterMajorZyTwo = masterMajorZyTwo;
    }

    public String getDoctorMajorYxOne() {
        return doctorMajorYxOne;
    }

    public void setDoctorMajorYxOne(String doctorMajorYxOne) {
        this.doctorMajorYxOne = doctorMajorYxOne;
    }

    public String getDoctorMajorYxTwo() {
        return doctorMajorYxTwo;
    }

    public void setDoctorMajorYxTwo(String doctorMajorYxTwo) {
        this.doctorMajorYxTwo = doctorMajorYxTwo;
    }

    public String getDoctorMajorZyOne() {
        return doctorMajorZyOne;
    }

    public void setDoctorMajorZyOne(String doctorMajorZyOne) {
        this.doctorMajorZyOne = doctorMajorZyOne;
    }

    public String getDoctorMajorZyTwo() {
        return doctorMajorZyTwo;
    }

    public void setDoctorMajorZyTwo(String doctorMajorZyTwo) {
        this.doctorMajorZyTwo = doctorMajorZyTwo;
    }

    public String getPostDoctorMajorYxOne() {
        return postDoctorMajorYxOne;
    }

    public void setPostDoctorMajorYxOne(String postDoctorMajorYxOne) {
        this.postDoctorMajorYxOne = postDoctorMajorYxOne;
    }

    public String getPostDoctorMajorYxTwo() {
        return postDoctorMajorYxTwo;
    }

    public void setPostDoctorMajorYxTwo(String postDoctorMajorYxTwo) {
        this.postDoctorMajorYxTwo = postDoctorMajorYxTwo;
    }

    public String getPostDoctorMajorZyOne() {
        return postDoctorMajorZyOne;
    }

    public void setPostDoctorMajorZyOne(String postDoctorMajorZyOne) {
        this.postDoctorMajorZyOne = postDoctorMajorZyOne;
    }

    public String getPostDoctorMajorZyTwo() {
        return postDoctorMajorZyTwo;
    }

    public void setPostDoctorMajorZyTwo(String postDoctorMajorZyTwo) {
        this.postDoctorMajorZyTwo = postDoctorMajorZyTwo;
    }

    public Integer getVisitingScholarMajorYxOne() {
        return visitingScholarMajorYxOne;
    }

    public void setVisitingScholarMajorYxOne(Integer visitingScholarMajorYxOne) {
        this.visitingScholarMajorYxOne = visitingScholarMajorYxOne;
    }

    public Integer getVisitingScholarMajorYxTwo() {
        return visitingScholarMajorYxTwo;
    }

    public void setVisitingScholarMajorYxTwo(Integer visitingScholarMajorYxTwo) {
        this.visitingScholarMajorYxTwo = visitingScholarMajorYxTwo;
    }

    public Integer getVisitingScholarMajorZyOne() {
        return visitingScholarMajorZyOne;
    }

    public void setVisitingScholarMajorZyOne(Integer visitingScholarMajorZyOne) {
        this.visitingScholarMajorZyOne = visitingScholarMajorZyOne;
    }

    public Integer getVisitingScholarMajorZyTwo() {
        return visitingScholarMajorZyTwo;
    }

    public void setVisitingScholarMajorZyTwo(Integer visitingScholarMajorZyTwo) {
        this.visitingScholarMajorZyTwo = visitingScholarMajorZyTwo;
    }

    public String getCurrentJobState() {
        return currentJobState;
    }

    public void setCurrentJobState(String currentJobState) {
        this.currentJobState = currentJobState;
    }

    public String getArrangeForType() {
        return arrangeForType;
    }

    public void setArrangeForType(String arrangeForType) {
        this.arrangeForType = arrangeForType;
    }

    public String getSelfEmploymentCode() {
        return selfEmploymentCode;
    }

    public void setSelfEmploymentCode(String selfEmploymentCode) {
        this.selfEmploymentCode = selfEmploymentCode;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getReside() {
        return reside;
    }

    public void setReside(String reside) {
        this.reside = reside;
    }

    public String getResideEn() {
        return resideEn;
    }

    public void setResideEn(String resideEn) {
        this.resideEn = resideEn;
    }

    public String getQualificationName() {
        return qualificationName;
    }

    public void setQualificationName(String qualificationName) {
        this.qualificationName = qualificationName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
