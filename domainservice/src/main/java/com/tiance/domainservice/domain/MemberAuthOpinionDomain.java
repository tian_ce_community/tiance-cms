package com.tiance.domainservice.domain;

import com.tiance.vo.BaseVO;

import java.util.Date;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：会员认证意见表
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class MemberAuthOpinionDomain extends BaseDomain{

    /**
     * ID
     */
    private Long id;


    /**
     * 电话
     * PHONE
     */
    private String phone;

    /**
     * 会员性别 MALE:男,FEMALE:女
     * MEMBER_SEX
     */
    private String memberSex;
    /**
     * 会员EMAIL
     * MEMBER_EMAIL
     */
    private String memberEmail;
    /**
     * 关注时间
     * SUBSCRIBE_TIME
     */
    private Date subscribeTime;


    /**
     * 审核认证人员ID
     * AUTH_USER_ID
     */
    private Long authUserId;

    /**
     * 认证信息ID
     */
    private Long authenticationInfoId;
    /**
     * 审核认证人员姓名
     * AUTH_USER_NAME
     */
    private String authUserName;
    /**
     * 审核认证人员意见
     * AUTH_OPINION
     */
    private String authOpinion1;

    private String authOpinion2;

    private String authOpinion3;

    /**
     * 审核状态(ID_CARD_VERIFY_SUCCESS、身份证认证成功;ID_CARD_VERIFY_FAIL:身份证认证失败;VERIFY_SUCCESS:会员认证成功;VERIFY_FAIL:会员认证失败)
     * AUTH_STATE
     */
    private String authState;

    private String authState1;

    private String authState2;

    private String authState3;

    /**
     * 审核微文地址
     * AUTH_WX_URL
     */
    private String authWxUrl;
    /**
     * 创建时间
     * CREATE_TIME
     */
    private Date createTime;

    public Long getAuthenticationInfoId() {
        return authenticationInfoId;
    }

    public void setAuthenticationInfoId(Long authenticationInfoId) {
        this.authenticationInfoId = authenticationInfoId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAuthUserId() {
        return authUserId;
    }

    public void setAuthUserId(Long authUserId) {
        this.authUserId = authUserId;
    }

    public String getAuthUserName() {
        return authUserName;
    }

    public void setAuthUserName(String authUserName) {
        this.authUserName = authUserName;
    }

    public String getAuthOpinion1() {
        return authOpinion1;
    }

    public void setAuthOpinion1(String authOpinion1) {
        this.authOpinion1 = authOpinion1;
    }

    public String getAuthOpinion2() {
        return authOpinion2;
    }

    public void setAuthOpinion2(String authOpinion2) {
        this.authOpinion2 = authOpinion2;
    }

    public String getAuthOpinion3() {
        return authOpinion3;
    }

    public void setAuthOpinion3(String authOpinion3) {
        this.authOpinion3 = authOpinion3;
    }

    public String getAuthState1() {
        return authState1;
    }

    public void setAuthState1(String authState1) {
        this.authState1 = authState1;
    }

    public String getAuthState2() {
        return authState2;
    }

    public void setAuthState2(String authState2) {
        this.authState2 = authState2;
    }

    public String getAuthState3() {
        return authState3;
    }

    public void setAuthState3(String authState3) {
        this.authState3 = authState3;
    }

    public String getAuthState() {
        return authState;
    }

    public void setAuthState(String authState) {
        this.authState = authState;
    }

    public String getAuthWxUrl() {
        return authWxUrl;
    }

    public void setAuthWxUrl(String authWxUrl) {
        this.authWxUrl = authWxUrl;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMemberSex() {
        return memberSex;
    }

    public void setMemberSex(String memberSex) {
        this.memberSex = memberSex;
    }

    public String getMemberEmail() {
        return memberEmail;
    }

    public void setMemberEmail(String memberEmail) {
        this.memberEmail = memberEmail;
    }

    public Date getSubscribeTime() {
        return subscribeTime;
    }

    public void setSubscribeTime(Date subscribeTime) {
        this.subscribeTime = subscribeTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
