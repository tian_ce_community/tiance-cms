package com.tiance.domainservice.domain;

import com.tiance.enums.IntegrationRuleCodeEnum;

import java.util.Date;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/4
 *
 * Description：
 *
 * Modification History:
 *
 */
public class IntegrationDomain extends BaseDomain{

    /**
     * 主键id
     * ID
     */
    private Long                    id;

    private IntegrationRuleCodeEnum integrationRuleCode;
    /**
     * 类型(INTEGRATION:积分，CREDIT:信用)
     * INTEGRATION_TYPE
     *
     */
    private String                  integrationType;
    /**
     * 操作类型（ADD:加;SUBTRACT:减
     * OPERATE_TYPE
     */
    private String                  operateType;

    /**
     * 归属人
     *
     MEMBER("MEMBER", "会员本人"),
     REFERRER("REFERRER", "推荐人"),
     ACTIVITY_ORGANIZER("ACTIVITY_ORGANIZER", "活动举办者"),
     ACTIVITY_OPARTICIPATOR("ACTIVITY_OPARTICIPATOR", "活动参加者"),
     DUTY_MANAGER("DUTY_MANAGER", "值班经理"),
     DUTY_APPLICANT("DUTY_APPLICANT", "值班申请者"),
     APPLICANT("APPLICANT", "申请者"),
     RESOURCE_USE_APPLICANT("RESOURCE_USE_APPLICANT", "资源使用申请者"),
     RESOURCE_PROVIDE_APPLICANT("RESOURCE_PROVIDE_APPLICANT", "资源提供申请者"),
     * BELONG_TYPT
     */
    private String                  belongType;
    /**
     * 分值或信用点
     * VALUE
     */
    private Integer                 integrationValue;
    /**
     * 创建时间
     * CREATE_TIME
     */
    private Date                    createTime;

    public String getBelongType() {
        return belongType;
    }

    public void setBelongType(String belongType) {
        this.belongType = belongType;
    }

    public IntegrationRuleCodeEnum getIntegrationRuleCode() {
        return integrationRuleCode;
    }

    public void setIntegrationRuleCode(IntegrationRuleCodeEnum integrationRuleCode) {
        this.integrationRuleCode = integrationRuleCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getIntegrationType() {
        return integrationType;
    }

    public void setIntegrationType(String integrationType) {
        this.integrationType = integrationType;
    }

    public String getOperateType() {
        return operateType;
    }

    public void setOperateType(String operateType) {
        this.operateType = operateType;
    }

    public Integer getIntegrationValue() {
        return integrationValue;
    }

    public void setIntegrationValue(Integer integrationValue) {
        this.integrationValue = integrationValue;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
