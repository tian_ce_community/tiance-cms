package com.tiance.domainservice.domain;

import java.util.Date;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/4
 *
 * Description：
 *
 * Modification History:
 *
 */
public class ActivityDomain extends BaseDomain{

    /**
     * ID
     */
    private Long id;
    /**
     * 申请人
     * APPLICANT_ID
     */
    private Long applicantId;
    /**
     * 申请人
     *
     */
    private String applicantName;
    /**
     * 审批人
     * APPROVAL_ID
     */
    private Long approvalId;
    /**
     * 联系人
     * CONTACTS_ID
     */
    private Long contactsId;
    /**
     * 活动名称
     * ACTIVITY_NAME
     */
    private String activityName;

    /**
     * 活动最大人数
     * ACTIVITY_MAX_NUM
     */
    private Integer activityMaxNum;
    /**
     * 活动积分
     * ACTIVITY_INTEGRAL
     */
    private Integer activityIntegral;
    /**
     * 活动报名人数
     */
    private Integer activitySignUpCount;

    /**
     * 活动签到
     */
    private Integer activitySignInCount;

    /**
     * APPLICANT_TYPE
     * 申请人类型（SYSTEM:系统;MEMBER:会员）
     */
    private String applicantType;
    /**
     * ACTIVITY_STATUS
     * 活动状态:(UN_AUDIT:申请待审核;APPLY_CANCEL:申请取消;AUDIT_PASS:申请审核通过;AUDIT_FAIL:申请审核不通过;ACTIVITY_SIGN_UP:活动报名中;ACTIVITY_IN_PROGRESS:活动进行中;ACTIVITY_CANCEL:活动取消;ACTIVITY_FINISHED:活动结束;ACTIVITY_APPRECIATED:活动已打赏)
     */
    private String activityStatus;
    /**
     * 联系人电话
     * CONTACTS_PHONE
     */
    private String contactsPhone;
    /**
     * 活动微文链接
     * ACTIVITY_URL
     */
    private String activityUrl;
    /**
     * 活动地址
     * ACTIVITY_ADDRESS
     */
    private String activityAddress;
    /**
     * ACTIVITY_COORDINATE
     */
    private String activityCoordinate;
    /**
     * REMARK
     */
    private String remark;
    /**
     * UPDATE_USER
     */
    private Long updateUser;
    /**
     * CREATE_USER
     */
    private Long createUser;
    /**
     * CREATE_TIME
     */
    private Date createTime;
    /**
     * UPDATE_TIME
     */
    private Date updateTime;

    /**
     * 活动开始时间
     * START_TIME
     */
    private Date startTime;
    /**
     * 活动结束时间
     * END_TIME
     */
    private Date endTime;

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    public Long getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Long updateUser) {
        this.updateUser = updateUser;
    }

    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(Long applicantId) {
        this.applicantId = applicantId;
    }

    public Long getApprovalId() {
        return approvalId;
    }

    public void setApprovalId(Long approvalId) {
        this.approvalId = approvalId;
    }

    public Long getContactsId() {
        return contactsId;
    }

    public void setContactsId(Long contactsId) {
        this.contactsId = contactsId;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public Integer getActivityMaxNum() {
        return activityMaxNum;
    }

    public void setActivityMaxNum(Integer activityMaxNum) {
        this.activityMaxNum = activityMaxNum;
    }

    public Integer getActivityIntegral() {
        return activityIntegral;
    }

    public void setActivityIntegral(Integer activityIntegral) {
        this.activityIntegral = activityIntegral;
    }

    public Integer getActivitySignUpCount() {
        return activitySignUpCount;
    }

    public void setActivitySignUpCount(Integer activitySignUpCount) {
        this.activitySignUpCount = activitySignUpCount;
    }

    public Integer getActivitySignInCount() {
        return activitySignInCount;
    }

    public void setActivitySignInCount(Integer activitySignInCount) {
        this.activitySignInCount = activitySignInCount;
    }

    public String getApplicantType() {
        return applicantType;
    }

    public void setApplicantType(String applicantType) {
        this.applicantType = applicantType;
    }

    public String getActivityStatus() {
        return activityStatus;
    }

    public void setActivityStatus(String activityStatus) {
        this.activityStatus = activityStatus;
    }

    public String getContactsPhone() {
        return contactsPhone;
    }

    public void setContactsPhone(String contactsPhone) {
        this.contactsPhone = contactsPhone;
    }

    public String getActivityUrl() {
        return activityUrl;
    }

    public void setActivityUrl(String activityUrl) {
        this.activityUrl = activityUrl;
    }

    public String getActivityAddress() {
        return activityAddress;
    }

    public void setActivityAddress(String activityAddress) {
        this.activityAddress = activityAddress;
    }

    public String getActivityCoordinate() {
        return activityCoordinate;
    }

    public void setActivityCoordinate(String activityCoordinate) {
        this.activityCoordinate = activityCoordinate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
