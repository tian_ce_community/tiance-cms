package com.tiance.domainservice.domain;

import java.util.Date;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：须知表
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class NoticeDomain extends BaseDomain{

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;

    /**
     * NOTICE_CONTENT
     */
    private String noticeContent;

    /**
     * 更新用户
     */
    private String updateUserName;
    /**
     * NOTICE_TYPE 须知类型(NORMAL_MEMBER:普通会员:VERIFIED_MEMBER:认证会员)
     */
    private String noticeType;

    private String remark;
    /**
     * UPDATE_USER
     */
    private Long updateUser;
    /**
     * CREATE_USER
     */
    private Long createUser;
    /**
     * CREATE_TIME
     */
    private Date createTime;
    /**
     * UPDATE_TIME
     */
    private Date updateTime;

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNoticeContent() {
        return noticeContent;
    }

    public void setNoticeContent(String noticeContent) {
        this.noticeContent = noticeContent;
    }

    public String getNoticeType() {
        return noticeType;
    }

    public void setNoticeType(String noticeType) {
        this.noticeType = noticeType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Long updateUser) {
        this.updateUser = updateUser;
    }

    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
