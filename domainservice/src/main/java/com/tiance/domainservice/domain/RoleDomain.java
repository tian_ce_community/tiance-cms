package com.tiance.domainservice.domain;

import java.util.Date;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：角色表
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class RoleDomain extends BaseDomain {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     * ID
     */
    private Long id;
    /**
     * 角色名称
     * ROLE_NAME
     */
    private String roleName;
    /**
     * 角色CODE
     */
    private String roleCode;

    /**
     * 备注
     * REMARK
     */
    private String remark;
    /**
     * 创建时间
     * CREATE_TIME
     */
    private Date createTime;
    private Date updateTime;

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }


    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
