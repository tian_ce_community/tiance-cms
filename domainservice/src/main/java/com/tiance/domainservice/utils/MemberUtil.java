package com.tiance.domainservice.utils;

import com.tiance.constants.CommonConstants;
import com.tiance.dal.dao.CommonDAO;
import com.tiance.dal.dataobject.CommonDO;
import com.tiance.dal.dataobject.IntegrationRuleDO;
import com.tiance.dal.dataobject.MemberDO;
import com.tiance.enums.IntegrationTypeEnum;
import com.tiance.integration.ProxyBeansFactory;
import com.tiance.integration.config.CommonProperties;
import com.tiance.utils.DataUtil;
import com.tiance.utils.StringUtil;

import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/15
 *
 * Description：
 *
 * Modification History:
 *
 */
public class MemberUtil {
    public static final ReentrantLock mainLock = new ReentrantLock();

    protected static CommonDAO commonDAO = ProxyBeansFactory.getInstance().getBean(CommonDAO.class);

    public static MemberDO changeIntegration(MemberDO oldMemberDO, String integrationType, IntegrationRuleDO integrationRuleDO ){
        MemberDO newMemberDO=new MemberDO();
        newMemberDO.setId(oldMemberDO.getId());
        if(IntegrationTypeEnum.INTEGRATION.getCode().equals(integrationType)){
            //todo 需要优化
            Integer newIntegration= DataUtil
                    .calculateTotalIntegral(oldMemberDO.getTotalIntegral(),integrationRuleDO.getIntegrationValue()*integrationRuleDO.getIntegrationDegree());
            newMemberDO.setTotalIntegral(newIntegration);
        }else if(IntegrationTypeEnum.CREDIT.getCode().equals(integrationType)){
            //todo 需要优化
            Integer newCredit= DataUtil.calculateTotalCredit(oldMemberDO.getTotalCredit(),integrationRuleDO.getCreditValue()*integrationRuleDO.getIntegrationDegree());
            newMemberDO.setTotalCredit(newCredit);
        }

        return newMemberDO;
    }

    public static MemberDO changeIntegration2(MemberDO oldMemberDO,Integer changeIntegrationValue,Integer changeCreditValue ){
        MemberDO newMemberDO=new MemberDO();
        newMemberDO.setId(oldMemberDO.getId());

            Integer newIntegration= DataUtil.calculateTotalIntegral(oldMemberDO.getTotalIntegral(),changeIntegrationValue);
            newMemberDO.setTotalIntegral(newIntegration);
            Integer newCredit= DataUtil.calculateTotalCredit(oldMemberDO.getTotalCredit(),changeCreditValue);
            newMemberDO.setTotalCredit(newCredit);


        return newMemberDO;
    }

    public static String generateMemberNo(){
        String memberNo = "V";
        try {
            mainLock.lock();
            CommonDO commonDO = commonDAO.queryById(CommonConstants.COMMON_ID);
            int len = commonDO.getMemberNo().toString().length();
            int len0;
            if (len < CommonConstants.MEMBER_NO_LEN) {
                len0 = CommonConstants.MEMBER_NO_LEN - len;
            } else {
                len0 = len;
            }
            memberNo = memberNo + StringUtil.generate0(len0) + commonDO.getMemberNo();
            CommonDO commonDO1 = new CommonDO();
            commonDO1.setId(CommonConstants.COMMON_ID);
            commonDO1.setMemberNo(commonDO.getMemberNo() + 1);
            commonDAO.update(commonDO1);
        }finally {
            mainLock.unlock();
        }
        return memberNo;
    }

    public static String generateMemberCardNo() {
        String memberCardNo="C";
        try {
            mainLock.lock();
            CommonDO commonDO = commonDAO.queryById(CommonConstants.COMMON_ID);
            int len = commonDO.getMemberCardNo().toString().length();
            int len0;
            if (len < CommonConstants.MEMBER_NO_LEN) {
                len0 = CommonConstants.MEMBER_NO_LEN - len;
            } else {
                len0 = len;
            }
            memberCardNo = memberCardNo + StringUtil.generate0(len0) + commonDO.getMemberCardNo();
            CommonDO commonDO1 = new CommonDO();
            commonDO1.setId(CommonConstants.COMMON_ID);
            commonDO1.setMemberCardNo(commonDO.getMemberCardNo() + 1);
            commonDAO.update(commonDO1);
        }finally {
            mainLock.unlock();
        }
        return memberCardNo;
    }
}
