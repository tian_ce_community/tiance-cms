
package com.tiance.domainservice.log.factory;


import com.tiance.dal.dataobject.LoginLogDO;
import com.tiance.dal.dataobject.OperationLogDO;
import com.tiance.enums.LogSucceedEnum;
import com.tiance.enums.LogTypeEnum;

import java.util.Date;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：日志对象创建工厂
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class LogFactory {

    /**
     * 创建操作日志
     */
    public static OperationLogDO createOperationLog(LogTypeEnum logType, Long userId, String bussinessName, String clazzName, String methodName, String msg, LogSucceedEnum succeed) {
        OperationLogDO operationLog = new OperationLogDO();
        operationLog.setLogType(logType.getMessage());
        operationLog.setLogName(bussinessName);
        operationLog.setUserId(userId);
        operationLog.setClassName(clazzName);
        operationLog.setMethod(methodName);
        operationLog.setCreateTime(new Date());
        operationLog.setSucceed(succeed.getMessage());
        operationLog.setMessage(msg);
        return operationLog;
    }

    /**
     * 创建登录日志
     */
    public static LoginLogDO createLoginLog(LogTypeEnum logType, Long userId, String msg, String ip) {
        LoginLogDO loginLog = new LoginLogDO();
        loginLog.setLogName(logType.getMessage());
        loginLog.setUserId(userId);
        loginLog.setCreateTime(new Date());
        loginLog.setSucceed(LogSucceedEnum.SUCCESS.getMessage());
        loginLog.setIpAddress(ip);
        loginLog.setMessage(msg);
        return loginLog;
    }
}
