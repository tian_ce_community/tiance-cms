
package com.tiance.domainservice.log.factory;

import com.tiance.common.ServletContextHolder;
import com.tiance.dal.dataobject.LoginLogDO;
import com.tiance.dal.dataobject.OperationLogDO;
import com.tiance.dal.mapper.LoginLogDOMapper;
import com.tiance.dal.mapper.OperationLogDOMapper;
import com.tiance.enums.LogSucceedEnum;
import com.tiance.enums.LogTypeEnum;
import com.tiance.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.TimerTask;

/**
 * 日志操作任务创建工厂
 *
 * @author fengshuonan
 * @date 2016年12月6日 下午9:18:27
 */
public class LogTaskFactory {

    private static Logger               logger             = LoggerFactory.getLogger(LogTaskFactory.class);
    private static LoginLogDOMapper     loginLogMapper     = ServletContextHolder.getBean(LoginLogDOMapper.class);
    private static OperationLogDOMapper operationLogMapper = ServletContextHolder.getBean(OperationLogDOMapper.class);

    public static TimerTask loginLog(final Long userId, final String ip) {
        return new TimerTask() {
            @Override
            public void run() {
                try {
                    LoginLogDO loginLog = LogFactory.createLoginLog(LogTypeEnum.LOGIN, userId, null, ip);
                    loginLogMapper.insert(loginLog);
                } catch (Exception e) {
                    logger.error("创建登录日志异常!", e);
                }
            }
        };
    }

    public static TimerTask loginLog(final String username, final String msg, final String ip) {
        return new TimerTask() {
            @Override
            public void run() {
                LoginLogDO loginLog = LogFactory.createLoginLog(
                        LogTypeEnum.LOGIN_FAIL, null, "账号:" + username + "," + msg, ip);
                try {
                    loginLogMapper.insert(loginLog);
                } catch (Exception e) {
                    logger.error("创建登录失败异常!", e);
                }
            }
        };
    }

    public static TimerTask exitLog(final Long userId, final String ip) {
        return new TimerTask() {
            @Override
            public void run() {
                LoginLogDO loginLog = LogFactory.createLoginLog(LogTypeEnum.EXIT, userId, null, ip);
                try {
                    loginLogMapper.insert(loginLog);
                } catch (Exception e) {
                    logger.error("创建退出日志异常!", e);
                }
            }
        };
    }

    public static TimerTask bussinessLog(final Long userId, final String bussinessName, final String clazzName, final String methodName, final String msg) {
        return new TimerTask() {
            @Override
            public void run() {
                OperationLogDO operationLog = LogFactory.createOperationLog(
                        LogTypeEnum.BUSSINESS, userId, bussinessName, clazzName, methodName, msg, LogSucceedEnum.SUCCESS);
                try {
                    operationLogMapper.insert(operationLog);
                } catch (Exception e) {
                    logger.error("创建业务日志异常!", e);
                }
            }
        };
    }

    public static TimerTask exceptionLog(final Long userId, final Exception exception) {
        return new TimerTask() {
            @Override
            public void run() {
                String msg = StringUtil.getExceptionMsg(exception);
                OperationLogDO operationLog = LogFactory.createOperationLog(
                        LogTypeEnum.EXCEPTION, userId, "", null, null, msg, LogSucceedEnum.FAIL);
                try {
                    operationLogMapper.insert(operationLog);
                } catch (Exception e) {
                    logger.error("创建异常日志异常!", e);
                }
            }
        };
    }
}
