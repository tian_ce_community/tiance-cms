package com.tiance.base;

import org.apache.shiro.authc.UsernamePasswordToken;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/31
 *
 * Description：
 *
 * Modification History:
 *
 */
public class LoginToken extends UsernamePasswordToken {

    public LoginToken() {
        super();
    }

    public LoginToken(String username, String password) {
        super(username, password);
    }

    public LoginToken(String username, String password, boolean rememberMe, String host) {
        super(username, password, rememberMe, host);
    }
}
