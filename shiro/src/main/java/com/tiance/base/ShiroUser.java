package com.tiance.base;

import java.io.Serializable;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/31
 *
 * Description：
 *
 * Modification History:
 *
 */
public class ShiroUser implements Serializable{

    private Long id;
    private String userName;
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
