package com.tiance.shiro.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：
 *
 * Modification History:
 *
 */
public class MyHandlerExceptionResolver implements HandlerExceptionResolver {

    /** logger */
    private static final Logger logger = LoggerFactory.getLogger(MyHandlerExceptionResolver.class);

    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        logger.info("--进入自定义异常处理-MyHandlerExceptionResolver-");
        ModelAndView mv = new ModelAndView("redirect:/home/index");
        request.setAttribute("msg", "错大师傅");
//      logger.error("MyHandlerExceptionResolver-resolveException-发生IO异常");
        return mv;
    }
}
