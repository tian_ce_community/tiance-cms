package com.tiance.shiro.filter;

import com.tiance.base.LoginToken;
import com.tiance.exception.BusinessException;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.apache.shiro.web.util.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：
 *
 * Modification History:
 *
 */
public class LoginAuthenticationFilter extends FormAuthenticationFilter {
    private static final Logger log = LoggerFactory.getLogger(LoginAuthenticationFilter.class);

    /** logger */

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        String prefix = "LoginAuthenticationFilter-onAccessDenied--->";

        String requestURI = WebUtils.getPathWithinApplication(WebUtils.toHttp(request));
        log.info(prefix + "获取到请求requestURI："+requestURI);

        if (isLoginRequest(request, response)) {//判断请求是否是登录请求
            if (!isLoginSubmission(request, response)) {//判断请求是否是post方法
                //如果是get方法则会返回true,跳转到登陆页面
                return true;
            }else{
                // 校验验证码
                // 从session获取正确的验证码
                HttpSession session = ((HttpServletRequest)request).getSession();
                //页面输入的验证码
                String randomcode = request.getParameter("validateCode");
                //从session中取出验证码
                String validateCode = (String) session.getAttribute("_code");
                log.info("获取到页面验证码："+randomcode + ", session中验证码: "+validateCode);
                if (randomcode!=null && validateCode!=null) {
                    if (!randomcode.toLowerCase().equals(validateCode)) {
                        // randomCodeError表示验证码错误
                        request.setAttribute("shiroLoginFailure", "kaptchaValidateFailed");
                       // throw new BusinessException("验证码错误");
                        //拒绝访问，不再校验账号和密码
                    }
                }
                return true;

            }
        }


        //
        if (!isLoginRequest(request, response)) {
            log.info(prefix + "获取到请求 不满足isLoginRequest，不是登录请求");
            if (log.isTraceEnabled()) {
                log.trace("Attempting to access a path which requires authentication.  Forwarding to the " +
                        "Authentication url [" + getLoginUrl() + "]");
            }

            HttpServletRequest req = (HttpServletRequest) request;
            HttpServletResponse res = (HttpServletResponse) response;

            // 如果是ajax请求响应头会有，x-requested-with 在响应头设置session状态
            if (req.getHeader("x-requested-with") != null
                    && "XMLHttpRequest".equalsIgnoreCase(req.getHeader("x-requested-with"))) {
                res.setHeader("sessionstatus", "timeout");
            } else {
                saveRequestAndRedirectToLogin(request, response);
            }
            return false;
        }

        return super.onAccessDenied(request, response);
    }




    @Override
    protected LoginToken createToken(ServletRequest request,
                                     ServletResponse response) {
        String username = getUsername(request);
        String password = getPassword(request);
        boolean rememberMe = isRememberMe(request);
        String host = getHost(request);
        LoginToken oginToken = new LoginToken(username, password, rememberMe, host);
        return oginToken;
    }
}
