package com.tiance.shiro.filter;

import com.tiance.dal.dataobject.UserDO;
import com.tiance.domainservice.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.web.filter.PathMatchingFilter;
import org.apache.shiro.web.util.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 * 该过滤器用于在过滤器链的最后，对当前登录用户信息存入session和request中
 * Description：
 *
 * Modification History:
 *
 */

public class FrontSessionFilter extends PathMatchingFilter {

    /** logger */
    private static final Logger logger = LoggerFactory.getLogger(FrontSessionFilter.class);


    @Override
    protected boolean onPreHandle(ServletRequest request, ServletResponse response, Object mappedValue)

            throws Exception {
        Session session = SecurityUtils.getSubject().getSession();
        Object openId=session.getAttribute("openid");
        if(null==openId){
            // 清除登录前请求路径
            WebUtils.getAndClearSavedRequest(request);
            WebUtils.redirectToSavedRequest(request, response, "/front/init.action");
        }
        return false;
    }




}
