package shiro;

import com.tiance.dal.dataobject.UserDO;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/1
 *
 * Description：
 *
 * Modification History:
 *
 */
public class PasswordHelper {
    private static String algorithmName = "MD5";
    private static int hashIterations = 1024;

    public static UserDO encryptPassword(UserDO user) {
        String newPassword = new SimpleHash(algorithmName, user.getPassWord(),  ByteSource.Util.bytes(user.getUserName()), hashIterations).toHex();
        user.setPassWord(newPassword);
        return user;

    }

    public static String encryptPassword(String userName,String password) {
        String newPassword = new SimpleHash(algorithmName, password,  ByteSource.Util.bytes(userName), hashIterations).toHex();
        return newPassword;

    }
    public static void main(String[] args) {
        PasswordHelper passwordHelper = new PasswordHelper();
        UserDO user = new UserDO();
        user.setUserName("admin");
        user.setPassWord("nstiptv");
        passwordHelper.encryptPassword(user);
        System.out.println(user);
    }

}
