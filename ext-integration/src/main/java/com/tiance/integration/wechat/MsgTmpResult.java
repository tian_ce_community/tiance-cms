package com.tiance.integration.wechat;

import java.io.Serializable;

/**
 * ${DESCRIPTION} 发送模板消息回调
 *
 * @version v 0.1 2017/6/28 上午11:36 Exp $$
 * @AUTHOR hmz
 */


public class MsgTmpResult extends WxBaseResult  {

    /**
     * errcode : 0
     * errmsg : ok
     * msgid : 200228332
     */


    private String msgid;


    public String getMsgid() {
        return msgid;
    }

    public void setMsgid(String msgid) {
        this.msgid = msgid;
    }

    @Override
    public String toString() {
        return "MsgTmpResult{" +
                "msgid=" + msgid +
                "} " + super.toString();
    }
}
