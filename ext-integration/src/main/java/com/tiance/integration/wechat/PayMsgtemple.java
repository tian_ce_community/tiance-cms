package com.tiance.integration.wechat;

import com.alibaba.fastjson.JSON;

import java.io.Serializable;

/**
 * ${DESCRIPTION} 支付的消息模板
 *
 * @version v 0.1 2017/6/28 下午2:04 Exp $$
 * @AUTHOR hmz
 */


public class PayMsgtemple implements Serializable{


    public static String    first="first";
    public static String keyword1="keyword1";
    public static String keyword2="keyword2";
    public static String keyword3="keyword3";
    public static String  keyword4="keyword4";
    public static String  remark="remark";
    private  String    wxTplId;
    private  String    appId;
    private  String    openId;
    private  String    jumpUrl;

    public String getWxTplId() {
        return wxTplId;
    }

    public void setWxTplId(String wxTplId) {
        this.wxTplId = wxTplId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getJumpUrl() {
        return jumpUrl;
    }

    public void setJumpUrl(String jumpUrl) {
        this.jumpUrl = jumpUrl;
    }

    /**
     * touser : OPENID
     * template_id : ngqIpbwh8bUfcSsECmogfXcV14J0tQlEpBO27izEYtY
     * url : http://weixin.qq.com/download
     * miniprogram : {"appid":"xiaochengxuappid12345","pagepath":"index?foo=bar"}
     * data : {"first":{"value":"恭喜你购买成功！","color":"#173177"},"keyword1":{"value":"巧克力","color":"#173177"},"keyword2":{"value":"39.8元","color":"#173177"},"keyword3":{"value":"2014年9月22日","color":"#173177"},"keyword4":{"value":"交易号","color":"#173177"},"remark":{"value":"欢迎再次购买！","color":"#173177"}}
     */

    private String touser;
    private String template_id;
    private String url;
    private MiniprogramBean miniprogram;
    private DataBean data;

    public PayMsgtemple() {
    }

    public PayMsgtemple(String touser, String template_id, String url, MiniprogramBean miniprogram, DataBean data) {
        this.touser = touser;
        this.template_id = template_id;
        this.url = url;
        this.miniprogram = miniprogram;
        this.data = data;
    }

    public String getTouser() {
        return touser;
    }

    public void setTouser(String touser) {
        this.touser = touser;
    }

    public String getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(String template_id) {
        this.template_id = template_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public MiniprogramBean getMiniprogram() {
        return miniprogram;
    }

    public void setMiniprogram(MiniprogramBean miniprogram) {
        this.miniprogram = miniprogram;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class MiniprogramBean implements Serializable {
        public MiniprogramBean() {
        }

        public MiniprogramBean(String appid, String pagepath) {
            this.appid = appid;
            this.pagepath = pagepath;
        }

        /**
         * appid : xiaochengxuappid12345
         * pagepath : index?foo=bar
         */

        private String appid;
        private String pagepath;

        public String getAppid() {
            return appid;
        }

        public void setAppid(String appid) {
            this.appid = appid;
        }

        public String getPagepath() {
            return pagepath;
        }

        public void setPagepath(String pagepath) {
            this.pagepath = pagepath;
        }
    }

    public static class DataBean implements Serializable {

        public DataBean(FirstBean first, Keyword1Bean keyword1, Keyword2Bean keyword2, Keyword3Bean keyword3, Keyword4Bean keyword4, RemarkBean remark) {
            this.first = first;
            this.keyword1 = keyword1;
            this.keyword2 = keyword2;
            this.keyword3 = keyword3;
            this.keyword4 = keyword4;
            this.remark = remark;
        }

        /**
         * first : {"value":"恭喜你购买成功！","color":"#173177"}
         * keyword1 : {"value":"巧克力","color":"#173177"}
         * keyword2 : {"value":"39.8元","color":"#173177"}
         * keyword3 : {"value":"2014年9月22日","color":"#173177"}
         * keyword4 : {"value":"交易号","color":"#173177"}
         * remark : {"value":"欢迎再次购买！","color":"#173177"}
         */



        private FirstBean first;
        private Keyword1Bean keyword1;
        private Keyword2Bean keyword2;
        private Keyword3Bean keyword3;
        private Keyword4Bean keyword4;
        private RemarkBean remark;

        @Override
        public String toString() {
            return "{" +
                    "first=" + first +
                    ", keyword1=" + keyword1 +
                    ", keyword2=" + keyword2 +
                    ", keyword3=" + keyword3 +
                    ", keyword4=" + keyword4 +
                    ", remark=" + remark +
                    '}';
        }

        public FirstBean getFirst() {
            return first;
        }

        public void setFirst(FirstBean first) {
            this.first = first;
        }

        public Keyword1Bean getKeyword1() {
            return keyword1;
        }

        public void setKeyword1(Keyword1Bean keyword1) {
            this.keyword1 = keyword1;
        }

        public Keyword2Bean getKeyword2() {
            return keyword2;
        }

        public void setKeyword2(Keyword2Bean keyword2) {
            this.keyword2 = keyword2;
        }

        public Keyword3Bean getKeyword3() {
            return keyword3;
        }

        public void setKeyword3(Keyword3Bean keyword3) {
            this.keyword3 = keyword3;
        }

        public Keyword4Bean getKeyword4() {
            return keyword4;
        }

        public void setKeyword4(Keyword4Bean keyword4) {
            this.keyword4 = keyword4;
        }

        public RemarkBean getRemark() {
            return remark;
        }

        public void setRemark(RemarkBean remark) {
            this.remark = remark;
        }

        public static class FirstBean implements Serializable{
            public FirstBean(String value, String color) {
                this.value = value;
                this.color = color;
            }

            /**
             * value : 恭喜你购买成功！
             * color : #173177
             */


            private String value;
            private String color;


            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getColor() {
                return color;
            }

            public void setColor(String color) {
                this.color = color;
            }

            @Override
            public String toString() {
                return JSON.toJSONString(this);
            }
        }

        public static class Keyword1Bean implements Serializable {
            /**
             * value : 巧克力
             * color : #173177
             */

            private String value;
            private String color;

            public Keyword1Bean(String value, String color) {
                this.value = value;
                this.color = color;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getColor() {
                return color;
            }

            public void setColor(String color) {
                this.color = color;
            }

            @Override
            public String toString() {
                 return JSON.toJSONString(this);
            }
        }

        public static class Keyword2Bean implements Serializable{
            /**
             * value : 39.8元
             * color : #173177
             */

            private String value;
            private String color;

            public Keyword2Bean(String value, String color) {
                this.value = value;
                this.color = color;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getColor() {
                return color;
            }

            public void setColor(String color) {
                this.color = color;
            }

            @Override
            public String toString() {
                return JSON.toJSONString(this);
            }
        }

        public static class Keyword3Bean implements Serializable{
            /**
             * value : 2014年9月22日
             * color : #173177
             */

            private String value;
            private String color;

            public Keyword3Bean(String value, String color) {
                this.value = value;
                this.color = color;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getColor() {
                return color;
            }

            public void setColor(String color) {
                this.color = color;
            }

            @Override
            public String toString() {
                return JSON.toJSONString(this);
            }
        }

        public static class Keyword4Bean implements Serializable{
            /**
             * value : 交易号
             * color : #173177
             */

            private String value;
            private String color;

            public Keyword4Bean(String value, String color) {
                this.value = value;
                this.color = color;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getColor() {
                return color;
            }

            public void setColor(String color) {
                this.color = color;
            }

            @Override
            public String toString() {
                return JSON.toJSONString(this);
            }
        }

        public static class RemarkBean implements Serializable{
            /**
             * value : 欢迎再次购买！
             * color : #173177
             */

            private String value;
            private String color;

            public RemarkBean(String value, String color) {
                this.value = value;
                this.color = color;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getColor() {
                return color;
            }

            public void setColor(String color) {
                this.color = color;
            }

            @Override
            public String toString() {
                return JSON.toJSONString(this);
            }
        }
    }

    @Override
    public String toString() {
        return "PayMsgtemple{" +
                "touser='" + touser + '\'' +
                ", template_id='" + template_id + '\'' +
                ", url='" + url + '\'' +
                ", miniprogram=" + miniprogram +
                ", data=" + data +
                '}';
    }
}
