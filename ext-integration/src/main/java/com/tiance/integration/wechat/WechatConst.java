package com.tiance.integration.wechat;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */

public class WechatConst {
    /**
     * 获取微信用户信息
     */
    public static final String userinfo = "https://api.weixin.qq.com/cgi-bin/user/info";
    /**
     * 网页授权oauth2微信授权
     */
    public static final String authorize = "https://open.weixin.qq.com/connect/oauth2/authorize";
    /**
     * 获取token
     */
    public static final String access_token = "https://api.weixin.qq.com/sns/oauth2/access_token";
    /**
     * 创建mean
     */
    public static final String createmenu = "https://api.weixin.qq.com/cgi-bin/menu/create";
    /**
     * 发送模板消息
     */
    public static final String sendtemplatemsg = "https://api.weixin.qq.com/cgi-bin/message/template/send";
    /**
     * 获取AccessToken
     */
    public static final String getAccessToken = "https://api.weixin.qq.com/cgi-bin/token";
    /**
     * 设置行业信息
     */
    public static final String setIndustry = "https://api.weixin.qq.com/cgi-bin/template/api_set_industry";
    /**
     * 获取设置的行业信息
     */
    public static final String getIndustry = "https://api.weixin.qq.com/cgi-bin/template/get_industry";
    /**
     * 获取模板id
     */
    public static final String getTemplateId = "https://api.weixin.qq.com/cgi-bin/template/api_add_template";
    /**
     * 获取模板id
     */
    public static final String getAllTemplate = "https://api.weixin.qq.com/cgi-bin/template/get_all_private_template";

        public static final String getTicket = "https://api.weixin.qq.com/cgi-bin/ticket/getticket";
    /**
     * accesstokenkey 微信token缓存key
     */
    public static final String GETACCESS_TOKENKEY = "ACCESS_TOKENKEY";
    /**
     * ordercachkey 订单缓存key
     */
    public static final String  ORDERCACHKEY= "ORDERCACHKEY";


}
