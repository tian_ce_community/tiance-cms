package com.tiance.integration.wechat;

import java.io.Serializable;

/**
 * ${DESCRIPTION}
 *
 * @version v 0.1 2017/6/28 下午5:16 Exp $$
 * @AUTHOR hmz
 */


public class WxBaseResult implements Serializable {
    private int errcode;
    private String errmsg;

    public int getErrcode() {
        return errcode;
    }

    public void setErrcode(int errcode) {
        this.errcode = errcode;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    @Override
    public String toString() {
        return "com.u51pay.qctrade.ext.integration.mode.WxBaseResult{" +
                "errcode=" + errcode +
                ", errmsg='" + errmsg + '\'' +
                '}';
    }
}
