package com.tiance.integration.wechat;

import java.io.Serializable;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/3/15
 *
 * Description：
 *
 * Modification History:
 *
 */
public class JsapiTicket extends WxBaseResult{

            /**
             *
             */
            private static final long serialVersionUID = -357009110782376503L;
            //jsapi_ticket
            private String ticket;

            private String expires_in;

            public String getTicket() {
                return ticket;
            }

            public void setTicket(String ticket) {
                this.ticket = ticket;
            }

            public String getExpires_in() {
                return expires_in;
            }

            public void setExpires_in(String expires_in) {
                this.expires_in = expires_in;
            }


}
