package com.tiance.integration.wechat;

/**
 * ${DESCRIPTION} 授权信息
 *
 * @version v 0.1 2017/6/27 下午2:24 Exp $$
 * @AUTHOR hmz
 */


public class AccessToken extends WxBaseResult {

    /**
     * access_token
     */
    private String accessToken;
    /**
     * access_token凭证的有效时间（秒）
     */
    private long expires_in;
    /**
     * access_token凭证的获得时间（秒）
     */
    private long startTimeSecond;

    public AccessToken(String accessToken, long expires_in, long startTimeSecond) {
        this.accessToken = accessToken;
        this.expires_in = expires_in;
        this.startTimeSecond = startTimeSecond;
    }

    public AccessToken() {
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public long getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(long expires_in) {
        this.expires_in = expires_in;
    }

    public long getStartTimeSecond() {
        return startTimeSecond;
    }

    public void setStartTimeSecond(long startTimeSecond) {
        this.startTimeSecond = startTimeSecond;
    }
}
