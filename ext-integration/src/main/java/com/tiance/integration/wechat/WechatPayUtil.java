package com.tiance.integration.wechat;

import com.tiance.exception.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/21
 *
 * Description：
 *
 * Modification History:
 *
 */
public class WechatPayUtil {
    /**
     * 根据useragent 判断客服端类型
     * @param request
     * @return
     */
    public static void isWechatClientRequest(HttpServletRequest request) {
        String Useragent = request.getHeader("User-Agent");
        if (Useragent.contains("MicroMessenger")){
            //不支持企业微信扫码
            if (Useragent.contains("wxwork")){
                throw new BusinessException("请使用微信!");
            }
        }else {
            throw new BusinessException("请使用微信!");
        }
    }

    private      static Logger logger = LoggerFactory.getLogger(WechatPayUtil.class);



    public static String getWeChatOpenId(HttpServletRequest request, String appId, String secret)
            throws IOException {
        String code = request.getParameter("code");
        String state = request.getParameter("state");
        String openid = WechatClient.getOpenid(appId, secret, code);

        logger.info("weixin返回的code："+code);
        logger.info("weixin返回state："+state);

        return openid;
    }


    /**
     * 设置模板消息
     * @param openid
     * @param jumpurl
     * @param providerName
     * @param amount
     * @param time
     * @param reserveId
     * @param template_id
     * @return
     */
    public static PayMsgtemple getPayMsgtemple(String openid, String jumpurl, String providerName, String amount, String time, String reserveId, String template_id) {
        PayMsgtemple payMsgtemple = new PayMsgtemple();
        payMsgtemple.setTouser(openid);//设置openid
        /// TODO: 2019-01-12 不设置跳转链接
        //        payMsgtemple.setUrl(jumpurl);// 设置跳转url
        payMsgtemple.setTemplate_id(template_id);// 设置模板id
        //设置消息内容 消息头
        PayMsgtemple.DataBean.FirstBean firstBean =  new PayMsgtemple.DataBean.FirstBean("微信支付成功","#000");
        //设置商户id
        PayMsgtemple.DataBean.Keyword1Bean keyword1Bean = new PayMsgtemple.DataBean.Keyword1Bean( providerName, "#173177");
        //设置商户交易金额
        PayMsgtemple.DataBean.Keyword2Bean keyword2Bean = new PayMsgtemple.DataBean.Keyword2Bean( amount, "#173177");
        //设置商户交易时间
        PayMsgtemple.DataBean.Keyword3Bean keyword3Bean = new PayMsgtemple.DataBean.Keyword3Bean(time, "#173177");
        //设置商户交易时间
        PayMsgtemple.DataBean.Keyword4Bean keyword4Bean = new PayMsgtemple.DataBean.Keyword4Bean( reserveId, "#173177");
        // 设置mark 提示
        PayMsgtemple.DataBean.RemarkBean remarkBean = new PayMsgtemple.DataBean.RemarkBean("\n","#FF0000");
        PayMsgtemple.DataBean dataBean = new PayMsgtemple.DataBean(firstBean,keyword1Bean,keyword2Bean,keyword3Bean,keyword4Bean,remarkBean);
        payMsgtemple.setData(dataBean);
        return payMsgtemple;
    }
}
