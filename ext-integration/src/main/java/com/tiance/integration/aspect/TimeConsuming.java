package com.tiance.integration.aspect;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2018/12/21
 *
 * Description：
 *
 * Modification History:
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface TimeConsuming {
    /**
     * 日志打印时用于标识方法的作用
     * @return
     */
    String message() default "";

}
