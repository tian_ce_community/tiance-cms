package com.tiance.integration.aspect;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2018/12/21
 *
 * Description：
 *
 * Modification History:
 *
 */
@Aspect
public class TimeConsumingAspect extends BaseAdvice {

	private static final Logger log = LoggerFactory.getLogger(TimeConsumingAspect.class);

	/**
	 * 设置切点
	 */
	@Pointcut("@annotation(TimeConsuming)")
	public void timeConsuming() {

	}

	/**
	 * 日志处理
	 *
	 * @param joinPoint
	 * @return
	 * @throws Throwable
	 */
	@Around("timeConsuming()")
	public Object logServiceMethodAccess(ProceedingJoinPoint joinPoint) throws Throwable {

		Object object = null;

		Method method = this.getCurrentMethod(joinPoint);
		Annotation annotation = this.getMethodAnnotation(method, TimeConsuming.class);
		String message = (String) this.quietGetFromAnnotation("message", annotation);

		long start = System.currentTimeMillis();
		try {
			object = joinPoint.proceed();
			
		} finally{
			long end = System.currentTimeMillis();

			long time = end - start;

			log.info(String.format("[TimeConsuming]-[%s][method=%s], 耗时: costTime=%s ms", message, joinPoint.getSignature().toShortString(),
					time));
		}

		return object;
	}

}