package com.tiance.integration.idcard;

import com.alibaba.fastjson.JSON;
import com.tiance.exception.BusinessException;
import com.tiance.integration.ProxyBeansFactory;
import com.tiance.integration.config.CommonProperties;
import com.tiance.utils.HttpUtils;
import com.tiance.utils.JsonUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/22
 *
 * Description：身份证校验工具
 *
 * Modification History:
 *
 */

/**
 * 重要提示如下:
 * HttpUtils请从
 * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
 * 下载
 *
 * 相应的依赖请参照
 * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
 */
public class IDCardUtil {

    private   static Logger logger = LoggerFactory.getLogger(IDCardUtil.class);

    private static String host="https://idenauthen.market.alicloudapi.com";
    private static String path = "/idenAuthentication";

    protected static CommonProperties commonProperties = ProxyBeansFactory.getInstance().getBean(CommonProperties.class);

    public static void main(String[] args) {
        verifyIdCardInfo("652323199107043516","雷霆");
    }
    /**
     * 校验身份证信息
     * @return
     */
    public static boolean verifyIdCardInfo(String idCardNumber,String name){

        //如果未开启身份证校验 则直接返回
        if(!commonProperties.isVerifyIDCardNumber()){
            return true;
        }

        String method = "POST";
        Map<String, String> headers = new HashMap<>(2);
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + commonProperties.getAppCode());
        //根据API的要求，定义相对应的Content-Type
        headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        Map<String, String> bodys = new HashMap<>(2);
        if(StringUtils.isBlank(idCardNumber)){
            throw new BusinessException("身份验证接口调用异常:身份证不能为空!");
        }
        if(StringUtils.isBlank(name)){
            throw new BusinessException("身份验证接口调用异常:姓名不能为空!");
        }

        bodys.put("idNo", idCardNumber);
        bodys.put("name", name);
        HttpResponse response;
        Map<String, String> resultMap;
        try {
            logger.info("身份验证开始，参数：{}", JSON.toJSONString(bodys));
            response = HttpUtils.doPost(host, path, method, headers, new HashMap(1), bodys);
            logger.info("身份验证返回信息:"+EntityUtils.toString(response.getEntity()));
            //获取response的body
            resultMap=JsonUtil.convertJsonToMap(EntityUtils.toString(response.getEntity()));

        } catch (Exception e) {
            logger.error("身份验证接口调用异常:",e);
            throw new BusinessException("身份验证接口调用异常");
        }

        if(!"0000".equals(resultMap.get("respCode"))){
            throw new BusinessException("身份验证接口调用异常:"+resultMap.get("respMessage"));
        }
        return  true;
    }
}
