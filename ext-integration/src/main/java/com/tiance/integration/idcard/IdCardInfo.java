package com.tiance.integration.idcard;

import java.io.Serializable;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/22
 *
 * Description：身份证实体
 *     {"name":"**","idNo":"***","respMessage":"身份证信息匹配","respCode":"0000",
 *     "province":"**","city":"**","county":"**","birthday":"19911101","sex":"F","age":"28"}

 *{"name":"雷霆","idNo":"4231424","respMessage":"身份证号格式错误，请检查后重新输入","respCode":"0004"}
 * Modification History:
 *
 */
public class IdCardInfo implements Serializable {


    /**
     * 姓名
     */
    private String name;
    /**
     * 身份证号
     */
    private String idNo;
    /**
     * 返回信息
     *
     */
    private String respMessage;
    /***
     * 返回码
     */
    private String respCode;
    /**
     * 省份
     */
    private String province;
    /**
     * 市
     */
    private String city;
    /**
     * 县
     */
    private String county;
    /**
     * 生日
     */
    private String birthday;
    /**
     * 性别
     */
    private String sex;
    /**
     * 年龄
     */
    private String age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public String getRespCode() {
        return respCode;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}
