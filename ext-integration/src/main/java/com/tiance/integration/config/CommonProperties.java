package com.tiance.integration.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：公共属性
 *
 * Modification History:
 *
 */
@Component
@ConfigurationProperties(prefix = "tiance.common")
public class CommonProperties {

    @Value("${server.servlet.context-path}")
    private String contextPath;
    /**
     * 静态文件路径
     */
    @Value("${spring.mvc.static-path-pattern}")
    private String staticPathPattern;

    /**
     * 图片上传路径
     */
    private String uploadFilePath;
    /**
     * 图片上传路径前缀
     */
    private String uploadImagePrefixPath;
    private String redirectURI;

    private boolean verifyIDCardNumber;
    private String appCode;
    private String environment;
    private String wechatAppId;
    private String wechatSecret;

    public String getUploadImagePrefixPath() {
        return uploadImagePrefixPath;
    }

    public void setUploadImagePrefixPath(String uploadImagePrefixPath) {
        this.uploadImagePrefixPath = uploadImagePrefixPath;
    }

    public boolean isVerifyIDCardNumber() {
        return verifyIDCardNumber;
    }

    public void setVerifyIDCardNumber(boolean verifyIDCardNumber) {
        this.verifyIDCardNumber = verifyIDCardNumber;
    }

    public String getStaticPathPattern() {
        return staticPathPattern;
    }

    public void setStaticPathPattern(String staticPathPattern) {
        this.staticPathPattern = staticPathPattern;
    }

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public String getRedirectURI() {
        return redirectURI;
    }

    public void setRedirectURI(String redirectURI) {
        this.redirectURI = redirectURI;
    }

    public String getContextPath() {
        return contextPath;
    }

    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }

    public String getWechatAppId() {
        return wechatAppId;
    }

    public void setWechatAppId(String wechatAppId) {
        this.wechatAppId = wechatAppId;
    }

    public String getWechatSecret() {
        return wechatSecret;
    }

    public void setWechatSecret(String wechatSecret) {
        this.wechatSecret = wechatSecret;
    }

    public String getUploadFilePath() {
        return uploadFilePath;
    }

    public void setUploadFilePath(String uploadFilePath) {
        this.uploadFilePath = uploadFilePath;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }
}
