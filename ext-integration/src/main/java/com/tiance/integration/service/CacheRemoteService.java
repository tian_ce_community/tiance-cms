package com.tiance.integration.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2018/7/23
 *
 * Description：
 *
 * Modification History:
 *
 */
public class CacheRemoteService {

    private static final Logger logger = LoggerFactory.getLogger(CacheRemoteService.class);

    private static final int expire=60*60*12;

}
