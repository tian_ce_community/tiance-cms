package com.tiance.integration.cache;


import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author 雷霆
 * @version 1.0.0 map 缓存
 * @date$ 2018/1/20
 *
 * Description：
 *
 * Modification History:
 *
 */
public class MapCacheUtil {

    public static ConcurrentHashMap<String,Integer> cache=new ConcurrentHashMap<String,Integer>();

    public static final ReentrantLock mainLock = new ReentrantLock();


    public static boolean  lock(String key){
        mainLock.lock();
        boolean success= cache.putIfAbsent(key,1)==null ? true:false;
        mainLock.unlock();
        return success;
    }

    public static void unlock(String key){
        mainLock.lock();
        cache.remove(key);
        mainLock.unlock();
    }

    public static void main(String[] args) {
        Boolean lock=lock("request");
        Boolean lock2=lock("request");
        unlock("request");
        Boolean lock3=lock("request");
    }

}
