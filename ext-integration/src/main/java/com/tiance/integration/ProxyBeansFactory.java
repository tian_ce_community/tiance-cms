package com.tiance.integration;

import com.tiance.utils.StringUtil;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：
 *
 * Modification History:
 *
 */
@Component
public class ProxyBeansFactory implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    private static ProxyBeansFactory self;

    public ProxyBeansFactory() {
        self = this;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ProxyBeansFactory.applicationContext = applicationContext;
    }

    public static ProxyBeansFactory getInstance() {
        return self;
    }

    public <T> T getBean(String name, Class<T> clazz) {
        return applicationContext.getBean(name, clazz);
    }

    public <T> T getBean(Class<T> clazz) {
        return applicationContext.getBean(StringUtil.lowerFirst(clazz.getSimpleName()), clazz);
    }
}
