# tiance-cms

#### 介绍
天策社区后台管理系统

#### 软件架构
本系统开发框架有：
1.springboot 2.0.4.RELEASE
2.shiro 1.4.0
3.mybatis
4.mysql8.0.11
5.layui+bootstrap


#### 安装教程

1. 执行tiance-cms.sql文件 创建数据库和表

#### 使用说明

1. 打开web下的Main类  运行main方法
2. 登录 :http://127.0.0.1:8080/tiance/login
3. 用户/密码 admin/nstiptv


#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
