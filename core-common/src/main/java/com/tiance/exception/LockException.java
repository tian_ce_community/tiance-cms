package com.tiance.exception;

import com.tiance.enums.ErrorCodeEnum;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class LockException extends MemcacheException {

    public LockException(ErrorCodeEnum cashierErrorCode) {
        super(cashierErrorCode);
    }

    public LockException(ErrorCodeEnum cashierErrorCode, String extension) {
        super(cashierErrorCode, extension);
    }

    public LockException(String message, ErrorCodeEnum cashierErrorCode, String extension) {
        super(message, cashierErrorCode, extension);
    }

    public LockException(String message, Throwable cause, ErrorCodeEnum cashierErrorCode,
                         String extension) {
        super(message, cause, cashierErrorCode, extension);
    }
}
