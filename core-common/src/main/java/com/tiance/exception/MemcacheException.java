package com.tiance.exception;

import com.tiance.enums.ErrorCodeEnum;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class MemcacheException extends RuntimeException {


    /**
     * 错误码
     */
    private ErrorCodeEnum cashierErrorCode;

    /**
     * 扩展
     */
    private String extension;

    public MemcacheException(ErrorCodeEnum cashierErrorCode) {
        this.cashierErrorCode = cashierErrorCode;
    }
    public MemcacheException(ErrorCodeEnum cashierErrorCode, String extension) {
        this.cashierErrorCode = cashierErrorCode;
        this.extension = extension;
    }

    public MemcacheException(String message, ErrorCodeEnum cashierErrorCode, String extension) {
        super(message);
        this.cashierErrorCode = cashierErrorCode;
        this.extension = extension;
    }

    public MemcacheException(String message, Throwable cause, ErrorCodeEnum cashierErrorCode,
                             String extension) {
        super(message, cause);
        this.cashierErrorCode = cashierErrorCode;
        this.extension = extension;
    }



    public ErrorCodeEnum getTradeErrorCode() {
        return cashierErrorCode;
    }

    public void setTradeErrorCode(ErrorCodeEnum cashierErrorCode) {
        this.cashierErrorCode = cashierErrorCode;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
}
