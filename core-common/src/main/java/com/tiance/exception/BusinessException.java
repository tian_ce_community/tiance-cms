package com.tiance.exception;

import com.tiance.enums.ErrorCodeEnum;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：错误异常
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class BusinessException extends RuntimeException {

	private static final long serialVersionUID = -1L;

	/**
	 * 错误码
	 */
	private ErrorCodeEnum errorCode;

    private String message;

	/**
	 * 扩展
	 */
	private String extension;

	public ErrorCodeEnum getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(ErrorCodeEnum errorCode) {
		this.errorCode = errorCode;
	}

	public BusinessException() {
	}

	public BusinessException(ErrorCodeEnum errorCode) {
		super(errorCode.name());
		this.errorCode = errorCode;
		this.message = errorCode.getMsg();
	}

	public BusinessException(String message) {
		super(message);
		this.message = message;
	}

	public BusinessException(ErrorCodeEnum cashierErrorCode, String message) {
		super(message);
		this.errorCode = cashierErrorCode;
		this.message = message;
	}

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BusinessException(Throwable cause) {
		super(cause);
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

}
