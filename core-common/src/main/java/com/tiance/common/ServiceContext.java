package com.tiance.common;


import java.util.HashMap;
import java.util.Map;


/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：
 *
 * Modification History:
 *
 */
public class ServiceContext {

    /**
     * 使用的字符集
     */
    private String charset;

    /**
     * 客户端IP地址
     */
    private String clientIp;

    /**
     * 合作方reffer信息
     */
    private String partnerReffer;

    /**
     * 扩展参数
     */
    private Map<String, Object> extend = new HashMap<String, Object>();

    /**
     * 接口服务名
     */
    private String service;

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public String getPartnerReffer() {
        return partnerReffer;
    }

    public void setPartnerReffer(String partnerReffer) {
        this.partnerReffer = partnerReffer;
    }

    public Map<String, Object> getExtend() {
        return extend;
    }

    public void setExtend(Map<String, Object> extend) {
        this.extend = extend;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }
}