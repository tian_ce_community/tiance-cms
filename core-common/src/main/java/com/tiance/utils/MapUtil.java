package com.tiance.utils;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2018/4/24
 *
 * Description：
 *
 * Modification History:
 *
 */
public class MapUtil {

    public static Map params2Map(Object [] ...args ){

        if(null==args){
            return null;
        }

        Map<String,Object> map=new HashMap<String,Object> ();

        for(Object [] array:args){
            if(array.length==2){
                map.put(array[0]+"",array[1]);
            }
        }

        return map;
    }
}
