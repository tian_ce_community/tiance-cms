package com.tiance.utils;

import com.tiance.exception.BusinessException;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/15
 *
 * Description：
 *
 * Modification History:
 *
 */
public class DataUtil {
    public static Integer calculateTotalIntegral(Integer originalIntegral, Integer newIntegral) {
        if(null==originalIntegral){
            originalIntegral=0;
        }
        if(null==newIntegral){
            throw new BusinessException("错误的积分值");
        }
        return   originalIntegral+newIntegral;
    }



    public static Integer calculateTotalCredit(Integer originalCredit, Integer newCredit) {
        if(null==originalCredit){
            originalCredit=0;
        }
        if(null==newCredit){
            throw new BusinessException("错误的信用值");
        }
        return   originalCredit+newCredit;
    }
}
