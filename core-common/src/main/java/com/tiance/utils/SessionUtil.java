package com.tiance.utils;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：
 *
 * Modification History:
 *
 */
public class SessionUtil {
    public SessionUtil() {
    }

    public static String getSessionId() {
        String sessionId = System.currentTimeMillis() + "" + (int)(Math.random() * 10000.0D);
        return sessionId;
    }
}
