package com.tiance.utils;

import com.tiance.exception.BusinessException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/15
 *
 * Description：
 *
 * Modification History:
 *
 */
public class Validator {




	public static boolean assertNull(Object object, String msg){
		if(null == object){
			if(null == msg){
				return false;
			}
			throw new BusinessException(msg);
		}

		if(object instanceof String){
			if(StringUtils.isBlank((String)object)){
				if(null == msg){
					return false;
				}
				throw new BusinessException(msg);
			}

		}

		if(object instanceof List){
			if(CollectionUtils.isEmpty((List)object)){
				if(null == msg){
					return false;
				}
				throw new BusinessException(msg);
			}

		}

		return true;

	}

	public static boolean assertTrue(boolean isTrue, String msg){
		if(isTrue){
			throw new BusinessException(msg);
		}
		return true;

	}

	public static void main(String[] args) {
		List lsit=new ArrayList();
		lsit.add(1);
		assertNull(lsit,"dfadsf");
	}
}