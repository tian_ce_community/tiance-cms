package com.tiance.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2018/8/3
 *
 * Description：
 *
 * Modification History:
 *
 */
public class StringUtil {

    public static char SEPARATOR_COMMA=',';
    public static String M15="***************";
    public static String M7="*******";
    public static final String REGEX1=":=";
    public static final String FILE_SUFFIX=".txt";
    public static final String REGEX2="-";
    public static final String UNDERLINE="_";





    /**
     public static boolean isMobileNO(String mobiles) {
     String telRegex = "[1][3456789]\\d{9}";
     // "[1]"代表第1位为数字1，"[3578]"代表第二位可以为3、5、8中的一个，"\\d{9}"代表后面是可以是0～9的数字，有9位。
     if (StringUtils.isEmpty(mobiles)) {
     return false;
     } else
     return mobiles.matches(telRegex);
     }

     * 使得字符串首字母小写
     * @return
     */
     public static String lowerFirst(String str){

         if(StringUtils.isEmpty(str)){
             return str;
         }

         char [] chars=str.toCharArray();
         chars[0]+=32;
         return String.valueOf(chars);

     }

    public static String trimNull(String str) {
      if(StringUtils.isBlank(str)){
          return "";
      }
        return str;
    }
    public static String trimNull(Long str) {
        if(null==str){
            return "";
        }
        return str.toString();
    }


    public static String getRandomString(int length) {
        String base = "abcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();

        for(int i = 0; i < length; ++i) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }

        return sb.toString();
    }

    public static String getExceptionMsg(Exception e) {
        StringWriter sw = new StringWriter();

        try {
            e.printStackTrace(new PrintWriter(sw));
        } finally {
            try {
                sw.close();
            } catch (IOException var8) {
                var8.printStackTrace();
            }

        }

        return sw.getBuffer().toString().replaceAll("\\$", "T");
    }
    public  static Map<String, String> string2Map(String responseStr) {
        if(StringUtils.isEmpty(responseStr)){
            return null;
        }

        JSONObject jsonObject = JSON.parseObject(responseStr);

        Map<String, String> map = new TreeMap<String, String>();
        Set<String> keySet = jsonObject.keySet();

        for (String key : keySet) {
            map.put(key, (String)jsonObject.get(key));
        }
        return map;
    }

    public static Map<String,String> json2Map(String json){

        if(StringUtils.isEmpty(json)){
            return null;
        }

        Map<String,String> map1 = (Map<String,String>) JSON.parse(json);
        for (String key : map1.keySet()) {
            System.out.println(key+":"+map1.get(key));
        }
        return map1;
    }


    public static String setNodeText(String xmlString, String nodeName,String newNodeValue) throws Exception {
        String beginName = "<" + nodeName + ">";
        String endName = "</" + nodeName + ">";
        int beginIndex = xmlString.indexOf(beginName);
        if(beginIndex == -1) {
            return xmlString;
        } else {
            int endIndex = xmlString.indexOf(endName);
            if(endIndex == -1) {
                return xmlString;
            } else {
                String oldValue = xmlString.substring(beginIndex + beginName.length(), endIndex);
                oldValue=beginName+oldValue+endName;
                newNodeValue=beginName+newNodeValue+endName;
                String[] strings= xmlString.split(oldValue);
                return strings[0]+newNodeValue+strings[1];
            }
        }
    }


    public static String getNodeText(String xmlString, String nodeName) {
        String beginName = "<" + nodeName + ">";
        String endName = "</" + nodeName + ">";
        int beginIndex = xmlString.indexOf(beginName);
        if(beginIndex == -1) {
            return "";
        } else {
            int endIndex = xmlString.indexOf(endName);
            if(endIndex == -1) {
                return "";
            } else {
                String nodeValue = xmlString.substring(beginIndex + beginName.length(), endIndex);
                return nodeValue;
            }
        }
    }





    public static String generate0(int num){
        String key="0";
        String str="";
        for(int i=0;i<num;i++){
            str+=key;
        }
        return str;
    }


}
