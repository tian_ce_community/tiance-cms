package com.tiance.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2018/8/3
 *
 * Description：
 *
 * Modification History:
 *
 */
@Component
public class SpringContextUtil implements ApplicationContextAware {

    private static ApplicationContext appCtx;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        appCtx = applicationContext;
    }

    /**
     * 根据Id获取Bean
     * @param beanName
     * @return
     */
    public static Object getBeanWithName(String beanName) {
        if (appCtx != null) {
            return appCtx.getBean(beanName);
        }
        return null;
    }

}
