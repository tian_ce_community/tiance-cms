package com.tiance.utils;

import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2018/8/3
 *
 * Description：
 *
 * Modification History:
 *
 */
public class DateUtils {
    public static final String YYYY_MM_DD = "yyyy-MM-dd";
    public static final String YYYYMM = "yyyyMM";
    public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

    public static final String YYYYMMDD = "yyyyMMdd";
    public static final String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
    public static final String YYYYMMDDHHMMssSSS = "yyyyMMddHHmmssSSS";

    public static SimpleDateFormat getSdf(String format) {
        return new SimpleDateFormat(format);
    }


    public static String parserStr(Date date, String format) {
        if (null==date || StringUtils.isEmpty(format)) {
            return null;
        }

        String dateStr;
        try {
            dateStr = getSdf(format).format(date);
        } catch (Exception e) {
            e.printStackTrace();
            dateStr = null;
        }
        return dateStr;
    }

    public static Date parserDate(String dateStr, String format) {
        if (StringUtils.isEmpty(dateStr) || StringUtils.isEmpty(format)) {
            return null;
        }

        Date d;
        try {
            d = getSdf(format).parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
            d = null;
        }
        return d;
    }

    public static Date parseFromYYYYMM(String dateStr) {
        return parserDate(dateStr, YYYYMM);
    }

    public static Date parseFromYYYY_MM_DD(String dateStr) {
        return parserDate(dateStr, YYYY_MM_DD);
    }

    public static Date parseFromYYYY_MM_DD_HH_MM_SS(String dateStr) {
        return parserDate(dateStr, YYYY_MM_DD_HH_MM_SS);
    }

    public static String parseFromYYYYMMDDHHMMSS(Date date) {
        return parserStr(date, YYYYMMDDHHMMSS);
    }

    public static Date parseFromYYYYMMDDHHMMSS(String dateStr) {
        return parserDate(dateStr, YYYYMMDDHHMMSS);
    }

    /**
     * 传入一个日期和需要增加的天数，返回对应的日期
     * 如果day=1 则返回明天
     * 如果day=-1 则返回昨天
     * 如果day=0 则返回当天
     *
     * @param date
     * @return
     * @throws Exception
     */
    public static Date getOneDay(Date date, int day) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_MONTH, day);
        return cal.getTime();
    }

    /**
     * 传入一个日期和需要增加的天数，返回对应的日期的开始时间
     * 如果day=1 则返回明天
     * 如果day=-1 则返回昨天
     * 如果day=0 则返回当天
     *
     * @param date
     * @return
     * @throws Exception
     */
    public static Date getOneDayStart(Date date, int day) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * 传入一个日期，返回下月的开始时间
     *
     * @param date
     * @return
     * @throws Exception
     */
    public static Date getStartOfNextMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, +1);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * 传入一个日期，返回上月的开始时间
     *
     * @param date
     * @return
     * @throws Exception
     */
    public static Date getStartOfPreMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, -1);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * 传入一个日期，返回当前日期的的开始时间
     *
     * @param date
     * @return
     * @throws Exception
     */
    public static Date getStart(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * 传入一个日期，返回当月的开始时间
     *
     * @param date
     * @return
     * @throws Exception
     */
    public static Date getStartOfMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, 0);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();
    }

    /**
     * 传入一个日期，返回某月的开始时间
     * 如果month=1 则返回下月
     * 如果month=-1 则返回上月
     * 如果month=0 则返回当月
     *
     * @param date
     * @return
     * @throws Exception
     */
    public static Date getStartOfOneMonth(Date date, int month) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();
    }

    /**
     * 传入一个日期，返回当月某一天的开始时间
     *
     * @param date
     * @return
     * @throws Exception
     */
    public static Date getOneDayStartOfMonth(Date date, int day) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, 0);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();
    }

    /**
     * 传入一个日期，返回上月某一天的开始时间
     *
     * @param date
     * @return
     * @throws Exception
     */
    public static Date getOneDayStartOfPreMonth(Date date, int day) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, -1);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();
    }

    /**
     * 传入一个日期，返回上月的结束时间
     *
     * @param date
     * @return
     * @throws Exception
     */
    public static Date getEndOfPreMonth(Date date) throws Exception {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, -1);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        return cal.getTime();
    }

    /**
     * 传入一个日期，返回上月的结束时间
     *
     * @param date
     * @return
     * @throws Exception
     */
    public static Date getDateByMinute(Date date, int minute) throws Exception {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, minute);
        return cal.getTime();
    }

    public static String praseDate2YYYYMMDD(Date date) {
        if (null == date) {
            date = new Date();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        String mouthStr;
        String dayStr;
        if (month < 10) {
            mouthStr = "0" + month;
        } else {
            mouthStr = "" + month;
        }

        if (day < 10) {
            dayStr = "0" + day;
        } else {
            dayStr = "" + day;
        }
        return year + mouthStr + dayStr;
    }



    public static String getYYYYMMDD(Date date) {
        return new SimpleDateFormat(YYYYMMDD).format(date).toString();
    }

    public static String getYYYYMMDDHHMMSS(Date date) {
        return new SimpleDateFormat(YYYYMMDDHHMMSS).format(date).toString();
    }


    public static String getYYYY_MM_DD_HH_MM_SS(Date date) {
        return new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS).format(date).toString();
    }

    public static String getYYYYMMDDHHMMssSSS(Date date) {
        return new SimpleDateFormat(YYYYMMDDHHMMssSSS).format(date).toString();
    }

    public static int getDay(Date date) {
        if (null == date) {
            date = new Date();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    public static void main(String[] args) {
        try {

            //getStartTime(SettlementCycleEnum.WEEK.getCode(), null, null);

            String dateStr = "201712";

            Date date = parseFromYYYYMM(dateStr);
            //String date=getYearAndMonth(getStartOfPreMonth(getStartOfPreMonth(getStartOfPreMonth(new Date()))));
            //System.out.print(date);
        } catch (Exception w) {

        }
    }

}
