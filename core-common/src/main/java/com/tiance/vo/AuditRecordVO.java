package com.tiance.vo;

import java.util.Date;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：活动，任务信息，会议室，值班审核记录表
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class AuditRecordVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     * ID
     */
    private Long id;

    /**
     * 名称
     */
    private String entityName;


    /**
     * 实体ID
     * ENTITY_ID
     */
    private Long entityId;

    /**
     * 实体类型(ACTIVITY:活动;TASK:任务，PUBLISH_INFO:信息;MEETING_ROOM:会议室;DUTY_APPLICANT:值班申请)
     * ENTITY_TYPE
     */
    private String entityType;
    /**
     * 审核状态（UN_AUDIT:待审核;AUDIT_FAIL:审核不通过;AUDIT_PASS:审核通过）
     * AUDIT_STATE
     */
    private String auditState;

    /**
     * 申请人意见
     * APPROVAL_OPINION
     */
    private String approvalOpinion;
    /**
     * 审批人
     * APPROVAL_ID
     */
    private Long approvalId;


    /**
     * 备注
     * REMARK
     */
    private String remark;
    /**
     * 申请时间
     * CREATE_TIME
     */
    private Date createTime;
    /**
     * 修改时间
     * UPDATE_TIME
     */
    private Date updateTime;
    /**
     * 申请时间
     * APPLICANT_TIME
     */
    private Date applicantTime;
    /**
     * 审批时间
     * APPROVAL_TIME
     */
    private Date approvalTime;

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getAuditState() {
        return auditState;
    }

    public void setAuditState(String auditState) {
        this.auditState = auditState;
    }

    public String getApprovalOpinion() {
        return approvalOpinion;
    }

    public void setApprovalOpinion(String approvalOpinion) {
        this.approvalOpinion = approvalOpinion;
    }

    public Long getApprovalId() {
        return approvalId;
    }

    public void setApprovalId(Long approvalId) {
        this.approvalId = approvalId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getApplicantTime() {
        return applicantTime;
    }

    public void setApplicantTime(Date applicantTime) {
        this.applicantTime = applicantTime;
    }

    public Date getApprovalTime() {
        return approvalTime;
    }

    public void setApprovalTime(Date approvalTime) {
        this.approvalTime = approvalTime;
    }
}
