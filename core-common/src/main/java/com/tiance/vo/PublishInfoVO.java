package com.tiance.vo;

import java.util.Date;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/4
 *
 * Description：
 *
 * Modification History:
 *
 */
public class PublishInfoVO extends BaseVO{
    /**
     * 主键id
     * ID
     */
    private Long id;
    /**
     * 申请人ID
     * APPLICANT_ID
     */
    private Long applicantId;
    /**
     * 申请人类型（SYSTEM:系统;MEMBER:会员）
     * APPLICANT_TYPE
     */
    private String applicantType;


    /**
     * 申请人
     */
    private String applicantName;

    /**
     * 联系人
     * CONTACTS_ID
     */
    private Long contactsId;
    /**
     * 联系人
     */
    private String contactsName;
    /**
     * 联系人电话
     * CONTACTS_PHONE
     */
    private String contactsPhone;
    /**
     * 信息名称
     * INFO_NAME
     */
    private String infoName;
    /**
     * 信息类型
     * INFO_TYPE
     */
    private String infoType;
    /**
     * 信息状态(UN_AUDIT:申请待审核;APPLY_CANCEL:申请取消;AUDIT_PASS:申请审核通过;AUDIT_FAIL:申请审核不通过;INFO_SIGN_UP :信息报名中，INFO_IN_PROGRESS:信息实施进行中;INFO_CANCEL:信息取消;INFO_FINISHED:信息结束;INFO_APPRECIATED:信息已打赏)
     * INFO_STATUS
     */
    private String infoStatus;
    /**
     * 消息URL
     * INFO_URL
     */
    private String infoUrl;
    /**
     * 审批人
     * APPROVAL_ID
     */
    private Long approvalId;
    /**
     * APPROVAL_OPINION
     */
    private String approvalOpinion;


    /**
     * 备注
     * REMARK
     */
    private String remark;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 创建时间
     * CREATE_TIME
     */
    private Date createTime;
    /**
     * 审批时间
     * APPROVAL_TIME
     */
    private Date approvalTime;
    /**
     * 申请时间
     * APPLICANT_TIME
     */
    private Date applicantTime;
    /**
     * 更新时间
     * UPDATE_TIME
     */
    private Date updateTime;


    private String createTimeStr;


    private String updateTimeStr;

    public String getCreateTimeStr() {
        return createTimeStr;
    }

    public void setCreateTimeStr(String createTimeStr) {
        this.createTimeStr = createTimeStr;
    }

    public String getUpdateTimeStr() {
        return updateTimeStr;
    }

    public void setUpdateTimeStr(String updateTimeStr) {
        this.updateTimeStr = updateTimeStr;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    public String getContactsName() {
        return contactsName;
    }

    public void setContactsName(String contactsName) {
        this.contactsName = contactsName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getApplicantType() {
        return applicantType;
    }

    public void setApplicantType(String applicantType) {
        this.applicantType = applicantType;
    }



    public String getContactsPhone() {
        return contactsPhone;
    }

    public void setContactsPhone(String contactsPhone) {
        this.contactsPhone = contactsPhone;
    }

    public String getInfoName() {
        return infoName;
    }

    public void setInfoName(String infoName) {
        this.infoName = infoName;
    }

    public String getInfoType() {
        return infoType;
    }

    public void setInfoType(String infoType) {
        this.infoType = infoType;
    }

    public String getInfoStatus() {
        return infoStatus;
    }

    public void setInfoStatus(String infoStatus) {
        this.infoStatus = infoStatus;
    }

    public String getInfoUrl() {
        return infoUrl;
    }

    public void setInfoUrl(String infoUrl) {
        this.infoUrl = infoUrl;
    }


    public String getApprovalOpinion() {
        return approvalOpinion;
    }

    public void setApprovalOpinion(String approvalOpinion) {
        this.approvalOpinion = approvalOpinion;
    }

    public Long getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(Long applicantId) {
        this.applicantId = applicantId;
    }

    public Long getContactsId() {
        return contactsId;
    }

    public void setContactsId(Long contactsId) {
        this.contactsId = contactsId;
    }

    public Long getApprovalId() {
        return approvalId;
    }

    public void setApprovalId(Long approvalId) {
        this.approvalId = approvalId;
    }

    public Date getApprovalTime() {
        return approvalTime;
    }

    public void setApprovalTime(Date approvalTime) {
        this.approvalTime = approvalTime;
    }

    public Date getApplicantTime() {
        return applicantTime;
    }

    public void setApplicantTime(Date applicantTime) {
        this.applicantTime = applicantTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
