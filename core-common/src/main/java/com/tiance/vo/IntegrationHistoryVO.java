package com.tiance.vo;

import java.util.Date;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/4
 *
 * Description：
 *
 * Modification History:
 *
 */
public class IntegrationHistoryVO extends BaseVO{


    /**
     * 主键id
     * ID
     */
    private Long id;

    /**
     * 类型(INTEGRATION:积分，CREDIT:信用)
     * INTEGRATION_TYPE
     *
     */
    private String integrationType;
    /**
     * 操作类型（ADD:加;SUBTRACT:减
     * OPERATE_TYPE
     */
    private String operateType;
    /**
     * 分值或信用点
     * VALUE
     */
    private Integer integrationValue;
    /**
     * 创建时间
     * CREATE_TIME
     */
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getIntegrationType() {
        return integrationType;
    }

    public void setIntegrationType(String integrationType) {
        this.integrationType = integrationType;
    }

    public String getOperateType() {
        return operateType;
    }

    public void setOperateType(String operateType) {
        this.operateType = operateType;
    }

    public Integer getIntegrationValue() {
        return integrationValue;
    }

    public void setIntegrationValue(Integer integrationValue) {
        this.integrationValue = integrationValue;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
