package com.tiance.vo;

import com.tiance.page.QueryPage;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/4
 *
 * Description：
 *
 * Modification History:
 *
 */
public class BaseVO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * OPENID
     */
    private String openid;

    /**
     * OPENID
     */
    private Long         memberId;
    private String       errorInfo;
    /**
     * 状态范围
     */
    private List<String> statusScope;

    public List<String> getStatusScope() {
        return statusScope;
    }

    public void setStatusScope(List<String> statusScope) {
        this.statusScope = statusScope;
    }

    public String getErrorInfo() {
        return errorInfo;
    }

    public void setErrorInfo(String errorInfo) {
        this.errorInfo = errorInfo;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    /**
     * 分页信息
     */
    private QueryPage queryPage;

    public QueryPage getQueryPage() {
        return queryPage;
    }

    public void setQueryPage(QueryPage queryPage) {
        this.queryPage = queryPage;
    }

}
