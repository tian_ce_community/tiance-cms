package com.tiance.vo;

import com.tiance.page.QueryPage;

import java.util.Date;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/4
 *
 * Description：
 *
 * Modification History:
 *
 */
public class SignInVO extends BaseVO{


    /**
     * 签到状态 SIGN_IN:已签到;NOT_SIGN_IN:未签到
     */
    private String  signInStatus;
    /**
     * 主键id
     * ID
     */
    private Long      id;


    /**
     * 实体ID
     * ENTITY_ID
     */
    private Long      entityId;

    /**
     * 实体类型(ACTIVITY:活动)
     * ENTITY_TYPE
     */
    private String entityType;
    /**
     * 指定名称（活动就是活动名称，会议中心就是会议中心名称）
     * APPOINT_NAME
     */
    private String appointName;
    /**
     * 指定坐标点
     * APPOINT_COORDINATE
     */
    private String appointCoordinate;
    /**
     * 实际签到坐标点
     * SIGN_IN_COORDINATE
     */
    private String signInCoordinate;
    /**
     * 签到类型(DAILY_SIGN_IN:每日签到;
     * CONFERENCE_CENTER_SIGN_IN:会议中心签到(线下签到);
     * ACTIVITY_SIGN_IN:活动签到(个人),
     *
     * 
     * SIGN_IN_TYPE
     */
    private String signInType;
    /**
     * 备注
     * REMARK
     */
    private String remark;
    /**
     * 创建时间
     * CREATE_TIME
     */
    private Date createTime;
    /**
     * 签到时间
     * SIGN_IN_TIME
     */
    private Date signInTime;

    public String getSignInStatus() {
        return signInStatus;
    }

    public void setSignInStatus(String signInStatus) {
        this.signInStatus = signInStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getAppointName() {
        return appointName;
    }

    public void setAppointName(String appointName) {
        this.appointName = appointName;
    }

    public String getAppointCoordinate() {
        return appointCoordinate;
    }

    public void setAppointCoordinate(String appointCoordinate) {
        this.appointCoordinate = appointCoordinate;
    }

    public String getSignInCoordinate() {
        return signInCoordinate;
    }

    public void setSignInCoordinate(String signInCoordinate) {
        this.signInCoordinate = signInCoordinate;
    }

    public String getSignInType() {
        return signInType;
    }

    public void setSignInType(String signInType) {
        this.signInType = signInType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getSignInTime() {
        return signInTime;
    }

    public void setSignInTime(Date signInTime) {
        this.signInTime = signInTime;
    }
}
