package com.tiance.vo;

import java.util.Date;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：
 *用户表
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class UserVO extends BaseVO {


    /**
     * 主键id ID
     */
    private Long id;
    /**
     * 角色ID ROLE_ID
     */
    private Long roleId;
    /**
     * 角色码
     */
    private String roleCode;
    /**
     * 用户名 USER_NAME
     */
    private String userName;
    /**
     * 密码 PASS_WORD
     */
    private String passWord;
    /**(
     * 旧密码
     */
    private String oldPassword;
    /**
     * 新密码
     */
    private String newPassword;
    /**
     * 确认新密码
     */
    private String confirmNewPassword;
    /**
     * 用户类型 ADMIN:管理员,NORMAL_USER:普通用户 USER_TYPE
     */
    private String userType;
    /**
     * 用户性别 MALE:男,FEMALE:女  USER_SEX
     */
    private String userSex;
    /**
     * 用户电话 USER_PHONE
     */
    private String userPhone;
    /**
     * 生日 USER_BIRTHDAY
     */
    private String userBirthday;
    /**
     *用户头像  USER_PHOTO
     */
    private String userPhoto;
    /**
     * 用户EMAIL USER_EMAIL
     */
    private String userEmail;
    /**
     * 用户真实姓名 REAL_NAME
     */
    private String realName;
    /**
     * 用户状态 NORMAL:正常,LOCK:锁定,DELETE:删除 STATUS
     */
    private String status;
    /**
     * 版本（乐观锁保留字段） VERSION
     */
    private String version;
    /**
     * md5密码盐 SALT
     */
    private Integer salt;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     *修改时间  UPDATE_TIME
     */
    private Date updateTime;
    /**
     *创建人 CREATE_USER
     */
    private String createUser;
    /**
     * 修改人 PDATE_USER
     */
    private String updateUser;
    /**
     * 备注
     */
    private String remark;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmNewPassword() {
        return confirmNewPassword;
    }

    public void setConfirmNewPassword(String confirmNewPassword) {
        this.confirmNewPassword = confirmNewPassword;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserSex() {
        return userSex;
    }

    public void setUserSex(String userSex) {
        this.userSex = userSex;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserBirthday() {
        return userBirthday;
    }

    public void setUserBirthday(String userBirthday) {
        this.userBirthday = userBirthday;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getSalt() {
        return salt;
    }

    public void setSalt(Integer salt) {
        this.salt = salt;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
