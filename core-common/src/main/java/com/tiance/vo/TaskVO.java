package com.tiance.vo;

import java.util.Date;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：活动表
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class TaskVO extends BaseVO{

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 申请人
     * APPLICANT_ID
     */
    private Long applicantId;
    /**
     * 申请人
     *
     */
    private String applicantName;
    /**
     * 审批人
     * APPROVAL_ID
     */
    private Long approvalId;
    /**
     * 联系人
     * CONTACTS_ID
     */
    private Long contactsId;
    /**
     * 任务名称
     * TASK_NAME
     */
    private String taskName;
    /**
     * 任务最大人数
     * TASK_MAX_NUM
     */
    private String taskMaxNum;
    /**
     * 任务积分
     * TASK_INTEGRAL
     */
    private String taskIntegral;
    /**
     * APPLICANT_TYPE
     * 申请人类型（SYSTEM:系统;MEMBER:会员）
     */
    private String applicantType;
    /**
     * TASK_STATUS
     * 任务状态:(UN_AUDIT:申请待审核;APPLY_CANCEL:申请取消;AUDIT_PASS:申请审核通过;AUDIT_FAIL:申请审核不通过;TASK_SIGN_UP:任务报名中;TASK_IN_PROGRESS:任务实施进行中;TASK_CANCEL:任务取消;TASK_FINISHED:任务结束;TASK_APPRECIATED:任务已打赏;TASK_ALLOT:任务已分配
     */
    private String taskStatus;
    /**
     * 联系人电话
     * CONTACTS_PHONE
     */
    private String contactsPhone;
    /**
     * 任务微文链接
     * TASK_URL
     */
    private String taskUrl;
    /**M
     * 任务描述
     * TASK_MEMO
     */
    private String taskMemo;

    /**
     * REMARK
     */
    private String remark;
    /**
     * UPDATE_USER
     */
    private Long updateUser;
    /**
     * CREATE_USER
     */
    private Long createUser;
    /**
     * CREATE_TIME
     */
    private Date createTime;
    /**
     * UPDATE_TIME
     */
    private Date updateTime;

    /**
     * 任务开始时间
     * START_TIME
     */
    private Date startTime;
    /**
     * 任务结束时间
     * END_TIME
     */
    private Date endTime;

    private String createTimeStr;


    private String updateTimeStr;

    public String getCreateTimeStr() {
        return createTimeStr;
    }

    public void setCreateTimeStr(String createTimeStr) {
        this.createTimeStr = createTimeStr;
    }

    public String getUpdateTimeStr() {
        return updateTimeStr;
    }

    public void setUpdateTimeStr(String updateTimeStr) {
        this.updateTimeStr = updateTimeStr;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(Long applicantId) {
        this.applicantId = applicantId;
    }

    public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    public Long getApprovalId() {
        return approvalId;
    }

    public void setApprovalId(Long approvalId) {
        this.approvalId = approvalId;
    }

    public Long getContactsId() {
        return contactsId;
    }

    public void setContactsId(Long contactsId) {
        this.contactsId = contactsId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskMaxNum() {
        return taskMaxNum;
    }

    public void setTaskMaxNum(String taskMaxNum) {
        this.taskMaxNum = taskMaxNum;
    }

    public String getTaskIntegral() {
        return taskIntegral;
    }

    public void setTaskIntegral(String taskIntegral) {
        this.taskIntegral = taskIntegral;
    }

    public String getApplicantType() {
        return applicantType;
    }

    public void setApplicantType(String applicantType) {
        this.applicantType = applicantType;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getContactsPhone() {
        return contactsPhone;
    }

    public void setContactsPhone(String contactsPhone) {
        this.contactsPhone = contactsPhone;
    }

    public String getTaskUrl() {
        return taskUrl;
    }

    public void setTaskUrl(String taskUrl) {
        this.taskUrl = taskUrl;
    }

    public String getTaskMemo() {
        return taskMemo;
    }

    public void setTaskMemo(String taskMemo) {
        this.taskMemo = taskMemo;
    }

    public Long getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Long updateUser) {
        this.updateUser = updateUser;
    }

    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
