package com.tiance.vo;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：会议室表
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class MeetingRoomOccupationVO extends BaseVO{

    private static final long serialVersionUID = 1L;
    /**
     * ID
     */
    private Long id;

    /**
     * 时间
     * NAME
     */
    private String day;
    /**
     * 使用状态
     * NORMAL  正常
     * FORBIDDEN 禁用
     */
    private String useStatus;

    /**
     * 时间段类型(MORNING:上午;AFTERNOON:下午:ALL_DAY:全天;OTHER:其他)
     */
    private String timeType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getUseStatus() {
        return useStatus;
    }

    public void setUseStatus(String useStatus) {
        this.useStatus = useStatus;
    }

    public String getTimeType() {
        return timeType;
    }

    public void setTimeType(String timeType) {
        this.timeType = timeType;
    }
}
