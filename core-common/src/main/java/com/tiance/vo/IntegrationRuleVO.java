package com.tiance.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：积分规则表
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class IntegrationRuleVO extends BaseVO {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;

    /**
     * 分类
     * RULE_TYPE
     */
    private String ruleType;
    /**
     * 操作类型（ADD:加;SUBTRACT:减
     * OPERATE_TYPE
     */
    private String operateType;
    /**
     * 名称
     * RULE_NAME
     */
    private String ruleName;
    /**
     * 名称CODE
     * RULE_CODE
     */
    private String ruleCode;
    /**
     * 操作人员类型
     * OPERATOR_TYPE
     */
    private String operatorType;
    /**
     * 归属人员类型
     * BELONG_TYPE
     */
    private String belongType;
    /**
     * 积分(次)
     * INTEGRATION_DEGREE
     */
    private Integer integrationDegree;
    /**
     * 积分（分值
     * INTEGRATION_VALUE
     */
    private Integer integrationValue;
    /**
     * 信用
     * CREDIT_VALUE
     */
    private Integer creditValue;
    /**
     * 排序
     * SORT
     */
    private Integer sort;

    /**
     * REMARK
     */
    private String remark;

    /**
     * CREATE_TIME
     */
    private Date createTime;

    private Date updateTime;

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getOperateType() {
        return operateType;
    }

    public void setOperateType(String operateType) {
        this.operateType = operateType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRuleType() {
        return ruleType;
    }

    public void setRuleType(String ruleType) {
        this.ruleType = ruleType;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getRuleCode() {
        return ruleCode;
    }

    public void setRuleCode(String ruleCode) {
        this.ruleCode = ruleCode;
    }

    public String getOperatorType() {
        return operatorType;
    }

    public void setOperatorType(String operatorType) {
        this.operatorType = operatorType;
    }

    public String getBelongType() {
        return belongType;
    }

    public void setBelongType(String belongType) {
        this.belongType = belongType;
    }

    public Integer getIntegrationDegree() {
        return integrationDegree;
    }

    public void setIntegrationDegree(Integer integrationDegree) {
        this.integrationDegree = integrationDegree;
    }

    public Integer getIntegrationValue() {
        return integrationValue;
    }

    public void setIntegrationValue(Integer integrationValue) {
        this.integrationValue = integrationValue;
    }

    public Integer getCreditValue() {
        return creditValue;
    }

    public void setCreditValue(Integer creditValue) {
        this.creditValue = creditValue;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
