package com.tiance.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：会议中心表
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public class ConventionCenterVO extends BaseVO{

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 会议中心名称
     * NAME
     */
    private String name;
    /**
     * 会议中心位置
     * POSITION
     */
    private String position;
    /**
     * 会议中心坐标
     * COORDINATE
     */
    private String coordinate;
    /**
     * 会议中心状态(NORMAL:正常，FORBIDDEN:禁用)
     * STATUS
     */
    private String status;


    /**
     * CREATE_TIME
     */
    private Date createTime;
    /**
     * UPDATE_TIME
     */
    private Date updateTime;
    /**
     * REMARK
     */
    private String remark;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(String coordinate) {
        this.coordinate = coordinate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
