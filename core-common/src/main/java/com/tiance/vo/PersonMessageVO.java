package com.tiance.vo;

import java.util.Date;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/4
 *
 * Description：
 *
 * Modification History:
 *
 */
public class PersonMessageVO extends BaseVO{

    /**
     * 主键id
     * ID
     */
    private Long id;

    /**
     * 消息类型
     * MESSAGE_TYPE
     */
    private String messageType;
    /**
     * 消息URL
     * MESSAGE_URL
     */
    private String messageUrl;
    /**
     * 是否已读（READ:已读;UN_READ:未读
     * READ_STATUS
     */
    private String readStatus;
    /**
     * 内容
     * CONTENT
     */
    private String content;

    /**
     * 消息URL类型(IMAGE:图片;PAGE:网页)
     */
    private String messageUrlType;


    /**
     * 标题
     */
    private String title;

    /**
     * 创建时间
     * CREATE_TIME
     */
    private Date createTime;

    private String createTimeStr;

    private Date updateTime;

    private String updateTimeStr;

    public String getMessageUrlType() {
        return messageUrlType;
    }

    public void setMessageUrlType(String messageUrlType) {
        this.messageUrlType = messageUrlType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreateTimeStr() {
        return createTimeStr;
    }

    public void setCreateTimeStr(String createTimeStr) {
        this.createTimeStr = createTimeStr;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateTimeStr() {
        return updateTimeStr;
    }

    public void setUpdateTimeStr(String updateTimeStr) {
        this.updateTimeStr = updateTimeStr;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getMessageUrl() {
        return messageUrl;
    }

    public void setMessageUrl(String messageUrl) {
        this.messageUrl = messageUrl;
    }

    public String getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(String readStatus) {
        this.readStatus = readStatus;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

}
