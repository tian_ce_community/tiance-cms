package com.tiance.page;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/16
 *
 * Description：
 *
 * Modification History:
 *
 */
public class QueryBase implements Serializable {
    private static final long serialVersionUID = 8462451875633214573L;
    private static final Integer defaultPageSize = new Integer(20);
    private static final Integer defaultFriatPage = new Integer(1);
    private static final Integer defaultTotleItem = new Integer(0);
    private Integer totalItem;
    private Integer pageSize;
    private Integer currentPage;
    private BigDecimal totalSum = new BigDecimal(0);
    private int startRow;
    private int endRow;
    private boolean needQeryTotal = false;
    private boolean needDelete = false;
    private boolean needQueryAll = false;

    public QueryBase() {
    }

    public BigDecimal getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(BigDecimal totalSum) {
        this.totalSum = totalSum;
    }

    public boolean isAllowedQuery() {
        throw new RuntimeException("Please implement it in sub classes.");
    }

    protected final Integer getDefaultPageSize() {
        return defaultPageSize;
    }

    public Integer getCurrentPage() {
        return this.currentPage == null?defaultFriatPage:this.currentPage;
    }

    public void setCurrentPage(Integer cPage) {
        if(cPage != null && cPage.intValue() > 0) {
            this.currentPage = cPage;
        } else {
            this.currentPage = defaultFriatPage;
        }

    }

    public Integer getPageSize() {
        return this.pageSize == null?this.getDefaultPageSize():this.pageSize;
    }

    public boolean hasSetPageSize() {
        return this.pageSize != null;
    }

    public void setPageSize(Integer pSize) {
        if(pSize == null) {
            throw new IllegalArgumentException("PageSize can't be null.");
        } else if(pSize.intValue() <= 0) {
            throw new IllegalArgumentException("PageSize must great than zero.");
        } else {
            this.pageSize = pSize;
        }
    }

    public Integer getTotalItem() {
        return this.totalItem == null?defaultTotleItem:this.totalItem;
    }

    public void setTotalItem(Integer tItem) {
        if(tItem == null) {
            tItem = new Integer(0);
        }

        this.totalItem = tItem;
    }

    public int getTotalPage() {
        int pgSize = this.getPageSize().intValue();
        int total = this.getTotalItem().intValue();
        int result = total / pgSize;
        if(total % pgSize != 0) {
            ++result;
        }

        return result;
    }

    public int getPageFristItem() {
        if(!this.needQeryTotal) {
            int cPage = this.getCurrentPage().intValue();
            if(cPage == 1) {
                return 0;
            } else {
                --cPage;
                int pgSize = this.getPageSize().intValue();
                return pgSize * cPage ;
            }
        } else {
            return 0;
        }
    }

    public int getPageLastItem() {
        if(!this.needQeryTotal) {
            int cPage = this.getCurrentPage().intValue();
            int pgSize = this.getPageSize().intValue();
            int assumeLast = pgSize * cPage;
            int totalItem = this.getTotalItem().intValue();
            int pageLastItem= assumeLast > totalItem?totalItem:assumeLast;
            return pageLastItem-1;
        } else {
            return this.getTotalItem().intValue()-1;
        }
    }

    public int getEndRow() {
        return this.endRow;
    }

    public void setEndRow(int endRow) {
        this.endRow = endRow;
    }

    public int getStartRow() {
        return this.startRow;
    }

    public void setStartRow(int startRow) {
        this.startRow = startRow;
    }

    protected String getSQLBlurValue(String value) {
        return value == null?null:value + '%';
    }

    public boolean isNeedQeryTotal() {
        return this.needQeryTotal;
    }

    public void setNeedQeryTotal(boolean needQeryTotal) {
        this.needQeryTotal = needQeryTotal;
    }

    public boolean isNeedDelete() {
        return this.needDelete;
    }

    public void setNeedDelete(boolean needDelete) {
        this.needDelete = needDelete;
    }

    public boolean isNeedQueryAll() {
        return this.needQueryAll;
    }

    public void setNeedQueryAll(boolean needQueryAll) {
        this.needQueryAll = needQueryAll;
    }

    public <K extends QueryBase> void copyProperties(K k) {
        if(k != null) {
            k.setCurrentPage(this.currentPage);
            k.setEndRow(this.endRow);
            k.setNeedDelete(this.needDelete);
            k.setNeedQeryTotal(this.needQeryTotal);
            k.setNeedQueryAll(this.needQueryAll);
            k.setStartRow(this.startRow);
            k.setTotalItem(this.totalItem);
            if(this.totalSum != null) {
                k.setTotalSum(this.totalSum);
            }

        }
    }
}
