package com.tiance.page;

import java.math.BigDecimal;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/16
 *
 * Description：
 *
 * Modification History:
 *
 */
public class QueryPage extends QueryBase {
    private static final long serialVersionUID = -8204495521073718612L;
    private BigDecimal sumAccount;
    private BigDecimal sumFee;

    public BigDecimal getSumAccount() {
        return sumAccount;
    }

    public void setSumAccount(BigDecimal sumAccount) {
        this.sumAccount = sumAccount;
    }

    public BigDecimal getSumFee() {
        return sumFee;
    }

    public void setSumFee(BigDecimal sumFee) {
        this.sumFee = sumFee;
    }

    @Override
    public boolean isAllowedQuery() {
        return true;
    }

    @Override
    public String toString() {

        return "QueryPage{" + "sumAccount=" + sumAccount + ", sumFee=" + sumFee + '}';
    }
}
