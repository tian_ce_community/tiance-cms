package com.tiance.constants;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 */
public class CommonConstants {

    public static final Long COMMON_ID=1L;
    public static final int MEMBER_NO_LEN=6;
    public static final String defaultCoding="UTF-8";
    public static final String defaultValue="-";

    public static final String CURRENT_USER = "user";
    /** mdc key*/
    public static final String  MDC_KEY                                          = "mdcKey";

    /* MDC session */
    public static final String OPENID = "openid";
    public static final String MEMBER_ID = "memberId";
    public static final String USER = "USER";
    public static final String SESSION_ID = "sessionId";

    /*
     * 请求相应默认编码
     */
    public final static String DEFAULT_ENCODE = "utf-8";

    public static final String APP_NAME = "tiance-cms";


    /*
    * 连接符
    */
    public static final String  and                                           = "&";
    public static final String  eq                                            = "=";
    public static final String  empty                                         = "";
    public static final String  caret                                         = "^";
    public static final String  bar                                           = "|";
    public static final String  comma                                         = ",";
    public static final String  pause                                         = "、";
    public static final String  colon                                         = ":";

    public static final String UNDER_LINE                                     = "_";

    /** 接口调用超长时间范围 3秒钟*/
    public static final long    API_CALL_TIME_EXCEED_LENGTH                   = 3 * 1000;

    public static final String SIGN_IN  = "signIn";
    public static final String SIGN_OUT  = "signOut";
    public static final String APPRECIATE  = "appreciate";
}
