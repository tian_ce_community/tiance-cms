
package com.tiance.enums;
/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：
 *
 * Modification History:
 *
 */
public enum OperateTypeEnum {

    DEFAULT("默认"),
    FRONT_INTERFACE("前端接口"),
    BACK_INTERFACE("后台接口");

    String message;

    OperateTypeEnum(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
