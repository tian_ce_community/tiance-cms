/**
 * 
 */
package com.tiance.enums;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/18
 *
 * Description：图片格式
 *
 * Modification History:
 *
 */
public enum ImageTypeEnum {

    GIF("GIF",".gif",FileTypeEnum.IMAGE) ,
    JPEG("JPEG",".jpeg",FileTypeEnum.IMAGE),
    JPG("JPG",".jpg",FileTypeEnum.IMAGE),
    PNG("PNG",".png",FileTypeEnum.IMAGE),
    ;
    private String code;
    private String suffix;
    private FileTypeEnum fileType;

    ImageTypeEnum(String code, String suffix, FileTypeEnum fileType) {
        this.code = code;
        this.suffix = suffix;
        this.fileType = fileType;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public FileTypeEnum getFileType() {
        return fileType;
    }

    public void setFileType(FileTypeEnum fileType) {
        this.fileType = fileType;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public static ImageTypeEnum getByCode(String code){
        for(ImageTypeEnum ar: ImageTypeEnum.values()){
            if(ar.getCode().equals(code)){
                return ar;
            }
        }
        return null;
    }
}
