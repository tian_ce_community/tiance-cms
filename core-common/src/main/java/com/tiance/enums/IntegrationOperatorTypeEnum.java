package com.tiance.enums;


import org.apache.commons.lang3.StringUtils;

/**
 * Created by 雷霆 on 2019/1/30.
 * <p>
 * Description：积分操作人员规则
 * <p>
 * Modification History:
 *
 * @version 1.0.0
 */
public enum IntegrationOperatorTypeEnum {

	SYSTEM("SYSTEM", "系统后台"),
	SYSTEM_AUTO("SYSTEM_AUTO", "系统自动"),
	VERIFIED_MEMBER("VERIFIED_MEMBER", "认证会员"),
	VERIFIED_MEMBER_JOIN_ACTIVITY("VERIFIED_MEMBER_JOIN_ACTIVITY", "认证会员-活动参加者"),
	;
	private String code;
	private String msg;

	IntegrationOperatorTypeEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}


	/**
	 * 通过代码获取枚举项
	 * @param code
	 * @return AgentFeeStatusEnum
	 */
	public static IntegrationOperatorTypeEnum getByCode(String code) {
		if (StringUtils.isBlank(code)) {
			return null;
		}
		for (IntegrationOperatorTypeEnum item : IntegrationOperatorTypeEnum.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		return null;
	}
	
}
