
package com.tiance.enums;


/**
 * Created by 雷霆 on 2019/1/30.
 *
 * Description：错误码定义
 *
 * Modification History:
 *
 * @version 1.0.0
 *
 */
public enum ErrorCodeEnum {

    /**
     * 账户问题
     */
    NOT_LOGIN("401", "当前用户未登录"),
    USER_ALREADY_REG("401", "该用户已经注册"),
    NO_THIS_USER("400", "没有此用户"),
    ACCOUNT_FREEZED("401", "账号被冻结"),
    OLD_PWD_NOT_RIGHT("402", "原密码不正确"),
    TWO_PWD_NOT_MATCH("405", "两次输入密码不一致"),


    SUCCESS("0000", "成功"),
    PARAM_ERROR("1001", "参数校验错误"),
    BUSINESS_ERROR("0201", "业务错误"),
    OTHER_ERROR("1111", "其它错误"),
    SYSTEM_ERROR("9999", "系统错误"),
    ;


    private String code;

    private String msg;

    ErrorCodeEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static ErrorCodeEnum getByCode(String code) {
        for (ErrorCodeEnum e : ErrorCodeEnum.values()) {
            if (e.getCode().equals(code)) {
                return e;
            }
        }
        return SYSTEM_ERROR;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "ErrorCodeEnum [msg=" + msg + ", code=" + code + "]";
    }
}
