
package com.tiance.enums;


/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/1/30
 *
 * Description：业务是否成功的日志记录
 *
 * Modification History:
 *
 */
public enum LogSucceedEnum {

    SUCCESS("成功"),
    FAIL("失败");

    String message;

    LogSucceedEnum(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
