package com.tiance.enums;


import org.apache.commons.lang3.StringUtils;

/**
 * Created by 雷霆 on 2019/1/30.
 * <p>
 * Description：积分/信用类型
 * <p>
 * Modification History:
 *
 * @version 1.0.0
 */
public enum IntegrationTypeEnum {

	INTEGRATION("INTEGRATION", "积分"),
	CREDIT("CREDIT", "信用/府宝");
	private String code;
	private String msg;

	IntegrationTypeEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	/**
	 * 通过代码获取枚举项
	 * @param code
	 * @return AgentFeeStatusEnum
	 */
	public static IntegrationTypeEnum getByCode(String code) {
		if (StringUtils.isBlank(code)) {
			return null;
		}
		for (IntegrationTypeEnum item : IntegrationTypeEnum.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		return null;
	}
	
}
