package com.tiance.enums;


import com.tiance.exception.BusinessException;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by 雷霆 on 2019/1/30.
 * <p>
 * Description：会员性别
 * <p>
 * Modification History:
 *
 * @version 1.0.0
 */
public enum SexEnum {

	MALE("MALE", "男"),
	FEMALE("FEMALE", "女");
	private String code;
	private String msg;

	SexEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	/**
	 * 通过代码获取枚举项
	 * @param code
	 * @return AgentFeeStatusEnum
	 */
	public static SexEnum getByCode(String code) {
		if (StringUtils.isBlank(code)) {
			return null;
		}
		for (SexEnum item : SexEnum.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		return null;
	}

	/**
	 * 通过代码获取枚举项
	 * @param code
	 * @return AgentFeeStatusEnum
	 */
	public static String getCodeByWxSex(int code) {
		if (1==code) {
			return SexEnum.MALE.getCode();
		}
		if (2==code) {
			return SexEnum.FEMALE.getCode();
		}
		throw new BusinessException("未知的性别类型");
	}
}
