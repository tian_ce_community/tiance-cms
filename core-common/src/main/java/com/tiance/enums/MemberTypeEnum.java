package com.tiance.enums;


import org.apache.commons.lang3.StringUtils;

/**
 * Created by 雷霆 on 2019/1/30.
 * <p>
 * Description：会员类型
 * <p>
 * Modification History:
 *
 * @version 1.0.0
 */
public enum MemberTypeEnum {

	GUEST("GUEST", "游客（允许授权)"),
	REGULAR_MEMBER("REGULAR_MEMBER", "普通会员"),
	VERIFIED_MEMBER("VERIFIED_MEMBER", "认证会员"),
	DUTY_MANAGER("DUTY_MANAGER", "值班经理");
	private String code;
	private String msg;

	MemberTypeEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	/**
	 * 通过代码获取枚举项
	 * @param code
	 * @return AgentFeeStatusEnum
	 */
	public static MemberTypeEnum getByCode(String code) {
		if (StringUtils.isBlank(code)) {
			return null;
		}
		for (MemberTypeEnum item : MemberTypeEnum.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		return null;
	}
	
}
