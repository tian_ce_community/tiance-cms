package com.tiance.enums;


import org.apache.commons.lang3.StringUtils;

/**
 * Created by 雷霆 on 2019/1/30.
 * <p>
 * Description：实体类型
 * <p>
 * Modification History:
 *
 * @version 1.0.0
 */
public enum InfoTypeEnum {

	BASE_INFO("BASE_INFO", "基本信息"),
	PRE_WORK_INFO("PRE_WORK_INFO", "原工作信息"),
	NOW_WORK_INFO("NOW_WORK_INFO", "现工作信息"),
	ALL_INFO("ALL_INFO", "所有信息"),
	;
	private String code;
	private String msg;

	InfoTypeEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	/**
	 * 通过代码获取枚举项
	 * @param code
	 * @return AgentFeeStatusEnum
	 */
	public static InfoTypeEnum getByCode(String code) {
		if (StringUtils.isBlank(code)) {
			return null;
		}
		for (InfoTypeEnum item : InfoTypeEnum.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		return null;
	}
	
}
