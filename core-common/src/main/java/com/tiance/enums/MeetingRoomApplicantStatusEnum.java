package com.tiance.enums;


import org.apache.commons.lang3.StringUtils;

/**
 * Created by 雷霆 on 2019/1/30.
 * <p>
 * Description：会议室申请状态
 * <p>
 * Modification History:
 *
 * @version 1.0.0
 */
public enum MeetingRoomApplicantStatusEnum {

	UN_AUDIT("UN_AUDIT", "申请待审核"),
	APPLY_CANCEL("APPLY_CANCEL", "申请取消"),
	AUDIT_PASS("AUDIT_PASS", "申请审核通过"),
	AUDIT_FAIL("AUDIT_FAIL", "申请审核不通过"),
	USING("USING", "会议室使用中"),
	USE_FINISHED("USE_FINISHED", "会议室使用结束"),
	;
	private String code;
	private String msg;

	MeetingRoomApplicantStatusEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	/**
	 * 通过代码获取枚举项
	 * @param code
	 * @return AgentFeeStatusEnum
	 */
	public static MeetingRoomApplicantStatusEnum getByCode(String code) {
		if (StringUtils.isBlank(code)) {
			return null;
		}
		for (MeetingRoomApplicantStatusEnum item : MeetingRoomApplicantStatusEnum.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		return null;
	}
	
}
