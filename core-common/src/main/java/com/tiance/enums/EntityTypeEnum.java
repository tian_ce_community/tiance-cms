package com.tiance.enums;


import org.apache.commons.lang3.StringUtils;

/**
 * Created by 雷霆 on 2019/1/30.
 * <p>
 * Description：实体类型
 * <p>
 * Modification History:
 *
 * @version 1.0.0
 */
public enum EntityTypeEnum {

	MEMBER("MEMBER", "会员"),
	ACTIVITY("ACTIVITY", "活动"),
	TASK("TASK", "任务"),
	PUBLISH_INFO("PUBLISH_INFO", "发布信息"),
	MEETING_ROOM_APPLICANT("MEETING_ROOM_APPLICANT", "会议室申请"),
	DUTY_APPLICANT("DUTY_APPLICANT", "值班申请"),
	SUBSTITUTE_DUTY_APPLICANT("SUBSTITUTE_DUTY_APPLICANT", "替班申请"),
	;
	private String code;
	private String msg;

	EntityTypeEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	/**
	 * 通过代码获取枚举项
	 * @param code
	 * @return AgentFeeStatusEnum
	 */
	public static EntityTypeEnum getByCode(String code) {
		if (StringUtils.isBlank(code)) {
			return null;
		}
		for (EntityTypeEnum item : EntityTypeEnum.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		return null;
	}
	
}
