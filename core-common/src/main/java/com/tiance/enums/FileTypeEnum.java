/**
 * 
 */
package com.tiance.enums;

/**
 *
 * @author 雷霆
 * @version 1.0.0
 * @date$ 2019/2/18
 *
 * Description：文件格式
 *
 * Modification History:
 *
 */
public enum FileTypeEnum {

    IMAGE("IMAGE","图片") ,
    EXCEL("EXCEL","excel"),
    WORD("WORD","word文档"),
    ;
    private String code;
    private String message;
    FileTypeEnum(String code, String message){
        this.code = code;
        this.message = message;
    }
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public static FileTypeEnum getByCode(String code){
        for(FileTypeEnum ar: FileTypeEnum.values()){
            if(ar.getCode().equals(code)){
                return ar;
            }
        }
        return null;
    }
}
