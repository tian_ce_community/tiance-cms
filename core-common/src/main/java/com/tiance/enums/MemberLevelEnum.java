package com.tiance.enums;


import org.apache.commons.lang3.StringUtils;

/**
 * Created by 雷霆 on 2019/1/30.
 * <p>
 * Description：会员星级
 * <p>
 * Modification History:
 *
 * @version 1.0.0
 */
public enum MemberLevelEnum {

	LEVEL_1("1", "1"),
	LEVEL_2("2", "2"),
	LEVEL_3("3", "3"),
	LEVEL_4("4", "4"),
	LEVEL_5("5", "5");
	private String code;
	private String msg;

	MemberLevelEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	/**
	 * 通过代码获取枚举项
	 * @param code
	 * @return AgentFeeStatusEnum
	 */
	public static MemberLevelEnum getByCode(String code) {
		if (StringUtils.isBlank(code)) {
			return null;
		}
		for (MemberLevelEnum item : MemberLevelEnum.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		return null;
	}
	
}
