package com.tiance.enums;


import org.apache.commons.lang3.StringUtils;

/**
 * Created by 雷霆 on 2019/1/30.
 * <p>
 * Description：发布信息状态
 * <p>
 * Modification History:
 *
 * @version 1.0.0
 */
public enum PublishInfoStatusEnum {

	UN_AUDIT("UN_AUDIT", "申请待审核"),
	APPLY_CANCEL("APPLY_CANCEL", "申请取消"),
	AUDIT_PASS("AUDIT_PASS", "申请审核通过"),
	AUDIT_FAIL("AUDIT_FAIL", "申请审核不通过"),
	INFO_SIGN_UP("INFO_SIGN_UP", "发布信息报名中"),
	INFO_IN_PROGRESS("INFO_IN_PROGRESS", "发布信息进行中"),
	INFO_CANCEL("INFO_CANCEL", "发布信息取消"),
	INFO_FINISHED("INFO_FINISHED", "发布信息结束"),
	INFO_APPRECIATED("INFO_APPRECIATED", "发布信息已打赏");
	private String code;
	private String msg;

	PublishInfoStatusEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	/**
	 * 通过代码获取枚举项
	 * @param code
	 * @return AgentFeeStatusEnum
	 */
	public static PublishInfoStatusEnum getByCode(String code) {
		if (StringUtils.isBlank(code)) {
			return null;
		}
		for (PublishInfoStatusEnum item : PublishInfoStatusEnum.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		return null;
	}
	
}
