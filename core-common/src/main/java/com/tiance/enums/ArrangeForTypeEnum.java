package com.tiance.enums;


import org.apache.commons.lang3.StringUtils;

/**
 * Created by 雷霆 on 2019/1/30.
 * <p>
 * Description  安置后类型
 * <p>
 * Modification History:
 *
 * @version 1.0.0
 */
public enum ArrangeForTypeEnum {

	SELF_EMPLOYMENT("SELF_EMPLOYMENT", "自主择业"),
	DIMISSION("DIMISSION", "离职");
	private String code;
	private String msg;

	ArrangeForTypeEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	/**
	 * 通过代码获取枚举项
	 * @param code
	 * @return AgentFeeStatusEnum
	 */
	public static ArrangeForTypeEnum getByCode(String code) {
		if (StringUtils.isBlank(code)) {
			return null;
		}
		for (ArrangeForTypeEnum item : ArrangeForTypeEnum.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		return null;
	}
	
}
