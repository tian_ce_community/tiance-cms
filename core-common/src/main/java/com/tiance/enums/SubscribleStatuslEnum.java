package com.tiance.enums;


import com.tiance.exception.BusinessException;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by 雷霆 on 2019/1/30.
 * <p>
 * Description：会员订阅状态
 * <p>
 * Modification History:
 *
 * @version 1.0.0
 */
public enum SubscribleStatuslEnum {

	NOT_SUBSCRIBE("NOT_SUBSCRIBE", "表示没有关注，拉不到其他信息"),
	SUBSCRIBED("SUBSCRIBED", "表示关注，可以拉到其他授权信息");
	private String code;
	private String msg;

	SubscribleStatuslEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	/**
	 * 通过代码获取枚举项
	 * @param code
	 * @return AgentFeeStatusEnum
	 */
	public static SubscribleStatuslEnum getByCode(String code) {
		if (StringUtils.isBlank(code)) {
			return null;
		}
		for (SubscribleStatuslEnum item : SubscribleStatuslEnum.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		return null;
	}

	public static String getCodeByWx(int subscribe) {
		if (1==subscribe) {
			return SubscribleStatuslEnum.SUBSCRIBED.getCode();
		}
		if (0==subscribe) {
			return SubscribleStatuslEnum.NOT_SUBSCRIBE.getCode();
		}
		throw new BusinessException("未知的关注类型");
	}
}
