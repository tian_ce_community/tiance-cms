package com.tiance.enums;


import org.apache.commons.lang3.StringUtils;

/**
 * Created by 雷霆 on 2019/1/30.
 * <p>
 * Description：赞赏类型
 * <p>
 * Modification History:
 *
 * @version 1.0.0
 */
public enum AppreciateTypeEnum {

	SITE_APPRECIATE("SITE_APPRECIATE", "现场签到赞赏"),
	ACTIVITY_APPRECIATE("ACTIVITY_APPRECIATE", "活动赞赏");
	private String code;
	private String msg;

	AppreciateTypeEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	/**
	 * 通过代码获取枚举项
	 * @param code
	 * @return AgentFeeStatusEnum
	 */
	public static AppreciateTypeEnum getByCode(String code) {
		if (StringUtils.isBlank(code)) {
			return null;
		}
		for (AppreciateTypeEnum item : AppreciateTypeEnum.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		return null;
	}
	
}
