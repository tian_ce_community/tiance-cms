package com.tiance.enums;


import org.apache.commons.lang3.StringUtils;

/**
 * Created by 雷霆 on 2019/1/30.
 * <p>
 * Description：签到类型
 * <p>
 * Modification History:
 *
 * @version 1.0.0
 */
public enum SignInTypeEnum {

	DAILY_SIGN_IN("DAILY_SIGN_IN", "每日签到"),
	ACTIVITY_SIGN_IN("ACTIVITY_SIGN_IN", "活动签到"),
	CONFERENCE_CENTER_SIGN_IN("CONFERENCE_CENTER_SIGN_IN", "会议中心签到");
	private String code;
	private String msg;

	SignInTypeEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	/**
	 * 通过代码获取枚举项
	 * @param code
	 * @return AgentFeeStatusEnum
	 */
	public static SignInTypeEnum getByCode(String code) {
		if (StringUtils.isBlank(code)) {
			return null;
		}
		for (SignInTypeEnum item : SignInTypeEnum.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		return null;
	}
	
}
