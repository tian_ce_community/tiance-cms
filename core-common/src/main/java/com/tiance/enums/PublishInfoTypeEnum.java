package com.tiance.enums;


import org.apache.commons.lang3.StringUtils;

/**
 * Created by 雷霆 on 2019/1/30.
 * <p>
 * Description：发布信息类型
 * <p>
 * Modification History:
 *
 * @version 1.0.0
 */
public enum PublishInfoTypeEnum {

	RECRUIT_INFO("RECRUIT_INFO", "招聘信息"),
	ENTERPRISE_INFO("ENTERPRISE_INFO", "企业信息"),
	PRODUCT_INFO("PRODUCT_INFO", "产品信息"),
	TECHNIQUE_INFO("TECHNIQUE_INFO", "技术信息"),
	SERVICE_INFO("SERVICE_INFO", "服务信息"),
	OTHER("OTHER", "其他");
	private String code;
	private String msg;

	PublishInfoTypeEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	/**
	 * 通过代码获取枚举项
	 * @param code
	 * @return AgentFeeStatusEnum
	 */
	public static PublishInfoTypeEnum getByCode(String code) {
		if (StringUtils.isBlank(code)) {
			return null;
		}
		for (PublishInfoTypeEnum item : PublishInfoTypeEnum.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		return null;
	}
	
}
