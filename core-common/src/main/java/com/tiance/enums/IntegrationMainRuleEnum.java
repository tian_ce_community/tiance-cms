package com.tiance.enums;


import org.apache.commons.lang3.StringUtils;

/**
 * Created by 雷霆 on 2019/1/30.
 * <p>
 * Description：积分规则
 * <p>
 * Modification History:
 *
 * @version 1.0.0
 */
public enum IntegrationMainRuleEnum {

	MEMBER_REGISTER("MEMBER_REGISTER", "会员注册"),
	MEMBER_VERIFIED("MEMBER_VERIFIED", "会员认证"),
	SIGN_IN_ON_LINE("SIGN_IN_ON_LINE", "会员线上签到"),
	SIGN_IN_OFF_LINE("SIGN_IN_OFF_LINE", "会员线下签到"),
	;
	private String code;
	private String msg;

	IntegrationMainRuleEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	/**
	 * 通过代码获取枚举项
	 * @param code
	 * @return AgentFeeStatusEnum
	 */
	public static IntegrationMainRuleEnum getByCode(String code) {
		if (StringUtils.isBlank(code)) {
			return null;
		}
		for (IntegrationMainRuleEnum item : IntegrationMainRuleEnum.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		return null;
	}
	
}
