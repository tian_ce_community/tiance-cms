package com.tiance.enums;


import org.apache.commons.lang3.StringUtils;

/**
 * Created by 雷霆 on 2019/1/30.
 * <p>
 * Description：会员认证状态
 * <p>
 * Modification History:
 *
 * @version 1.0.0
 */
public enum MemberAuthStatusEnum {

	ID_CARD_VERIFY_SUCCESS("ID_CARD_VERIFY_SUCCESS", "身份证认证成功"),
	ID_CARD_VERIFY_FAIL("ID_CARD_VERIFY_FAIL", "身份证认证失败"),
	VERIFY_FAIL("VERIFY_FAIL", "后台认证失败"),
	VERIFY_SUCCESS("VERIFY_SUCCESS", "会员认证成功");
	private String code;
	private String msg;

	MemberAuthStatusEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	/**
	 * 通过代码获取枚举项
	 * @param code
	 * @return AgentFeeStatusEnum
	 */
	public static MemberAuthStatusEnum getByCode(String code) {
		if (StringUtils.isBlank(code)) {
			return null;
		}
		for (MemberAuthStatusEnum item : MemberAuthStatusEnum.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		return null;
	}
	
}
