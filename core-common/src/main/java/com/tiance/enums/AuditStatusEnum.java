package com.tiance.enums;


import org.apache.commons.lang3.StringUtils;

/**
 * Created by 雷霆 on 2019/1/30.
 * <p>
 * Description：审核状态
 * <p>
 * Modification History:
 *
 * @version 1.0.0
 */
public enum AuditStatusEnum {

	UN_AUDIT("UN_AUDIT", "待审核"),
	AUDIT_FAIL("AUDIT_FAIL", "审核不通过"),
	AUDIT_PASS("AUDIT_PASS", "审核通过"),
	;
	private String code;
	private String msg;

	AuditStatusEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	/**
	 * 通过代码获取枚举项
	 * @param code
	 * @return AgentFeeStatusEnum
	 */
	public static AuditStatusEnum getByCode(String code) {
		if (StringUtils.isBlank(code)) {
			return null;
		}
		for (AuditStatusEnum item : AuditStatusEnum.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		return null;
	}
	
}
