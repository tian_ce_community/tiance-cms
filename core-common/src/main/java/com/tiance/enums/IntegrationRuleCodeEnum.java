package com.tiance.enums;


import org.apache.commons.lang3.StringUtils;

/**
 * Created by 雷霆 on 2019/1/30.
 * <p>
 * Description：积分规则
 * <p>
 * Modification History:
 *
 * @version 1.0.0
 */
public enum IntegrationRuleCodeEnum {

	MEMBER_REGISTER_SUCCESS("MEMBER_REGISTER_SUCCESS", "会员注册成功",IntegrationMainRuleEnum.MEMBER_REGISTER),
	MEMBER_VERIFY_APPLIED("MEMBER_VERIFY_APPLIED", "会员提交认证申请",IntegrationMainRuleEnum.MEMBER_VERIFIED),
	MEMBER_VERIFY_SUCCESS("MEMBER_VERIFY_SUCCESS", "会员提交认证成功",IntegrationMainRuleEnum.MEMBER_VERIFIED),
	REGULAR_MEMBER_SIGN_IN_ON_LINE("REGULAR_MEMBER_SIGN_IN_ON_LINE", "注册会员线上签到",IntegrationMainRuleEnum.SIGN_IN_ON_LINE),
	VERIFIED_MEMBER_SIGN_IN_ON_LINE("VERIFIED_MEMBER_SIGN_IN_ON_LINE", "认证会员线上签到",IntegrationMainRuleEnum.SIGN_IN_ON_LINE),
	VERIFIED_MEMBER_SIGN_IN_OFF_LINE("VERIFIED_MEMBER_SIGN_IN_OFF_LINE", "认证会员线下签到",IntegrationMainRuleEnum.SIGN_IN_OFF_LINE),
	;
	private String code;
	private String msg;
	private IntegrationMainRuleEnum rule;

	IntegrationRuleCodeEnum(String code, String msg, IntegrationMainRuleEnum rule) {
		this.code = code;
		this.msg = msg;
		this.rule = rule;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}

	public IntegrationMainRuleEnum getRule() {
		return rule;
	}

	public void setRule(IntegrationMainRuleEnum rule) {
		this.rule = rule;
	}

	/**
	 * 通过代码获取枚举项
	 * @param code
	 * @return AgentFeeStatusEnum
	 */
	public static IntegrationRuleCodeEnum getByCode(String code) {
		if (StringUtils.isBlank(code)) {
			return null;
		}
		for (IntegrationRuleCodeEnum item : IntegrationRuleCodeEnum.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		return null;
	}
	
}
