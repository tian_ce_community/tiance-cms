package com.tiance.enums;


import org.apache.commons.lang3.StringUtils;

/**
 * Created by 雷霆 on 2019/1/30.
 * <p>
 * Description：任务状态
 * <p>
 * Modification History:
 *
 * @version 1.0.0
 */
public enum TaskStatusEnum {

	UN_AUDIT("UN_AUDIT", "申请待审核"),
	APPLY_CANCEL("APPLY_CANCEL", "申请取消"),
	AUDIT_PASS("AUDIT_PASS", "申请审核通过"),
	AUDIT_FAIL("AUDIT_FAIL", "申请审核不通过"),
	TASK_SIGN_UP("TASK_SIGN_UP", "任务报名中"),
	TASK_IN_PROGRESS("TASK_IN_PROGRESS", "任务进行中"),
	TASK_CANCEL("TASK_CANCEL", "任务取消"),
	TASK_FINISHED("TASK_FINISHED", "任务结束"),
	TASK_APPRECIATED("TASK_APPRECIATED", "任务已打赏"),
	TASK_ALLOT("TASK_ALLOT", "任务已分配"),
	;
	private String code;
	private String msg;

	TaskStatusEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	/**
	 * 通过代码获取枚举项
	 * @param code
	 * @return AgentFeeStatusEnum
	 */
	public static TaskStatusEnum getByCode(String code) {
		if (StringUtils.isBlank(code)) {
			return null;
		}
		for (TaskStatusEnum item : TaskStatusEnum.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		return null;
	}
	
}
