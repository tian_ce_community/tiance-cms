package com.tiance.enums;


import org.apache.commons.lang3.StringUtils;

/**
 * Created by 雷霆 on 2019/1/30.
 * <p>
 * Description：认证文件类型
 * <p>
 * Modification History:
 *
 * @version 1.0.0
 */
public enum AuthFileTypeEnum {

	ID_CARD_FRONT("ID_CARD_FRONT", "身份证正面"),
	ID_CARD_BACK("ID_CARD_BACK", "身份证背面"),
	CHOOSE_JOB_CERTIFICATE("CHOOSE_JOB_CERTIFICATE", "择业证"),
	JOB_QUALIFICATION_CERTIFICATE("JOB_QUALIFICATION_CERTIFICATE", "职业资格证"),
	RETIREMENT_RESUME_CERTIFICATE("RETIREMENT_RESUME_CERTIFICATE", "退役时履历表"),
	;
	private String code;
	private String msg;

	AuthFileTypeEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	/**
	 * 通过代码获取枚举项
	 * @param code
	 * @return AgentFeeStatusEnum
	 */
	public static AuthFileTypeEnum getByCode(String code) {
		if (StringUtils.isBlank(code)) {
			return null;
		}
		for (AuthFileTypeEnum item : AuthFileTypeEnum.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		return null;
	}
	
}
