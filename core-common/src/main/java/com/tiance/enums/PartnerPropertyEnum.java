package com.tiance.enums;


import org.apache.commons.lang3.StringUtils;

/**
 * Created by 雷霆 on 2019/1/30.
 * <p>
 * Description：是否合伙人
 * <p>
 * Modification History:
 *
 * @version 1.0.0
 */
public enum PartnerPropertyEnum {

	PARTNER("PARTNER", "是合伙人"),
	NOT_PARTNER("NOT_PARTNER", "不是合伙人"),
	;
	private String code;
	private String msg;

	PartnerPropertyEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	/**
	 * 通过代码获取枚举项
	 * @param code
	 * @return AgentFeeStatusEnum
	 */
	public static PartnerPropertyEnum getByCode(String code) {
		if (StringUtils.isBlank(code)) {
			return null;
		}
		for (PartnerPropertyEnum item : PartnerPropertyEnum.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		return null;
	}
	
}
