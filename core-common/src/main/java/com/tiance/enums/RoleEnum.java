package com.tiance.enums;


import org.apache.commons.lang3.StringUtils;

/**
 * Created by 雷霆 on 2019/1/30.
 * <p>
 * Description：角色
 * <p>
 * Modification History:
 *
 * @version 1.0.0
 */
public enum RoleEnum {

	ADMIN("ADMIN", "管理员"),
	OPERATOR("OPERATOR", "(普通用户)操作员"),
	TEMP_USER("TEMP_USER", "临时用户");

	private String code;
	private String msg;

	RoleEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	/**
	 * 通过代码获取枚举项
	 * @param code
	 * @return AgentFeeStatusEnum
	 */
	public static RoleEnum getByCode(String code) {
		if (StringUtils.isBlank(code)) {
			return null;
		}
		for (RoleEnum item : RoleEnum.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		return null;
	}
	
}
