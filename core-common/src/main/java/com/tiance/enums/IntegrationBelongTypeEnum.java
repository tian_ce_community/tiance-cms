package com.tiance.enums;


import org.apache.commons.lang3.StringUtils;

/**
 * Created by 雷霆 on 2019/1/30.
 * <p>
 * Description：积分归属
 * <p>
 * Modification History:
 *
 * @version 1.0.0
 */
public enum IntegrationBelongTypeEnum {

	MEMBER("MEMBER", "会员本人"),
	REFERRER("REFERRER", "推荐人"),
	ACTIVITY_ORGANIZER("ACTIVITY_ORGANIZER", "活动举办者"),
	ACTIVITY_OPARTICIPATOR("ACTIVITY_OPARTICIPATOR", "活动参加者"),
	DUTY_MANAGER("DUTY_MANAGER", "值班经理"),
	DUTY_APPLICANT("DUTY_APPLICANT", "值班申请者"),
	APPLICANT("APPLICANT", "申请者"),
	RESOURCE_USE_APPLICANT("RESOURCE_USE_APPLICANT", "资源使用申请者"),
	RESOURCE_PROVIDE_APPLICANT("RESOURCE_PROVIDE_APPLICANT", "资源提供申请者"),
	;
	private String code;
	private String msg;

	IntegrationBelongTypeEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	/**
	 * 通过代码获取枚举项
	 * @param code
	 * @return AgentFeeStatusEnum
	 */
	public static IntegrationBelongTypeEnum getByCode(String code) {
		if (StringUtils.isBlank(code)) {
			return null;
		}
		for (IntegrationBelongTypeEnum item : IntegrationBelongTypeEnum.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		return null;
	}
	
}
