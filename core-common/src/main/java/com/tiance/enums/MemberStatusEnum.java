package com.tiance.enums;


import org.apache.commons.lang3.StringUtils;

/**
 * Created by 雷霆 on 2019/1/30.
 * <p>
 * Description：会员状态
 * <p>
 * Modification History:
 *
 * @version 1.0.0
 */
public enum MemberStatusEnum {

	REGISTER_SUCCESS("REGISTER_SUCCESS", "会员注册成功"),
	VERIFY_APPLIED("VERIFY_APPLIED", "会员认证申请已提交"),
	BACKGROUND_VERIFY_FAIL("BACKGROUND_VERIFY_FAIL", "后台认证失败"),
	VERIFY_SUCCESS("VERIFY_SUCCESS", "会员认证成功");
	private String code;
	private String msg;

	MemberStatusEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	/**
	 * 通过代码获取枚举项
	 * @param code
	 * @return AgentFeeStatusEnum
	 */
	public static MemberStatusEnum getByCode(String code) {
		if (StringUtils.isBlank(code)) {
			return null;
		}
		for (MemberStatusEnum item : MemberStatusEnum.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		return null;
	}
	
}
